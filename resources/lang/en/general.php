<?php

return [
    'login' => [
        'success' => 'Logged in Successfully! Redirecting...',
        'failed' => 'Login and/or password did not match to the database.'
    ],
    'logout' => [
        'success' => 'Logged out Successfully.',
        'failed' => 'Sorry, the user cannot be logged out.',
    ],
    'token' => [
        'success' => 'Token created successfully.',
        'failed' => 'Could not create token.',
        'invalid' => 'Token is Invalid.',
        'expired' => 'Token is Expired.',
        'unauthorization' => 'Authorization Token not found.',
        'blacklist' => 'Token is blacklist.',
    ],
    'create' => [
        'success' => '":name" created successfully.',
        'failed' => '":name" unsuccessful created.'
    ],
    'update' => [
        'success' => '":name" updated successfully.',
        'failed' => '":name" unsuccessful updated.'
    ],
    'delete' => [
        'success' => '":name" deleted successfully.',
        'failed' => '":name" unsuccessful deleted.'
    ],
    'restore' => [
        'success' => '":name" restored successfully.',
        'failed' => '":name" unsuccessful restored.'
    ],
    'not_found' => 'Not Found',
    'calculation' => [
        'success' => 'The sum of ":attribute" with ":value" is correct.',
        'failed' => 'The sum of ":attribute" must be ":value".'
    ],
    'limit' => [
        'failed' => 'Max.limit :attribute >= :value.'
    ],
    'import' => [
        'success' => 'file import successfully.',
        'failed' => 'file import failed.'
    ],
    'export' => [
        'success' => 'file export successfully.',
        'failed' => 'file export failed.'
    ],
    // Auth
    'menu' => 'Menu',
    'role' => 'Role',
    'user' => 'User',
    // Master
    'coa' => 'COA',
    'journal' => 'Journal',
    'adjustment' => 'Adjustment',
    // Master
    'perusahaan' => 'Company',
    'customer' => 'Customer',
    'karyawan' => 'Employee',
    'product' => [
        'default' => 'Product',
        'unit' => 'Product Unit',
        'category' => 'Product Category',
        'detail' => 'Product Detail',
    ],
    'supplier' => 'Supplier',
    'tukang' => 'Craftsman',
    'vendor' => 'Vendor',
    // Project
    'order' => [
        'default' => 'Order',
        'termin' => 'Termin',
        'termin_detail' => 'Termin Detail',
    ],
    'estimasi' => 'Estimation',
    // Pembelian
    'po' => [
        'default' => 'Purchase Order',
        'detail' => 'Purchase Order Detail',
    ],
    'purchase' => [
        'default' => 'Purchase',
        'detail' => 'Purchase Detail',
    ],
    'kasbon' => 'Cash Receipt',
];
