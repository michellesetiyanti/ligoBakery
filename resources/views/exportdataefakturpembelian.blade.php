
              <table>
                <thead>
                <tr>
                  <td>FM</td>
                  <td>KODE JENIS TRANSAKSI</td>
                  <td>FG PENGGANTI</td>
                  <td>NOMOR FAKTUR</td>
                  <td>MASA PAJAK</td>
                  <td>TAHUN PAJAK</td>
                  <td>TGL FAKTUR</td>
                  <td>NPWP</td>
                  <td>NAMA</td>
                  <td>DPP</td>
                  <td>PPN</td>
                  <td>PPNBM</td>
                  <td>APAKAH DIKREDITKAN</td> 
                </tr> 
                </thead>
                <tbody>
                    @php
                    $id=1; 
                   
                    $efaktur2 = session('efaktur2');
                    $efaktur3 = session('efaktur3');
                    $efaktur4 = session('efaktur4');
                    $jl1 = strlen($efaktur4);
                    $ef4 = $efaktur4; 
                    
                    
                    @endphp
                    @foreach($pembelians as $pembelian)
                    
                    @php
                     $jl2 = strlen($ef4);
                     
                    if($jl2<$jl1){
                        $jl3=$jl1-$jl2;
                            if($jl3==1){
                                $ef4="0$ef4";
                            }else if($jl3==2){
                                $ef4="00$ef4";
                            }else if($jl3==3){
                                $ef4="000$ef4";
                            }
                    }
                   
                    $ef3 = "$efaktur2$efaktur3$ef4";
                    
                    
                    // $nofaktur = $pembelian['faktur']; 
                    $date = $pembelian['date'];
                    $year = substr($date,6,4);
                    $month = substr($date,3,2); 
                  
                    
                    $npwp = $pembelian['company']['npwp'];
                    // $gender = $pembelian['customer']['gender'];
                    // if($gender=="S"){ swasta
                    // $ef1= "01";
                    // $ef2= "0";
                    // }else{ pemerintah
                    // $ef1= "02";
                    // $ef2= "0";
                    // }

                    $ef1= "01";
                    $ef2= "0";
                    
                    $n1 = substr($npwp,0,2);
                    $n2 = substr($npwp,3,3);
                    $n3 = substr($npwp,7,3);
                    $n4 = substr($npwp,11,1);
                    $n5 = substr($npwp,13,3);
                    $n6 = substr($npwp,17,3);
                    $cek = substr($npwp,0,1);
                    if($cek>0){
                    $npwp = "xz'$n1$n2$n3$n4$n5$n6";
                    }else{
                    $npwp = "$n1$n2$n3$n4$n5$n6";
                    }
                    
                    $nama = $pembelian['company']['name'];
                     
                    if($npwp==null){
                    $npwp="";
                    }
                    $ongkir = $pembelian['ongkir'];
                    $biaya_lain = $pembelian['biaya_lain'];
                    $amount = round($pembelian['total']);
                    $tax = round($pembelian['tax']);
                    $discount = round($pembelian['discount']);
                    $jumlah_dpp = $amount - $tax - $ongkir - $biaya_lain;
                    $payment = $pembelian['payment'];
                    if($payment == "credit"){
                      $credit = 1;
                    }else{
                      $credit = 0;
                    }
                    @endphp
                    <tr>
                      <td>FM</td>
                      <td>{{$ef1}}</td>
                      <td>{{$ef2}}</td>
                      <td>{{$ef3}}</td>
                      <td>{{$month}}</td>
                      <td>{{$year}}</td>
                      <td>{{$date}}</td>
                      <td>{{$npwp}}</td>
                      <td>{{$nama}}</td>
                      <td>{{$jumlah_dpp}}</td>
                      <td>{{$tax}}</td>
                      <td>0</td>
                      <td>{{$credit}}</td> 
                    </tr> 
                    
                    @php
                    $ef4= $ef4 + 1;
                    @endphp
                @endforeach
                </tbody>
              </table>
              