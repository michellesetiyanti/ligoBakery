<h2>Report Penjualan Detail</h2>
<table style="width:100%">
    <thead>
          <tr style="text-align:center; border-bottom:1px solid; border-top:1px solid;">
        <th>No</th>
        <th style="text-align:left; ">Data</th> 
    </tr>
    </thead>
  
    @php
        $id=1;
        print($penjualans);
    @endphp
    @foreach($penjualans as $penjualan) 
    
    <tr style="border-bottom:1px solid;" >
        <td style="text-align:center;"> 
            {{$id}}
        </td>
        <td >
            <table style="width:100%">
                <tr style="border-bottom:1px solid;">
                    <td>
                        <b>Tanggal :</b>  
                        {{$penjualan['date']}}
                    </td>
                    <td>
                        <b>No Faktur :</b> 
                        {{$penjualan['faktur']}} 
                    </td>
                    <td>  
                        <b>Customer :</b>  
                        {{$penjualan['customer']['name']}} 
                    </td>
                    <td>  
                        <b>Pembayaran :</b>  
                        {{$penjualan['payment']}} 
                    </td>
                </tr>
                <tr style="text-align:left; border-bottom:1px solid;">
                    <td>
                        <b>Keterangan</b>  
                    </td>
                    <td>
                        <b>Jumlah</b> 
                    </td>
                    <td>  
                        <b>Harga Satuan</b> 
                    </td>
                    <td>  
                        <b>Sub Total</b> 
                    </td>
                </tr>
                @foreach($penjualan['product'] as $penjualandetail)
                <tr style="border-bottom:1px solid;">
                    <td>  
                        {{$penjualandetail['description']}} 
                    </td>
                    <td> 
                        {{$penjualandetail['qty']['lg']}} {{$penjualandetail['unit']['lg']}}, {{$penjualandetail['qty']['sm']}} {{$penjualandetail['unit']['sm']}}
                    </td>
                    <td> 
                        {{$penjualandetail['price']['sm']}} 
                    </td>
                    <td> 
                        {{$penjualandetail['amount']}} 
                    </td>
                </tr>
                @endforeach
                <tr>
                    <td><b>Sales: </b></td>
                    <td colspan="2" style="text-align:center;">
                        <b>Tax: </b>
                        Rp.{{$penjualan['tax']}}
                    </td>
                    <td>
                        <b>Total: </b>
                        Rp.{{$penjualan['total']}} 
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Input By :</b>
                        {{$penjualan['user']['name']}} 
                    </td>
                    <td colspan="2" style="text-align:center;">
                        <b>Discount :</b>
                        {{$penjualan['discount']}} 
                    </td>
                    <td>
                        <b>Pembayaran</b>
                        Rp.{{$penjualan['total']}}  
                    </td>
                </tr>

            </table>  

                
        </td> 
    </tr> 

        @php
        $id++;
        @endphp
    @endforeach

    
</table> 