<h2>Report Pembelian Detail</h2>
<table style="width:100%">
    <thead>
          <tr style="text-align:center; border-bottom:1px solid; border-top:1px solid;">
        <th>No</th>
        <th style="text-align:left; ">Data</th> 
    </tr>
    </thead>
  
    @php
        $id=1;
    @endphp
    @foreach($pembelians as $pembelian) 
    
    <tr style="border-bottom:1px solid;" >
        <td style="text-align:center;"> 
            {{$id}}
        </td>
        <td >
            <table style="width:100%">
                <tr style="border-bottom:1px solid;">
                    <td>
                        <b>Tanggal :</b>  
                        {{$pembelian['date']}}
                    </td>
                    <td>
                        <b>No Faktur :</b> 
                        {{$pembelian['faktur']}} 
                    </td>
                    <td>  
                        <b>Supplier :</b>  
                        {{$pembelian['supplier']['name']}} 
                    </td>
                    <td>  
                        <b>Pembayaran :</b>  
                        {{$pembelian['payment']}} 
                    </td>
                </tr>
                <tr style="text-align:left; border-bottom:1px solid;">
                    <td>
                        <b>Keterangan</b>  
                    </td>
                    <td>
                        <b>Jumlah</b> 
                    </td>
                    <td>  
                        <b>Harga Satuan</b> 
                    </td>
                    <td>  
                        <b>Sub Total</b> 
                    </td>
                </tr>
                @foreach($pembelian['product'] as $pembeliandetail)
                <tr style="border-bottom:1px solid;">
                    <td>  
                        {{$pembeliandetail['description']}} 
                    </td>
                    <td> 
                        {{$pembeliandetail['qty']['lg']}} {{$pembeliandetail['unit']['lg']}}, {{$pembeliandetail['qty']['sm']}} {{$pembeliandetail['unit']['sm']}}
                    </td>
                    <td> 
                        {{$pembeliandetail['price']['sm']}} 
                    </td>
                    <td> 
                        {{$pembeliandetail['amount']}} 
                    </td>
                </tr>
                @endforeach
                <tr>
                    <td><b>Sales: </b></td>
                    <td colspan="2" style="text-align:center;">
                        <b>Tax: </b>
                        Rp.{{$pembelian['tax']}}
                    </td>
                    <td>
                        <b>Total: </b>
                        Rp.{{$pembelian['total']}} 
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Input By :</b>
                        {{$pembelian['user']['name']}} 
                    </td>
                    <td colspan="2" style="text-align:center;">
                        <b>Discount :</b>
                        {{$pembelian['discount']}} 
                    </td>
                    <td>
                        <b>Pembayaran</b>
                        Rp.{{$pembelian['total']}}  
                    </td>
                </tr>

            </table>  

                
        </td> 
    </tr> 

        @php
        $id++;
        @endphp
    @endforeach

    
</table> 