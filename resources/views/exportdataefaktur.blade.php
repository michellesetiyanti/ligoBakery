
              <table>
                <thead>
                <tr>
                  <td>FK</td>
                  <td>KD_JENIS_TRANSAKSI</td>
                  <td>FG_PENGGANTI</td>
                  <td>NOMOR_FAKTUR</td>
                  <td>MASA_PAJAK</td>
                  <td>TAHUN_PAJAK</td>
                  <td>TANGGAL_FAKTUR</td>
                  <td>NPWP</td>
                  <td>NAMA</td>
                  <td>ALAMAT_LENGKAP</td>
                  <td>JUMLAH_DPP</td>
                  <td>JUMLAH_PPN</td>
                  <td>JUMLAH_PPNBM</td>
                  <td>ID_KETERANGAN_TAMBAHAN</td>
                  <td>FG_UANG_MUKA</td>
                  <td>UANG_MUKA_DPP</td>
                  <td>UANG_MUKA_PPN</td>
                  <td>UANG_MUKA_PPNBM</td>
                  <td>REFERENSI</td>
                </tr>
                <tr>
                  <td>LT</td>
                  <td>NPWP</td>
                  <td>NAMA</td>
                  <td>JALAN</td>
                  <td>BLOK</td>
                  <td>NOMOR</td>
                  <td>RT</td>
                  <td>RW</td>
                  <td>KECAMATAN</td>
                  <td>KELURAHAN</td>
                  <td>KABUPATEN</td>
                  <td>PROPINSI</td>
                  <td>KODE_POS</td>
                  <td>NOMOR_TELEPON</td>
                </tr>
                <tr>
                  <td>OF</td>
                  <td>KODE_OBJEK</td>
                  <td>NAMA</td>
                  <td>HARGA_SATUAN</td>
                  <td>JUMLAH_BARANG</td>
                  <td>HARGA_TOTAL</td>
                  <td>DISKON</td>
                  <td>DPP</td>
                  <td>PPN</td>
                  <td>TARIF_PPNBM</td>
                  <td>PPNBM</td>
                </tr>
                </thead>
                <tbody>
                    @php
                    $id=1; 
                   
                    $efaktur2 = session('efaktur2');
                    $efaktur3 = session('efaktur3');
                    $efaktur4 = session('efaktur4');
                    $jl1 = strlen($efaktur4);
                    $ef4 = $efaktur4; 
                    
                    
                    @endphp
                    @foreach($penjualans as $penjualan)
                    
                    @php
                     $jl2 = strlen($ef4);
                     
                    if($jl2<$jl1){
                        $jl3=$jl1-$jl2;
                            if($jl3==1){
                                $ef4="0$ef4";
                            }else if($jl3==2){
                                $ef4="00$ef4";
                            }else if($jl3==3){
                                $ef4="000$ef4";
                            }
                    }
                   
                    $ef3 = "$efaktur2$efaktur3$ef4";
                    
                    
                    $nofaktur = $penjualan['faktur']; 
                    $date = $penjualan['date'];
                    $year = substr($date,6,4);
                    $month = substr($date,3,2); 
                  
                    
                    $npwp = $penjualan['customer']['npwp'];
                    // $gender = $penjualan['customer']['gender'];
                    // if($gender=="S"){ swasta
                    // $ef1= "01";
                    // $ef2= "0";
                    // }else{ pemerintah
                    // $ef1= "02";
                    // $ef2= "0";
                    // }

                    $ef1= "01";
                    $ef2= "0";
                    
                    $n1 = substr($npwp,0,2);
                    $n2 = substr($npwp,3,3);
                    $n3 = substr($npwp,7,3);
                    $n4 = substr($npwp,11,1);
                    $n5 = substr($npwp,13,3);
                    $n6 = substr($npwp,17,3);
                    $cek = substr($npwp,0,1);
                    if($cek<1){
                    $npwp = "xz'$n1$n2$n3$n4$n5$n6";
                    }else{
                    $npwp = "$n1$n2$n3$n4$n5$n6";
                    }
                    
                    $nama = $penjualan['customer']['pkp_name'];
                    if($nama =="NULL"){
                    $nama = $penjualan['customer']['name'];
                    }
                    $address = $penjualan['customer']['pkp_address'];
                    if($address ==null){
                    $address = $penjualan['customer']['address'];
                    }
                    if($npwp==null){
                    $npwp="";
                    }
                    $ongkir = $penjualan['ongkir'];
                    $biaya_lain = $penjualan['biaya_lain'];
                    $amount = round($penjualan['total']);
                    $tax = round($penjualan['tax']);
                    $discount = round($penjualan['discount']);
                    $jumlah_dpp = $amount - $tax - $ongkir - $biaya_lain;
                    @endphp
                    <tr>
                      <td>FK</td>
                      <td>{{$ef1}}</td>
                      <td>{{$ef2}}</td>
                      <td>{{$ef3}}</td>
                      <td>{{$month}}</td>
                      <td>{{$year}}</td>
                      <td>{{$date}}</td>
                      <td>{{$npwp}}</td>
                      <td>{{$nama}}</td>
                      <td>{{$address}}</td>
                      <td>{{$jumlah_dpp}}</td>
                      <td>{{$tax}}</td>
                      <td>0</td>
                      <td></td>
                      <td>0</td>
                      <td>0</td>
                      <td>0</td>
                      <td>0</td>
                      <td>{{$nofaktur}}</td>
                    </tr>
                          @php
                      
                          $ef4= $ef4 + 1;
                          
                          @endphp
                          
                            @foreach($penjualan['product'] as $penjualandetail)
                           
                                @php
                                $idproduct = $penjualandetail['id'];
                                $unit_qty = $penjualandetail['unit_qty'];
                                $price_lg = $penjualandetail['price']['lg'];
                                $price_sm = $penjualandetail['price']['sm'];
                                $unit_lg = $penjualandetail['unit']['lg'];
                                $unit_sm = $penjualandetail['unit']['sm'];
                                $qty_lg = $penjualandetail['qty']['lg'];
                                $qty_sm = $penjualandetail['qty']['sm'];
                                $taxproduct = $penjualandetail['tax'];
                                $disc = round($penjualandetail['discount']);
                                $prname= $penjualandetail['description'];

                               

                                if($qty_lg != 0 && $qty_sm != 0){   

                                    $curpricesm = $penjualandetail['price']['sm']; 
                                    $curpricelg = $penjualandetail['price']['lg'];  
                                    $jmlhlg =  $qty_lg;
                                    $jmlhsm =  $qty_sm;
                                    $disc2  = $disc / 2;
                                      
                                    $totpricepluslg = round($curpricelg*$jmlhlg);
                                    $taxlg=$totpricepluslg * (round($taxproduct)/100);
                                    $dpplg = round($totpricepluslg) - round($disc2);    

                                    $totpriceplussm = round($curpricesm*$jmlhsm);
                                    $taxsm=$totpriceplussm * (round($taxproduct)/100);
                                    $dppsm = round($totpriceplussm) - round($disc2);  
                                 //price_lg   
                                echo"<tr>";
                                  echo"<td>OF</td>";
                                  echo"<td></td>";
                                  echo"<td>$prname - $unit_lg</td>";
                                  echo"<td>$curpricelg</td>";
                                  echo"<td>$jmlhlg</td>";
                                  echo"<td>$totpricepluslg</td>";
                                  echo"<td>$disc2</td>";
                                  echo"<td>$dpplg</td>";
                                  echo"<td>$taxlg</td>";
                                  echo"<td>0</td>";
                                  echo"<td>0</td>";
                                echo"</tr>"; 

                                //price_sm   
                                echo"<tr>";
                                  echo"<td>OF</td>";
                                  echo"<td></td>";
                                  echo"<td>$prname - $unit_sm</td>";
                                  echo"<td>$curpricesm</td>";
                                  echo"<td>$jmlhsm</td>";
                                  echo"<td>$totpriceplussm</td>";
                                  echo"<td>$disc2</td>";
                                  echo"<td>$dppsm</td>";
                                  echo"<td>$taxsm</td>";
                                  echo"<td>0</td>";
                                  echo"<td>0</td>";
                                echo"</tr>"; 

                                }else if($qty_lg == 0 || $qty_sm == 0){  
                                    if($qty_lg == 0){
                                        $jmlh =  $qty_sm;
                                        $unit= $unit_sm;
                                        $curprice = $penjualandetail['price']['sm']; 
                                    }else if ($qty_sm == 0){
                                        $unit= $unit_lg;
                                        $jmlh =  $qty_lg;
                                        $curprice = $penjualandetail['price']['lg'];  
                                    } 
                                    $totpriceplus = round($curprice*$jmlh);
                                    $tax=$totpriceplus * (round($taxproduct)/100);
                                    $dpp = round($totpriceplus) - round($disc);  

                                echo"<tr>";
                                  echo"<td>OF</td>";
                                  echo"<td></td>";
                                  echo"<td>$prname - $unit</td>";
                                  echo"<td>$curprice</td>";
                                  echo"<td>$jmlh</td>";
                                  echo"<td>$totpriceplus</td>";
                                  echo"<td>$disc</td>";
                                  echo"<td>$dpp</td>";
                                  echo"<td>$tax</td>";
                                  echo"<td>0</td>";
                                  echo"<td>0</td>";
                                echo"</tr>";  
                                } 
                                @endphp 
                            @endforeach
                          
                @endforeach
                </tbody>
              </table>
              