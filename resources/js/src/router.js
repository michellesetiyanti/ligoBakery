/*=========================================================================================
  File Name: router.js
  Description: Routes for vue-router. Lazy loading is enabled.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior() {
        return {
            x: 0,
            y: 0
        }
    },
    routes: [
        // =============================================================================
        // FULL PAGE LAYOUTS
        // =============================================================================
        {
            path: '',
            component: () => import('@/layouts/full-page/FullPage.vue'),
            children: [
                // =============================================================================
                // PAGES
                // =============================================================================
                {
                    path: '/',
                    name: 'page-login',
                    component: () => import('@/views/pages/Login.vue')
                },
                {
                    path: '/pages/register',
                    name: 'page-register',
                    component: () => import('@/views/pages/Register.vue')
                },
                {
                    path: '/pages/error-404',
                    name: 'page-error-404',
                    component: () => import('@/views/pages/Error404.vue')
                },
                {
                    path: '/pages/invoiceorder/:idorder',
                    name: 'pages-invoiceorder',
                    component: () => import('@/views/pages/PrintInvoiceOrder.vue'),
                },
            ]
        },
        {
            // =============================================================================
            // MAIN LAYOUT ROUTES
            // =============================================================================
            path: '',
            component: () => import('./layouts/main/Main.vue'),
            // component: () => import('./views/pages/Login.vue'),
            // component: () => import('@/layouts/full-page/FullPage.vue'),
            children: [
                // =============================================================================
                // Theme Routes
                // =============================================================================

                {
                    path: '/home',
                    name: 'home',
                    component: () => import('./views/Home.vue')
                },
                {
                    path: '/page2',
                    name: 'page-2',
                    component: () => import('./views/Page2.vue')
                },
                {
                    path: '/pages/user',
                    name: 'pages-user',
                    component: () => import('./views/pages/MasterData/UserScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page User', active: true },
                        ],
                        pageTitle: 'Manage User',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/workshop',
                    name: 'pages-workshop',
                    component: () => import('./views/pages/Operational/WorkshopScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Workshop', active: true },
                        ],
                        pageTitle: 'Page Workshop',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/produksigagal',
                    name: 'pages-produksigagal',
                    component: () => import('./views/pages/MasterData/ProduksiGagalScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Produksi Gagal', active: true },
                        ],
                        pageTitle: 'Page Produksi Gagal',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/produksi',
                    name: 'pages-produksi',
                    component: () => import('./views/pages/MasterData/ProduksiScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Produksi', active: true },
                        ],
                        pageTitle: 'Page Produksi',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewproduksidetail/:idproduksi',
                    name: 'pages-viewproduksidetail',
                    component: () => import('./views/pages/MasterData/ViewProduksiDetail.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Produksi Detail', active: true },
                        ],
                        pageTitle: 'Page Produksi Detail',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formproduksi',
                    name: 'pages-formproduksi',
                    component: () => import('./views/form/FormProduksi.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Produksi', url: '/pages/produksi' },
                            { title: 'Page Form Produksi', active: true },
                        ],
                        pageTitle: 'Page Form Produksi',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formproduksigagal/:idproduksi',
                    name: 'pages-formproduksigagal',
                    component: () => import('./views/form/FormProduksiGagal.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Produksi', url: '/pages/produksi' },
                            { title: 'Page Form Produksi Gagal', active: true },
                        ],
                        pageTitle: 'Page Form Produksi Gagal',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/editproduksi/:idproduksi',
                    name: 'pages-editproduksi',
                    component: () => import('./views/form/FormProduksi.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Produksi', url: '/pages/produksi' },
                            { title: 'Page Form Produksi', active: true },
                        ],
                        pageTitle: 'Page Form Produksi',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/menujual',
                    name: 'pages-menujual',
                    component: () => import('./views/pages/MasterData/MenuJualScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Menu', active: true },
                        ],
                        pageTitle: 'Page Menu',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/menusisajual',
                    name: 'pages-menusisa',
                    component: () => import('./views/pages/MasterData/MenuSisaJualScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Menu', active: true },
                        ],
                        pageTitle: 'Page Menu',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formmenusisajual',
                    name: 'pages-formmenusisajual',
                    component: () => import('./views/form/formmenusisajual.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Menu', url: '/pages/menujual' },
                            { title: 'Page Form Menu Sisa Jual', active: true },
                        ],
                        pageTitle: 'Page Form Menu Sisa Jual',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/editmenusisajual/:idmenusisajual',
                    name: 'pages-editmenusisajual',
                    component: () => import('./views/form/formmenusisajual.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Menu', url: '/pages/menujual' },
                            { title: 'Page Form Menu Sisa Jual', active: true },
                        ],
                        pageTitle: 'Page Form Menu Sisa Jual',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewmenusisajual/:idmenujual',
                    name: 'pages-viewmenusisajual',
                    component: () => import('./views/pages/MasterData/ViewMenuSisaJual.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Menu Sisa', url: '/pages/menusisajual' },
                            { title: 'View Menu Sisa Jual Detail', active: true },
                        ],
                        pageTitle: 'View Menu Sisa Jual Detail',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/categorypayment',
                    name: 'pages-categorypayment',
                    component: () => import('./views/pages/MasterData/PaymentCategoryScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Category Payment', active: true },
                        ],
                        pageTitle: 'Page Category Payment',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/paymenttype',
                    name: 'pages-paymenttype',
                    component: () => import('./views/pages/MasterData/PaymentTypeScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Payment Type', active: true },
                        ],
                        pageTitle: 'Page Payment Type',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formmenujual',
                    name: 'pages-formmenujual',
                    component: () => import('./views/form/FormMenuJual.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Menu', url: '/pages/menujual' },
                            { title: 'Page Form Menu Jual', active: true },
                        ],
                        pageTitle: 'Page Form Menu Jual',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/editmenujual/:idmenujual',
                    name: 'pages-editmenujual',
                    component: () => import('./views/form/FormMenuJual.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Menu', url: '/pages/menujual' },
                            { title: 'Page Form Menu Jual', active: true },
                        ],
                        pageTitle: 'Page Form Menu Jual',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewmenujual/:idmenujual',
                    name: 'pages-viewmenujual',
                    component: () => import('./views/pages/MasterData/MenuJualDetailScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Menu', url: '/pages/menujual' },
                            { title: 'View Menu Detail', active: true },
                        ],
                        pageTitle: 'View Menu Detail',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formworkshop',
                    name: 'pages-formworkshop',
                    component: () => import('./views/form/FormWorkshop.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Form Workshop', active: true },
                        ],
                        pageTitle: 'Form Workshop',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/editworkshop/:idworkshop',
                    name: 'pages-editworkshop',
                    component: () => import('./views/form/FormWorkshop.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Form Workshop', active: true },
                        ],
                        pageTitle: 'Form Workshop',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewworkshop/:idworkshop',
                    name: 'pages-viewworkshop',
                    component: () => import('./views/pages/Operational/ViewWorkshopDetail.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'View Workshop Detail', active: true },
                        ],
                        pageTitle: 'View Workshop Detail',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/spk',
                    name: 'pages-spk',
                    component: () => import('./views/pages/Operational/SPKScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page SPK', active: true },
                        ],
                        pageTitle: 'Page SPK',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/confirmspk',
                    name: 'pages-confirmspk',
                    component: () => import('./views/pages/Operational/SPKConfirmScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Confirm SPK', active: true },
                        ],
                        pageTitle: 'Page Confirm SPK',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formadendum/:idorder',
                    name: 'pages-formadendum',
                    component: () => import('./views/form/FormAdendum.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Form Adendum', active: true },
                        ],
                        pageTitle: 'Form Adendum',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formspk',
                    name: 'pages-formspk',
                    component: () => import('./views/form/FormSPK.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Form SPK', active: true },
                        ],
                        pageTitle: 'Form SPK',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/editspk/:idspk',
                    name: 'pages-editspk',
                    component: () => import('./views/form/FormSPK.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Form SPK', active: true },
                        ],
                        pageTitle: 'Form SPK',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewspk/:idspk',
                    name: 'pages-viewspk',
                    component: () => import('./views/pages/Operational/ViewSPK.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'View SPK', active: true },
                        ],
                        pageTitle: 'View SPK',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/penawaran',
                    name: 'pages-penawaran',
                    component: () => import('./views/pages/Operational/PenawaranScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Penawaran', active: true },
                        ],
                        pageTitle: 'Page Penawaran',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formpenawaran',
                    name: 'pages-formpenawaran',
                    component: () => import('./views/form/FormPenawaran.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Form Penawaran', active: true },
                        ],
                        pageTitle: 'Form Penawaran',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formrevisipenawaran/:idpenawaran',
                    name: 'pages-formrevisipenawaran',
                    component: () => import('./views/form/FormRevisiPenawaran.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Form Revisi Penawaran', active: true },
                        ],
                        pageTitle: 'Form Revisi Penawaran',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/editpenawaran/:idpenawaran',
                    name: 'pages-editpenawaran',
                    component: () => import('./views/form/FormPenawaran.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Form Penawaran', active: true },
                        ],
                        pageTitle: 'Form Penawaran',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewpenawaran/:idpenawaran',
                    name: 'pages-viewpenawaran',
                    component: () => import('./views/pages/Operational/ViewPenawaran.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'View Penawaran', active: true },
                        ],
                        pageTitle: 'View Penawaran',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/borongan',
                    name: 'pages-borongan',
                    component: () => import('./views/pages/Operational/BoronganScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Borongan', active: true },
                        ],
                        pageTitle: 'Page Borongan',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formborongan',
                    name: 'pages-formborongan',
                    component: () => import('./views/form/FormBorongan.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Form Borongan', active: true },
                        ],
                        pageTitle: 'Form Borongan',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewborongan/:idborongan',
                    name: 'pages-viewborongan',
                    component: () => import('./views/pages/Operational/ViewBoronganDetailScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'View Borongan', active: true },
                        ],
                        pageTitle: 'View Borongan',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formborongan/:idborongan',
                    name: 'pages-editborongan',
                    component: () => import('./views/form/FormBorongan.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Form Borongan', active: true },
                        ],
                        pageTitle: 'Form Borongan',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/gajitukang',
                    name: 'pages-gajitukang',
                    component: () => import('./views/pages/Finance/GajiTukangScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Gaji Tukang', active: true },
                        ],
                        pageTitle: 'Page Gaji Tukang',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/reviewgajitukang/:idabsen',
                    name: 'pages-reviewgajitukang',
                    component: () => import('./views/pages/Operational/ReviewGajiTukangScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Gaji Tukang', active: true },
                        ],
                        pageTitle: 'Page Gaji Tukang',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/vendorproject',
                    name: 'pages-venderproject',
                    component: () => import('./views/pages/Finance/VendorProjectScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Vendor Project', active: true },
                        ],
                        pageTitle: 'Page Vendor Project',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/vendorprojectdetail/:idborongan',
                    name: 'pages-venderprojectdetail',
                    component: () => import('./views/pages/Finance/VendorProjectDetailScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Detail Vendor Project', active: true },
                        ],
                        pageTitle: 'Page Detail Vendor Project',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/invoice',
                    name: 'pages-invoice',
                    component: () => import('./views/pages/Finance/InvoiceScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Invoice', active: true },
                        ],
                        pageTitle: 'Page Invoice',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/invoicedetail/:idorder',
                    name: 'pages-invoicedetail',
                    component: () => import('./views/pages/Finance/InvoiceDetailScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Invoice Detail', active: true },
                        ],
                        pageTitle: 'Page Invoice Detail',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/term/:idterm/idspk/:idspk',
                    name: 'pages-addterm',
                    component: () => import('./views/form/FormTerm.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Termin', active: true },
                        ],
                        pageTitle: 'Page Termin',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/userrole',
                    name: 'pages-userrole',
                    component: () => import('./views/pages/MasterData/UserRoleScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page User Role', active: true },
                        ],
                        pageTitle: 'Manage User Role',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/usercredential/:idrole',
                    name: 'pages-usercredential',
                    component: () => import('./views/form/FormCredential.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page User Role', url: '/pages/userrole' },
                            { title: 'Page User Credential', active: true },
                        ],
                        pageTitle: 'Manage User Credential',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/supplier',
                    name: 'pages-supplier',
                    component: () => import('./views/pages/MasterData/SuplierScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Supplier', active: true },
                        ],
                        pageTitle: 'Manage Supplier',
                        rule: 'editor'
                    },
                }, 
                {
                    path: '/pages/craftsman',
                    name: 'pages-craftsman',
                    component: () => import('./views/pages/MasterData/CraftmanScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Craftsman', active: true },
                        ],
                        pageTitle: 'Manage Craftsman',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/vendor',
                    name: 'pages-vendor',
                    component: () => import('./views/pages/MasterData/VendorScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Vendor', active: true },
                        ],
                        pageTitle: 'Manage Vendor',
                        rule: 'editor'
                    },
                },
                { //bw
                  path: '/pages/aset',
                  name: 'pages-aset',
                  component: () => import('./views/pages/MasterData/AsetScreen.vue'),
                  meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/' },
                        { title: 'Page Aset', active: true },
                    ],
                    pageTitle: 'Manage Aset',
                    rule: 'editor'
                  },
                },
                { //bw
                  path: '/pages/manageaset',
                  name: 'pages-manageaset',
                  component: () => import('./views/pages/Operational/ManageAsetScreen.vue'),
                  meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/' },
                        { title: 'Page Manage Aset', active: true },
                    ],
                    pageTitle: 'Manage Aset',
                    rule: 'editor'
                  },
                }, 
                {
                    path: '/pages/company',
                    name: 'pages-company',
                    component: () => import('./views/pages/MasterData/CompanyScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Company', active: true },
                        ],
                        pageTitle: 'Manage Company',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/satuan',
                    name: 'pages-satuan',
                    component: () => import('./views/pages/MasterData/SatuanScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Satuan', active: true },
                        ],
                        pageTitle: 'Manage Satuan',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/sales',
                    name: 'pages-sales',
                    component: () => import('./views/pages/MasterData/SalesScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Sales', active: true },
                        ],
                        pageTitle: 'Manage Sales',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/product',
                    name: 'pages-product',
                    component: () => import('./views/pages/MasterData/ProductScreenBW.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Product', active: true },
                        ],
                        pageTitle: 'Manage Product',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/productcategory',
                    name: 'pages-productcategory ',
                    component: () => import('./views/pages/MasterData/ProductCategoryScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Product Category', active: true },
                        ],
                        pageTitle: 'Manage Product Category',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/karyawan',
                    name: 'pages-karyawan',
                    component: () => import('./views/pages/MasterData/KaryawanScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Employees', active: true },
                        ],
                        pageTitle: 'Manage Employees',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/warehouse',
                    name: 'pages-warehouse',
                    component: () => import('./views/pages/MasterData/WarehouseScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Page Warehouse', active: true },
                        ],
                        pageTitle: 'Manage Warehouse',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/coa',
                    name: 'pages-coa',
                    component: () => import('./views/pages/Accounting/CoaScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'COA', active: true },
                        ],
                        pageTitle: 'Chart of Accounts',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/journal',
                    name: 'pages-journal',
                    component: () => import('./views/pages/Accounting/JournalScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Journal', active: true },
                        ],
                        pageTitle: 'Journal',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/journaldetail/:idjournal',
                    name: 'pages-journaldetail',
                    component: () => import('./views/pages/Accounting/JournalDetailScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Journal', url: '/pages/journal' },
                            { title: 'Journal Detail', active: true },
                        ],
                        pageTitle: 'Journal Detail',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formjournal',
                    name: 'pages-formjournal ',
                    component: () => import('./views/form/FormJournal.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Journal', url: '/pages/journal' },
                            { title: 'Form Journal', active: true },
                        ],
                        pageTitle: 'Form Journal',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/editjournal/:idjournal',
                    name: 'editjournal',
                    component: () => import('./views/form/FormJournal.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Journal', url: '/pages/journal' },
                            { title: 'Form Journal', active: true },
                        ],
                        pageTitle: 'Form Journal',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/generalledger',
                    name: 'pages-generalledger',
                    component: () => import('./views/pages/Accounting/GeneralLedgerScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'General Ledger', active: true },
                        ],
                        pageTitle: 'General Ledger',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/generalledgerdetail/:gl_id',
                    name: 'pages-generalledgerdetail',
                    component: () => import('./views/pages/Accounting/GeneralLedgerDetailScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'General Ledger', url: '/pages/generalledger' },
                            { title: 'General Ledger Detail', active: true },
                        ],
                        pageTitle: 'General Ledger',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/labarugi',
                    name: 'pages-labarugi',
                    component: () => import('./views/pages/Accounting/LabaRugiScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Laba Rugi', active: true },
                        ],
                        pageTitle: 'Laba Rugi',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/hpp',
                    name: 'pages-hpp',
                    component: () => import('./views/pages/Accounting/HppScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'HPP', active: true },
                        ],
                        pageTitle: 'HPP',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/hppdetail/:idproduct/:datefrom/:dateto',
                    name: 'pages-hppdetail',
                    component: () => import('./views/pages/Accounting/HppDetailScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'HPP', url: '/pages/hpp' },
                            { title: 'HPP Detail', active: true },
                        ],
                        pageTitle: 'HPP Detail',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/neraca',
                    name: 'pages-neraca',
                    component: () => import('./views/pages/Accounting/NeracaScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Neraca', active: true },
                        ],
                        pageTitle: 'Neraca',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/reportproject',
                    name: 'pages-reportproject',
                    component: () => import('./views/pages/Report/ReportProjectScreen.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Report Project', active: true },
                        ],
                        pageTitle: 'Report Project',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/reportpenjualan',
                    name: 'pages-reportpenjualan',
                    component: () => import('./views/pages/Report/ReportPenjualanScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Report Penjualan',
                                active: true
                            },
                        ],
                        pageTitle: 'Report Penjualan',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/reportpenjualandetail',
                    name: 'pages-reportpenjualandetail',
                    component: () => import('./views/pages/Report/ReportPenjualanDetailScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Report Penjualan Detail',
                                active: true
                            },
                        ],
                        pageTitle: 'Report Penjualan Detail',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/reportpiutang',
                    name: 'pages-reportpiutang',
                    component: () => import('./views/pages/Report/ReportPiutangScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Report Piutang',
                                active: true
                            },
                        ],
                        pageTitle: 'Report Piutang',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/reportpiutangterbayar',
                    name: 'pages-reportpiutangterbayar',
                    component: () => import('./views/pages/Report/ReportPiutangTerbayarScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Report Piutang Terbayar',
                                active: true
                            },
                        ],
                        pageTitle: 'Report Piutang Terbayar',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/reportpembelian',
                    name: 'pages-reportpembelian',
                    component: () => import('./views/pages/Report/ReportPembelianScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Report Pembelian',
                                active: true
                            },
                        ],
                        pageTitle: 'Report Pembelian',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/reportpembeliandetail',
                    name: 'pages-reportpembeliandetail',
                    component: () => import('./views/pages/Report/ReportPembelianDetailScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Report Pembelian Detail',
                                active: true
                            },
                        ],
                        pageTitle: 'Report Pembelian Detail',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/reporthutang',
                    name: 'pages-reporthutang',
                    component: () => import('./views/pages/Report/ReportHutangScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Report Hutang',
                                active: true
                            },
                        ],
                        pageTitle: 'Report Hutang',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/reporthutangterbayar',
                    name: 'pages-reporthutangterbayar',
                    component: () => import('./views/pages/Report/ReportHutangTerbayarScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Report Hutang Terbayar',
                                active: true
                            },
                        ],
                        pageTitle: 'Report Hutang Terbayar',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/reportjumlahstok',
                    name: 'pages-reportjumlahstok',
                    component: () => import('./views/pages/Report/ReportStockProductScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Report Jumlah Stock',
                                active: true
                            },
                        ],
                        pageTitle: 'Report Jumlah Stock',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/reportmutasistock',
                    name: 'pages-reportmutasistock',
                    component: () => import('./views/pages/Report/ReportMutasiStockScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Report Mutasi Stock',
                                active: true
                            },
                        ],
                        pageTitle: 'Report Mutasi Stock',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/kas',
                    name: 'pages-kas',
                    component: () => import('./views/pages/Report/ReportKasScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Report Cash',
                                active: true
                            },
                        ],
                        pageTitle: 'Report Cash',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formpenyusutanaset',
                    name: 'pages-formpenyusutanaset',
                    component: () => import('./views/form/FormPenyusutanAset.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form Penyusutan Aset',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Penyusutan Aset',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/penyusutanaset',
                    name: 'pages-penyusutanaset',
                    component: () => import('./views/pages/Accounting/PenyusutanAsetScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penyusutan Aset Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Penyusutan Aset Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/closing',
                    name: 'pages-closing',
                    component: () => import('./views/pages/Accounting/ClosingScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Closing Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Closing Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/settingclosing',
                    name: 'pages-settingclosing',
                    component: () => import('./views/pages/Accounting/SettingClosingScreen.vue'),
                    meta: {
                        breadcrumb: [
                            {
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Closing Page',
                                url: '/pages/closing'
                            },
                            {
                                title: 'Setting Closing Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Setting Closing Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/editpenyusutanaset/:idaset',
                    name: 'pages-editpenyusutanaset',
                    component: () => import('./views/form/FormPenyusutanAset.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penyusutan Aset Page',
                                url: '/pages/penyusutanaset'
                            },
                            {
                                title: 'Form Penyusutan Aset',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Penyusutan Aset',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/detailpenyusutanaset/:idaset',
                    name: 'pages-detailpenyusutanaset',
                    component: () => import('./views/pages/Accounting/PenyusutanAsetDetailScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penyusutan Aset Page',
                                url: '/pages/penyusutanaset'
                            },
                            {
                                title: 'Penyusutan Aset Detail Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Penyusutan Aset Detail Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/cashmanagement',
                    name: 'pages-cashmanagement',
                    component: () => import('./views/pages/Finance/CashManagementScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Cash Management',
                                active: true
                            },
                        ],
                        pageTitle: 'Cash Management',
                        rule: 'editor'
                    },
                }, 

                // ===========================================================================
                // AGRO - SISTEM GUDANG
                // ===========================================================================
            //     { //agro
            //         path: '/pages/formpenjualan/:idpenjualan/:tipe/:tipeTransaksi',
            //         name: 'pages-formpenjualan',
            //         component: () => import('./views/pages/Penjualan/FormPenjualanScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Form Penjualan Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Page Form Penjualan',
            //             rule: 'editor'
            //         },
            //     },

            //     { //agro
            //         path: '/pages/viewpenjualandetail/:idpenjualan/:tipe',
            //         name: 'pages-viewpenjualan',
            //         component: () => import('./views/pages/Penjualan/ViewPenjualanScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Penjualan Page',
            //                     url: '/pages/penjualan'
            //                 },
            //                 {
            //                     title: 'Penjualan Detail Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Penjualan Detail Page',
            //             rule: 'editor'
            //         },
            //     },

            //     { //agro
            //         path: '/pages/viewsuratjalandetail/:iddo',
            //         name: 'pages-viewsuratjalan',
            //         component: () => import('./views/pages/Penjualan/ViewSuratJalan.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Penjualan Page',
            //                     url: '/pages/penjualan'
            //                 },
            //                 {
            //                     title: 'Surat Jalan Detail Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Surat Jalan Detail Page',
            //             rule: 'editor'
            //         },
            //     },
            //     { //agro
            //         path: '/pages/reviewsuratjalan/:idpenjualan',
            //         name: 'pages-reviewsuratjalan',
            //         component: () => import('./views/pages/Penjualan/ReviewSuratJalanScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Penjualan Page',
            //                     url: '/pages/penjualan'
            //                 },
            //                 {
            //                     title: 'Review Surat Jalan Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Review Surat Jalan Page',
            //             rule: 'editor'
            //         },
            //     },

            //     { // agro
            //         path: '/pages/popenjualan',
            //         name: 'pages-popenjualan',
            //         component: () => import('./views/pages/Penjualan/POPenjualanScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'PO Project Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'PO Project Page',
            //             rule: 'editor'
            //         },
            //     },

            //     { //agro
            //         path: '/pages/penjualan/:tipe',
            //         name: 'pages-penjualan',
            //         component: () => import('./views/pages/Penjualan/PenjualanScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Penjualan Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Penjualan Page',
            //             rule: 'editor'
            //         },
            //     },

            //     { //agro
            //         path: '/pages/formreviewpenjualan/:idpenjualan/:tipeTransaksi',
            //         name: 'pages-formreviewpenjualan',
            //         component: () => import('./views/pages/Penjualan/FormReviewPenjualan.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Form Review Penjualan Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Form Review Penjualan Page',
            //             rule: 'editor'
            //         },
            //     },

            //     { //agro
            //         path: '/pages/revisifaktur/:tipeTransaksi',
            //         name: 'pages-revisifaktur',
            //         component: () => import('./views/pages/Penjualan/RevisiFakturScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Revisi Faktur Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Revisi Faktur Page',
            //             rule: 'editor'
            //         },
            //     },

            //     { //page piutang agro
            //         path: '/pages/piutang',
            //         name: 'pages-piutang',
            //         component: () => import('./views/pages/Penjualan/PiutangScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Piutang Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Piutang Page',
            //             rule: 'editor'
            //         },
            //     },

            //     { //page hutang agro
            //         path: '/pages/hutang',
            //         name: 'pages-hutang',
            //         component: () => import('./views/pages/Pembelian/HutangScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Hutang Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Hutang Page',
            //             rule: 'editor'
            //         },
            //     },

            //     { //agro
            //         path: '/pages/pembelian',
            //         name: 'pages-pembelian',
            //         component: () => import('./views/pages/Pembelian/PembelianScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Pembelian Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Pembelian Page',
            //             rule: 'editor'
            //         },
            //     },

            //     { //agro
            //         path: '/pages/viewpembeliandetail/:idpembelian/:tipe',
            //         name: 'pages-viewpembelian',
            //         component: () => import('./views/pages/Pembelian/ViewPembelianScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Pembelian Page',
            //                     url: '/pages/pembelian'
            //                 },
            //                 {
            //                     title: 'Pembelian Detail Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Pembelian Detail Page',
            //             rule: 'editor'
            //         },
            //     },

            //     { //agro
            //         path: '/pages/po',
            //         name: 'pages-po',
            //         component: () => import('./views/pages/Pembelian/POScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Purchase Order Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Purchase Order Page',
            //             rule: 'editor'
            //         },
            //     },

            //     { //agro
            //         path: '/pages/formpo/:idPembelian/:tipe',
            //         name: 'pages-formpo',
            //         component: () => import('./views/pages/Pembelian/FormPOScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Form Purchase Order',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Form Purchase Order',
            //             rule: 'editor'
            //         },
            //     },
            //     { //agro
            //         path: '/pages/pembelian/revisifaktur',
            //         name: 'pages-pembelianrevisifaktur',
            //         component: () => import('./views/pages/Pembelian/RevisiFakturScreen.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Revisi Faktur Pembelian',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Revisi Faktur Pembelian',
            //             rule: 'editor'
            //         },
            //     },

            //     { //agro
            //         path: '/pages/formreviewpembelian/:idPembelian',
            //         name: 'pages-formreviewpembelian',
            //         component: () => import('./views/pages/Pembelian/FormReviewPembelian.vue'),
            //         meta: {
            //             breadcrumb: [{
            //                     title: 'Home',
            //                     url: '/'
            //                 },
            //                 {
            //                     title: 'Form Review Pembelian Page',
            //                     active: true
            //                 },
            //             ],
            //             pageTitle: 'Form Review Pembelian Page',
            //             rule: 'editor'
            //         },
            //     },

            //     {//agro
            //       path: '/pages/customer',
            //       name: 'pages-customer',
            //       component: () => import('./views/pages/MasterData/CustomerScreenAgro.vue'),
            //       meta: {
            //           breadcrumb: [{
            //                   title: 'Home',
            //                   url: '/'
            //               },
            //               {
            //                   title: 'Customer',
            //                   active: true
            //               },
            //           ],
            //           pageTitle: 'Manage Customer',
            //           rule: 'editor'
            //       },
            //   },

                //  END AGRO - SISTEM GUDANG
                //  ===============================================================================

                // ================================================================================
                // BW - SISTEM GUDANG
                // ================================================================================
                { //bw
                    path: '/pages/formpenjualan/:idpenjualan/:tipe',
                    name: 'pages-formpenjualan',
                    component: () => import('./views/pages/PenjualanBW/FormPenjualanScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form Penjualan Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Page Form Penjualan',
                        rule: 'editor'
                    },
                },

                { // bw
                    path: '/pages/viewpenjualandetail/:idpenjualan/:tipe',
                    name: 'pagesbw-viewpenjualan',
                    component: () => import('./views/pages/PenjualanBW/ViewPenjualanScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penjualan Page',
                                url: '/pages/penjualan'
                            },
                            {
                                title: 'Penjualan Detail Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Penjualan Detail Page',
                        rule: 'editor'
                    },
                },

                { //  bw
                    path: '/pages/viewsuratjalandetail/:iddo',
                    name: 'pagesbw-viewsuratjalan',
                    component: () => import('./views/pages/PenjualanBW/ViewSuratJalan.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penjualan Page',
                                url: '/pages/penjualan'
                            },
                            {
                                title: 'Surat Jalan Detail Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Surat Jalan Detail Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/reviewsuratjalan/:idpenjualan',
                    name: 'pagesbw-reviewsuratjalan',
                    component: () => import('./views/pages/PenjualanBW/ReviewSuratJalanScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penjualan Page',
                                url: '/pages/penjualan'
                            },
                            {
                                title: 'Review Surat Jalan Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Review Surat Jalan Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/ro/reviewsuratjalan/:idpenjualan',
                    name: 'pagesbw-ro/reviewsuratjalan',
                    component: () => import('./views/pages/Project/ReviewSuratJalanScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penjualan Page',
                                url: '/pages/penjualan'
                            },
                            {
                                title: 'Review Surat Jalan Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Review Surat Jalan Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/ro',
                    name: 'pages-ro',
                    component: () => import('./views/pages/Project/POPenjualanScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'RO Project Page',
                                active: true
                            },
                        ],
                        pageTitle: 'RO Project Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/formpopenjualan/:idPenjualan/:tipe',
                    name: 'pages-formpopenjualan',
                    component: () => import('./views/pages/Project/FormPOPenjualan.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form PO Project Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Form PO Project Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/formro/:transaksi/:id/:tipe',
                    name: 'pages-formro',
                    component: () => import('./views/pages/Project/FormApproveRO.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form Approve RO',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Approve RO',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/formreviewpopenjualan/:idpopenjualan',
                    name: 'pages-formreviewpopenjualan',
                    component: () => import('./views/form/FormReviewPOPenjualan.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form Review PO Penjualan Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Review PO Penjualan Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/formreviewpenjualan/:idpenjualan',
                    name: 'pages-formreviewpenjualan',
                    component: () => import('./views/pages/Penjualan/FormReviewPenjualan.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form Review Penjualan Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Review Penjualan Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/ro/penjualan',
                    name: 'pages-ro-penjualan',
                    component: () => import('./views/pages/Project/PenjualanScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'RO Penjualan Page',
                                active: true
                            },
                        ],
                        pageTitle: 'RO Penjualan Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/ro/pembelian',
                    name: 'pages-ro-pembelian',
                    component: () => import('./views/pages/Project/PembelianScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Pembelian RO Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Pembelian RO Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/ro/:transaksi/:id/detail',
                    name: 'pages-ro-detail',
                    component: () => import('./views/pages/Project/ViewDetailScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Detail RO',
                                active: true
                            },
                        ],
                        pageTitle: 'Detail RO',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/penjualan',
                    name: 'pages-penjualan',
                    component: () => import('./views/pages/PenjualanBW/PenjualanScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penjualan Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Penjualan Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/pembelian',
                    name: 'pages-pembelian',
                    component: () => import('./views/pages/PembelianBW/PembelianScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Pembelian Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Pembelian Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/pembelian/revisifaktur',
                    name: 'pages-pembelianrevisifaktur',
                    component: () => import('./views/pages/PembelianBW/RevisiFakturScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Revisi Faktur Pembelian',
                                active: true
                            },
                        ],
                        pageTitle: 'Revisi Faktur Pembelian',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/formreviewpembelian/:idPembelian',
                    name: 'pages-formreviewpembelian',
                    component: () => import('./views/pages/PembelianBW/FormReviewPembelian.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form Review Pembelian Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Review Pembelian Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/viewpembeliandetail/:idpembelian/:tipe',
                    name: 'pagesbw-viewpembelian',
                    component: () => import('./views/pages/PembelianBW/ViewPembelianScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Pembelian Page',
                                url: '/pages/pembelian'
                            },
                            {
                                title: 'Pembelian Detail Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Pembelian Detail Page',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/po',
                    name: 'pages-po',
                    component: () => import('./views/pages/PembelianBW/POScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Purchase Order Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Purchase Order Page',
                        rule: 'editor'
                    },
                },

                {
                    path: '/pages/formreviewpo/:idpopembelian',
                    name: 'pages-formreviewpo',
                    component: () => import('./views/form/FormReviewPOPembelian.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form Review PO Pembelian',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Review PO Pembelian',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/formpo/:idPembelian/:tipe',
                    name: 'pagesbw-formpo',
                    component: () => import('./views/pages/PembelianBW/FormPOScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form Purchase Order',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Purchase Order',
                        rule: 'editor'
                    },
                },

                { //bw
                    path: '/pages/revisifaktur',
                    name: 'pages-revisifaktur',
                    component: () => import('./views/pages/PenjualanBW/RevisiFakturScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Revisi Faktur Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Revisi Faktur Page',
                        rule: 'editor'
                    },
                },

                { //pages piutang bw
                    path: '/pages/piutang',
                    name: 'pages-piutang',
                    component: () => import('./views/pages/PenjualanBW/PiutangScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Piutang Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Piutang Page',
                        rule: 'editor'
                    },
                },

                { //page hutang bw
                    path: '/pages/hutang',
                    name: 'pages-hutang',
                    component: () => import('./views/pages/PembelianBW/HutangScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Hutang Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Hutang Page',
                        rule: 'editor'
                    },
                },
                { //BW
                  path: '/pages/customer',
                  name: 'pages-customer',
                  component: () => import('./views/pages/MasterData/CustomerScreen.vue'),
                  meta: {
                    breadcrumb: [
                        { title: 'Home', url: '/' },
                        { title: 'Customer', active: true },
                    ],
                    pageTitle: 'Manage Customer',
                    rule: 'editor'
                  },
                },

                // END BW - SISTEM GUDANG
                // ==========================================================================


                {
                    path: '/pages/penyesuaianstock',
                    name: 'pages-penyesuaianstock',
                    component: () => import('./views/pages/Gudang/PenyesuaianStockScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penyesuaian Stock Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Penyesuaian Stock Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formpenyesuaianstock',
                    name: 'pages-formpenyesuaianstock',
                    component: () => import('./views/form/FormPenyesuaianStock.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form Penyesuaian Stock Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Penyesuaian Stock Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formpenyesuaianstock/:idadjustment',
                    name: 'pages-editadjustment',
                    component: () => import('./views/form/FormPenyesuaianStock.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form Penyesuaian Stock Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Penyesuaian Stock Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewpenyesuaianstock/:idadjustment',
                    name: 'pages-viewadjustment',
                    component: () => import('./views/pages/Gudang/ViewPenyesuaianStock.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'View Penyesuaian Stock Page',
                                active: true
                            },
                        ],
                        pageTitle: 'View Penyesuaian Stock Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/estimasi',
                    name: 'pages-estimasi',
                    component: () => import('./views/pages/Operational/EstimasiScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Estimasi Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Estimasi Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formestimasi/:idorder',
                    name: 'pages-formestimasi',
                    component: () => import('./views/form/FormEstimasiScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form Estimasi Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Estimasi Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewestimasi/:idorder',
                    name: 'pages-viewestimasi',
                    component: () => import('./views/pages/Operational/ViewEstimasiDetailScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'View Estimasi Page',
                                active: true
                            },
                        ],
                        pageTitle: 'View Estimasi Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewtotalestimasi/:idorder',
                    name: 'pages-viewtotalestimasi',
                    component: () => import('./views/pages/Operational/ViewTotalEstimasiScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'View Estimasi Page',
                                active: true
                            },
                        ],
                        pageTitle: 'View Estimasi Page',
                        rule: 'editor'
                    },
                }, 
                {
                    path: '/pages/license',
                    name: 'pages-license',
                    component: () => import('./views/license.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'License Page',
                                active: true
                            },
                        ],
                        pageTitle: 'License Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/itemhppawal',
                    name: 'pages-itemhppawal',
                    component: () => import('./views/HppAwalScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Set HPP Awal Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Set HPP Awal Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/profile',
                    name: 'pages-profile',
                    component: () => import('./views/form/FormProfile.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Profile Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Profile Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/absentukang',
                    name: 'pages-absentukang',
                    component: () => import('./views/pages/Operational/AbsenTukangScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Absen Tukang Page',
                                active: true
                            },
                        ],
                        pageTitle: 'Absen Tukang Page',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formabsentukang/:idabsen/:tipe',
                    name: 'pages-formabsentukang',
                    component: () => import('./views/form/FormAbsenTukang.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Form Absen Tukang',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Absen Tukang',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewabsentukang/:idabsen',
                    name: 'pages-viewabsentukang',
                    component: () => import('./views/pages/Operational/ViewAbsenTukangScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Absen Tukang Page',
                                active: false
                            },
                            {
                                title: 'View Absen Tukang',
                                active: true
                            },
                        ],
                        pageTitle: 'View Absen Tukang',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/penjualan/faktur/print/:idPenjualan',
                    name: 'pages-printpenjualan',
                    component: () => import('./views/pages/Print/PrintPenjualan.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penjualan',
                                active: false
                            },
                            {
                                title: 'Print Faktur',
                                active: true
                            },
                        ],
                        pageTitle: 'Print Faktur',
                        rule: 'editor'
                    },
                }, {
                    path: '/pages/pembelian/faktur/print/:idPembelian',
                    name: 'pages-printpembelian',
                    component: () => import('./views/pages/Print/PrintPembelian.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Pembelian',
                                active: false
                            },
                            {
                                title: 'Print Faktur',
                                active: true
                            },
                        ],
                        pageTitle: 'Print Faktur',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/pembelian/order/print/:idPembelian',
                    name: 'pages-printorderpembelian',
                    component: () => import('./views/pages/Print/PrintOrderPembelian.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Pembelian',
                                active: false
                            },
                            {
                                title: 'Print Order Pembelian',
                                active: true
                            },
                        ],
                        pageTitle: 'Print  Order Pembelian',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/penawaran/print/:idPenawaran',
                    name: 'pages-printpenawaran',
                    component: () => import('./views/pages/Print/SuratPenawaran1.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penawaran',
                                active: false
                            },
                            {
                                title: 'Print Penawaran',
                                active: true
                            },
                        ],
                        pageTitle: 'Print Penawaran',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/penawaran/print/lampiran/:idPenawaran',
                    name: 'pages-printlampiranpenawaran',
                    component: () => import('./views/pages/Print/LampiranPenawaran1.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penawaran',
                                active: false
                            },
                            {
                                title: 'Print Penawaran',
                                active: true
                            },
                        ],
                        pageTitle: 'Print Penawaran',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/penawaran2/print/:idPenawaran',
                    name: 'pages-printpenawaran2',
                    component: () => import('./views/pages/Print/SuratPenawaran2.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penawaran',
                                active: false
                            },
                            {
                                title: 'Print Penawaran 2',
                                active: true
                            },
                        ],
                        pageTitle: 'Print Penawaran 2',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/penawaran2/print/lampiran/:idPenawaran',
                    name: 'pages-printlampiranpenawaran2',
                    component: () => import('./views/pages/Print/LampiranPenawaran2.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Penawaran',
                                active: false
                            },
                            {
                                title: 'Print Penawaran',
                                active: true
                            },
                        ],
                        pageTitle: 'Print Penawaran',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/kasbon',
                    name: 'pages-kasbon',
                    component: () => import('./views/pages/Operational/KasbonScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Kasbon',
                                active: true
                            },
                        ],
                        pageTitle: 'Page Kasbon',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewkasbon/:idKasbon',
                    name: 'pages-viewkasbon',
                    component: () => import('./views/pages/Operational/ViewKasbonScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Detail Kasbon',
                                active: true
                            },
                        ],
                        pageTitle: 'Page Detail Kasbon',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/viewrealisasikasbon/:idKasbon',
                    name: 'pages-viewrealisasikasbon',
                    component: () => import('./views/pages/Operational/ViewRealKasbonScreen.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Detail Realisasi Kasbon',
                                active: true
                            },
                        ],
                        pageTitle: 'Page Detail Realisasi Kasbon',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formkasbon/:idKasbon/:tipe',
                    name: 'pages-formkasbon',
                    component: () => import('./views/form/FormKasbon.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Kasbon',
                                active: true
                            },
                            {
                                title: 'Form Kasbon',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Kasbon',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/formrealisasikasbon/:idKasbon/:tipe',
                    name: 'pages-formrealisasikasbon',
                    component: () => import('./views/form/FormRealisasiKasbon.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Kasbon',
                                active: true
                            },
                            {
                                title: 'Form Realisasi Kasbon',
                                active: true
                            },
                        ],
                        pageTitle: 'Form Realisasi Kasbon',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/tandaterima/print',
                    name: 'pages-printtandaterima',
                    component: () => import('./views/pages/Print/TandaTerima.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Print Tanda Terima',
                                active: true
                            },
                        ],
                        pageTitle: 'Print  Tanda Terima',
                        rule: 'editor'
                    },
                },
                {
                    path: '/pages/suratjalan/print/:idSuratjalan',
                    name: 'pages-printsuratjalan',
                    component: () => import('./views/pages/Print/PrintSuratJalan.vue'),
                    meta: {
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },
                            {
                                title: 'Print Surat Jalan',
                                active: true
                            },
                        ],
                        pageTitle: 'Print Surat Jalan',
                        rule: 'editor'
                    },
                },
            ],
        },

        // Redirect to 404 page, if no match found
        {
            path: '*',
            redirect: '/pages/error-404'
        }
    ],
})

router.afterEach(() => {
    // Remove initial loading
    const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = "none";
    }
})

export default router
