/*=========================================================================================
  File Name: main.js
  Description: main vue(js) file
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

import Vue from 'vue'
import App from './App.vue'
import VueCurrencyInput from 'vue-currency-input'
import JsonExcel from 'vue-json-excel'
import VTooltip from 'v-tooltip'
Vue.use(VTooltip)

import VueHtmlToPaper from 'vue-html-to-paper'

Vue.use(require('vue-moment'));

// Vuesax Component Framework
import Vuesax from 'vuesax'

Vue.use(Vuesax);
Vue.component('downloadExcel', JsonExcel)

// axios
import axios from "./axios.js"
import qs from "query-string"

const base = 'http://ligobakery.itcs.co.id/api';
const base2 = 'http://ligobakery.itcs.co.id'; 
// const base = 'http://192.168.1.198:8000/api'; 
// const base2 = 'http://192.168.1.198:8000';  


Vue.prototype.$http = axios;  
Vue.prototype.$qs = qs; 
Vue.prototype.$baseURL = base; 
Vue.prototype.$baseURL2 = base2; 
// Vue.prototype.$moment = VueMoment;

Vue.prototype.$token = localStorage.token; 

if (localStorage.hasOwnProperty('user')) {
  Vue.prototype.$user  = JSON.parse(localStorage.user);
  Vue.prototype.$company =Vue.prototype.$user.company ;
  Vue.prototype.$role = Vue.prototype.$user.role ;
  Vue.prototype.$menu = Vue.prototype.$user.menus;
  Vue.prototype.$karyawan = Vue.prototype.$user.karyawan;  
}else{
  Vue.prototype.$user=null;
  Vue.prototype.$company =null ;
  Vue.prototype.$role = null ;
  Vue.prototype.$menu = null;
  Vue.prototype.$karyawan = null;  
} 

const pluginOptions = {
  /* see config reference */
  globalOptions: { currency: 'IDR',locale: 'id' },
}

Vue.use(VueCurrencyInput,pluginOptions)

// Theme Configurations
import '../themeConfig.js'


// Globally Registered Components
import './globalComponents.js'


// Vue Router
import router from './router'


// Vuex Store
import store from './store/store'


// Vuejs - Vue wrapper for hammerjs
import { VueHammer } from 'vue2-hammer'
Vue.use(VueHammer)


Vue.filter('currency', function (value) {
  if (!value) return '0'
  let val = (value/1).toFixed(0).replace('.', ',')
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
})

Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.filter('auth', function (value) {
  if (value === 'Unauthenticated') {
    window.location.href = '/';  
  }
})

Vue.filter('limit', function (value) {
  return value.substr(0, 20)+' ...';
})

Vue.filter('denda', function (value) {

})


// PrismJS
import 'prismjs'
import 'prismjs/themes/prism-tomorrow.css'


// VueHtmlToPaper
const optionsHtml = {
    name: "_blank",
    specs: ["fullscreen=yes", "titlebar=yes", "scrollbars=yes"],
    styles: [
        "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css",
        "https://unpkg.com/kidlat-css/css/kidlat.css", 
        base2+"/css/app.css",
        base2+"/css/main.css",
        base2+"/css/vuesax.css",
        base2+"/css/prism-tomorrow.css",
        base2+"/css/iconfont.css",
        base2+"/css/material-icons/material-icons.css"
    ]
};

Vue.use(VueHtmlToPaper, optionsHtml);

// Vue select css
// Note: In latest version you have to add it separately
// import 'vue-select/dist/vue-select.css';


Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
