const option_region = [
    {
        id:11,
        location: "Aceh",
        code: "AC"
    },
    {
        id:51,
        location: "Bali",
        code: "BA"
    },
    {
        id:36,
        location: "Banten",
        code: "BT"
    },
    {
        id:17,
        location: "Bengkulu",
        code: "BE"
    },
    {
        id:75,
        location: "Gorontalo",
        code: "GO"
    },
    {
        id:31,
        location: "DKI Jakarta",
        code: "JK"
    },
    {
        id:15,
        location: "Jambi",
        code: "JA"
    },
    {
        id:32,
        location: "Jawa Barat",
        code: "JB"
    },
    {
        id:33,
        location: "Jawa Tengah",
        code: "JT"
    },
    {
        id:35,
        location: "Jawa Timur",
        code: "JI"
    },
    {
        id:61,
        location: "Kalimantan Barat",
        code: "KB"
    },
    {
        id:63,
        location: "Kalimantan Selatan",
        code: "KS"
    },
    {
        id:62,
        location: "Kalimantan Tengah",
        code: "KT"
    },
    {
        id:64,
        location: "Kalimantan Timur",
        code: "KI"
    },
    {
        id:65,
        location: "Kalimantan Utara",
        code: "KU"
    },
    {
        id:19,
        location: "Kepulauan Bangka Belitung",
        code: "BB"
    },
    {
        id:21,
        location: "Kepulauan Riau",
        code: "KR"
    },
    {
        id:18,
        location: "Lampung",
        code: "LA "
    },
    {
        id:81,
        location: "Maluku",
        code: "MA"
    },
    {
        id:82,
        location: "Maluku Utara",
        code: "MU"
    },
    {
        id:52,
        location: "Nusa Tenggara Barat",
        code: "NB"
    },
    {
        id:53,
        location: "Nusa Tenggara Timur",
        code: "NT"
    },
    {
        id:94,
        location: "Papua",
        code: "PA"
    },
    {
        id:91,
        location: "Papua Barat",
        code: "PB"
    },
    {
        id:76,
        location: "Sulawesi Barat",
        code: "SR"
    },
    {
        id:73,
        location: "Sulawesi Selatan",
        code: "SN"
    },
    {
        id:72,
        location: "Sulawesi Tengah",
        code: "ST"
    },
    {
        id:74,
        location: "Sulawesi Tenggara",
        code: "SG"
    },
    {
        id:71,
        location: "Sulawesi Utara",
        code: "SA"
    },
    {
        id:13,
        location: "Sumatera Barat",
        code: "SB"
    },
    {
        id:16,
        location: "Sumatera Selatan",
        code: "SS "
    },
    {
        id:12,
        location: "Sumatera Utara",
        code: "SU"
    },
    {
        id:34,
        location: "Daerah Istimewa Yogyakarta",
        code: "YO"
    },
  ];
  
  export default {
    data() {
      return {
        option_region: option_region
      };
    },
    computed: {
    }
  };
  