export default [
    {
        url: "/home",
        name: "Home",
        slug: "home",
        icon: "HomeIcon",
    },
    {
        url: "null",
        name: "Workshop",
        icon: "EditIcon",
        submenu: [
            { url: "/pages/workshop", name: "List Workshop", slug: "workshop", }, ]
    },
    {
        url: null,
        name: "Project",
        icon: "EditIcon",
        submenu: [
            { url: "/pages/spk", name: "SPK", slug: "createspk", },
            { url: "/pages/confirmspk", name: "Confirm SPK", slug: "confirmspk", },
            { url: "/pages/estimasi", name: "Estimasi", slug: "createestimasi", },
            { url: '/pages/borongan', name: "Borongan", slug: "pages-borongan", },
            { url: '/pages/penawaran', name: "Penawaran", slug: "pages-penawaran", },
            { url: '/pages/kasbon', name: "Kasbon", slug: "pages-kasbon", },
            { url: '/pages/absentukang', name: "Absen Tukang", slug: "pages-absentukang", },
        ]
    }, 
    //BW
    {
        url: null,
        name: "Penjualan BW",
        icon: "EditIcon",
        submenu: [ 
            { url: '/pages/bw/formpenjualan/0/new', name: "Buat Penjualan", slug: "pagesbw-formpenjualan", },
            { url: '/pages/bw/penjualan', name: "Penjualan", slug: "pagesbw-penjualan", },
            { url: '/pages/bw/revisifaktur', name: "Revisi Faktur", slug: "pagesbw-revisifaktur", },

        ]
    },
    // {
    //   url: null,
    //   name: "Penjualan (Company)",
    //   icon: "EditIcon",
    //   submenu: [
    //     { url: '/pages/formpenjualan/0/new/company', name: "Buat Penjualan", slug: "pages-formpenjualan", },
    //     { url: '/pages/penjualan/company', name: "Penjualan", slug: "pages-penjualan", },
    //     { url: '/pages/revisifaktur/company', name: "Revisi Faktur", slug: "pages-revisifaktur", }, 
    //   ]
    // },  
    // // Agro
    // {
    //     url: null,
    //     name: "Penjualan (Free Market)",
    //     icon: "EditIcon",
    //     submenu: [
    //         { url: '/pages/formpenjualan/0/new/freemarket', name: "Buat Penjualan", slug: "pages-formpenjualan", },
    //         { url: '/pages/penjualan/freemarket', name: "Penjualan", slug: "pages-license", },
    //         { url: '/pages/revisifaktur/freemarket', name: "Revisi Faktur", slug: "pages-revisifaktur", }, 
    //     ]
    // }, 
    // {
    //   url: null,
    //   name: "Pembelian",
    //   icon: "EditIcon",
    //   submenu: [
    //     { url: '/pages/popenjualan', name: "PO Project", slug: "pages-purchaseorderpenjualan", },
    //     { url: '/pages/po',  name: "Purchase Order", slug: "pages-po", },
    //     { url: '/pages/pembelian', name: "Pembelian", slug: "pages-pembelian", }, 
    //   ]
    // },
    {
        url: null,
        name: "Pembelian BW",
        icon: "EditIcon",
        submenu: [
            { url: '/pages/popenjualan', name: "PO Project", slug: "pages-purchaseorderpenjualan", },
            { url: '/pages/po', name: "Purchase Order", slug: "pages-po", },
            { url: '/pages/pembelian', name: "Pembelian", slug: "pages-pembelian", },
        ]
    },
    {
        url: null,
        name: "Master Data",
        icon: "DatabaseIcon",
        submenu: [
            { url: '/pages/user', name: "User", slug: "pages-user", },
            { url: '/pages/userrole', name: "User Role", slug: "pages-userrole", },
            { url: '/pages/supplier', name: "Supplier", slug: "pages-supplier", },
            { url: '/pages/customer', name: "Customer", slug: "pages-customer", },
            { url: '/pages/karyawan', name: "Karyawan", slug: "pages-karyawan", },
            { url: '/pages/craftsman', name: "Tukang", slug: "pages-craftsman", },
            { url: '/pages/vendor', name: "Vendor", slug: "pages-vendor", },
            { url: '/pages/company', name: "Company", slug: "pages-company", },
            { url: '/pages/warehouse', name: "Warehouse", slug: "pages-warehouse", },
            { url: '/pages/product', name: "Product", slug: "pages-product", },
            { url: '/pages/productcategory', name: "Product Category", slug: "pages-productcategory", },
            { url: '/pages/satuan', name: "Satuan", slug: "pages-satuan", },
            { url: '/pages/sales', name: "Sales", slug: "pages-sales", },
        ]
    },
    {
        url: null,
        name: "Accounting",
        icon: "BookOpenIcon",
        submenu: [
            { url: '/pages/coa', name: "Manage COA", slug: "pages-coa", },
            { url: '/pages/generalledger', name: "General Ledger", slug: "pages-gl", },
            { url: '/pages/journal', name: "Journal", slug: "pages-journal", },
            { url: '/pages/labarugi', name: "Laba Rugi", slug: "pages-labarugi", },
            { url: '/pages/neraca', name: "Neraca", slug: "pages-neraca", },
            { url: '/pages/penyusutanaset', name: "Penyusutan Aset", slug: "pages-penyusutanaset", },
            { url: '/pages/closing', name: "Closing", slug: "pages-closing", },
        ]
    },
    {
        url: null,
        name: "Gudang",
        icon: "ArchiveIcon",
        submenu: [
            { url: '/pages/penyesuaianstock', name: "Penyesuaian Stock", slug: "pages-penyesuaianstock", }, 
          ]
    },

    {
        url: null,
        name: "Finance",
        icon: "DollarSignIcon",
        submenu: [
            { url: '/pages/cashmanagement', name: "Cash Management", slug: "pages-cashmanagement", },
            { url: '/pages/invoice', name: "Invoice",  slug: "pages-invoice", },
            { url: '/pages/vendorproject', name: "Vendor Project", slug: "pages-vendorproject", },
            { url: '/pages/gajitukang', name: "Gaji Tukang", slug: "pages-gaji-tukang", },
            { url: '/pages/hutang', name: "Hutang", slug: "pages-hutang", },
            { url: '/pages/piutang', name: "Piutang", slug: "pages-piutang", },
        ]
    },
    {
        url: null,
        name: "Reports",
        icon: "BookIcon",
        submenu: [
            { url: '/pages/reportproject', name: "Project", slug: "pages-reportproject", },
            { url: '/pages/reportpenjualan', name: "Penjualan", slug: "pages-reportpenjualan", },
            { url: '/pages/reportpenjualandetail', name: "Penjualan Detail", slug: "pages-reportpenjualandetail", },
            { url: '/pages/reportpiutang', name: "Piutang", slug: "pages-reportpiutang", },
            { url: '/pages/reportpiutangterbayar', name: "Piutang Terbayar", slug: "pages-piutangterbayar", },
            { url: '/pages/reportpembelian', name: "Pembelian", slug: "pages-reportpembelian", },
            { url: '/pages/reportpembeliandetail', name: "Pembelian Detail", slug: "pages-reportpembeliandetail", },
            { url: '/pages/reporthutang', name: "Hutang", slug: "pages-reporthutang", },
            { url: '/pages/reporthutangterbayar', name: "Hutang Terbayar", slug: "pages-reporthutangterbayar", },
            { url: '/pages/reportjumlahstok', name: "Jumlah Stok", slug: "pages-reportjumlahstock", },
            { url: '/pages/reportmutasistock', name: "Mutasi Stock", slug: "pages-reportmutasistock", },
            { url: '/pages/kas', name: "Kas", slug: "pages-kas", },
        ]
    },
    {
        url: null,
        name: "Settings",
        icon: "SettingsIcon",
        submenu: [
            { url: '/pages/license',  name: "License", slug: "pages-license", }, 
          ]
    },
]
