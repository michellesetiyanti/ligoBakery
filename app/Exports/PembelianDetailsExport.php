<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PembelianDetailsExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function  view(): View
    {
        $detail = session('detail');

        $params = ['pembelians'=>$detail];
        return view('exportdatareportpembeliandetail', $params);
    }
}
