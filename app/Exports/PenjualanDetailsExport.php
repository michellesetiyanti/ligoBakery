<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PenjualanDetailsExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function  view(): View
    {
        $detail = session('detail');

        $params = ['penjualans'=>$detail];
        return view('exportdatareportpenjualandetail', $params);
    }
}
