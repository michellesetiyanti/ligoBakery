<?php

namespace App\Contracts;

/**
 * Interface RepositoryInterface
 */
interface RepositoryInterface
{
    public function all(array $data = ['*']);

    public function save(array $data = ['*']);

    public function update(array $data = ['*'], $id);

    public function delete($id);

    public function show($id);
}
