<?php

namespace App\Abstracts;

use App\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;

abstract class BaseRepository implements RepositoryInterface
{
    /**
     * The repository model.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;
    
    private $relation = false;
    private $group = false;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->app = new App;
        $this->makeModel();
    }

    public function makeModel()
    {
        $model = $this->app->make($this->model());
        if (!$model instanceof Model) {
            throw new \Exception("Class {$model} must be an instance of " . Model::class);
        }
        return $this->model = $model;
    }

    // Get instances of model
    public function with($data)
    {
        $this->relation = $data;
        return $this;
    }

    // Get instances of model
    public function groupBy($data)
    {
        $this->group = $data;
        return $this;
    }

    // Get instances of model
    public function all(array $data = ['*'])
    {
        $model = $this->model;
        $primaryKey = $model->getKeyName();
        $fillable = $model->getFillable();
        if (array_key_exists('perpage', $data) && array_key_exists('page', $data)) {
            $limit = $data['perpage'] ?? 10;
            $offset = $limit * ($data['page'] ? $data['page'] - 1 : 0);
            $model = $model->offset($offset)->limit($limit);
        }
        if ($this->relation) {
            $model = $model->with($this->relation);
        }
        if ($this->group) {
            $model = $model->groupBy($this->group);
        } 
        if (array_key_exists('sorting', $data) && !empty($data['sorting'])) {
            list($order, $sort) = explode('|', $data['sorting']);
            $model = $model->orderBy($order, $sort);
        } else {
            $model = $model->orderBy('created_at', 'DESC');
        }
        if (array_key_exists('search', $data)) {
            $search = $data['search'];
            if (is_array($search)) {
                $model = $model->where($search);
            } else {
                $model = $model->where(function($query) use ($search, $fillable) {
                    for ($i=0; $i < count($fillable); $i++) {
                        if ($i == 0) {
                            $query->where($fillable[$i], 'LIKE', '%'.$search.'%');
                        }
                        $query->orWhere($fillable[$i], 'LIKE', '%'.$search.'%');
                    }
                });
            }
        }
        if (array_key_exists('filter', $data) && !empty($data['filter'])) {
            $filter = $data['filter'];
            $model = $model->where(function($query) use ($filter) {
                $whereIn = $whereInOr = [];
                $explode = explode(',', $filter);
                if (count($explode) > 1) {
                    for ($i=0; $i < count($explode); $i++) {
                        $exp = explode('|', $explode[$i]);
                        $exp_1 = explode(':', $exp[1]);
                        if (count($exp_1) > 1) {
                            $whereIn = array_merge($whereIn, $exp_1);
                            $query->whereIn($exp[0], $whereIn);
                        } else {
                            $query->where($exp[0], $exp[2] ?? '=', $exp[1]);
                        }
                        if ($i > 0) {
                            if (count($exp_1) > 1) {
                                $whereInOr = array_merge($whereInOr, $exp_1);
                                $query->whereIn($exp[0], $whereInOr, 'or');
                            } else {
                                $query->orWhere($exp[0], $exp[2] ?? '=', $exp[1]);
                            }
                        }
                    }
                } else {
                    $explode = explode(';', $filter);
                    if (count($explode) > 1) {
                        for ($i=0; $i < count($explode); $i++) {
                            $exp = explode('|', $explode[$i]);
                            $exp_1 = explode(':', $exp[1]);
                            if (count($exp_1) > 1) {
                                $whereIn = array_merge($whereIn, $exp_1);
                                $query->whereIn($exp[0], $whereIn);
                            } else {
                                $query->where($exp[0], $exp[2] ?? '=', $exp[1]);
                            }
                        }
                    } else {
                        $explode = explode('|', $filter);
                        $explode_1 = explode(':', $explode[1]);
                        if (count($explode_1) > 1) {
                            $whereIn = array_merge($whereIn, $explode_1);
                            $query->whereIn($explode[0], $whereIn);
                        } else {
                            $query->where($explode[0], $explode[2] ?? '=', $explode[1]);
                        }
                    }
                }
            });
        }
        if (array_key_exists('date_from', $data) || array_key_exists('date_to', $data)) {
            $from = \Carbon\Carbon::createFromFormat('d/m/Y', $data['date_from'] ?? date('d/m/Y'));
            if (empty($data['date_to'])) {
                $model = $model->where('created_at', '<=', $from->add(1, 'day')->toDateString());
            } else {
                $to = \Carbon\Carbon::createFromFormat('d/m/Y', $data['date_to']);
                $model = $model->whereBetween('created_at', [$from->toDateString(), $to->add(1, 'day')->toDateString()]);
            }
        }
        return $model->get();
    }

    // save a new record in the database
    public function save(array $data = ['*'])
    {
        $record = $this->model->fill($data);
        if ($record->save()) {
            return $record;
        }
    }

    // update record in the database
    public function update(array $data = ['*'], $id)
    {
        $record = $this->model->find($id);
        $record->fill($data);
        if ($record->save()) {
            return $record;
        }
    }

    // delete record from the database
    public function delete($id)
    {
        $record = $this->model->find($id);
        if ($record->delete()) {
            return $record;
        }
    }

    // show the record with the given id
    public function show($id)
    {
        $model = $this->model;
        if ($this->relation) {
            $model = $model->with($this->relation);
        }
        return $model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    abstract protected function model();
}
