<?php

use App\Repositories\Auth\Role;
Use App\Repositories\Auth\User;
Use App\Repositories\Master\Company;

if (!function_exists('uniqueCompany'))
{
    /**
     * uniqueCompany
     *
     * @param  string   $string
     * @param  string   $separator
     * @param  int      $pad_len
     * @param  string   $format (date format: https://www.php.net/manual/en/function.date.php)
     * @return string
     */
    function uniqueCompany($company, $word_len = 2, $total_len = 6)
    {
        $args = [];
        $company = \Str::slug($company, '_');
        $explode = explode('_', $company);
        for ($i=0; $i < count($explode); $i++) {
            $push = substr($explode[$i], 0, $word_len);
            array_push($args, $push);
        }
        $company = implode('', $args).rand();
        $company = \Str::limit($company, $total_len, false);
        return $company;
    }
}

if (!function_exists('createCompany'))
{
    /**
     * createCompany
     *
     * @param  array    $data
     * @param  bool     $warehouse
     * @param  bool     $coa
     * @param  array    $user
     * @param  string   $compType
     * @return
     */
    function createCompany(array $data, bool $warehouse = false, bool $coa = false, array $user = [], string $compType = 'general')
    {
        $companyRepo = new Company;
        $args = [
            'parent_id' => null,
            'name' => 'Company',
            'address' => null,
            'phone' => null,
            'currency_code' => 'IDR', // https://www.iban.com/currency-codes
            'npwp' => null,
            'taxable' => 'no', // in:yes,no
            'taxable_rate' => 0,
            'status' => 'active', // in:active,inactive
        ];
        $data = array_replace($args, $data);
        $su = [
            'email' => 'owner@email.com',
            'password' => 'owner123',
        ];
        $su = array_replace($su, $user);
        \DB::beginTransaction();
        try {
            $model = (new Company)->getModel();
            $company = $model->create($data);
            // User Owner
            $roles = (new Role)->all();
            $company->users()->create([
                'company_id' => $company->company_id,
                'type' => $roles->where('code', 'owner')->pluck('code')->first(),
                'username' => 'owner',
                'email' => $su['email'],
                'password' => $su['password'],
            ]);
            // Warehouse
            if ($warehouse) {
                $data = \Arr::only($data, ['name', 'address', 'phone', 'currency_code', 'npwp', 'taxable', 'taxable_rate', 'status']);
                $company->warehouse()->create($data);
            }
            // COA
            if ($coa) {
                if ($compType == 'agro') {
                    $coas = file_get_contents(base_path('import').'/agro/coas.csv');
                } else {
                    $coas = file_get_contents(base_path('import').'/coas.csv');
                }
                $coas = str_replace(['(',')','"',';',"'"], '', $coas);
                $coas = explode("\n", $coas);
                if ($company->coas->isEmpty()) {
                    foreach ($coas as $coa) {
                        if ($compType == 'agro') {
                            list($coaId, $companyId, $parent, $code, $name, $description, $level, $fromcode, $normalBalance, $createdAt, $updatedAt) = array_pad(explode(',', $coa), count($coas), null);
                        } else {
                            list($coaId, $companyId, $parent, $code, $name, $description, $level, $fromcode, $normalBalance, $createdAt, $updatedAt) = array_pad(explode(',', $coa), count($coas), null);
                        }
                        if ($parent) {
                            $company->coas()->create([
                                'parent' => trim($parent),
                                'code' => trim($code),
                                'name' => trim($name),
                                'description' => str_replace('NULL', null, trim($description)),
                                'level' => trim($level),
                                'fromcode' => trim($fromcode),
                                'normal_balance' => trim($normalBalance),
                            ]);
                            if (strlen(trim($code)) > 2) {
                                $company->generalLedgers()->create([
                                    'coa_code' => trim($code),
                                    'coa_name' => trim($name),
                                ]);
                            }
                        }
                    }
                }
            }
            \DB::commit();
            return $company;
        } catch (\Exception $e) {
            \DB::rollback();
            return $e->getMessage();
        }
    }
}