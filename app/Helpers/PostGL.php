<?php
 
Use App\Models\Accounting\GeneralLedgerDetail;
Use App\Models\Accounting\GeneralLedger;
Use App\Models\Accounting\Coa;
Use App\Models\Master\Company;

if (!function_exists('postgljournal'))
{
    function postgljournal($company_id, $coa_code, $ref, $description, $debit, $credit)
    {  
        $coa = Coa::where('code','=',$coa_code)->first();
        $normal_balance = $coa->normal_balance;

        $gl = GeneralLedger::where('coa_code','=',$coa_code)->first();
        $gl_id = $gl->gl_id;
        $gldebit = $gl->debit;
        $glcredit = $gl->credit;
        $Balance = $gl->balance;

        $totalDebit = $gldebit+$debit;
        $totalCredit = $glcredit+$credit;

        if($normal_balance == "D"){
            $totalBalance = $totalDebit - $totalCredit;
        }else{
            $totalBalance = $totalCredit - $totalDebit; 
        } 

        $data = array(
        'debit' => $totalDebit,
        'credit' => $totalCredit,
        'balance' => $totalBalance
        );
        try {
            GeneralLedger::where('gl_id',$gl_id)->update($data);

            $gldetail = new GeneralLedgerDetail;
            $gldetail->gl_id=$gl_id;
            $gldetail->reference=$ref;
            $gldetail->description=$description;
            $gldetail->debit=$debit;
            $gldetail->credit=$credit;
            $gldetail->save(); 

            return "Success";
        } catch (\Throwable $th) {
            return "Error : $th";
        }
        
    }

    function postglclosing($company_id, $coa_code, $ref, $description, $debit, $credit,$date)
    {  
        $coa = Coa::where('code','=',$coa_code)->first();
        $normal_balance = $coa->normal_balance;

        $gl = GeneralLedger::where('coa_code','=',$coa_code)->first();
        $gl_id = $gl->gl_id;
        $gldebit = $gl->debit;
        $glcredit = $gl->credit;
        $Balance = $gl->balance;

        $totalDebit = $gldebit+$debit;
        $totalCredit = $glcredit+$credit;

        if($normal_balance == "D"){
            $totalBalance = $totalDebit - $totalCredit;
        }else{
            $totalBalance = $totalCredit - $totalDebit; 
        } 

        $data = array(
        'debit' => $totalDebit,
        'credit' => $totalCredit,
        'balance' => $totalBalance
        );
        try {
            GeneralLedger::where('gl_id',$gl_id)->update($data);

            $gldetail = new GeneralLedgerDetail;
            $gldetail->gl_id=$gl_id;
            $gldetail->reference=$ref;
            $gldetail->description=$description;
            $gldetail->debit=$debit;
            $gldetail->credit=$credit;
            $gldetail->created_at=$date;
            $gldetail->updated_at=$date;
            $gldetail->save(); 

            return "Success";
        } catch (\Throwable $th) {
            return "Error : $th";
        }
        
    }
}

if (!function_exists('postgl'))
{
    //pembayaran piutang, cash in , invoice in
    function postgl($company_id, $coa_code, $ref, $description, $amount){ 

        $coa = Coa::where('code','=',$coa_code)->first();
        $normal_balance = $coa->normal_balance;

        $gl = GeneralLedger::where('coa_code','=', $coa_code)->first();
        $gl_id = $gl->gl_id;
        $gldebit = $gl->debit;
        $glcredit = $gl->credit;
        $startBalance = $gl->start_balance;
        $balance = $gl->balance;   

        if($normal_balance == "D"){
            $totalDebit = $gldebit + $amount;
            $totalCredit = $glcredit + 0;

            $debit = $amount;
            $credit = 0;
            $totalBalance = $startBalance + $totalDebit - $totalCredit;

        }else{
            $totalDebit = $gldebit + 0;
            $totalCredit = $glcredit + $amount;

            $debit = 0;
            $credit = $amount;
            $totalBalance = $startBalance + $totalCredit - $totalDebit;  
        } 

        $data = array(
            'debit' => $totalDebit,
            'credit' => $totalCredit,
            'balance' => $totalBalance
        );
        
        try {
            $cek = GeneralLedger::where('gl_id',$gl_id)->update($data);
            
            $gldetail = new GeneralLedgerDetail;
            $gldetail->company_id=$company_id;
            $gldetail->gl_id=$gl_id;
            $gldetail->reference=$ref;
            $gldetail->description=$description;
            $gldetail->debit=$debit;
            $gldetail->credit=$credit;
            $gldetail->save(); 


            $array = array(
                'status'=>'success',
                'message'=>'Post Success',
                'data'=>"Posting $ref Success"
            );
            return $array;
        } catch (\Throwable $th) {
            $array = array(
                'status'=>'failed',
                'message'=>'Post Failed',
                'data'=>"Error:  $th"
            );
            return $array;
        }
        
    }
}

if (!function_exists('postpenjualan'))
{
    //cash_or_Credit
    function postpenjualan($company_id, $tipe, $ref, $amountPlus, $tax, $discount, $amount)
    { 
        $dtCompany = Company::where("company_id", '=', $company_id)->first();
        $coaPenjualan = $dtCompany->coa_penjualan;
        $coaPiutang = $dtCompany->coa_piutang;
        $coaPajak = $dtCompany->coa_pajak;
        $coaDiscount = $dtCompany->coa_discount;
        $desc = "Penjualan ".$ref;
        //post penjualan
        $cekjual = postgl($company_id, $coaPenjualan, $ref, $desc, $amountPlus);
        if($cekjual['status'] =="failed") {
            $array = array(
                "msg"=>"failed post jual",
                'data'=>$cekjual
            );
            return $array;
        }
        //post pajak
        $cekpajak = postgl($company_id, $coaPajak, $ref, $desc, $tax);
        if($cekpajak['status'] =="failed") {
            $array = array(
                "msg"=>"failed post pajak",
                'data'=>$cekpajak
            );
            return $array;
        }
        if($tipe=="credit"){
            //post piutang
            $cekcredit = postgl($company_id, $coaPiutang, $ref, $desc, $amount);
            if($cekcredit['status'] =="failed") {
                $array = array(
                    "msg"=>"failed post credit",
                    'data'=>$cekcredit
                );
                return $array;
            }
        } 
        //post discount
        if($discount != 0){
            $cekdiscount = postgl($company_id, $coaDiscount, $ref, $desc, $discount);
            if($cekdiscount['status'] =="failed") {
                    $array = array(
                        "msg"=>"failed post discount",
                        'data'=>$cekdiscount
                    );
                    return $array;
                }
        }

        return "success";
        
    } 
}

if (!function_exists('postpembelian'))
{
    //cash_or_Credit
    function postpembelian($company_id, $tipe, $ref, $amountPlus, $tax, $discount, $amount)
    { 
        $dtCompany = Company::where("company_id", '=', $company_id)->first();
        $coaPembelian = $dtCompany->coa_pembelian;
        $coaHutang = $dtCompany->coa_hutang;
        $coaPajak = $dtCompany->coa_pajak_out;
        $coaDiscount = $dtCompany->coa_discount_buy;
        $desc = "Pembelian ".$ref;
        //post Pembelian
        postgl($company_id, $coaPembelian, $ref, $desc, $amountPlus);
        //post pajak
        postgl($company_id, $coaPajak, $ref, $desc, $tax); 
        if($tipe=="credit"){
            //post hutang
            postgl($company_id, $coaHutang, $ref, $desc, $amount);
        } 
        //post discount
        postgl($company_id, $coaDiscount, $ref, $desc, $discount); 
    }
}
    //cash_or_Credit
    function revisipostpenjualan($company_id, $tipe, $ref, $amountPlus, $tax, $discount, $amount)
    {   
        $dtCompany = Company::where("company_id", '=', $company_id)->first();
        $coaPenjualan = $dtCompany->coa_penjualan;
        $coaPiutang = $dtCompany->coa_piutang;
        $coaPajak = $dtCompany->coa_pajak;
        $coaDiscount = $dtCompany->coa_discount;
        $desc = "Revisi Penjualan ".$ref; 

        //post penjualan
        $cekeditpostjual = editpostgl($company_id, $coaPenjualan, $ref, $desc, $amountPlus);
        if($cekeditpostjual['status'] =="failed") {
            $array = array(
                "msg"=>"failed Edit Post jual",
                'data'=>$cekeditpostjual
            );
            return $array;
        }
        //post pajak
        $cekeditpostpajak = editpostgl($company_id, $coaPajak, $ref, $desc, $tax);
        if($cekeditpostpajak['status'] =="failed") {
            $array = array(
                "msg"=>"failed Edit Post Pajak",
                'data'=>$cekeditpostpajak
            );
            return $array;
        }
        if($tipe=="Credit"){
            //post piutang
            $cekeditpostpiutang = editpostgl($company_id, $coaPiutang, $ref, $desc, $amount);
            if($cekeditpostpiutang['status'] =="failed") {
                $array = array(
                    "msg"=>"failed edit post piutang",
                    'data'=>$cekeditpostpiutang
                );
                return $array;
            }
        } 
        if($discount != 0){
            //post discount
            $cekeditpostdiscount = editpostgl($company_id, $coaDiscount, $ref, $desc, $discount);
            if($cekeditpostdiscount['status'] =="failed") {
                $array = array(
                    "msg"=>"failed edit post discount",
                    'data'=>$cekeditpostpiutang
                );
                return $array;
            }
        }
        
        return "success";
    } 

    function revisipostpembelian($company_id, $tipe, $ref, $amountPlus, $tax, $discount, $amount)
    {
        $dtCompany = Company::where("company_id", '=', $company_id)->first();
        $coaPembelian = $dtCompany->coa_pembelian;
        $coaHutang = $dtCompany->coa_hutang;
        $coaPajak = $dtCompany->coa_pajak_out;
        $coaDiscount = $dtCompany->coa_discount_buy;
        $desc = "Revisi Pembelian ".$ref;
        //post Pembelian
        editpostgl($company_id, $coaPembelian, $ref, $desc, $amountPlus);
        
        //post pajak
        editpostgl($company_id, $coaPajak, $ref, $desc, $tax); 
        if($tipe=="credit"){
            //post hutang
            editpostgl($company_id, $coaHutang, $ref, $desc, $amount);        
        } 
        //post discount
        editpostgl($company_id, $coaDiscount, $ref, $desc, $discount);         
    }

    function voidpostpenjualan($company_id, $tipe, $ref, $amountPlus, $tax, $discount, $amount)
    {   
        $dtCompany = Company::where("company_id", '=', $company_id)->first();
        $coaPenjualan = $dtCompany->coa_penjualan;
        $coaPiutang = $dtCompany->coa_piutang;
        $coaPajak = $dtCompany->coa_pajak;
        $coaDiscount = $dtCompany->coa_discount;
        $desc = "Void Penjualan ".$ref; 

        //post penjualan
        $cekdeljual = delpostgl($company_id, $coaPenjualan, $ref);
        if($cekdeljual['status'] =="failed") {
                    $array = array(
                        "msg"=>"failed delete post penjualan",
                        'data'=>$cekdeljual
                    );
                    return $array;
                }
        //post pajak
        $cekdelpajak =  delpostgl($company_id, $coaPajak, $ref);
        if($cekdelpajak['status'] =="failed") {
                    $array = array(
                        "msg"=>"failed delete post pajak",
                        'data'=>$cekdelpajak
                    );
                    return $array;
                }
        if($tipe=="Credit"){
            //post piutang
            $cekdelpiutang =delpostgl($company_id, $coaPiutang, $ref);
            if($cekdelpiutang['status'] =="failed") {
                    $array = array(
                        "msg"=>"failed delete post piutang",
                        'data'=>$cekdelpiutang
                    );
                    return $array;
                }
        } 
        
        if($discount != 0){
            //post discount
            $cekdeldiscount =delpostgl($company_id, $coaDiscount, $ref);
            if($cekdeldiscount['status'] =="failed") {
                        $array = array(
                            "msg"=>"failed delete post discount",
                            'data'=>$cekdeldiscount
                        );
                        return $array;
                    }
        }
        
        return "success";
    } 

    function voidpostpembelian($company_id, $tipe, $ref, $amountPlus, $tax, $discount, $amount)
    { 
        $dtCompany = Company::where("company_id", '=', $company_id)->first();
        $coaPembelian = $dtCompany->coa_pembelian;
        $coaHutang = $dtCompany->coa_hutang;
        $coaPajak = $dtCompany->coa_pajak_out;
        $coaDiscount = $dtCompany->coa_discount_buy;
        $desc = "Void Pembelian ".$ref;
        //post Pembelian
        delpostgl($company_id, $coaPembelian, $ref);
        //post pajak
        delpostgl($company_id, $coaPajak, $ref); 
        if($tipe=="Credit"){
            //post hutang
            delpostgl($company_id, $coaHutang, $ref);
        } 
        //post discount
        delpostgl($company_id, $coaDiscount, $ref); 
    }



if (!function_exists('postpaid'))
{
    //pembayaran hutang, gaji tukang, cash out
    function postpaid($company_id, $coa_code, $ref, $description, $amount)
    { 
        $coa = Coa::where('code','=',$coa_code)->first();
        $normal_balance = $coa->normal_balance;

        $gl = GeneralLedger::where('coa_code','=',$coa_code)->first();
        $gl_id = $gl->gl_id;
        $gldebit = $gl->debit;
        $glcredit = $gl->credit;
        $startBalance = $gl->start_balance;
        $balance = $gl->balance;  
        $desc ="$description $ref";

        if($balance < $amount){
            $array = array(
                'status'=>'failed',
                'message'=>'Post Failed',
                'errors'=>"Balance Didn't Enough"
            );
            return $array;
        }

        if($normal_balance == "D"){
            $totalDebit = $gldebit+0;
            $totalCredit = $glcredit+$amount;

            $debit = 0;
            $credit = $amount;
            $totalBalance = $startBalance + $totalDebit - $totalCredit;

        }else{
            $totalDebit = $gldebit+$amount;
            $totalCredit = $glcredit+0;

            $debit = $amount;
            $credit = 0;
            $totalBalance = $startBalance + $totalCredit - $totalDebit;  
        } 

        $data = array(
        'debit' => $totalDebit,
        'credit' => $totalCredit,
        'balance' => $totalBalance
        );
        try {
            GeneralLedger::where('gl_id',$gl_id)->update($data);

            $gldetail = new GeneralLedgerDetail;
            $gldetail->company_id=$company_id;
            $gldetail->gl_id=$gl_id;
            $gldetail->reference=$ref;
            $gldetail->description=$desc;
            $gldetail->debit=$debit;
            $gldetail->credit=$credit;
            $gldetail->save(); 

            $array = array(
                'status'=>'success',
                'message'=>'Post Success',
                'data'=>"Payment $ref Success"
            );
            return $array;
        } catch (\Throwable $th) {
            $array = array(
                'status'=>'failed',
                'message'=>'Post Failed',
                'data'=>"Error:  $th"
            );
            return $array;
        }
       
    }
}

    function editpostgl($company_id, $coa_code, $ref, $desc, $amount) {
        $dataCoa = Coa::where('code', $coa_code)->first();
        $normal_balance = $dataCoa->normal_balance;

        $dataGL = GeneralLedger::where('coa_code', $coa_code)->first();
        $glid = $dataGL->gl_id;
        $glDebit = $dataGL->debit;
        $glCredit = $dataGL->credit;
        $glStartBalance = $dataGL->start_balance;
        $glBalance = $dataGL->balance;

        $dataGlDetail = GeneralLedgerDetail::where('company_id', $company_id)->where('gl_id', $glid)->where('reference',$ref)->first();
        $glDetailDebit = $dataGlDetail->debit;
        $glDetailCredit = $dataGlDetail->credit; 
        
        //kurangi nilai yang lama
        $totalDebit = $glDebit - $glDetailDebit;
        $totalCredit = $glCredit - $glDetailCredit; 

        if($normal_balance == "D"){
            //tambah dengan nilai yg baru
            $totalDebit = $totalDebit + $amount;
            $totalCredit = $totalCredit ;

            $debit = $amount;
            $credit = 0;

            $totalBalance = $glStartBalance + $totalDebit - $totalCredit; 
        }else{
            //tambah dengan nilai yg baru
            $totalDebit = $totalDebit;
            $totalCredit = $totalCredit + $amount;

             $debit = 0;
            $credit = $amount;

           $totalBalance = $glStartBalance + $totalCredit - $totalDebit; 
        } 

        $data = array(
            'debit'=>$totalDebit,
            'credit'=>$totalCredit,
            'balance'=>$totalBalance,
        ); 
 
        $data2 = array(
            'description'=>$desc,
            'debit'=>$debit,
            'credit'=>$credit,
        );

        try {
            GeneralLedger::where('company_id',$company_id)->where('coa_code',$coa_code)->update($data);
            GeneralLedgerDetail::where('company_id',$company_id)->where('gl_id',$glid)->where('reference',$ref)->update($data2);
            $array = array(
                'status'=>'success',
                'message'=>'Post Edit Success',
                'data'=>"Edit Posting $ref Success"
            );
            return $array;
        } catch (\Throwable $th) {
            $array = array(
                'status'=>'failed',
                'message'=>'Edit Post Failed',
                'data'=>"Error:  $th"
            );
            return $array;
        }

    }

    function delpostgl($company_id,$coa_code, $ref){
        
        $dataCoa = Coa::where('code',$coa_code)->first();
        $normal_balance = $dataCoa->normal_balance; 

        $dataGL = GeneralLedger::where('coa_code',$coa_code)->first();
        $glid = $dataGL->gl_id;
        $glDebit = $dataGL->debit;
        $glCredit = $dataGL->credit;
        $glStartBalance = $dataGL->start_balance;
        $glBalance = $dataGL->balance;

        $dataGlDetail = GeneralLedgerDetail::where('company_id',$company_id)->where('gl_id',$glid)->where('reference',$ref)->first();
        $glDetailDebit = $dataGlDetail->debit;
        $glDetailCredit = $dataGlDetail->credit;

        $totalDebit = $glDebit - $glDetailDebit;
        $totalCredit = $glCredit - $glDetailCredit;
        if($normal_balance =="D"){
           $totalBalance = $glStartBalance + $totalDebit - $totalCredit; 
        }else{
           $totalBalance = $glStartBalance + $totalCredit - $totalDebit; 
        } 

        $data = array(
            'debit'=>$totalDebit,
            'credit'=>$totalCredit,
            'balance'=>$totalBalance,
        );

        try {
            GeneralLedger::where('company_id',$company_id)->where('coa_code',$coa_code)->update($data);
            GeneralLedgerDetail::where('company_id',$company_id)->where('gl_id',$glid)->where('reference',$ref)->delete();
            $array = array(
                'status'=>'success',
                'message'=>'Delete Post Success',
                'data'=>"Post $ref Delete Success"
            );
            return $array;
        } catch (\Throwable $th) {
            $array = array(
                'status'=>'failed',
                'message'=>'Delete Post Failed',
                'data'=>"Error:  $th"
            );
            return $array;
        }


    }

    function delpostgl2($company_id,$coa_code, $ref,$debit,$credit){
        
        $dataCoa = Coa::where('code',$coa_code)->first();
        $normal_balance = $dataCoa->normal_balance; 

        $dataGL = GeneralLedger::where('coa_code',$coa_code)->first();
        $glid = $dataGL->gl_id;
        $glDebit = $dataGL->debit;
        $glCredit = $dataGL->credit;
        $glStartBalance = $dataGL->start_balance;
        $glBalance = $dataGL->balance;


        $dataGlDetail = GeneralLedgerDetail::where('company_id',$company_id)->where('gl_id',$glid)->where('reference',$ref)->where('debit','=',$debit)->where('credit','=',$credit)->first();
        
        $glDetailDebit = $dataGlDetail->debit;
        $glDetailCredit = $dataGlDetail->credit;

        $totalDebit = $glDebit - $glDetailDebit;
        $totalCredit = $glCredit - $glDetailCredit;
        if($normal_balance =="D"){
           $totalBalance = $glStartBalance + $totalDebit - $totalCredit; 
        }else{
           $totalBalance = $glStartBalance + $totalCredit - $totalDebit; 
        } 

        $data = array(
            'debit'=>$totalDebit,
            'credit'=>$totalCredit,
            'balance'=>$totalBalance,
        );

        try {
            GeneralLedger::where('company_id',$company_id)->where('coa_code',$coa_code)->update($data);
            GeneralLedgerDetail::where('company_id',$company_id)->where('gl_id',$glid)->where('reference',$ref)->where('debit','=',$debit)->where('credit','=',$credit)->delete();
            return "success";
        } catch (\Throwable $th) {
            return $th;
        }


    }

if (!function_exists('glByDesc'))
{
    function glByDesc(int $company_id, string $ref)
    {
        return GeneralLedgerDetail::where('company_id', $company_id)->where('description', 'like', '%'.$ref.'%')->get();
    }
}
