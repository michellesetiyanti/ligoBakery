<?php

if (!function_exists('uniqueCode'))
{
    /**
     * uniqueCode
     *
     * @param  string   $string
     * @param  string   $separator
     * @param  int      $pad_len
     * @param  string   $format (date format: https://www.php.net/manual/en/function.date.php)
     * @return string
     */
    function uniqueCode($string, $separator = '/', $pad_len = 5, $format = 'Ym')
    {
        try {
            $output = '';
            if ($separator) {
                $explode = explode($separator, $string);
                $unique = end($explode);
                if (is_numeric($unique)) {
                    if (in_array(date($format), $explode)) {
                        for ($i=0; $i < count($explode) - 1; $i++) { 
                            $output.= $explode[$i].$separator;
                        }
                        $output.= str_pad($unique + 1, $pad_len, '0', STR_PAD_LEFT);
                    } else {
                        $output.= reset($explode).$separator.date($format).$separator.str_pad(1, $pad_len, '0', STR_PAD_LEFT);
                    }
                } else {
                    $output.= $string.$separator.date($format).$separator.str_pad(1, $pad_len, '0', STR_PAD_LEFT);
                }
            } else {
                preg_match('/\d+/', $string, $matches);
                if (!empty($matches[0])) {
                    $unique = str_pad($matches[0] + 1, $pad_len, '0', STR_PAD_LEFT);
                    $output = preg_replace('/\d+/', $unique, $string);
                } else {
                    $output = $string.str_pad(1, $pad_len, '0', STR_PAD_LEFT);
                }
            }
            return $output;
        } catch (\Exception $e) {
            throw $e->getMessage();
        }
    }
}

if (!function_exists('calcAmount'))
{
    /**
     * Calculation for total amount including taxes and discounts.
     *
     * @param  int $price
     * @param  int $tax (%)
     * @return int
     */
    function calcAmount($price, $tax = 0)
    {
        try {
            $taxValue = $price * ($tax / 100);
            $amount = $price + $taxValue;
            return $amount;
        } catch (\Exception $e) {
            throw $e->getMessage();
        }
    }
}

if (!function_exists('authExecute'))
{
    /**
     * authExecute
     *
     * @return array
     */
    function authExecute($relations = 'karyawan')
    {
        $onlyRendered = ['user_id', 'username', 'type', 'email'];
        $args = func_get_args();
        if (!in_array(false, $args)) {
            if (func_num_args() <= 0) {
                $onlyRendered = array_merge($onlyRendered, (array) $relations);
            } else {
                $relations = $args;
                $onlyRendered = array_merge($onlyRendered, $relations);
            }
        }
        $data = \Auth::user()->with($relations)->first()->toArray();
        $data = \Arr::only($data, $onlyRendered);
        $data = \Arr::add($data, 'entry_at', \Carbon\Carbon::now()->format('d/m/Y'));
        return $data;
    }
}

if (!function_exists('prosesHutang'))
{
    function prosesHutang($value, $coa = null)
    {
        $model = new \App\Models\Pembelian\Hutang;
        $fillable = collect($model->getFillable());
        $data = $fillable->map(function ($item) use ($value) {
            $args[$item] = null;
            if (in_array($item, array_keys($value))) {
                $args[$item] = $value[$item];
            }
            return $args;
        })->collapse();
        $hutang = $model->where('company_id', $data->get('company_id'))->where('pembelian_id', $data->get('pembelian_id'))->first();
        if (empty($hutang)) {
            $hutang = $model->create($data->only($fillable)->toArray());
        } else {
            $hutang->update($data->only($fillable)->toArray());
        }
        $hutangDetail = $hutang->hutangDetails->first();
        if (empty($hutangDetail)) {
            $hutang->hutangDetails()->create([
                'coa_code' => $coa ?? request('coa_code'),
                'total' => $hutang->total,
                'paid_off' => $hutang->paid_off,
                'balance' => $hutang->balance,
            ]);
        } else {
            $hutangDetail->update([
                'coa_code' => $coa ?? $hutangDetail->coa_code,
                'total' => $hutang->total,
                'paid_off' => $hutang->paid_off,
                'balance' => $hutang->balance,
            ]);
        }
        return $hutang;
    }
}

if (!function_exists('prosesPiutang'))
{
    function prosesPiutang($value, $coa = null)
    {
        $model = new \App\Models\Penjualan\Piutang;
        $fillable = collect($model->getFillable());
        $data = $fillable->map(function ($item) use ($value) {
            $args[$item] = null;
            if (in_array($item, array_keys($value))) {
                $args[$item] = $value[$item];
            }
            return $args;
        })->collapse();
        $piutang = $model->where('company_id', $data->get('company_id'))->where('penjualan_id', $data->get('penjualan_id'))->first();
        if (empty($piutang)) {
            $piutang = $model->create($data->only($fillable)->toArray());
        } else {
            $piutang->update($data->only($fillable)->toArray());
        }
        $piutangDetail = $piutang->piutangDetails->first();
        if (empty($piutangDetail)) {
            $piutang->piutangDetails()->create([
                'coa_code' => $coa ?? request('coa_code'),
                'total' => $piutang->total,
                'paid_off' => $piutang->paid_off,
                'balance' => $piutang->balance,
            ]);
        } else {
            $piutangDetail->update([
                'coa_code' => $coa ?? $piutangDetail->coa_code,
                'total' => $piutang->total,
                'paid_off' => $piutang->paid_off,
                'balance' => $piutang->balance,
            ]);
        }
        return $piutang;
    }
}

if (!function_exists('sm2lg'))
{
    function sm2lg(int $product_id, int $qty_sm)
    {
        $model = new \App\Models\Master\Product;
        $result = $model->find($product_id);
        $temp = $qty_sm / $result->unit_qty;
        $qty_lg = $temp;
        $qty_sm = 0;
        if (is_float($temp)) {
            $qty_lg = floor($temp);
            $qty_sm = ($temp - $qty_lg) * $result->unit_qty;
        }
        return compact('qty_lg', 'qty_sm');
    }
}

if (!function_exists('fifoStock'))
{
    function fifoStock(string $ref, int $product_id, array $qty, string $status = 'penjualan')
    {
        $order = ['lg' => 0, 'sm' => 0];
        $order = array_replace($order, $qty);
        $qty_lg = $order['lg'];
        $qty_sm = $order['sm'];
        $model = new \App\Models\Master\ProductDetail;
        $result = $model->where('product_id', $product_id)->where(function ($query) {
            $query->where('cur_qty_lg', '>', 0)->orWhere('cur_qty_sm', '>', 0);
        })->orderBy('product_d_id', 'ASC')->get();
        $cur_stock_lg = $result->sum('cur_qty_lg');
        $cur_stock_sm = $result->sum('cur_qty_sm');
        $product = $result->map->product->first();
        if ($product->pack && $product->pack_qty) {
            if ($qty_sm > $cur_stock_sm) {
                $lg2sm = $result->where('cur_qty_lg', '>', 0)->first();                
                $lg2sm->update([
                    'cur_qty_lg' => $lg2sm->cur_qty_lg - 1,
                    'cur_qty_sm' => (1 * $product->unit_qty) + $lg2sm->cur_qty_sm
                ]);
            }
        }
        $data = collect();
        if ($qty_lg <= $cur_stock_lg || $qty_sm <= $cur_stock_sm) {
            for ($i = 0; $i < $result->count(); $i++) {
                $stock['status'] = $status;
                $stock['ref'] = $ref;
                $stock['batch'] = $result[$i]->batch;
                $stock['product_id'] = $result[$i]->product_id;
                $stock['unit_lg'] = $result[$i]->product->unit_lg;
                $stock['unit_sm'] = $result[$i]->product->unit_sm;
                $stock['price_lg'] = $result[$i]->price_lg;
                $stock['price_sm'] = $result[$i]->price_sm;
                if ($qty_lg > 0) {
                    $temp_lg = $qty_lg;
                    $qty_lg = $qty_lg - $result[$i]->cur_qty_lg;
                    if ($qty_lg > 0) {
                        $cur_qty_lg = 0;
                        $new_qty_lg = $result[$i]->cur_qty_lg;
                    } else {
                        $cur_qty_lg = $result[$i]->cur_qty_lg - $temp_lg;
                        $new_qty_lg = $result[$i]->cur_qty_lg - $cur_qty_lg;
                    }
                    if ($new_qty_lg > 0) {
                        $create_lg = $model->create(array_merge($stock, ['qty_lg' => $new_qty_lg]));
                        $result[$i]->update(['cur_qty_lg' => $cur_qty_lg]);
                        $data->push($create_lg);
                    }
                }
                if ($qty_sm > 0) {
                    $temp_sm = $qty_sm;
                    $qty_sm = $qty_sm - $result[$i]->cur_qty_sm;
                    if ($qty_sm > 0) {
                        $cur_qty_sm = 0;
                        $new_qty_sm = $result[$i]->cur_qty_sm;
                    } else {
                        $cur_qty_sm = $result[$i]->cur_qty_sm - $temp_sm;
                        $new_qty_sm = $result[$i]->cur_qty_sm - $cur_qty_sm;
                    }
                    if ($new_qty_sm > 0) {
                        $create_sm = $model->create(array_merge($stock, ['qty_sm' => $new_qty_sm]));
                        $result[$i]->update(['cur_qty_sm' => $cur_qty_sm]);
                        $data->push($create_sm);
                    }
                }
            }
            app(\App\Http\Controllers\Api\Master\ProductController::class)->updateStock($product_id);
        }
        return $data;
    }
}
