<?php

namespace App\Repositories\Accounting;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class JournalDetail extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Accounting\JournalDetail::class;
    }
}
