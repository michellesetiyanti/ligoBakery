<?php

namespace App\Repositories\Accounting;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class GeneralLedger extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Accounting\GeneralLedger::class;
    }
}
