<?php

namespace App\Repositories\Accounting;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class GeneralLedgerDetail extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Accounting\GeneralLedgerDetail::class;
    }
}
