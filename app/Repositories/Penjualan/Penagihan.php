<?php

namespace App\Repositories\Penjualan;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class Penagihan extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Penjualan\Penagihan::class;
    }
}
