<?php

namespace App\Repositories\Finance;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class Invoice extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Finance\Invoice::class;
    }
}
