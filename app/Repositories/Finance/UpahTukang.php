<?php

namespace App\Repositories\Finance;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class UpahTukang extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Finance\UpahTukang::class;
    }
}
