<?php

namespace App\Repositories\Finance;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class BoronganKerja extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Finance\BoronganKerja::class;
    }
}
