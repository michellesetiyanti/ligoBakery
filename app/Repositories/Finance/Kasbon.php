<?php

namespace App\Repositories\Finance;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class Kasbon extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Finance\Kasbon::class;
    }
}
