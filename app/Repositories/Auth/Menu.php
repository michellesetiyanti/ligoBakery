<?php

namespace App\Repositories\Auth;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class Menu extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Auth\Menu::class;
    }
}
