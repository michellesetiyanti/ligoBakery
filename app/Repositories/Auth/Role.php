<?php

namespace App\Repositories\Auth;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class Role extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Auth\Role::class;
    }
}
