<?php

namespace App\Repositories\Master;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class Workshop extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Master\Workshop::class;
    }
}
