<?php

namespace App\Repositories\Master;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Master\ProductCategory::class;
    }
}
