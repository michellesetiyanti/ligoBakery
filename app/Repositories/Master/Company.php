<?php

namespace App\Repositories\Master;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Company extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Master\Company::class;
    }
}
