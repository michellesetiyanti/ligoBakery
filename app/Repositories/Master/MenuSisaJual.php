<?php

namespace App\Repositories\Master;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class MenuSisaJual extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Master\MenuSisaJual::class;
    }
}
