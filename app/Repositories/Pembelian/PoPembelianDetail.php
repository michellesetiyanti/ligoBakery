<?php

namespace App\Repositories\Pembelian;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class PoPembelianDetail extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Pembelian\PoPembelianDetail::class;
    }
}
