<?php

namespace App\Repositories\Pembelian;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class Hutang extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Pembelian\Hutang::class;
    }
}
