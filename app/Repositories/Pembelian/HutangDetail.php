<?php

namespace App\Repositories\Pembelian;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class HutangDetail extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Pembelian\HutangDetail::class;
    }
}
