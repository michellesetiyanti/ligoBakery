<?php

namespace App\Repositories\Pembelian;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class PoPembelian extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Pembelian\PoPembelian::class;
    }
}
