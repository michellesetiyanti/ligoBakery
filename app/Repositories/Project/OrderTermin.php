<?php

namespace App\Repositories\Project;

use App\Abstracts\BaseRepository;
use Illuminate\Database\Eloquent\Model;

class OrderTermin extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return \App\Models\Project\OrderTermin::class;
    }
}
