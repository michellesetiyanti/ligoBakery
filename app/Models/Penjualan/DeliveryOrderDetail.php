<?php

namespace App\Models\Penjualan;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrderDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'do_id',
        'product_id',
        'unit_lg',
        'unit_sm',
        'delv_qty_lg',
        'delv_qty_sm',
        'recv_qty_lg',
        'recv_qty_sm',
        'note',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'do_d_id';
    
    protected $appends = [
        'delv_volume',
        'recv_volume'
    ];

    public static function boot()
    {
        parent::boot();
    }
    
    public function deliveryOrder()
    {
        return $this->belongsTo(DeliveryOrder::class, 'do_id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Models\Master\Product::class, 'product_id');
    }
    
    public function getDelvVolumeAttribute()
    {
        if ($this->product->pack && $this->product->pack_qty) {
            $qty_sm = $this->delv_qty_lg * $this->product->unit_qty;
            $qty_sm = ($qty_sm + $this->delv_qty_sm) * $this->product->pack_qty;
            return collect([
                'order' => [
                    'unit' => $this->product->pack,
                    'qty' => $qty_sm,
                ],
                'detail' => [
                    'unit_lg' => $this->unit_lg,
                    'unit_sm' => $this->unit_sm,
                    'qty_lg' => $this->delv_qty_lg,
                    'qty_sm' => $this->delv_qty_sm,
                ]
            ]);
        }
        return false;
    }
    
    public function getRecvVolumeAttribute()
    {
        if ($this->product->pack && $this->product->pack_qty) {
            $qty_sm = $this->recv_qty_lg * $this->product->unit_qty;
            $qty_sm = ($qty_sm + $this->recv_qty_sm) * $this->product->pack_qty;
            return collect([
                'order' => [
                    'unit' => $this->product->pack,
                    'qty' => $qty_sm,
                ],
                'detail' => [
                    'unit_lg' => $this->unit_lg,
                    'unit_sm' => $this->unit_sm,
                    'qty_lg' => $this->recv_qty_lg,
                    'qty_sm' => $this->recv_qty_sm,
                ]
            ]);
        }
        return false;
    }
}
