<?php

namespace App\Models\Penjualan;

use Illuminate\Database\Eloquent\Model;

class Piutang extends Model
{
    /**
     * Database table name
     */
    protected $table = 'piutang';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'penjualan_id',
        'total',
        'paid_off',
        'balance',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'piutang_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function penjualan()
    {
        return $this->belongsTo(Penjualan::class, 'penjualan_id');
    }
    
    public function piutangDetails()
    {
        return $this->hasMany(PiutangDetail::class, 'piutang_id')->with('coa');
    }
}
