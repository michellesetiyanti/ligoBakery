<?php

namespace App\Models\Penjualan;

use Illuminate\Database\Eloquent\Model;

class PiutangDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'piutang_id',
        'coa_code',
        'total',
        'paid_off',
        'balance',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'piutang_d_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function piutang()
    {
        return $this->belongsTo(Piutang::class, 'piutang_id');
    }
    
    public function coa()
    {
        return $this->belongsTo(\App\Models\Accounting\Coa::class, 'coa_code', 'code');
    }
}
