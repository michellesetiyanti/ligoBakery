<?php

namespace App\Models\Penjualan;

use Illuminate\Database\Eloquent\Model;

class PenjualanDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'penjualan_id',
        'product_id',
        'unit_lg',
        'unit_sm',
        'qty_lg',
        'qty_sm',
        'price_lg',
        'price_sm',
        'tax',
        'discount',
        'amount',
        'revisi',
        'rev_qty_lg',
        'rev_qty_sm',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'penjualan_d_id';
    
    protected $appends = [
        'volume',
        'dpp',
        'do_status',
    ];
    
    public function penjualan()
    {
        return $this->belongsTo(Penjualan::class, 'penjualan_id')->with('piutang');
    }
    
    public function product()
    {
        return $this->belongsTo(\App\Models\Master\Product::class, 'product_id');
    }
    public function getVolumeAttribute()
    {
        if ($this->product->pack && $this->product->pack_qty) {
            $qty_sm = $this->qty_lg * $this->product->unit_qty;
            $qty_sm = ($qty_sm + $this->qty_sm) * $this->product->pack_qty;
            return collect([
                'order' => [
                    'unit' => $this->product->pack,
                    'qty' => $qty_sm,
                ],
                'detail' => [
                    'unit_lg' => $this->unit_lg,
                    'unit_sm' => $this->unit_sm,
                    'qty_lg' => $this->qty_lg,
                    'qty_sm' => $this->qty_sm,
                ]
            ]);
        }
        return false;
    }
    
    public function getDppAttribute()
    {
        $dpp = $this->amount + $this->discount;
        $tax = (100 / ($this->tax + 100)) * $dpp;
        $tax = $dpp - $tax;
        return $dpp - $tax;
    }
    
    public function getDoStatusAttribute()
    {
        if (isset($this->penjualan->os_do)) {
            return $this->penjualan->os_do->where('product_id', $this->product_id)->pluck('status')->first();
        }
        return null;
    }
}
