<?php

namespace App\Models\Penjualan;

use Illuminate\Database\Eloquent\Model;

class Denda extends Model
{
    /**
     * Database table name
     */
    protected $table = 'denda';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'code_penjualan',
        'bulan',
        'persentase',
        'nominal',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'denda_id';

    protected $appends = [
        //
    ];

    public static function boot()
    {
        parent::boot();
    }
    
    public function penjualan()
    {
        return $this->belongsTo(Penjualan::class, 'code', 'code_penjualan');
    }
}
