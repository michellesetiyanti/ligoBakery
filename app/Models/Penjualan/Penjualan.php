<?php

namespace App\Models\Penjualan;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    /**
     * Database table name
     */
    protected $table = 'penjualan';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'sales_id',
        'customer_id',
        'wh_id',
        'po_pro_id',
        'code',
        'code_spk',
        'ongkir',
        'ongkir_desc',
        'biaya_lain',
        'biaya_lain_desc',
        'due_date',
        'status',
        'coa_code',
        'faktur',
        'notes',
        'piutang_status',
        'approve_date',
        // Agro
        'ref',
        'ref_date',
        'trans_type',
        'serial',
        'signed',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
        'due_date'  => 'date:d/m/Y',
        // Agro
        'ref_date'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'penjualan_id';

    protected $appends = [
        'over_due',
        'discount',
        'tax',
        'total',
        'dpp',
        'os_do',
        'do_status',
        'due_date_day',
        'pro_order',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->serial = \Str::random(40);
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function karyawan()
    {
        return $this->belongsTo(\App\Models\Master\Karyawan::class, 'sales_id')->with('user');
    }
    
    public function customer()
    {
        return $this->belongsTo(\App\Models\Master\Customer::class, 'customer_id');
    }
    
    public function warehouse()
    {
        return $this->belongsTo(\App\Models\Master\Warehouse::class, 'wh_id');
    }
    
    public function poProject()
    {
        return $this->belongsTo(\App\Models\Pembelian\PoProject::class, 'po_pro_id');
    }
    
    public function order()
    {
        return $this->belongsTo(\App\Models\Project\Order::class, 'code_spk', 'code_spk')->with('company');
    }
    
    public function penjualanDetails()
    {
        return $this->hasMany(PenjualanDetail::class, 'penjualan_id')->with(['penjualan', 'product']);
    }
    
    public function piutang()
    {
        return $this->hasMany(Piutang::class, 'penjualan_id')->with('piutangDetails');
    }
    
    public function deliveryOrders()
    {
        return $this->hasMany(DeliveryOrder::class, 'penjualan_id')->with(['deliveryOrderDetails', 'penjualan']);
    }
    
    public function penagihan()
    {
        return $this->hasMany(Penagihan::class, 'penjualan_id');
    }
    
    public function denda()
    {
        return $this->hasOne(Denda::class, 'code_penjualan', 'code');
    }
    
    public function setDueDateAttribute($value)
    {
        if ($value) {
            $this->attributes['due_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }
    
    public function getDiscountAttribute()
    {
        return $this->penjualanDetails()->sum('discount');
    }
    
    public function getTaxAttribute()
    {
        if ($this->penjualanDetails()->exists() && $this->penjualanDetails()->where('tax', '<>', 0)->sum('tax') > 0) {
            $totalAmounts = $this->penjualanDetails()->where('tax', '<>', 0)->sum('amount') + $this->penjualanDetails()->where('tax', '<>', 0)->sum('discount');
            $dpp = (100 / (($this->penjualanDetails()->where('tax', '<>', 0)->sum('tax') / $this->penjualanDetails()->where('tax', '<>', 0)->count()) + 100)) * $totalAmounts;
            return $totalAmounts - $dpp;
        } else {
            return 0;
        }
    }
    
    public function getTotalAttribute()
    {   
        $detail = $this->penjualanDetails()->get();
        $amount = $detail->where('revisi', '!=', 'delete')->sum('amount');
        return $amount + $this->ongkir + $this->biaya_lain;
    }
    
    public function getDppAttribute()
    {
        $dpp = $this->total + $this->discount;
        $dpp-= $this->ongkir - $this->biaya_lain;
        $dpp-= $this->tax;
        return $dpp;
    }
   
    public function getOverDueAttribute()
    {
        if ($this->piutang->isEmpty()) {
            return null;
        }
        $piutang = $this->piutang->first();
        $today = \Carbon\Carbon::now()->format('Y-m-d');
        $due_date = $this->due_date->format('Y-m-d');
        switch ($this->status) {
            case 'paid':
                $over_due = ($piutang && $piutang->updated_at >= $due_date) ? \Carbon\Carbon::parse($due_date)->diffInDays($piutang->updated_at) : 0;
                $over_due = $over_due.' days';
            break;
            case 'unpaid':
                $over_due = ($this->created_at->format('Y-m-d') != $due_date && $today >= $due_date) ? \Carbon\Carbon::parse($due_date)->diffInDays($today) : 0;
                $over_due = $over_due.' days';
            break;
            default:
                $over_due = null;
            break;
        }
        return $over_due;
    }
    
    public function getOsDoAttribute()
    {
        $penjualanDetails = $this->penjualanDetails()->select('product_id', 'qty_lg', 'qty_sm', 'price_lg', 'price_sm')->get();
        $do = $this->deliveryOrders()->select('do_id', 'code', 'note')->get();
        if ($do->isNotEmpty()) {
            $osDo = collect();
            foreach ($penjualanDetails as $penjualanDetail) {
                $doIds = $do->flatMap->deliveryOrderDetails->where('product_id', $penjualanDetail['product_id'])->flatten();
                $totalDoLg = $doIds->sum('recv_qty_lg');
                $totalDoSm = $doIds->sum('recv_qty_sm');
                $qty_lg = $penjualanDetail['qty_lg'] - $totalDoLg;
                $qty_sm = $penjualanDetail['qty_sm'] - $totalDoSm;
                if ($penjualanDetail['product']['pack'] && $penjualanDetail['product']['pack_qty']) {
                    $totalDoRecv = ($totalDoLg * $penjualanDetail['product']['unit_qty']) + $totalDoSm;
                    $sm2lg = sm2lg($penjualanDetail['product_id'], $totalDoRecv);
                    $qty_lg = $penjualanDetail['qty_lg'] - $sm2lg['qty_lg'];
                    $qty_sm = $penjualanDetail['qty_sm'] - $sm2lg['qty_sm'];
                    $qty_sm_vol = $qty_lg * $penjualanDetail['product']['unit_qty'];
                    $qty_sm_vol = ($qty_sm_vol + $qty_sm) * $penjualanDetail['product']['pack_qty'];
                    $volume = collect([
                        'order' => [
                            'unit' => $penjualanDetail['product']['pack'],
                            'qty' => $qty_sm_vol,
                        ],
                        'detail' => [
                            'unit_lg' => $penjualanDetail['product']['unit_lg'],
                            'unit_sm' => $penjualanDetail['product']['unit_sm'],
                            'qty_lg' => $qty_lg,
                            'qty_sm' => $qty_sm,
                        ]
                    ]);
                }
                $osDo->push([
                    'product_id' => $penjualanDetail['product_id'],
                    'qty_lg' => $qty_lg,
                    'qty_sm' => $qty_sm,
                    'price_lg' => $penjualanDetail['price_lg'],
                    'price_sm' => $penjualanDetail['price_sm'],
                    'status' => ($qty_lg > 0) || ($qty_sm > 0) ? 'uncomplete' : 'complete',
                    'product' => $penjualanDetail['product'],
                    'volume' => $volume ?? false,
                ]);
            }
            return $osDo;
        } else {
            if (in_array($this->faktur, ['approved'])) {
                return data_fill($penjualanDetails, '*.status', 'uncomplete');
            } else {
                return null;
            }
        }
    }
    
    public function getDoStatusAttribute()
    {
        if ($this->os_do) {
            $count = collect($this->os_do)->where('status', 'uncomplete')->count();
            return $count > 0 ? 'uncomplete' : 'complete';
        }
        return false;
    }

    // Agro    
    public function setRefDateAttribute($value)
    {
        $this->attributes['ref_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }
    
    public function getDueDateDayAttribute()
    {
        if ($this->due_date) {
            return $this->created_at->diffInDays($this->due_date->add(1, 'day'));
        }
        return null;
    }
    
    public function getProOrderAttribute()
    {
        $res = $this->code_spk;
        if ($res) {
            return \App\Models\Project\Order::where('code_spk', $res)->with('orderTermins')->first();
        }
        return $res;
    }

    public function setSignedAttribute($value)
    {
        if ($value) {
            settype($value, 'array');
            $signed = json_encode($value);
        } else {
            $signed = $value;
        }
        $this->attributes['signed'] = $signed;
    }
    
    public function getSignedAttribute($value)
    {
        if ($value) {
            return json_decode($value);
        }
        return $value;
    }
}
