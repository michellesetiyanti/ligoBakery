<?php

namespace App\Models\Penjualan;

use Illuminate\Database\Eloquent\Model;

class Penagihan extends Model
{
    /**
     * Database table name
     */
    protected $table = 'penagihan';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'penjualan_id',
        'address',
        'receiver',
        'expedition',
        'due_date',
        'nominal',
        'status',
        'serial'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
        'due_date'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'penagihan_id';

    protected $appends = [
        //
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $unique = uniqueCompany(auth()->user()->company->name);
            $model->serial = $unique.rand(pow(10, 12), pow(10, 13) - 1);
        });
    }
    
    public function penjualan()
    {
        return $this->belongsTo(Penjualan::class, 'penjualan_id');
    }
    
    public function setDueDateAttribute($value)
    {
        $this->attributes['due_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }
}
