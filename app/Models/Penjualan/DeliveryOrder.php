<?php

namespace App\Models\Penjualan;

use Illuminate\Database\Eloquent\Model;

class DeliveryOrder extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'penjualan_id',
        'code',
        'note',
        'trans_type',
        'penempatan',        
        'signed',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'company_id',
        'user_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'do_id';

    protected $appends = [
        //
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
            if (empty($model->code)) {
                $lastCode = static::where('code', 'LIKE', '%DO%')->latest('do_id')->pluck('code')->first();
                $model->code = $lastCode ? uniqueCode($lastCode, '/') : uniqueCode('DO', '/');
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function penjualan()
    {
        return $this->belongsTo(Penjualan::class, 'penjualan_id')->with(['user', 'company']);
    }
    
    public function deliveryOrderDetails()
    {
        return $this->hasMany(DeliveryOrderDetail::class, 'do_id')->with('product');
    }

    public function setSignedAttribute($value)
    {
        if ($value) {
            settype($value, 'array');
            $signed = json_encode($value);
        } else {
            $signed = $value;
        }
        $this->attributes['signed'] = $signed;
    }
    
    public function getSignedAttribute($value)
    {
        if ($value) {
            return json_decode($value);
        }
        return $value;
    }
}
