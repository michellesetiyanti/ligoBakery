<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class Closing extends Model
{
    protected $table = "closing";
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'journal_code', 
        'bulan',
        'tahun', 
        'status',
    ];

     protected $hidden = [
        'company_id',
    ];

    protected $casts = [
        'status'  => 'boolean',
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'closing_id';
    
    public static function boot()
    {
         parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->company_id = $auth->company_id;
            } 
        });
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }

}
