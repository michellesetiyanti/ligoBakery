<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class PersediaanBarang extends Model
{
    protected $table = "persediaan_barang";
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'bulan',
        'tahun', 
        'jumlah',
    ];

     protected $hidden = [
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'persediaan_barang_id';
    
    public static function boot()
    {
         parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->company_id = $auth->company_id;
            } 
        });
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }

}
