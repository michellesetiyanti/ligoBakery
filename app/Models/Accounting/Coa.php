<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class Coa extends Model
{
    /**
     * Database table name
     */
    protected $table = 'coa';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'parent',
        'code',
        'name',
        'description',
        'level',
        'fromcode',
        'normal_balance',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'coa_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->company_id = $auth->company_id;
            }
            if (empty($model->code)) {
                $model->code = uniqueCode("COA_".$model->coa_id);
            }
        });
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function generalLedgers()
    {
        return $this->hasMany(\App\Models\Accounting\GeneralLedger::class, 'coa_code', 'code')->with('generalLedgerDetails');
    }
    
    public function kas()
    {
        return $this->hasMany(\App\Models\Operational\Kas::class, 'coa_code', 'code');
    }
    
    public function hutangDetails()
    {
        return $this->hasMany(\App\Models\Pembelian\HutangDetail::class, 'coa_code', 'code');
    }
    
    public function piutangDetails()
    {
        return $this->hasMany(\App\Models\Penjualan\PiutangDetail::class, 'coa_code', 'code');
    }
}
