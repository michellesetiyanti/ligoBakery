<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class HppTotal extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'date',
        'hpp_awal',
        'hpp_akhir',
        'description',
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->company_id)) {
                $model->company_id = auth()->user()->company->company_id;
            }
        });
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
}
