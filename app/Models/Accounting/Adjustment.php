<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class Adjustment extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'wh_id',
        'user_id',
        'date_entry',
        'reference',
        'description',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        // 'user_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'adjust_id';

    protected $appends = [
        // 'import',
        //'export',
    ];
    
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
            }
        });
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\Auth\User::class, 'user_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(\App\Models\Master\Warehouse::class, 'wh_id');
    }
    
    public function adjustmentDetails()
    {
        return $this->hasMany(AdjustmentDetail::class, 'adjust_id')->with('product');
    }

    // public function getImportAttribute()
    // {
    //     $fetch = [];
    //     $date = \Carbon\Carbon::parse($this->created_at);
    //     $path = 'adjustment/'.$date->isoFormat('YYYY/MM');
    //     $filename = 'ADJS_'.$this->adjust_id;
    //     foreach (glob(public_path('storage').'/'.$path.'/'.$filename.'*') as $file) {
    //         return str_replace(
    //             public_path('storage').'/'.$path,
    //             asset('storage').'/'.$path,
    //             $file
    //         );
    //     }
    // }

    // public function setImportAttribute($value)
    // {
    //     $date = \Carbon\Carbon::parse($this->created_at);
    //     $path = 'adjustment/'.$date->isoFormat('YYYY/MM');
    //     $filename = 'ADJS_'.$this->adjust_id;
    //     if ($value) {
    //         $filename = uniqueCode($filename, '-');
    //         $filename.= '.'.$value->getClientOriginalExtension();
    //         if ($this->import) {
    //             \Storage::disk('public')->delete(str_replace(asset('storage'), '', $this->import));
    //         }
    //         \Storage::disk('public')->put($path.'/'.$filename, file_get_contents($value));
    //         return asset('storage').'/'.$path.'/'.$filename;
    //     }
    //     return false;
    // }
}
