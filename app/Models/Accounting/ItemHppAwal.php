<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class ItemHppAwal extends Model
{
   protected $table = "hpp_item_awal";
   /**
     * Mass assignable columns
     */
    protected $fillable = [
        'product_id',
        'saldo_awal',
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->company_id)) {
                $model->company_id = auth()->user()->company->company_id;
            }
        });
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Models\Master\Product::class, 'product_id');
    }
}

