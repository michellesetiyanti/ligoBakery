<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class PenyusutanAset extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'code_aset',
        'name',
        'description',
        'date_buy',
        'date_use',
        'coa_code',
        'coa_penyusutan',
        'coa_beban',
        'umur_bulan',
        'bulan_tersisa',
        'qty',
        'coa_pengeluaran',
        'keterangan_pengeluaran',
        'amount',
        'input_by',
        'entry_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'company_id',
    ];

    protected $casts = [
        'date_buy'  => 'date:d/m/Y',
        'date_use'  => 'date:d/m/Y',
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'aset_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                // $model->input_by = $auth->user_id;
                $model->company_id = $auth->company->company_id;
            }
        });
    }
    
    public function input_by()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'input_by');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }

    public function coa_code()
    {
        return $this->belongsTo(\App\Models\Accounting\Coa::class, 'coa_code', 'code');
    }

    public function coa_penyusutan()
    {
        return $this->belongsTo(\App\Models\Accounting\Coa::class, 'coa_penyusutan', 'code');
    }

    public function coa_beban()
    {
        return $this->belongsTo(\App\Models\Accounting\Coa::class, 'coa_beban', 'code');
    }

    public function coa_pengeluaran()
    {
        return $this->belongsTo(\App\Models\Accounting\Coa::class, 'coa_pengeluaran', 'code');
    }
    
}
