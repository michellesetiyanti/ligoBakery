<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class GeneralLedger extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'coa_code',
        'coa_name',
        'debit',
        'credit',
        'start_balance',
        'balance',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'gl_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->company_id)) {
                $model->company_id = auth()->user()->company->company_id;
            }
        });
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function coa()
    {
        return $this->belongsTo(\App\Models\Accounting\Coa::class, 'coa_code','code');
    }
    
    public function glDetails()
    {
        return $this->hasMany(GeneralLedgerDetail::class, 'gl_id', 'gl_id');
    }
}