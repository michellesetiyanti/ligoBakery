<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class JournalDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'journal_id',
        'code',
        'description',
        'debit',
        'credit',
        'entry_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'entry_at'  => 'date:d/m/Y',
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'journal_d_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function coa()
    {
        return $this->belongsTo(\App\Models\Accounting\Coa::class, 'coa_code', 'code');
    }
     
}
