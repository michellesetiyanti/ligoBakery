<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class generalLedgerDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'gl_id',
        'reference',
        'description',
        'debit',
        'credit',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'company_id',
    ];

     /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
     protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'gl_d_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->company_id = $auth->company->company_id;
            }
        });
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    } 
    
    public function generalLedger()
    {
        return $this->belongsTo(GeneralLedger::class, 'gl_id');
    } 
}
