<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'code',
        'name',
        'description',
        'debit',
        'credit',
        'entry_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'entry_at'  => 'date:d/m/Y',
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'journal_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                $model->company_id = $auth->company->company_id;
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function journalDetails()
    {
        return $this->hasMany(JournalDetail::class, $this->primaryKey);
    }
}
