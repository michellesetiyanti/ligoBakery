<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class AdjustmentDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'adjust_id',
        'product_id',
        'qty_lg',
        'qty_sm',
        'price_sm',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'adjust_d_id';

    public function adjustment()
    {
        return $this->belongsTo(Adjustment::class, 'adjust_id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Models\Master\Product::class, 'product_id');
    }
}
