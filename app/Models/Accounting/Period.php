<?php

namespace App\Models\Accounting;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'month',
        'year',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'company_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'period_id';

    public static function boot()
    {
        parent::boot();
        if ($auth = auth()->user()) {
            $model->company_id = $auth->company_id;
        }
    }
}
