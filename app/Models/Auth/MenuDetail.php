<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class MenuDetail extends Model
{ 
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'menu_id',
        'slug',
        'name',
        'url',
        'readd',
        'createe',
        'confirmm',
        'updatee',
        'deletee',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
        'readd'  => 'boolean',
        'createe'  => 'boolean',
        'confirmm'  => 'boolean',
        'updatee'  => 'boolean',
        'deletee'  => 'boolean',
    ];

    protected $primaryKey = 'menu_d_id';
    
    public static function boot()
    {
        parent::boot(); 
    }

    public function parent()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    } 
}
