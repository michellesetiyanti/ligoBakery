<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'code',
        'name',
        'description',
        'status',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'role_id';
    
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->code)) {
                $model->code = \Str::random(8);
            }
            if (empty($model->description)) {
                $model->description = 'General Role for '.$model->name;
            }
        });
    }
    
    public function roleMenus()
    {
        return $this->hasOne(RoleMenu::class, 'role_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'type', 'code');
    }
}
