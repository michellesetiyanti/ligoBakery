<?php

namespace App\Models\Auth;

use Auth;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'karyawan_id',
        'username',
        'email',
        'password',
        'type',
        'locale',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
        'company_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'date:d/m/Y',
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'user_id';
    
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                if (empty($model->company_id)) {
                    $model->company_id = $auth->isMultiCompany() ? request('company_id') : $auth->company_id;
                }
            }
            $model->locale = app()->getLocale();
            if (empty($model->username)) {
                $model->username = $model->type.\Str::random(3);
            }
        });
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id')->with(['warehouse', 'companies']);
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'type', 'code')->with('roleMenus');
    }

    public function karyawan()
    {
        return $this->belongsTo(\App\Models\Master\Karyawan::class, 'karyawan_id');
    }
    
    // public function menus()
    // {
    //     return $this->hasManyThrough(Menu::class, RoleMenu::class, 'role_id', 'menu_id')->where('parent_id', '=', null)->with('submenu');
    // }
    
    // public function role()
    // {
    //     return $this->hasMany(Role::class, 'code' ,'type')->with('roleMenus');
    // }

    public function customers()
    {
        return $this->hasMany(\App\Models\Master\Customer::class, $this->primaryKey);
    }

    public function suppliers()
    {
        return $this->hasMany(\App\Models\Master\Supplier::class, $this->primaryKey);
    }

    public function vendors()
    {
        return $this->hasMany(\App\Models\Master\Vendor::class, $this->primaryKey);
    }

    public function tukang()
    {
        return $this->hasMany(\App\Models\Master\Tukang::class, $this->primaryKey);
    }

    public function products()
    {
        return $this->hasMany(\App\Models\Master\Product::class, $this->primaryKey);
    }  

    public function adjustments()
    {
        return $this->hasMany(\App\Models\Accounting\Adjustment::class, $this->primaryKey);
    }

    public function isOwner()
    {
        return in_array($this->type, ['owner']);
    }

    public function isSuperAdmin()
    {
        return in_array($this->type, ['owner', 'superadmin']);
    }

    public function isMultiCompany()
    {
        return $this->company->companies->isNotEmpty();
    }

    public function isMultiWarehouse()
    {
        return $this->company->warehouse->count() > 1;
    }
    
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public function responsiblePerson()
    {
        return $this->hasOne(\App\Models\Master\ResponsiblePerson::class, $this->primaryKey);
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
