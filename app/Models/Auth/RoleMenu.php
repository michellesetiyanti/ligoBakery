<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{  
    
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'role_id', 
        'access',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'role_m_id';
    
    public static function boot()
    {
        parent::boot();  
    }
    
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    } 
    
    public function getAccessAttribute($value)
    {
        return json_decode($value);
    }
}
