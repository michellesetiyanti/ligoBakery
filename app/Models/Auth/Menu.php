<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{ 
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'name',
        'icon',
        'showw',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
        'showw'  => 'boolean',
    ];

    protected $primaryKey = 'menu_id';
    
    public static function boot()
    {
        parent::boot(); 
    } 

    public function submenu()
    {
        return $this->hasMany(MenuDetail::class, 'menu_id');
    }
     
}
