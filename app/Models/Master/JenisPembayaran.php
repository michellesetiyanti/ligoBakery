<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class JenisPembayaran extends Model
{
     /**
     * Mass assignable columns
     */
     protected $fillable = [
        'category_pembayaran_id',
        'name',
        'coa_code',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'jenis_pembayaran_id';
    
     

    public function categoryPembayaran()
    {
        return $this->belongsTo(CategoryPembayaran::class, 'category_pembayaran_id');
    }

    public function coa()
    {
        return $this->belongsTo(\App\Models\Accounting\Coa::class, 'coa_code','code');
    }
}
