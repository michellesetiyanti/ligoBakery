<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'wh_id',
        'product_c_id',
        'code',
        'name',
        'description',
        'unit_lg',
        'unit_sm',
        'unit_qty',
        'pack',
        'pack_qty',
        'total_lg_qty',
        'total_sm_qty',
        'price_lg',
        'price_sm',
        'min_qty',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'wh_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'product_id';
    
    protected $appends = [
        'volume'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->wh_id)) {
                    $model->wh_id = $auth->company->warehouse()->pluck('wh_id')->first();
                }
            }
            if (empty($model->code)) {
                $latestProductCode = static::latest('product_id')->pluck('code')->first();
                $model->code = uniqueCode($latestProductCode ?? 'PROD', '.');
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'wh_id');
    }
    
    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class, 'product_c_id');
    }
    
    public function productDetails()
    {
        return $this->hasMany(ProductDetail::class, 'product_id', 'product_id');
    }
    
    public function estimasiDetails()
    {
        return $this->hasMany(\App\Models\Project\EstimasiDetail::class, 'product_id', 'product_id');
    }
    
    public function poProjectDetails()
    {
        return $this->hasMany(\App\Models\Pembelian\PoProjectDetail::class, 'product_id', 'product_id');
    }
    
    public function poPembelianDetails()
    {
        return $this->hasMany(\App\Models\Pembelian\PoPembelianDetail::class, 'product_id', 'product_id');
    }
    
    public function penjualanDetails()
    {
        return $this->hasMany(\App\Models\Penjualan\PenjualanDetail::class, 'product_id', 'product_id')->with('penjualan');
    }

    public function pembelianDetails()
    {
        return $this->hasMany(\App\Models\Pembelian\PembelianDetail::class, 'product_id', 'product_id')->with('pembelian');
    }
    
    public function doDetails()
    {
        return $this->hasMany(\App\Models\Penjualan\DeliveryOrderDetail::class, 'product_id', 'product_id')->with('deliveryOrder');
    }
    
    public function stock($unit = false)
    {
        $sales = $this->penjualanDetails->whereIn('penjualan.faktur', ['approved'])->flatten();
        $do = $this->doDetails->whereIn('deliveryOrder.penjualan_id', $sales->pluck('penjualan_id'))->flatten();
        $lg = $this->total_lg_qty;
        $lg-= $sales->sum('qty_lg') - $do->sum('recv_qty_lg');
        $sm = $this->total_sm_qty;
        $sm-= $sales->sum('qty_sm') - $do->sum('recv_qty_sm');
        $stock = compact('lg', 'sm');
        if ($this->pack && $this->pack_qty) {
            $sm2pack = ($lg * $this->unit_qty) + $sm;
            $sm2pack = $sm2pack * $this->pack_qty;
            $stock = array_merge($stock, compact('sm2pack'));
        } else {
            $lg2sm = ($lg * $this->unit_qty) + $sm;
            $stock = array_merge($stock, compact('lg2sm'));
        }
        if ($unit && array_key_exists($unit, $stock)) {
            return $stock[$unit];
        }
        return $stock;
    }
    
    public function getVolumeAttribute()
    {
        if ($this->pack && $this->pack_qty) {
            $qty_sm = $this->total_lg_qty * $this->unit_qty;
            $qty_sm = ($qty_sm + $this->total_sm_qty) * $this->pack_qty;
            return collect([
                'order' => [
                    'unit' => $this->pack,
                    'qty' => $qty_sm,
                ],
                'detail' => [
                    'unit_lg' => $this->unit_lg,
                    'unit_sm' => $this->unit_sm,
                    'qty_lg' => $this->total_lg_qty,
                    'qty_sm' => $this->total_sm_qty,
                ]
            ]);
        }
        return false;
    }
}
