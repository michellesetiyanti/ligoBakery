<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'code',
        'name',
        'nik',
        'nib',
        'spkp',
        'birthday_place',
        'note',
        'address',
        'vendor_loc',
        'vendor_pic',
        'phone',
        'phone2',
        'reference',
        'currency_code',
        'npwp',
        'pkp_name',
        'pkp_address',
        'bank_name',
        'bank_account',
        'credit_limit',
        'status',
        'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'customer_id';

    protected $appends = [
        'import',
        //'export',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
            if (empty($model->code)) {
                $model->code = \Str::random(8);
            }
            if (empty($model->currency_code)) {
                $model->currency_code = 'IDR';
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    
    public function orders()
    {
        return $this->hasMany(\App\Models\Project\Order::class, 'customer_id');
    }
    
    public function penjualan()
    {
        return $this->hasMany(\App\Models\Penjualan\Penjualan::class, 'customer_id');
    }

    public function getImportAttribute()
    {
        $fetch = [];
        $date = \Carbon\Carbon::parse($this->created_at);
        $path = 'customer/'.$date->isoFormat('YYYY/MM');
        $filename = 'CUST_'.$this->customer_id;
        foreach (glob(public_path('storage').'/'.$path.'/'.$filename.'*') as $file) {
            $fetch[] = str_replace(
                public_path('storage').'/'.$path,
                asset('storage').'/'.$path,
                $file
            );
        }
        return $fetch;
    }

    public function setImportAttribute($value)
    {
        $date = \Carbon\Carbon::parse($this->created_at);
        $path = 'customer/'.$date->isoFormat('YYYY/MM');
        $filename = 'CUST_'.$this->customer_id;
        if ($import = $this->import) {
            $filename = pathinfo(end($import))['filename'];
        }
        if ($value) {
            $filename = uniqueCode($filename, '-');
            $filename.= '.'.$value->getClientOriginalExtension();
            \Storage::disk('public')->put($path.'/'.$filename, file_get_contents($value));
            return asset('storage').'/'.$path.'/'.$filename;
        }
        return false;
    }
}
