<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'product_id',
        'status',
        'ref',
        'batch',
        'unit_lg',
        'unit_sm',
        'qty_lg',
        'qty_sm',
        'cur_qty_lg',
        'cur_qty_sm',
        'price_lg',
        'price_sm',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'product_d_id';
    
    protected $appends = [
        'volume'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $now = \Carbon\Carbon::now();
            $revisiIds = static::where('status', $model->status)->where('ref', 'like', '%[revisi]%')->get();
            $voidIds = static::where('status', $model->status)->where('ref', 'like', '%[void]%')->get();
            $unique = static::where('status', $model->status)->whereNotIn('ref', $voidIds->pluck('ref')->merge($revisiIds->pluck('ref')))->whereMonth('created_at', $now->format('m'))->count() + 1;
            $unique = str_pad($unique, strlen($unique) > 3 ? strlen($unique) : 3, 0, STR_PAD_LEFT);
            switch ($model->status) {
                case 'mutasi':
                    // $batch = 'MT'.$unique.$now->format('Ym');
                break;
                case 'pembelian':
                    $batch = 'PB'.$unique.$now->format('Ym');
                break;
            }
            if (empty($model->batch)) {
                $model->batch = $batch;
            }
        });
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    
    public function getVolumeAttribute()
    {
        if ($this->product->pack && $this->product->pack_qty) {
            $qty_sm = $this->qty_lg * $this->product->unit_qty;
            $qty_sm = ($qty_sm + $this->qty_sm) * $this->product->pack_qty;
            $cur_qty_sm = $this->cur_qty_lg * $this->product->unit_qty;
            $cur_qty_sm = ($cur_qty_sm + $this->cur_qty_sm) * $this->product->pack_qty;
            return collect([
                'stock' => [
                    'unit' => $this->product->pack,
                    'qty' => $qty_sm,
                    'cur_qty' => $cur_qty_sm,
                ],
                'detail' => [
                    'unit_lg' => $this->product->unit_lg,
                    'unit_sm' => $this->product->unit_sm,
                    'qty_lg' => $this->qty_lg,
                    'qty_sm' => $this->qty_sm,
                    'cur_qty_lg' => $this->cur_qty_lg,
                    'cur_qty_sm' => $this->cur_qty_sm,
                ]
            ]);
        }
        return false;
    }
}
