<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class PenjualanMenu extends Model
{
    protected $fillable = [
        'user_id', 
        'note', 
        'date', 
        'subtotal',
        'tax',
        'discount',
        'grandtotal',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'penjualan_menu_id';

     public static function boot()
    {
       parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
            }
          
        });
    }
 
    public function users(){
        return $this->belongsTo(config('auth.providers.users.model'),'user_id', 'user_id');
    }

    public function jenisPembayaran(){
        return $this->belongsTo(JenisPembayaran::class, 'jenis_pembayaran_id' ,'jenis_pembayaran_id')->with(['categoryPembayaran','coa']);
    }

    public function penjualanDetails()
    {
        return $this->hasMany(PenjualanMenuDetail::class, 'penjualan_menu_id' ,'penjualan_menu_id')->with('menuJual');
    }

    
}
