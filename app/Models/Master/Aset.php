<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Aset extends Model
{
   /**
     * Mass assignable columns
     */
     protected $fillable = [
        'code',
        'name',
        'description',
        'date_buy',
        'brand',
        'borrowed_by',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'aset_id';

    protected $appends = [
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            } 
        });
    }
     
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
     
}
