<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class WorkshopDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'wh_id',
        'workshop_id',
        'product_id',
        'unit_lg',
        'unit_sm',
        'qty_lg',
        'qty_sm',
        'price_lg',
        'price_sm',
        'tax',
        'discount',
        'amount',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'wh_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'workshop_d_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->wh_id)) {
                    $model->wh_id = $auth->company->warehouse->pluck('wh_id')->first();
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class, 'wh_id');
    }

    public function workshop()
    {
        return $this->belongsTo(Workshop::class, 'workshop_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
