<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'code',
        'code_professi',
        'name',
        'address',
        'phone',
        'reference',
        'currency_code',
        'npwp',
        'taxable',
        'taxable_rate',
        'penempatan',
        'entry_at',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
        // 'entry_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'vendor_id';

    protected $appends = [
        // 'import',
        //'export',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
            if (empty($model->code)) {
                $model->code = \Str::random(8);
            }
            if (empty($model->currency_code)) {
                $model->currency_code = 'IDR';
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    
    public function borongan()
    {
        return $this->hasMany(\App\Models\Project\Borongan::class, 'vendor_id', 'vendor_id');
    }

    public function getImportAttribute()
    {
        $fetch = [];
        $date = \Carbon\Carbon::parse($this->created_at);
        $path = 'vendor/'.$date->isoFormat('YYYY/MM');
        $filename = 'VEND_'.$this->vendor_id;
        foreach (glob(public_path('storage').'/'.$path.'/'.$filename.'*') as $file) {
            $fetch[] = str_replace(
                public_path('storage').'/'.$path,
                asset('storage').'/'.$path,
                $file
            );
        }
        return $fetch;
    }

    public function setImportAttribute($value)
    {
        $date = \Carbon\Carbon::parse($this->created_at);
        $path = 'vendor/'.$date->isoFormat('YYYY/MM');
        $filename = 'VEND_'.$this->vendor_id;
        if ($import = $this->import) {
            $filename = pathinfo(end($import))['filename'];
        }
        if ($value) {
            $filename = uniqueCode($filename, '-');
            $filename.= '.'.$value->getClientOriginalExtension();
            \Storage::disk('public')->put($path.'/'.$filename, file_get_contents($value));
            return asset('storage').'/'.$path.'/'.$filename;
        }
        return false;
    }
    
    public function setEntryAtAttribute($value)
    {
        $this->attributes['entry_at'] = \Carbon\Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }
}
