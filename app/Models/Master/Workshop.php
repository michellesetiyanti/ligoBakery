<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'work_order',
        'total',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'workshop_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function order()
    {
        return $this->belongsTo(\App\Models\Project\Order::class, 'order_id', 'order_id');
    }

    public function workshopDetails()
    {
        return $this->hasMany(WorkshopDetail::class, 'workshop_id')->with(['product']);
    }
}
