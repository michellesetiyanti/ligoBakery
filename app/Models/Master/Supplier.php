<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'code',
        'name',
        'address',
        'region',
        'phone',
        'pic',
        'email',
        'reference',
        'currency_code',
        'npwp',
        'taxable',
        'taxable_rate',
        'bank_name',
        'bank_branch',
        'bank_account',
        'pkp_name',
        'pkp_address',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'supplier_id';

    protected $appends = [
        'import',
        //'export',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
            if (empty($model->code)) {
                $model->code = \Str::random(8);
            }
            if (empty($model->currency_code)) {
                $model->currency_code = 'IDR';
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    
    public function poPembelian()
    {
        return $this->hasMany(\App\Models\Pembelian\PoPembelian::class, 'supplier_id', 'supplier_id');
    }
    
    public function pembelian()
    {
        return $this->hasMany(\App\Models\Pembelian\Pembelian::class, 'supplier_id', 'supplier_id');
    }

    public function getImportAttribute()
    {
        $fetch = [];
        $date = \Carbon\Carbon::parse($this->created_at);
        $path = 'supplier/'.$date->isoFormat('YYYY/MM');
        $filename = 'SUPP_'.$this->supplier_id;
        foreach (glob(public_path('storage').'/'.$path.'/'.$filename.'*') as $file) {
            $fetch[] = str_replace(
                public_path('storage').'/'.$path,
                asset('storage').'/'.$path,
                $file
            );
        }
        return $fetch;
    }

    public function setImportAttribute($value)
    {
        $date = \Carbon\Carbon::parse($this->created_at);
        $path = 'supplier/'.$date->isoFormat('YYYY/MM');
        $filename = 'SUPP_'.$this->supplier_id;
        if ($import = $this->import) {
            $filename = pathinfo(end($import))['filename'];
        }
        if ($value) {
            $filename = uniqueCode($filename, '-');
            $filename.= '.'.$value->getClientOriginalExtension();
            \Storage::disk('public')->put($path.'/'.$filename, file_get_contents($value));
            return asset('storage').'/'.$path.'/'.$filename;
        }
        return false;
    }
}
