<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class CategoryPembayaran extends Model
{
    protected $table = "category_pembayarans";
    /**
     * Mass assignable columns
     */
     protected $fillable = [
        'name',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'category_pembayaran_id';
    
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
           
        });
    }
 
}
