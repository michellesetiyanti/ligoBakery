<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class MenuSisaJualDetail extends Model
{
    protected $fillable = [
        'id_menu_sisa_jual', 
        'id_menu_jual', 
        'qty',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'id_menu_sisa_jual_detail';

     public static function boot()
    {
       parent::boot();
        static::creating(function ($model) {
            
        });
    }
  

    public function menuJual()
    {
        return $this->belongsTo(MenuJual::class, 'id_menu_jual' ,'id_menu_jual');
    }
}
