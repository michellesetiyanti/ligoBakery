<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class ProduksiGagal extends Model
{
    protected $fillable = [
        'user_id', 
        'id_produksi_details', 
        'qty',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'id_produksi_gagal';

     public static function boot()
    {
       parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
            }
          
        });
    }
 
    public function users(){
        return $this->belongsTo(config('auth.providers.users.model'),'user_id', 'user_id');
    }

    public function produksiDetail()
    {
        return $this->belongsTo(ProduksiDetail::class, 'id_produksi_details' ,'id_produksi_details')->with(['menu_juals','produksi']);
    }

}
