<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'name',
        'address',
        'phone',
        'currency_code',
        'npwp',
        'taxable',
        'taxable_rate',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'company_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'wh_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function adjustments()
    {
        return $this->hasMany(\App\Models\Accounting\Adjustment::class, 'wh_id');
    }

    public function estimasi()
    {
        return $this->hasMany(\App\Models\Project\Estimasi::class, 'wh_id');
    }
}
