<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'parent_id',
        'name',
        'address',
        'phone',
        'currency_code',
        'npwp',
        'taxable',
        'taxable_rate',
        'code_numb',
        'code_alpha',
        'coa_penjualan',
        'coa_pembelian',
        'coa_hutang',
        'coa_piutang',
        'coa_pajak',
        'coa_pajak_out',
        'coa_discount',
        'coa_discount_buy',
        'coa_ikhtisar_labarugi',
        'coa_labarugi_ditahan',
        'bank_name',
        'bank_branch',
        'bank_account',
        'pkp_name',
        'pkp_address',
        'type',
        'owner',
        'owner_related',
        'status',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'company_id';
    
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->currency_code)) {
                $model->currency_code = 'IDR';
            }
        });
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }
    
    public function warehouse()
    {
        return $this->hasMany(Warehouse::class, 'company_id');
    }

    public function companies()
    {
        return $this->hasMany(static::class, 'parent_id');
    }

    public function users()
    {
        return $this->hasMany(\App\Models\Auth\User::class, 'company_id');
    }

    public function karyawan()
    {
        return $this->hasMany(\App\Models\Master\Karyawan::class, 'company_id');
    }

    public function coas()
    {
        return $this->hasMany(\App\Models\Accounting\Coa::class, 'company_id');
    }

    public function generalLedgers()
    {
        return $this->hasMany(\App\Models\Accounting\GeneralLedger::class, 'company_id')->with('generalLedgerDetails');
    }
}
