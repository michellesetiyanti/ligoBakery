<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Tukang extends Model
{
    /**
     * Database table name
     */
    protected $table = 'tukang';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'code',
        'nik',
        'name',
        'ttl',
        'address',
        'address_now',
        'phone',
        'phone2',
        'sibling_phone',
        'upah',
        'bank_name',
        'bank_branch',
        'bank_account',
        'penempatan',
        'induk_tukang',
        'description',
        'entry_at',
        'penempatan',
        'induk_tukang',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
        'entry_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'tukang_id';
    
    protected $appends = [
        'cradle',
        'born',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    
    public function upahTukangDetails()
    {
        return $this->hasMany(UpahTukangDetail::class, 'tukang_id');
    }
    
    public function setEntryAtAttribute($value)
    {
        $this->attributes['entry_at'] = \Carbon\Carbon::createFromFormat('d/m/Y', $value)->toDatetimeString();
    }
    
    public function getCradleAttribute()
    {
        if ($this->ttl) {
            return preg_replace('/(\w+), (\d{1,2})-(\d{1,2})-(19|20)(\d{2})/i', '$1', $this->ttl);
        }
        return false;
    }
    
    public function getBornAttribute()
    {
        if ($this->ttl) {
            return preg_replace('/(\w+), (\d{1,2})-(\d{1,2})-(19|20)(\d{2})/i', '$2-$3-${4}${5}', $this->ttl);
        }
        return false;
    }
}
