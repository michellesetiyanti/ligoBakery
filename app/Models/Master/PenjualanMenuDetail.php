<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class PenjualanMenuDetail extends Model
{
    protected $fillable = [
        'penjualan_menu_id', 
        'id_menu_jual', 
        'note',
        'qty',
        'price',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'penjualan_menu_detail_id';

     public static function boot()
    {
       parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
            }
          
        });
    }
 
     
    public function menuJual()
    {
        return $this->belongsTo(MenuJual::class, 'id_menu_jual' ,'id_menu_jual');
    }
}
