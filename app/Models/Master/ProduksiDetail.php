<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class ProduksiDetail extends Model
{
      /**
     * Mass assignable columns
     */
    protected $fillable = [
        'id_produksi',
        'id_menu_jual',
        'qty_produksi',
        'qty_jual',
        'qty_gagal',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'id_produksi_details';
    
    public static function boot()
    {
       parent::boot();
        
    }
    
    public function produksi(){
        return $this->belongsTo(Produksi::class, 'id_produksi');
    }
    
    public function menu_juals(){
        return $this->belongsTo(MenuJual::class,'id_menu_jual');
    }

    
}
