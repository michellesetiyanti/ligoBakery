<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{    
    /**
     * Database table name
     */
    protected $table = 'karyawan';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'position',
        'name',
        'nik',
        'ttl',
        'address',
        'address_now',
        'religion',
        'education',
        'datejoin',
        'penempatan',
        'phone',
        'phone2',
        'email',
        'emailperusahaan',
        'gender',
        'marriage',
        'photo',
        'npwp',
        'bpjs',
        'salary',
        'bank_name',
        'bank_branch',
        'bank_account',
        'status_karyawan',
        'induk_karyawan',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'company_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'karyawan_id';
    
    protected $appends = [
        'cradle',
        'born',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
            if (empty($model->induk_karyawan)) {
                $model->induk_karyawan = mt_rand();
            }
        });
    }
    
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    
    public function user()
    {
        return $this->hasOne(config('auth.providers.users.model'), 'karyawan_id');
    }
    
    public function penjualan()
    {
        return $this->hasMany(\App\Models\Penjualan\Penjualan::class, 'sales_id', 'karyawan_id')->with('penjualanDetails');
    }

    public function getPhotoAttribute($value)
    {
        return $value ? asset('storage').'/'.$value : null;
    }

    public function setPhotoAttribute($value)
    {
        $date = \Carbon\Carbon::parse($this->created_at);
        $path = 'employee/'.$date->isoFormat('YYYY/MM');
        $filename = 'EMPL_'.$this->karyawan_id;
        if ($value) {
            $filename = uniqueCode($filename, '-');
            $filename.= '.'.$value->getClientOriginalExtension();
            if ($this->attributes['photo']) {
                \Storage::disk('public')->delete($this->attributes['photo']);
            }
            \Storage::disk('public')->put($path.'/'.$filename, file_get_contents($value));
            $this->attributes['photo'] = $path.'/'.$filename;
        }
        return false;
    }
    
    public function getCradleAttribute()
    {
        if ($this->ttl) {
            return preg_replace('/(\w+), (\d{1,2})-(\d{1,2})-(19|20)(\d{2})/i', '$1', $this->ttl);
        }
        return false;
    }
    
    public function getBornAttribute()
    {
        if ($this->ttl) {
            return preg_replace('/(\w+), (\d{1,2})-(\d{1,2})-(19|20)(\d{2})/i', '$2-$3-${4}${5}', $this->ttl);
        }
        return false;
    }
}
