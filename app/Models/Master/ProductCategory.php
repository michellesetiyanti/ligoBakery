<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'product_c_id';

    public static function boot()
    {
        parent::boot();
    }
    
    public function product()
    {
        return $this->hasMany(Product::class, 'product_c_id');
    }
}
