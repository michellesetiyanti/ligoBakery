<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class MenuJual extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'id_menu_jual',
        'company_id',
        'menu',
        'qty',
        'price',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'id_menu_jual';
    
    public static function boot()
    {
       parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
          
        });
    }
 
    public function companies()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function recipes()
    {
        return $this->hasMany(RecipeDetail::class, 'id_menu_jual' ,'id_menu_jual')->with('product');
    }
 
}
