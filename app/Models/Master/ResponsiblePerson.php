<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class ResponsiblePerson extends Model
{
    /**
     * Database table name
     */
    protected $table = 'responsible_person';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'name',
        'description',
        'user',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        // 
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'rp_id';
    
    public function getUserAttribute($value)
    {
        return json_decode($value);
    }
}
