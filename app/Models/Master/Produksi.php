<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Produksi extends Model
{
      /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id', 
        'input_by',
        'date',
        'description',
        'status',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'id_produksi';
    
    public static function boot()
    {
       parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
          
        });
    }
 
    public function companies()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function users(){
        return $this->belongsTo(config('auth.providers.users.model'),'input_by', 'user_id');
    }

    public function produksiDetails(){
        return $this->hasMany(ProduksiDetail::class, 'id_produksi')->with('menu_juals');
    
    }


   

    
}
