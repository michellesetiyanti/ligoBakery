<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class MenuSisaJual extends Model
{
    protected $fillable = [
        'company_id', 
        'user_id',
        'date',
        'status',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'id_menu_sisa_jual';

     public static function boot()
    {
       parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
          
        });
    }
    
    public function users(){
        return $this->belongsTo(config('auth.providers.users.model'),'user_id', 'user_id');
    }

    public function companies()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function menuSisaDetails()
    {
        return $this->hasMany(MenuSisaJualDetail::class, 'id_menu_sisa_jual')->with('menuJual');
    }

    
}
