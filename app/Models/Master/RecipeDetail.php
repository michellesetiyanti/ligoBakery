<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class RecipeDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'id_recipe_details',
        'id_recipe', 
        'product_id', 
        'qty', 
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'id_recipe_details';
    
    public static function boot()
    {
       parent::boot(); 
    }
   
   public function product(){
        return $this->belongsTo(Product::class, 'product_id');
   }
}
