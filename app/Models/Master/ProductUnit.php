<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class ProductUnit extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'code',
        'name',
        'description',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'product_u_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (empty($model->code)) {
                $model->code = \Str::Slug($model->name);
            }
        });
    }
}
