<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
     /**
     * Mass assignable columns
     */
    protected $fillable = [
        'id_recipe',
        'company_id', 
        'id_menu_jual', 
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'id_recipe';
    
    public static function boot()
    {
       parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
          
        });
    }
 
    public function companies()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function recipeDetails(){
        return $this->hasMany(RecipeDetails::class,'id_recipe','id_recipe')->with('product');
    }
}
