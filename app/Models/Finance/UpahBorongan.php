<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

class UpahBorongan extends Model
{
    /**
     * Database table name
     */
    protected $table = 'upah_borongan';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'code',
        'vendor_name',
        'description',
        'progress',
        'upah',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at' => 'date:d/m/Y',
        'updated_at' => 'date:d/m/Y',
    ];

    protected $primaryKey = 'upah_b_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
}
