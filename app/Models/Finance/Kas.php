<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

class Kas extends Model
{
    /**
     * Database table name
     */
    protected $table = 'kas';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'from_coa',
        'to_coa',
        'reference',
        'description',
        'type',
        'nominal',
        'entry_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];
    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'kas_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                $model->company_id = $auth->company->company_id;
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }

    public function from_coa()
    {
        return $this->belongsTo(\App\Models\Accounting\Coa::class, 'from_coa', 'code');
    }
    public function to_coa()
    {
        return $this->belongsTo(\App\Models\Accounting\Coa::class, 'to_coa', 'code');
    }
}