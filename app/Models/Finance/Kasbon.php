<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

class Kasbon extends Model
{
    /**
     * Database table name
     */
    protected $table = 'kasbon';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'code',
        'code_spk',
        'nominal',
        'entry_at',
        'status',
        'notes',
        'approved',
        'paid_off',
        'paided',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
        'entry_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'kasbon_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
            if (empty($model->code)) {
                $latestCode = static::latest('kasbon_id')->pluck('code')->first();
                $model->code = uniqueCode($latestCode ?? 'KASBON', '/');
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function order()
    {
        return $this->belongsTo(\App\Models\Project\Order::class, 'code_spk', 'code_spk');
    }
    
    public function kasbonDetails()
    {
        return $this->hasMany(KasbonDetail::class, 'kasbon_id')->with('kasbon');
    }
    
    public function kasbonRealisasi()
    {
        return $this->hasMany(KasbonRealisasi::class, 'kasbon_id')->with('kasbon');
    }
    
    public function setEntryAtAttribute($value)
    {
        $this->attributes['entry_at'] = \Carbon\Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }
    
    public function getApprovedAttribute($value)
    {
        return json_decode($value);
    }
}
