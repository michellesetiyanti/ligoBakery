<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

class UpahTukang extends Model
{
    /**
     * Database table name
     */
    protected $table = 'upah_tukang';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'work_order',
        'periode',
        'location',
        'description',
        'nominal',
        'paid',
        'balance',
        'approved',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'upah_tkg_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function upahTukangDetails()
    {
        return $this->hasMany(UpahTukangDetail::class, 'upah_tkg_id')->with(['upahTukang', 'tukang']);
    }
    
    public function order()
    {
        return $this->belongsTo(\App\Models\Project\Order::class, 'work_order', 'work_order');
    }
    
    public function getApprovedAttribute($value)
    {
        return json_decode($value);
    }
}
