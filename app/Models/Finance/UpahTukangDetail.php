<?php

namespace App\Models\Finance;

use Illuminate\Database\Eloquent\Model;

class UpahTukangDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'upah_tkg_id',
        'tukang_id',
        'absent',
        'uangmakan_amount',
        'uangmakan_total',
        'upah',
        'amount',
        'user_pay',
        'date_pay',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'upah_tkg_d_id';

    protected $appends = [
        'total_kerja',
    ];

    public static function boot()
    {
        parent::boot();
    }
    
    public function upahTukang()
    {
        return $this->belongsTo(UpahTukang::class, 'upah_tkg_id');
    }
    
    public function tukang()
    {
        return $this->belongsTo(\App\Models\Master\Tukang::class, 'tukang_id');
    }

    public function setAbsentAttribute($value)
    {        
        settype($value, 'array');
        $this->attributes['absent'] = json_encode($value);
    }
    
    public function getAbsentAttribute($value)
    {
        return json_decode($value);
    }

    public function getTotalKerjaAttribute()
    {
        $absent = collect($this->absent);
        $regular = $absent->values()->sum('regular');
        $shift_1 = $absent->values()->sum('shift_1');
        $shift_2 = $absent->values()->sum('shift_2');

        return compact('regular', 'shift_1', 'shift_2');
    }
    
    public function getUangmakanTotalAttribute($value)
    {
        return json_decode($value);
    }
}
