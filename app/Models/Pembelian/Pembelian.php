<?php

namespace App\Models\Pembelian;

use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    /**
     * Database table name
     */
    protected $table = 'pembelian';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'supplier_id',
        'po_pb_id',
        'po_pro_id',
        'code',
        'due_date',
        'ongkir',
        'ongkir_desc',
        'biaya_lain',
        'biaya_lain_desc',
        'total',
        'payment',
        'status',
        'notes',
        'hutang_status',
        'approve_date',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
        'due_date'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'pembelian_id';
    
    protected $appends = [
        'name',
        'wh_id',
        'discount',
        'tax',
        'dpp',
        'over_due',
        'due_date_day',        
        'coa_code',       
        'order',     
        'code_spk',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
            if (empty($model->code)) {
                $latestCode = static::latest('pembelian_id')->pluck('code')->first();
                $model->code = uniqueCode($latestCode ?? 'PEMBELIAN', '/');
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id')->with('karyawan');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function supplier()
    {
        return $this->belongsTo(\App\Models\Master\Supplier::class, 'supplier_id');
    }
    
    public function poPembelian()
    {
        return $this->belongsTo(PoPembelian::class, 'po_pb_id');
    }
    
    public function poProject()
    {
        return $this->belongsTo(PoProject::class, 'po_pro_id')->with('company');
    }
    
    public function getOrderAttribute()
    {
        $order = null;
        if ($this->po_pro_id) {
            $order = $this->poProject->order;
        }
        return $order;
    }
    
    public function getCodeSpkAttribute()
    {
        $code_spk = null;
        if ($this->order) {
            $code_spk = $this->order->code_spk;
        }
        return $code_spk;
    }
    
    public function pembelianDetails()
    {
        return $this->hasMany(PembelianDetail::class, 'pembelian_id')->with('product');
    }
    
    public function hutang()
    {
        return $this->hasOne(Hutang::class, 'pembelian_id')->with('hutangDetails');
    }
    
    public function toc()
    {
        return $this->hasOne(Toc::class, 'po_pb_id', 'po_pb_id');
    }
    
    public function getNameAttribute()
    {
        return $this->poPembelian()->pluck('name')->first();
    }
    
    public function getWhIdAttribute()
    {
        return $this->poPembelian()->pluck('wh_id')->first();
    }
    
    public function getDiscountAttribute()
    {
        return $this->pembelianDetails()->sum('discount');
    }
    
    public function getTaxAttribute()
    {
        if ($this->pembelianDetails()->exists() && $this->pembelianDetails()->where('tax', '<>', 0)->sum('tax') > 0) {
            $totalAmounts = $this->pembelianDetails()->where('tax', '<>', 0)->sum('amount') + $this->pembelianDetails()->where('tax', '<>', 0)->sum('discount');
            $dpp = (100 / (($this->pembelianDetails()->where('tax', '<>', 0)->sum('tax') / $this->pembelianDetails()->where('tax', '<>', 0)->count()) + 100)) * $totalAmounts;
            return $totalAmounts - $dpp;
        } else {
            return 0;
        }
    }
    
    public function getDppAttribute()
    {
        $dpp = $this->total + $this->discount;
        $dpp-= $this->ongkir - $this->biaya_lain;
        $dpp-= $this->tax;
        return $dpp;
    }
    
    public function getOverDueAttribute()
    {
        if (empty($this->hutang)) {
            return null;
        }
        $hutang = $this->hutang;
        $today = \Carbon\Carbon::now()->toDateString();
        $due_date = $this->due_date->toDateString();
        switch ($this->status) {
            case 'paid':
                $over_due = ($hutang && $hutang->updated_at >= $due_date) ? \Carbon\Carbon::parse($due_date)->diffInDays($hutang->updated_at) : 0;
                $over_due = $over_due.' days';
            break;
            case 'unpaid':
                $over_due = ($this->created_at->toDateString() != $due_date && $today >= $due_date) ? \Carbon\Carbon::parse($due_date)->diffInDays($today) : 0;
                $over_due = $over_due.' days';
            break;
            default:
                $over_due = null;
            break;
        }
        return $over_due;
    }
    
    public function getDueDateDayAttribute()
    {
        if ($this->due_date) {
            return $this->created_at->diffInDays($this->due_date->add(1, 'day'));
        }
        return null;
    }
    
    public function getCoaCodeAttribute()
    {
        $coa_code = null;
        if ($this->po_pb_id) {
            $coa_code = $this->poPembelian()->pluck('coa_code')->first();
        }
        return $coa_code;
    }
}
