<?php

namespace App\Models\Pembelian;

use Illuminate\Database\Eloquent\Model;

class PoProject extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'wh_id',
        'code',
        'code_spk',
        'ongkir',
        'ongkir_desc',
        'biaya_lain',
        'biaya_lain_desc',
        'total',
        'status',
        'notes',
        'approved_at',
        'signed',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'po_pro_id';
    
    protected $appends = [
        'discount',
        'tax',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
            if (empty($model->code)) {
                $latestCode = static::latest('po_pro_id')->pluck('code')->first();
                $model->code = uniqueCode($latestCode ?? 'PO.PROJECT', '/');
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id')->with('karyawan');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function order()
    {
        return $this->belongsTo(\App\Models\Project\Order::class, 'code_spk', 'code_spk')->with('company', 'orderTermins');
    }
    
    public function warehouse()
    {
        return $this->belongsTo(\App\Models\Master\Warehouse::class, 'wh_id');
    }
    
    public function poProjectDetails()
    {
        return $this->hasMany(PoProjectDetail::class, 'po_pro_id', 'po_pro_id')->with(['product', 'poProject']);
    }
    
    public function penjualan()
    {
        return $this->hasOne(\App\Models\Penjualan\Penjualan::class, 'po_pro_id', 'po_pro_id')->with(['penjualanDetails', 'piutang']);
    }
    
    public function poPembelian()
    {
        return $this->hasOne(PoPembelian::class, 'po_pro_id', 'po_pro_id')->with(['poPembelianDetails']);
    }
    
    public function pembelian()
    {
        return $this->hasOne(Pembelian::class, 'po_pro_id', 'po_pro_id')->with(['pembelianDetails', 'hutang']);
    }
    
    public function getDiscountAttribute()
    {
        return $this->poProjectDetails()->sum('discount');
    }
    
    public function getTaxAttribute()
    {
        if ($this->poProjectDetails()->exists() && $this->poProjectDetails()->where('tax', '<>', 0)->sum('tax') > 0) {
            $totalAmounts = $this->poProjectDetails()->where('tax', '<>', 0)->sum('amount') + $this->poProjectDetails()->where('tax', '<>', 0)->sum('discount');
            $dpp = (100 / (($this->poProjectDetails()->where('tax', '<>', 0)->sum('tax') / $this->poProjectDetails()->where('tax', '<>', 0)->count()) + 100)) * $totalAmounts;
            return $totalAmounts - $dpp;
        } else {
            return 0;
        }
    }

    public function setSignedAttribute($value)
    {
        if ($value) {
            settype($value, 'array');
            $signed = json_encode($value);
        } else {
            $signed = $value;
        }
        $this->attributes['signed'] = $signed;
    }
    
    public function getSignedAttribute($value)
    {
        if ($value) {
            return json_decode($value);
        }
        return $value;
    }
}
