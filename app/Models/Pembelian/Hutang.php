<?php

namespace App\Models\Pembelian;

use Illuminate\Database\Eloquent\Model;

class Hutang extends Model
{
    /**
     * Database table name
     */
    protected $table = 'hutang';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'pembelian_id',
        'total',
        'paid_off',
        'balance',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'hutang_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function pembelian()
    {
        return $this->belongsTo(Pembelian::class, 'pembelian_id')->with(['supplier', 'poPembelian']);
    }
    
    public function hutangDetails()
    {
        return $this->hasMany(HutangDetail::class, 'hutang_id')->with('coa');
    }
}
