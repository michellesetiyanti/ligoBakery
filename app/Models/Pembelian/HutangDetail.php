<?php

namespace App\Models\Pembelian;

use Illuminate\Database\Eloquent\Model;

class HutangDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'hutang_id',
        'coa_code',
        'total',
        'paid_off',
        'balance',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'hutang_d_id';

    public static function boot()
    {
        parent::boot();
    }
    
    public function hutang()
    {
        return $this->belongsTo(Hutang::class, 'hutang_id');
    }
    
    public function coa()
    {
        return $this->belongsTo(\App\Models\Accounting\Coa::class, 'coa_code', 'code');
    }
}
