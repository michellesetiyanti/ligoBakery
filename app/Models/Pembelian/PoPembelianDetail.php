<?php

namespace App\Models\Pembelian;

use Illuminate\Database\Eloquent\Model;

class PoPembelianDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'po_pb_id',
        'product_id',
        'unit_lg',
        'unit_sm',
        'qty_lg',
        'qty_sm',
        'price_lg',
        'price_sm',
        'tax',
        'discount',
        'amount',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];
    
    protected $primaryKey = 'po_pb_d_id';
    
    protected $appends = [
        'volume',
        'dpp'
    ];

    public function poPembelian()
    {
        return $this->belongsTo(PoPembelian::class, 'po_pb_id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Models\Master\Product::class, 'product_id');
    }
    
    public function getVolumeAttribute()
    {
        if ($this->product->pack && $this->product->pack_qty) {
            $qty_sm = $this->qty_lg * $this->product->unit_qty;
            $qty_sm = ($qty_sm + $this->qty_sm) * $this->product->pack_qty;
            return collect([
                'order' => [
                    'unit' => $this->product->pack,
                    'qty' => $qty_sm,
                ],
                'detail' => [
                    'unit_lg' => $this->unit_lg,
                    'unit_sm' => $this->unit_sm,
                    'qty_lg' => $this->qty_lg,
                    'qty_sm' => $this->qty_sm,
                ]
            ]);
        }
        return false;
    }
    
    public function getDppAttribute()
    {
        $dpp = $this->amount + $this->discount;
        $tax = (100 / ($this->tax + 100)) * $dpp;
        $tax = $dpp - $tax;
        return $dpp - $tax;
    }
}
