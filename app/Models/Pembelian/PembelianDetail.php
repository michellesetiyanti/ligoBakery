<?php

namespace App\Models\Pembelian;

use Illuminate\Database\Eloquent\Model;

class PembelianDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'pembelian_id',
        'product_id',
        'unit_lg',
        'unit_sm',
        'qty_lg',
        'qty_sm',
        'price_lg',
        'price_sm',
        'tax',
        'discount',
        'amount',
        'revisi',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'pembelian_d_id';
    
    protected $appends = [
        'volume',
        'dpp',
        'po_qty_lg',
        'po_qty_sm',
        'po_volume',
    ];
    
    public function pembelian()
    {
        return $this->belongsTo(Pembelian::class, 'pembelian_id');
    }
    
    public function product()
    {
        return $this->belongsTo(\App\Models\Master\Product::class, 'product_id');
    }
    
    public function getVolumeAttribute()
    {
        if ($this->product->pack && $this->product->pack_qty) {
            $qty_sm = $this->qty_lg * $this->product->unit_qty;
            $qty_sm = ($qty_sm + $this->qty_sm) * $this->product->pack_qty;
            return collect([
                'order' => [
                    'unit' => $this->product->pack,
                    'qty' => $qty_sm,
                ],
                'detail' => [
                    'unit_lg' => $this->unit_lg,
                    'unit_sm' => $this->unit_sm,
                    'qty_lg' => $this->qty_lg,
                    'qty_sm' => $this->qty_sm,
                ]
            ]);
        }
        return false;
    }
    
    public function getPoQtyLgAttribute()
    {
        $beli = $this->pembelian()->first();
        $po_qty_lg = 0;
        if ($beli->poPembelian) {
            $po_qty_lg = $beli->poPembelian->poPembelianDetails->where('product_id', $this->product_id)->pluck('qty_lg')->first();
        }
        if ($beli->poProject) {
            $po_qty_lg = $beli->poProject->poProjectDetails->where('product_id', $this->product_id)->pluck('qty_lg')->first();
        }
        return $po_qty_lg;
    }
    
    public function getPoQtySmAttribute()
    {
        $beli = $this->pembelian()->first();
        $po_qty_sm = 0;
        if ($beli->poPembelian) {
            $po_qty_sm = $beli->poPembelian->poPembelianDetails->where('product_id', $this->product_id)->pluck('qty_sm')->first();
        }
        if ($beli->poProject) {
            $po_qty_sm = $beli->poProject->poProjectDetails->where('product_id', $this->product_id)->pluck('qty_sm')->first();
        }
        return $po_qty_sm;
    }
    
    public function getPoVolumeAttribute()
    {
        if ($this->product->pack && $this->product->pack_qty) {
            $qty_sm = $this->po_qty_lg * $this->product->unit_qty;
            $qty_sm = ($qty_sm + $this->po_qty_sm) * $this->product->pack_qty;
            return collect([
                'order' => [
                    'unit' => $this->product->pack,
                    'qty' => $qty_sm,
                ],
                'detail' => [
                    'unit_lg' => $this->unit_lg,
                    'unit_sm' => $this->unit_sm,
                    'qty_lg' => $this->po_qty_lg,
                    'qty_sm' => $this->po_qty_sm,
                ]
            ]);
        }
        return false;
    }
    
    public function getDppAttribute()
    {
        $dpp = $this->amount + $this->discount;
        $tax = (100 / ($this->tax + 100)) * $dpp;
        $tax = $dpp - $tax;
        return $dpp - $tax;
    }
}
