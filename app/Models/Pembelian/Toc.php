<?php

namespace App\Models\Pembelian;

use Illuminate\Database\Eloquent\Model;

class Toc extends Model
{
    /**
     * Database table name
     */
    protected $table = 'toc';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'po_pb_id',
        'pembelian_id',
        'follow_up',
        'payment_terms',
        'stb',
        'stb_desc',
        'retv_schedule',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'toc_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($model->follow_up == 'null') $model->follow_up = null;
            if ($model->payment_terms == 'null') $model->payment_terms = null;
            if ($model->stb_desc == 'null') $model->stb_desc = null;
            if ($model->retv_schedule == 'null') $model->retv_schedule = null;
        });
        static::updating(function ($model) {
            if ($model->follow_up == 'null') $model->follow_up = null;
            if ($model->payment_terms == 'null') $model->payment_terms = null;
            if ($model->stb_desc == 'null') $model->stb_desc = null;
            if ($model->retv_schedule == 'null') $model->retv_schedule = null;
        });
    }
    
    public function poPembelian()
    {
        return $this->belongsTo(PoPembelian::class, 'po_pb_id');
    }
    
    public function pembelian()
    {
        return $this->belongsTo(Pembelian::class, 'pembelian_id');
    }
}
