<?php

namespace App\Models\Pembelian;

use Illuminate\Database\Eloquent\Model;

class PoProjectDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'po_pro_id',
        'product_id',
        'unit_lg',
        'unit_sm',
        'qty_lg',
        'qty_sm',
        'price_lg',
        'price_sm',
        'tax',
        'discount',
        'amount',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'po_pro_d_id';

    public function poProject()
    {
        return $this->belongsTo(PoProject::class, 'po_pro_id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Models\Master\Product::class, 'product_id');
    }
}
