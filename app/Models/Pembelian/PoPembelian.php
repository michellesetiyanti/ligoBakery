<?php

namespace App\Models\Pembelian;

use Illuminate\Database\Eloquent\Model;

class PoPembelian extends Model
{
    /**
     * Database table name
     */
    protected $table = 'po_pembelian';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'supplier_id',
        'wh_id',
        'po_pro_id',
        'code',
        'code_spk',
        'name',
        'ongkir',
        'ongkir_desc',
        'biaya_lain',
        'biaya_lain_desc',
        'total',
        'due_date',
        'payment',
        'coa_code',
        'status',
        'notes',
        'requests',
        'serial',        
        'signed',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at' => 'date:d/m/Y',
        'updated_at' => 'date:d/m/Y',
        'due_date' => 'date:d/m/Y',
    ];

    protected $primaryKey = 'po_pb_id';
    
    protected $appends = [
        'discount',
        'tax',
        'toc',
        'dpp',
        'due_date_day',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->serial = \Str::random(40);
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
            if (empty($model->name)) {
                $latestName = static::latest('po_pb_id')->pluck('name')->first();
                $model->name = uniqueCode($latestName ?? 'PO_BELI', '/');
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id')->with('karyawan');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function supplier()
    {
        return $this->belongsTo(\App\Models\Master\Supplier::class, 'supplier_id');
    }
    
    public function warehouse()
    {
        return $this->belongsTo(\App\Models\Master\Warehouse::class, 'wh_id');
    }
    
    public function poPembelianDetails()
    {
        return $this->hasMany(PoPembelianDetail::class, 'po_pb_id')->with(['product', 'poPembelian']);
    }
    
    public function pembelian()
    {
        return $this->hasOne(Pembelian::class, 'po_pb_id')->with(['pembelianDetails', 'hutang', 'toc']);
    }
    
    public function poProject()
    {
        return $this->belongsTo(PoProject::class, 'po_pro_id')->with('company');
    }
    
    public function order()
    {
        return $this->belongsTo(\App\Models\Project\Order::class, 'code_spk', 'code_spk')->with('company');
    }
    
    public function toc()
    {
        return $this->hasOne(Toc::class, 'po_pb_id');
    }
    
    public function coa()
    {
        return $this->hasOne(\App\Models\Accounting\Coa::class, 'coa_code', 'code');
    }
    
    public function getTocAttribute()
    {
        if ($pembelian = $this->pembelian) {
            return $pembelian->toc;
        }
        return false;
    }
    
    public function getDiscountAttribute()
    {
        return $this->poPembelianDetails()->sum('discount');
    }
    
    public function getTaxAttribute()
    {
        if ($this->poPembelianDetails()->exists() && $this->poPembelianDetails()->where('tax', '<>', 0)->sum('tax') > 0) {
            $totalAmounts = $this->poPembelianDetails()->where('tax', '<>', 0)->sum('amount') + $this->poPembelianDetails()->where('tax', '<>', 0)->sum('discount');
            $dpp = (100 / (($this->poPembelianDetails()->where('tax', '<>', 0)->sum('tax') / $this->poPembelianDetails()->where('tax', '<>', 0)->count()) + 100)) * $totalAmounts;
            return $totalAmounts - $dpp;
        } else {
            return 0;
        }
    }
    
    public function getDppAttribute()
    {
        $dpp = $this->total + $this->discount;
        $dpp-= $this->ongkir - $this->biaya_lain;
        $dpp-= $this->tax;
        return $dpp;
    }
    
    public function setDueDateAttribute($value)
    {
        if ($value) {
            $this->attributes['due_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $value)->toDateString();
        }
    }
    
    public function getDueDateDayAttribute()
    {
        if ($this->due_date) {
            return $this->created_at->diffInDays($this->due_date->add(1, 'day'));
        }
        return null;
    }

    public function setSignedAttribute($value)
    {
        if ($value) {
            settype($value, 'array');
            $signed = json_encode($value);
        } else {
            $signed = $value;
        }
        $this->attributes['signed'] = $signed;
    }
    
    public function getSignedAttribute($value)
    {
        if ($value) {
            return json_decode($value);
        }
        return $value;
    }
}
