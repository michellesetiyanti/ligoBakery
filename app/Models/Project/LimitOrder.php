<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class LimitOrder extends Model
{
    protected $fillable = [
        'company_id',
        'order_id',
        'product_id',
        'qty_lg', 
        'qty_sm', 
        'price', 
        'entry_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
     protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at' => 'date:d/m/Y',
        'updated_at' => 'date:d/m/Y',
    ];

    protected $primaryKey = 'limit_id';
 

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function product()
    {
        return $this->belongsTo(\App\Models\Master\Product::class, 'product_id');
    }
    
    public function order()
    {
        return $this->belongsTo(\App\Models\Project\Order::class, 'order_id', 'order_id');
    }
     
 
}
