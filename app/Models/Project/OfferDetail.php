<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class OfferDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'offer_id',
        'item',
        'unit',
        'vol',
        'price',
        'total', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */ 

    protected $casts = [
        'created_at' => 'date:d/m/Y',
        'updated_at' => 'date:d/m/Y',
    ];

    protected $primaryKey = 'offer_d_id';
 

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
          
        });
    }
    
    public function offer()
    {
        return $this->belongsTo(Offer::class, 'offer_id');
    } 
 
}
