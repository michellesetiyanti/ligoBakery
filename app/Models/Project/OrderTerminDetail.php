<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class OrderTerminDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'order_id',
        'order_t_id',
        'description',
        'qty',
        'price',
        'total',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'order_t_d_id';
    
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
    
    public function orderTermin()
    {
        return $this->belongsTo(OrderTermin::class, 'order_t_id');
    }
}
