<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class EstimasiDetail extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'estimasi_id',
        'product_id',
        'unit_lg',
        'unit_sm',
        'qty_lg',
        'qty_sm',
        'price_lg',
        'price_sm',
        'tax',
        'discount',
        'amount',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'estimasi_d_id';

    public static function boot()
    {
        parent::boot();
    }
    
    public function estimasi()
    {
        return $this->belongsTo(Estimasi::class, 'estimasi_id');
    }
    
    public function product()
    {
        return $this->belongsTo(\App\Models\Master\Product::class, 'product_id');
    }
}
