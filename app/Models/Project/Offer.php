<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'customer_id',
        'nomor',
        'entry_at',
        'name',
        'hasil',
        'note',
        'amount',
        'status', 
        'model', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at' => 'date:d/m/Y',
        'updated_at' => 'date:d/m/Y',
    ];

    protected $primaryKey = 'offer_id';
 

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function customer()
    {
        return $this->belongsTo(\App\Models\Master\Customer::class, 'customer_id');
    }

    public function offerDetails()
    {
        return $this->hasMany(OfferDetail::class, 'offer_id', 'offer_id');
    }
     
}
