<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class Borongan extends Model
{
    /**
     * Database table name
     */
    protected $table = 'borongan';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'user_id',
        'company_id',
        'vendor_id',
        'order_id',
        'description',
        'nominal',
        'entry_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at' => 'date:d/m/Y',
        'updated_at' => 'date:d/m/Y',
    ];

    protected $primaryKey = 'borongan_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function vendor()
    {
        return $this->belongsTo(\App\Models\Master\Vendor::class, 'vendor_id');
    }
    
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'order_id');
    }
    
    public function borongan_details()
    {
        return $this->hasMany(\App\Models\Finance\BoronganKerja::class, 'borongan_id');
    }
     
}
