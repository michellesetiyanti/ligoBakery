<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class Estimasi extends Model
{
    /**
     * Database table name
     */
    protected $table = 'estimasi';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'wh_id',
        'order_id',
        'entry_at', 
        'total',
        'approved',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'estimasi_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function warehouse()
    {
        return $this->belongsTo(\App\Models\Master\Warehouse::class, 'wh_id');
    }
     
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'order_id');
    }
    
    public function estimasiDetails()
    {
        return $this->hasMany(EstimasiDetail::class, 'estimasi_id')->with(['product', 'estimasi']);
    }
    
    public function poProjects()
    {
        return $this->hasMany(\App\Models\Pembelian\PoProject::class, 'estimasi_id')->with(['product', 'estimasi']);
    }
   
}
