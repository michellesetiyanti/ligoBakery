<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class OrderTermin extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'order_id',
        'name',
        'description',
        'amount',
        'amount_detail',
        'termin_total',
        'paid',
        'balance',
        'inv_issued',
    ];

    protected $casts = [
        'created_at'  => 'date:d/m/Y',
        'updated_at'  => 'date:d/m/Y',
    ];

    protected $primaryKey = 'order_t_id';
    
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
    
    public function orderTerminDetails()
    {
        return $this->hasMany(OrderTerminDetail::class, 'order_t_id');
    }
}
