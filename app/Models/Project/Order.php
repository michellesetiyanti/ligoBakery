<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'customer_id',
        'code_spk',
        'work_order',
        'address',
        'add_agree',
        'time_work',
        'profit',
        'amount_profit',
        'cost_project', 
        'cost_job',
        'limit_cost',
        'cost',
        'paid',
        'discount',
        'tax',
        'realisasi_total',
        'realisasi_profit',
        'realisasi_cost',
        'amount_estimasi',
        'to_workshop',
        'adendum',
        'jenis_pekerjaan',
        'nominal',
        'status',
        'status_estimasi',
        'entry_at', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at' => 'date:d/m/Y',
        'updated_at' => 'date:d/m/Y',
    ];

    protected $primaryKey = 'order_id';

    protected $appends = [
        'import',
        //'export',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
    
    public function customer()
    {
        return $this->belongsTo(\App\Models\Master\Customer::class, 'customer_id');
    }
    
    public function workshops()
    {
        return $this->hasMany(\App\Models\Master\Workshop::class, 'customer_id');
    }
    
    public function estimasi()
    {
        return $this->hasMany(Estimasi::class, 'order_id', 'order_id')->with('estimasiDetails');
    }
    
    public function limitOrders()
    {
        return $this->hasMany(LimitOrder::class, 'order_id')->with('product');
    }
    
    public function orderTermins()
    {
        return $this->hasMany(OrderTermin::class, 'order_id')->with('orderTerminDetails');
    }
    
    public function orderTerminDetails()
    {
        return $this->hasMany(OrderTerminDetail::class, 'order_id');
    }
    
    public function borongan()
    {
        return $this->hasMany(Borongan::class, 'work_order', 'work_order')->with('boronganKerja');
    }
    
    public function invoices()
    {
        return $this->hasMany(\App\Models\Finance\Invoice::class, 'work_order', 'work_order')->with('invoiceDetails');
    }
    
    public function upahTukang()
    {
        return $this->hasMany(\App\Models\Finance\UpahTukang::class, 'work_order', 'work_order')->with('upahTukangDetails');
    }
    
    public function poProjects()
    {
        return $this->hasMany(\App\Models\Pembelian\PoProject::class, 'code_spk', 'code_spk')->with('poProjectDetails');
    }
    
    public function poPembelian()
    {
        return $this->hasMany(\App\Models\Pembelian\PoPembelian::class, 'code_spk', 'code_spk')->with('poPembelianDetails');
    }
    
    public function penjualan()
    {
        return $this->hasMany(\App\Models\Penjualan\Penjualan::class, 'code_spk', 'code_spk')->with('penjualanDetails');
    }
    
    public function kasbon()
    {
        return $this->hasMany(\App\Models\Finance\Kasbon::class, 'work_order', 'work_order')->with('kasbonDetails');
    } 

    public function getImportAttribute()
    {
        $fetch = [];
        $date = \Carbon\Carbon::parse($this->created_at);
        $path = 'order/'.$date->isoFormat('YYYY/MM');
        $filename = 'RAB_'.$this->order_id;
        foreach (glob(public_path('storage').'/'.$path.'/'.$filename.'*') as $file) {
            $fetch[] = str_replace(
                public_path('storage').'/'.$path,
                asset('storage').'/'.$path,
                $file
            );
        }
        return $fetch;
    }

    // public function setImportAttribute($value)
    // {
    //     $date = \Carbon\Carbon::parse($this->created_at);
    //     $path = 'order/'.$date->isoFormat('YYYY/MM');
    //     $filename = 'RAB_'.$this->order_id;
    //     if ($import = $this->import) {
    //         $filename = pathinfo(end($import))['filename'];
    //     }
    //     if ($value) {
    //         $filename = uniqueCode($filename, '-');
    //         $filename.= '.'.$value->getClientOriginalExtension();
    //         \Storage::disk('public')->put($path.'/'.$filename, file_get_contents($value));
    //         return asset('storage').'/'.$path.'/'.$filename;
    //     }
    //     return false;
    // }
}
