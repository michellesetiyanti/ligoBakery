<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class BoronganKerja extends Model
{
    /**
     * Database table name
     */
    protected $table = 'borongan_kerja';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'user_id',
        'borongan_id',
        'description',
        'progress',
        'amount',
        'user_pay',
        'date_pay',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
    ];

    protected $casts = [
        'created_at' => 'date:d/m/Y',
        'updated_at' => 'date:d/m/Y',
    ];

    protected $primaryKey = 'borongan_d_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
              
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    } 

    public function borongan()
    {
        return $this->belongsTo(Borongan::class, 'borongan_id', 'borongan_id');
    }  
     
}
