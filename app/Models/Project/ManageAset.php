<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ManageAset extends Model
{
    public $table = 'manage_asets';

    protected $fillable = [
        'aset_id',
        'name',
        'position', 
        'date_borrow', 
        'date_return', 
        'description', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
     protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $casts = [
        'created_at' => 'date:d/m/Y',
        'updated_at' => 'date:d/m/Y',
    ];

    protected $primaryKey = 'manage_aset_id';
 

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                // $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }

    public function aset()
    {
        return $this->belongsTo(\App\Models\Master\Aset::class, 'aset_id');
    }
     
}
