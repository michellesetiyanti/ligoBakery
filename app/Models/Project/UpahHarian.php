<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class UpahHarian extends Model
{
    /**
     * Database table name
     */
    protected $table = 'upah_harian';

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'company_id',
        'code',
        'name',
        'description',
        'upah',
        'approved',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_id',
        'company_id',
    ];

    protected $primaryKey = 'upah_h_id';

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if ($auth = auth()->user()) {
                $model->user_id = $auth->user_id;
                if (empty($model->company_id)) {
                    $model->company_id = $auth->company_id;
                }
            }
        });
    }
    
    public function user()
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'user_id');
    }
    
    public function company()
    {
        return $this->belongsTo(\App\Models\Master\Company::class, 'company_id');
    }
}
