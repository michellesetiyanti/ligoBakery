<?php

namespace App\Http\Middleware;

use Illuminate\Database\Eloquent\Model;
use Closure;

class NullToBlank
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->accepts('application/json')) {
            $null = 'null';
            foreach ($request->input() as $key => $value) {
                if (in_array($value, [strtolower($null), strtoupper($null), ucfirst($null)])) {
                    $request->request->set($key, null);
                }
            }
        }
        return $next($request);
    }
}
