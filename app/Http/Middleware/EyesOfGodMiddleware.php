<?php

namespace App\Http\Middleware;

use Closure;

class EyesOfGodMiddleware extends \EOGBaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if ($token = \Cookie::get("Bearer")) {
                $request->headers->set("Authorization", "Bearer $token");
            }
            $user = \EOGAuth::parseToken()->authenticate();
        } catch (\EOGException $e) {
            if ($e instanceof \EOGTokenInvalidException) {
                return response()->json([
                    'status' => 'failed',
                    'message' => trans('general.token.invalid'),
                    'errors' => $e->getMessage(),
                ]);
            } else if ($e instanceof \EOGTokenExpiredException) {
                return response()->json([
                    'status' => 'failed',
                    'message' => trans('general.token.expired'),
                    'errors' => $e->getMessage(),
                ]);
            } else {
                return response()->json([
                    'status' => 'failed',
                    'message' => trans('general.token.unauthorization'),
                    'errors' => $e->getMessage(),
                ]);
            }
        }
        return $next($request);
    }
}
