<?php

namespace App\Http\Controllers\Api\master;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Master\ProductUnit as ProductUnitRequest;
use App\Repositories\Master\ProductUnit as ProductUnitRepository;

class ProductunitController extends ApiController
{    
    protected $productUnit;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->productUnit = new ProductUnitRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $productUnit = $this->productUnit->all($request->all());
        return $this->success(null, 200, $productUnit);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductUnitRequest $request)
    {
        \DB::beginTransaction();
        try {
            $productUnit = $this->productUnit->save($request->all());
            \DB::commit();
            return $this->success(null, 200, $productUnit);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($productUnit = $this->productUnit->show($id)) {
            return $this->success(null, 200, $productUnit);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductUnitRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUnitRequest $request, $id)
    {
        $productUnit = $this->productUnit->show($id);
        \DB::beginTransaction();
        try{
            $productUnit->fill($request->merge([
                'id' => $id,
            ])->all());
            $productUnit->save();
            \DB::commit();
            return $this->success(null, 200, $productUnit);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productUnit = $this->productUnit->show($id);
        \DB::beginTransaction();
        try {
            $productUnit->delete();
            \DB::commit();
            return $this->success(null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
