<?php

namespace App\Http\Controllers\Api\Master;

use App\Models\Master\Product;
use App\Models\Master\Produksi;
use App\Models\Master\ProduksiGagal;
use App\Models\Master\ProduksiDetail;
use App\Models\Master\RecipeDetail;
use App\Models\Master\ProductDetail;
use App\Models\Master\MenuJual;

use Illuminate\Http\Request;
use App\Repositories\Master\Produksi as ProduksiRepository;
use App\Http\Controllers\Api\Controller as ApiController;

class ProduksiController extends ApiController
{

    protected $produksi;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->produksi = new ProduksiRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $sorting = 'company_id|DESC';
        $sorting = $request->sorting ? $request->sorting : $sorting;
        $dataproduksi = $this->produksi->with(['companies', 'produksiDetails','users'])->all($request->merge(compact('filter', 'sorting'))->all());
        return $this->success(null, 200, $dataproduksi);
    }


    public function indexgagal(\Illuminate\Http\Request $request){
        $data = ProduksiGagal::orderBy('id_produksi_gagal','DESC')->with(['produksiDetail','users'])->get();

        return $this->success(null, 200, $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $userid = auth()->user()->user_id;
         $produksidate = $request->produksidate;
         $description = $request->description;
         $idmenujual = $request->idmenujual;
         $qtyproduksi = $request->qtyproduksi;

         \DB::beginTransaction();
        try {
            $produksi = new Produksi;
            $produksi->description = $description;
            $produksi->date = $produksidate;
            $produksi->input_by = $userid;
            $produksi->status = 'open';
            $produksi->save();

            $idproduksi = $produksi->id_produksi;

            $produksidet = new ProduksiDetail;
            $produksidet->id_produksi = $idproduksi;
            $produksidet->id_menu_jual = $idmenujual;
            $produksidet->qty_produksi = $qtyproduksi;
            $produksidet->qty_jual = $qtyproduksi;
            $produksidet->save();


            \DB::commit();
            return $this->success(null, 200, $produksi);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function storeDetail(Request $request)
    {
         $idproduksi = $request->idproduksi;
         $idmenujual = $request->idmenujual;
         $qtyproduksi = $request->qtyproduksi;

         $cek = ProduksiDetail::where('id_produksi',$idproduksi)->where('id_menu_jual',$idmenujual)->count();
         if($cek != 0){
             return $this->fail('Post Error', 500, 'Menu Sudah Pernah di input');
         }

         \DB::beginTransaction();
        try {

            $produksidet = new ProduksiDetail;
            $produksidet->id_produksi = $idproduksi;
            $produksidet->id_menu_jual = $idmenujual;
            $produksidet->qty_produksi = $qtyproduksi;
            $produksidet->qty_jual = $qtyproduksi;
            $produksidet->save();

            \DB::commit();
            return $this->success(null, 200, $produksidet);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function storegagal(Request $request)
    {
         $userid = auth()->user()->user_id;
         $idproduksidetail = $request->idproduksidetail;
         $qty = $request->qty;

         $dataproduksi = ProduksiDetail::where('id_produksi_details',$idproduksidetail)->first();
         $id_produksi = $dataproduksi->id_produksi;
         $id_menu_jual = $dataproduksi->id_menu_jual;
         $qty_produksi = $dataproduksi->qty_produksi;
         $qty_gagal = $dataproduksi->qty_gagal;
         $qty_jual = $dataproduksi->qty_jual;
        
         $datamenujual = MenuJual::where('id_menu_jual',$id_menu_jual)->first();
         $qtymenujual = $datamenujual->qty;


         \DB::beginTransaction();
        try {
            if($qty_produksi - $qty != 0){
                $produksi = new ProduksiGagal;
                $produksi->user_id = $userid;
                $produksi->id_produksi_details = $idproduksidetail;
                $produksi->qty = $qty;
                $produksi->save();

                $qty_jual = $qty_jual - $qty ;
                $qty_gagal = $qty_gagal + $qty;
                $array = array(
                    'qty_gagal'=>$qty_gagal,
                    'qty_jual'=>$qty_jual,
                );
                ProduksiDetail::where('id_produksi_details',$idproduksidetail)->update($array);


                $qtymenujual = $qtymenujual - $qty;
                $array2 = array(
                    'qty'=>$qtymenujual
                );
                MenuJual::where('id_menu_jual',$id_menu_jual)->update($array2);

            }else{
                return $this->fail('Post Error', 500, 'Qty Produksi kurang dari qty gagal');
            }
           
            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\produksi  $produksi
     * @return \Illuminate\Http\Response
     */
    public function show(produksi $produksi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\produksi  $produksi
     * @return \Illuminate\Http\Response
     */
    public function edit(produksi $produksi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\produksi  $produksi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $description = $request->description;

         \DB::beginTransaction();
        try { 

             $array = array(
                 'description'=>$description
             );
             Produksi::where('id_produksi',$id)->update($array);


            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function updategagal(Request $request, $id)
    {
        $qtybaru = $request->qty;

        $data = ProduksiGagal::where('id_produksi_gagal',$id)->with('produksiDetail')->first();
        $qtyawal = $data->qty;
        $id_produksi_details = $data->id_produksi_details;
        $qtygagal = $data->produksiDetail->qty_gagal;
        $qtyjual = $data->produksiDetail->qty_jual;
        $id_menu_jual = $data->produksiDetail->id_menu_jual;

        $datamenujual = MenuJual::where('id_menu_jual',$id_menu_jual)->first();
        $qtymenujual = $datamenujual->qty;

         \DB::beginTransaction();
        try { 
            $qtygagal = $qtygagal-$qtyawal;
            $qtyjual = $qtyjual + $qtyawal;
            $qtymenujual = $qtymenujual +$qtyawal;

            $qtygagal = $qtygagal+$qtybaru;
            $qtyjual = $qtyjual - $qtybaru;
            $qtymenujual = $qtymenujual - $qtybaru;

             $array = array(
                 'qty_gagal'=>$qtygagal,
                 'qty_jual'=>$qtyjual
             );
             ProduksiDetail::where('id_produksi_details',$id_produksi_details)->update($array);

             $array2 = array(
                 'qty'=>$qtymenujual
            );
            MenuJual::where('id_menu_jual',$id_menu_jual)->update($array2);

            $array3 = array(
                'qty'=>$qtybaru
            );
            ProduksiGagal::where('id_produksi_gagal',$id)->update($array3);


            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    

    public function updatedetail(Request $request, $id)
    {
         $qtyproduksi = $request->qtyproduksi;

         \DB::beginTransaction();
        try { 
            $array = array(
                'qty_produksi'=>$qtyproduksi,
                'qty_jual'=>$qtyproduksi
            );

            ProduksiDetail::where('id_produksi_details',$id)->update($array);

            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function submitproduksi($id_produksi){

        $produksi_det = ProduksiDetail::where('id_produksi',$id_produksi)->get();


         \DB::beginTransaction();
        try { 

            foreach ($produksi_det as $key => $value) {
               $id_menu_jual = $value->id_menu_jual;
               $qty_produksi = $value->qty_produksi;

                $datarecipe = RecipeDetail::where('id_menu_jual',$id_menu_jual)->get();
                foreach ($datarecipe as $key2 => $value2) {
                    $qty = $value2->qty;
                    $totalqty = $qty_produksi * $qty;
                    $product_id = $value2->product_id;
                    
                    $productdata = Product::where('product_id',$product_id)->first();
                    $data_total_qty = $productdata->total_sm_qty;
                    $dataname = $productdata->name;

                    $cektotalstock = $data_total_qty - $totalqty;

                    if($cektotalstock < 0){
                         return $this->fail('Post Error', 500, "Stock $dataname Tidak Cukup  $cektotalstock $data_total_qty - $totalqty");
                    }else{
                       $arraytotalqty = array(
                           'total_sm_qty'=>$cektotalstock
                       ); 
                       Product::where('product_id',$product_id)->update($arraytotalqty);
                    }

                    $cekstockproduct = ProductDetail::where('product_id',$product_id)->where('cur_qty_sm','!=','0.00')->orderBy('product_d_id','ASC')->get();
                    foreach ($cekstockproduct as $key3 => $value3) {
                        $product_d_id = $value3->product_d_id;
                        $batch = $value3->batch;
                        $unit_lg = $value3->unit_lg;
                        $unit_sm = $value3->unit_sm;
                        $cur_qty_sm = $value3->cur_qty_sm;
                        $price_sm = $value3->price_sm;

                        if($totalqty <= $cur_qty_sm){
                            $cur_qty_sm = $cur_qty_sm - $totalqty;
 

                            $array = array(
                                'cur_qty_sm'=> $cur_qty_sm,
                            );
                            ProductDetail::where('product_d_id',$product_d_id)->update($array);


                            $prod_det = new ProductDetail;
                            $prod_det->product_id = $product_id;
                            $prod_det->status = 'produksi';
                            $prod_det->ref = $id_produksi;
                            $prod_det->batch = $batch;
                            $prod_det->unit_lg = $unit_lg;
                            $prod_det->unit_sm = $unit_sm;
                            $prod_det->qty_lg = '0.00';
                            $prod_det->qty_sm = $totalqty;
                            $prod_det->cur_qty_lg = '0.00';
                            $prod_det->cur_qty_sm = '0.00';
                            $prod_det->price_lg = '0.00';
                            $prod_det->price_sm = $price_sm;
                            $prod_det->save();
                            
                        }else{
                            $totalqty = $totalqty - $cur_qty_sm;

                            $array = array(
                                'cur_qty_sm'=> '0.00',
                            );
                            ProductDetail::where('product_d_id',$product_d_id)->update($array);


                            $prod_det = new ProductDetail;
                            $prod_det->product_id = $product_id;
                            $prod_det->status = 'produksi';
                            $prod_det->ref = $id_produksi;
                            $prod_det->batch = $batch;
                            $prod_det->unit_lg = $unit_lg;
                            $prod_det->unit_sm = $unit_sm;
                            $prod_det->qty_lg = '0.00';
                            $prod_det->qty_sm = $totalqty;
                            $prod_det->cur_qty_lg = '0.00';
                            $prod_det->cur_qty_sm = '0.00';
                            $prod_det->price_lg = '0.00';
                            $prod_det->price_sm = $price_sm;
                            $prod_det->save();
                        }


                    }//endforeach product Detail per product

                }//endforeach recipe Detail per recipe

                $datamenujual = MenuJual::where('id_menu_jual',$id_menu_jual)->first();
                $qtymenujual = $datamenujual->qty;

                $qtymenujual = $qtymenujual + $qty_produksi; 
                $updatemenu = array(
                    'qty'=>$qtymenujual
                );
                MenuJual::where('id_menu_jual',$id_menu_jual)->update($updatemenu);
            }//endfor each produksi detail per menu

            $array2 = array(
                'status'=>'complete'
            );
            Produksi::where('id_produksi',$id_produksi)->update($array2);

            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\produksi  $produksi
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         \DB::beginTransaction();
        try {  
            Produksi::where('id_produksi',$id)->delete();

            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function destroydetail($id)
    {
         \DB::beginTransaction();
        try {  
            ProduksiDetail::where('id_produksi_details',$id)->delete();

            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function destroygagal($id)
    {
        $data = ProduksiGagal::where('id_produksi_gagal',$id)->with('produksiDetail')->first();
        $qtyawal = $data->qty;
        $id_produksi_details = $data->id_produksi_details;
        $qtygagal = $data->produksiDetail->qty_gagal;
        $qtyjual = $data->produksiDetail->qty_jual;
        $id_menu_jual = $data->produksiDetail->id_menu_jual;

        $datamenujual = MenuJual::where('id_menu_jual',$id_menu_jual)->first();
        $qtymenujual = $datamenujual->qty;

         \DB::beginTransaction();
        try { 
            $qtygagal = $qtygagal-$qtyawal;
            $qtyjual = $qtyjual + $qtyawal;
            $qtymenujual = $qtymenujual +$qtyawal;


             $array = array(
                 'qty_gagal'=>$qtygagal,
                 'qty_jual'=>$qtyjual
             );
             ProduksiDetail::where('id_produksi_details',$id_produksi_details)->update($array);

             $array2 = array(
                 'qty'=>$qtymenujual
            );
            MenuJual::where('id_menu_jual',$id_menu_jual)->update($array2);

            ProduksiGagal::where('id_produksi_gagal',$id)->delete();


            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
