<?php

namespace App\Http\Controllers\Api\Master;

use App\Models\Master\Aset as ASET;
use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Master\Aset as AsetRequest;
use App\Repositories\Master\Aset as AsetRepository;
use App\Repositories\Master\Company as CompanyRepository;

class AsetController extends ApiController
{
    protected $aset;
    protected $company;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->aset = new AsetRepository;
        $this->company = new CompanyRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $aset = $this->aset->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $aset);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AsetRequest $request)
    {
        $company = $this->company->show($request->company_id);
        $code = $this->codeAsset([
            'comp_id' => $company->company_id,
            'comp_code' => $company->code_alpha,
            'type' => $request->description,
            'date_buy' => \Carbon\Carbon::parse(explode('GMT', $request->date_buy)[0])->toDateTimeString()
        ]);
        \DB::beginTransaction();
        try {
            // $com_id = $request->company_id;
            // $c = ASET::where('company_id',$com_id)->count();
            // $c = $c+1;
            // $c = str_pad($c,3,"0",STR_PAD_LEFT);
            // $code = "ASET-$c";
            $aset = $this->aset->save($request->merge([
                'code' => $code,
            ])->all()); 
            \DB::commit();
            return $this->success(null, 200, $aset);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\aset  $aset
     * @return \Illuminate\Http\Response
     */
    public function show(aset $aset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\aset  $aset
     * @return \Illuminate\Http\Response
     */
    public function edit(aset $aset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\aset  $aset
     * @return \Illuminate\Http\Response
     */
    public function update(AsetRequest $request, $id)
    {
        $aset = $this->aset->show($id);
        \DB::beginTransaction();
        try {
            $aset->update($request->merge([
                'id' => $id,
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $aset);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\aset  $aset
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aset = $this->aset->show($id);
        \DB::beginTransaction();
        try { 
                $aset->delete(); 
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    
    private function codeAsset(array $request)
    {
        $now = \Carbon\Carbon::now();
        $model = $this->aset->getModel();
        $format = 'AST [CV] [AT][BM][BY].[NO]';
        // =====
        $comp_id = isset($request['comp_id']) ? $request['comp_id'] : null;
        $comp_code = isset($request['comp_code']) ? $request['comp_code'] : null;
        $date_buy = isset($request['date_buy']) ? $request['date_buy'] : null;
        $type = isset($request['type']) ? $request['type'] : null;
        // =====
        switch (strtolower($type)) {
            case 'aset kantor':
                $code_AT = 'AK';
            break;
            case 'aset operasional':
                $code_AT = 'AO';
            break;
            case 'aset proyek':
                $code_AT = 'AP';
            break;
        }
        $buy = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date_buy);
        $code_BM = $buy->format('m');
        $code_BY = $buy->format('y');
        $code_CV = $comp_code;
        $noUrut = $model->where('company_id', $comp_id)->whereYear('created_at', $now->format('Y'))->orderBy('aset_id', 'DESC')->get();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('code')->first();
            preg_match('/AST '.$code_CV.' (AK|AO|AP)(\d{4}).(\d{3})/', $noUrut, $match);
            $noUrut = $match[3] + 1;
            $noUrut = str_pad($noUrut, 3, '0', STR_PAD_LEFT);
        } else {
            $noUrut = str_pad(1, 3, '0', STR_PAD_LEFT);
        }
        $code_NO = $noUrut;
        
        return str_replace(
            array('[CV]', '[AT]', '[NO]', '[BM]', '[BY]'),
            array($code_CV, $code_AT, $code_NO, $code_BM, $code_BY),
            $format
        );
    }
}
