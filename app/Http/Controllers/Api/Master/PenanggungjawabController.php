<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Repositories\Auth\User as UserRepository;
use App\Repositories\Master\ResponsiblePerson as RpRepository;
use Illuminate\Http\Request;

class PenanggungjawabController extends ApiController
{
    protected $penaggungjawab;
    protected $user;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->penaggungjawab = new RpRepository;
        $this->user = new UserRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pj = $this->penaggungjawab->all($request->all());

        return $this->success(null, 200, $pj);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|unique:responsible_person,name',
            'description' => 'nullable',
            'user.*.id' => 'nullable|exists:users,user_id',
        ]);
        if ($validator->fails()) {
            return $this->fail('fail_validation', 422, $validator->errors());
        }
        
        $args = collect();
        if ($request->input('user.*.id')) {   
            $users = $this->user->all()->whereIn('user_id', $request->input('user.*.id'))->flatten();
            foreach ($users as $user) {
                $user_id = $user->user_id;
                $username = $user->username;
                $email = $user->email;
                $type = $user->type;
                $company_id = $user->company_id;
                $company_name = $user->company->name;
                $karyawan_id = $user->karyawan_id;
                $karyawan_name = $user->karyawan->name ?? null;
                $args->push(compact('user_id', 'username', 'email', 'type', 'company_id', 'company_name', 'karyawan_id', 'karyawan_name'));
            }
        }

        $pj = $this->penaggungjawab->save([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'user' => json_encode($args),
        ]);        
        
        return $this->success(null, 200, $pj);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($pj = $this->penaggungjawab->show($id)) {
            return $this->success(null, 200, $pj);
        } else {
            return $this->notFound(null);
        }
    }    

    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|unique:responsible_person,name,'.$id.',rp_id',
            'description' => 'nullable',
            'user.*.id' => 'nullable|exists:users,user_id',
        ]);
        if ($validator->fails()) {
            return $this->fail('fail_validation', 422, $validator->errors());
        }

        $pj = $this->penaggungjawab->show($id);
        
        $args = collect();
        if ($request->input('user.*.id')) {   
            $users = $this->user->all()->whereIn('user_id', $request->input('user.*.id'))->flatten();
            foreach ($users as $user) {
                $user_id = $user->user_id;
                $username = $user->username;
                $email = $user->email;
                $type = $user->type;
                $company_id = $user->company_id;
                $company_name = $user->company->name;
                $karyawan_id = $user->karyawan_id;
                $karyawan_name = $user->karyawan->name ?? null;
                $args->push(compact('user_id', 'username', 'email', 'type', 'company_id', 'company_name', 'karyawan_id', 'karyawan_name'));
            }
        }

        $pj->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'user' => json_encode($args),
        ]);
        
        return $this->success(null, 200, $pj);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pj = $this->penaggungjawab->show($id);
        $pj->delete();
        
        return $this->success(null, 200);
    }
}
