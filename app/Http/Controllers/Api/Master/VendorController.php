<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Master\Vendor as VendorRequest;
use App\Repositories\Master\Vendor as VendorRepository;

class VendorController extends ApiController
{
    protected $vendor;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->vendor = new VendorRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $vendor = $this->vendor->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $vendor);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  VendorRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(VendorRequest $request)
    {
        \DB::beginTransaction();
        try {
            $code = $this->codeVendor([
                'penempatan' => $request->penempatan,
                'code_professi' => $request->code_professi,
                'entry_at' => \Carbon\Carbon::createFromFormat('d/m/Y', $request->entry_at)->format('d-m-Y'),
            ]);
            $vendor = $this->vendor->save($request->merge([
                'code' => $code,
                'currency_code' => $request->currency_code ?? 'IDR',
                'status' => $request->status ?? 'active'
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $vendor);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($vendor = $this->vendor->show($id)) {
            return $this->success(null, 200, $vendor);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  VendorRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function update(VendorRequest $request, $id)
    {
        $vendor = $this->vendor->show($id);
        \DB::beginTransaction();
        try {
            $code = $vendor->code;
            if (empty($code)) {
                $code = $this->codeVendor([
                    'penempatan' => $request->penempatan,
                    'code_professi' => $request->code_professi,
                    'entry_at' => \Carbon\Carbon::createFromFormat('d/m/Y', $request->entry_at)->format('d-m-Y'),
                ], $id);
            }
            $vendor->update($request->merge([
                'id' => $id,
                'code' => $code,
                'currency_code' => $request->currency_code ?? 'IDR',
                'status' => $request->status ?? 'active'
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $vendor);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vendor = $this->vendor->show($id);
        \DB::beginTransaction();
        try {
            if ($vendor->borongan->isEmpty()) {
                $vendor->delete();
            } else {
                $vendor->update(['status' => 'inactive']);
            }
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Import.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
            'currency_code' => 'required|string',
            'attachment.*' => 'file|image|mimes:jpeg,png,jpg,doc,ppt,xls,pdf|max:2048',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        // Vendor
        $vendor = $this->vendor->getModel();
        $vendorByName = $vendor->where('name', $request->name)->first();
        if (empty($vendorByName)) {
            $vendorByName = $vendor->create([
                'name' => $request->name,
                'currency_code' => $request->currency_code ?? 'IDR',
            ]);
        }
        try {
            $vendorByName->import = $request->attachment;
            $vendorByName->save();
            return $this->success(null, 200, $vendorByName->import);
        } catch (\Exception $e) {
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    
    private function codeVendor(array $request, $id = 0)
    {
        $now = \Carbon\Carbon::now();
        $auth = auth()->user();
        $model = $this->vendor->getModel();
        $format = '[CV] V.[NO].[PR].[DS].[JM][JY]';
        // =====
        $district = isset($request['penempatan']) ? $request['penempatan'] : null;
        $professi = isset($request['code_professi']) ? $request['code_professi'] : null;
        $join = isset($request['entry_at']) ? \Carbon\Carbon::createFromFormat("d-m-Y", $request['entry_at']) : null;
        // =====
        $code_CV = $auth->company->code_alpha ?? str_pad($auth->company_id, 5, '0', STR_PAD_LEFT);
        $code_DS = $district;
        $code_PR = $professi;
        $code_JM = empty($join) ? $now->format('m') : $join->format('m');
        $code_JY = empty($join) ? $now->format('y') : $join->format('y');
        $noUrut = $model->where('company_id', $auth->company_id)->where('vendor_id', '<>', $id)->where('penempatan', $district)->whereMonth('entry_at', empty($join) ? $now->format('m') : $join->format('m'))->orderBy('vendor_id', 'DESC')->get();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('code')->first();
            preg_match('/'.$code_CV.' V.(\d{2}).(\d{2}).'.$code_DS.'.'.$code_BM.$code_BY.'/', $noUrut, $match);
            $noUrut = $match[1] + 1;
            $noUrut = str_pad($noUrut, 2, '0', STR_PAD_LEFT);
        } else {
            $noUrut = str_pad(1, 2, '0', STR_PAD_LEFT);
        }
        $code_NO = $noUrut;
        
        return str_replace(
            array('[NO]', '[CV]', '[DS]', '[PR]', '[JM]', '[JY]'),
            array($code_NO, $code_CV, $code_DS, $code_PR, $code_JM, $code_JY),
            $format
        );
    }
}
