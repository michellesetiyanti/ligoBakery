<?php

namespace App\Http\Controllers\Api\Master;

use App\Models\Master\CategoryPembayaran;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Master\CategoryPembayaran as CategoryPembayaranRequest;
use App\Repositories\Master\CategoryPembayaran as CategoryPembayaranRepository;


class CategoryPembayaranController extends ApiController
{
    protected $categorypayment;
     /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->categorypayment = new CategoryPembayaranRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $categorypayment = $this->categorypayment->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $categorypayment);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(CategoryPembayaranRequest $request)
    {
        \DB::beginTransaction();
        try {
            $Cat = new CategoryPembayaran;
            $Cat->name = $request->name;
            $Cat->save();

          
            \DB::commit();
            return $this->success(null, 200, $Cat);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CategoryPembayaran  $categoryPembayaran
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryPembayaran $categoryPembayaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CategoryPembayaran  $categoryPembayaran
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryPembayaran $categoryPembayaran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoryPembayaran  $categoryPembayaran
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryPembayaranRequest $request, $id)
    {
        \DB::beginTransaction();
        try {
             $array = array(
                 'name'=>$request->name
             );
             CategoryPembayaran::where('category_pembayaran_id',$id)->update($array);
            \DB::commit();
            return $this->success("Data Update Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CategoryPembayaran  $categoryPembayaran
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
             CategoryPembayaran::where('category_pembayaran_id',$id)->delete($id);
            \DB::commit();
            return $this->success("Data Delete Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
