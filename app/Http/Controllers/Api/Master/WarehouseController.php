<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Master\Warehouse as WarehouseRequest;
use App\Repositories\Master\Warehouse as WarehouseRepository;

class WarehouseController extends ApiController
{
    protected $warehouse;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->warehouse = new WarehouseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                preg_match('/(?:'.$filter.')/i', $request->filter, $matches);
                if ($matches) $filter = $request->filter;
            break;
        }
        $warehouse = $this->warehouse->with(['company'])->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $warehouse);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  WarehouseRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WarehouseRequest $request)
    {
        \DB::beginTransaction();
        try {
            $warehouse = $this->warehouse->save($request->merge([
                'currency_code' => $request->currency_code ?? 'IDR',
                'status' => $request->status ?? 'active'
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $warehouse);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($warehouse = $this->warehouse->with(['company'])->show($id)) {
            return $this->success(null, 200, $warehouse);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  WarehouseRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WarehouseRequest $request, $id)
    {
        $warehouse = $this->warehouse->show($id);
        \DB::beginTransaction();
        try{
            $warehouse->update($request->merge([
                'id' => $id,
                'currency_code' => $request->currency_code ?? 'IDR',
                'status' => $request->status ?? 'active'
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $warehouse);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $warehouse = $this->warehouse->show($id);
        \DB::beginTransaction();
        try {
            if ($warehouse->adjustments->isEmpty()) {
                $warehouse->delete();
            } else {
                $warehouse->update(['status' => 'inactive']);
            }
            \DB::commit();
            return $this->success(null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
