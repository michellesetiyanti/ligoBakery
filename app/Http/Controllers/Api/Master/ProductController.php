<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Master\Product as ProductRequest;
use App\Repositories\Master\Product as ProductRepository;
use App\Repositories\Master\ProductDetail as ProductDetailRepository;
use App\Repositories\Penjualan\Penjualan as PenjualanRepository;
use App\Repositories\Pembelian\Pembelian as PembelianRepository;
Use App\Models\Master\Product as PRODUCT;
Use App\Models\Penjualan\Penjualan as PENJUALAN;
Use App\Models\Pembelian\Pembelian as PEMBELIAN;
Use App\Models\Accounting\ItemHppAwal as ITEMHPP;
use Carbon\Carbon;

class ProductController extends ApiController
{
    protected $pembelian;
    protected $penjualan;
    protected $product;
    protected $productDt;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth', ['except' => ['codeProduct']]);
        $this->pembelian = new PembelianRepository;
        $this->penjualan = new PenjualanRepository;
        $this->product = new ProductRepository;
        $this->productDt = new ProductDetailRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $wh_id = implode(':', auth()->user()->company->warehouse->pluck('wh_id')->toArray());
                $filter = 'wh_id|'.$wh_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $sorting = 'product_id|DESC';
        $sorting = $request->sorting ? $request->sorting : $sorting;
        $product = $this->product->with(['productCategory', 'productDetails', 'warehouse'])->all($request->merge(compact('filter', 'sorting'))->all());
        return $this->success(null, 200, $product);
    }

    public function warningqty(\Illuminate\Http\Request $request){
        $wh_id = auth()->user()->company->warehouse[0]->wh_id;
        $warningdata = collect();
        $data = PRODUCT::where('wh_id',$wh_id)->with(['productCategory', 'productDetails', 'warehouse'])->get();

        foreach ($data as $key => $value){
            $product_id = $value->product_id;
            $productCategory = $value->productCategory;
            $productDetails = $value->productDetails;
            $warehouse = $value->warehouse;
            $code = $value->code;
            $name = $value->name;
            $desc = $value->description;
            $unit_lg = $value->unit_lg;
            $unit_sm = $value->unit_sm;
            $unit_qty = $value->unit_qty;
            $total_qty = $value->total_sm_qty;
            $price_sm = $value->price_sm;
            $min_qty = $value->min_qty;

             $array = array(
                 'product_id'=> $product_id,
                 'productCategory'=> $productCategory,
                 'productDetails'=> $productDetails,
                 'warehouse'=>$warehouse,
                 'code'=>$code,
                 'name'=>$name,
                 'desc'=>$desc,
                 'unit_lg'=>$unit_lg,
                 'unit_sm'=>$unit_sm,
                 'unit_qty'=>$unit_qty,
                 'total_qty'=>$total_qty,
                 'price_sm'=>$price_sm,
                 'min_qty'=>$min_qty
             );   

             if($total_qty <= $min_qty){
                 $warningdata->push($array);
             }
        }

        return $this->success(null, 200, $warningdata);

    }

    public function indexhppitem(\Illuminate\Http\Request $request){
       
        $tgl = $request->dateFrom;
        $tgl1 = $request->dateTo;
        $product_id = $request->idproduct;
        $wh_id = auth()->user()->company->warehouse->pluck('wh_id')->toArray(); 

        $product1 = PRODUCT::with(['productDetails'=> function ($q) use ($tgl,$tgl1){
                            $q->where('created_at','<',$tgl);
                        }]) 
                        ->where('product_id','=',$product_id)
                        ->where('wh_id','=',$wh_id[0])->first();              

        $product2 = PRODUCT::with(['productDetails'=> function ($q) use ($tgl,$tgl1){
                            $q->where('created_at','>=',$tgl);
                            $q->where('created_at','<=',$tgl1);
                        }]) 
                        ->where('product_id','=',$product_id)
                        ->where('wh_id','=',$wh_id[0])->first();     
        $i = 1;

        $dataawalhpp = collect(); 
        $detailhpp = collect();
        $persediaan = collect();

        

        //ambil persediaan utk di awal bulan
        $prod_det = $product1->productDetails;
        $detunit_qty = $product1->unit_qty;
        for($d=0; $d < count($prod_det); $d++){
                $det = $prod_det[$d];
                $detid = $det->product_d_id;
                $detstatus = $det->status;
                $detref = $det->ref;
                $detbatch = $det->batch;
                $detqty_lg = $det->qty_lg;
                $detqty_sm = $det->qty_sm;
                $detprice_sm = $det->price_sm;
                $detcreated_at = Carbon::parse($tgl)->format('d-m-Y');

                if($detstatus == "pembelian" || $detstatus == "mutasi tambah"){
                    $total = (($detqty_lg * $detunit_qty ) + $detqty_sm )* $detprice_sm;
                        $persediaanar = array(
                            'product_d_id'=>$detid,
                            'product_id'=>$product_id,
                            'ref'=>$detref,
                            'batch'=>$detbatch,
                            'cur_qty_lg'=>$detqty_lg,
                            'cur_qty_sm'=>$detqty_sm,
                            'price_sm'=>$detprice_sm,
                            'total'=>$total,
                            'created_at'=>$detcreated_at
                        );
                        $persediaan->push($persediaanar);
                }else if($detstatus == "penjualan" || $detstatus == "mutasi kurang"){
                    
                    for($x=0; $x < count($persediaan); $x++){
                        $xbatch = $persediaan[$x]['batch'];
                        $xid = $persediaan[$x]['product_d_id'];
                        $xref = $persediaan[$x]['ref'];
                        $xbatch = $persediaan[$x]['batch'];
                        $xqty_lg = $persediaan[$x]['cur_qty_lg'];
                        $xqty_sm = $persediaan[$x]['cur_qty_sm'];
                        $xprice_sm = $persediaan[$x]['price_sm'];
                        
                        if($detbatch == $xbatch){
                            $xqty_lg = $xqty_lg - $detqty_lg;
                            $xqty_sm = $xqty_sm - $detqty_sm;
                            
                        }
                          $total = (($xqty_lg * $detunit_qty ) + $xqty_sm )* $xprice_sm;
                        $persediaanar = array(
                            'product_d_id'=>$xid,
                            'product_id'=>$product_id,
                            'ref'=>$xref,
                            'batch'=>$xbatch,
                            'cur_qty_lg'=>$xqty_lg,
                            'cur_qty_sm'=>$xqty_sm,
                            'price_sm'=>$xprice_sm,
                             'total'=>$total,
                            'created_at'=>$detcreated_at
                        );
                        $persediaan[$x] = $persediaanar; 
                    } 
                }
                
        }
        $persediaann=$persediaan->filter(function($item){
            return $item['cur_qty_lg'] != 0 || $item['cur_qty_sm'] != 0;
        }); 

        $persediaan = collect();
        foreach ($persediaann as  $value) {
            $persediaan->push($value);
        }
        $prod_det = $product2->productDetails;
        $detunit_qty = $product2->unit_qty;
        $persediaaninput;
        for($d=0; $d < count($prod_det); $d++){//proses detail bulan period
            $masukan=collect();
            $keluaran=collect();
                $det = $prod_det[$d];
                $detid = $det->product_d_id;
                $detstatus = $det->status;
                $detref = $det->ref;
                $detbatch = $det->batch;
                $detunit_lg = $det->unit_lg;
                $detqty_lg = $det->qty_lg;
                $detqty_sm = $det->qty_sm;
                $detunit_sm = $det->unit_sm;
                $detprice_sm = $det->price_sm;
                $detcreated_at = Carbon::parse($det->created_at)->format('d-m-Y'); //tgl per item
 

                if($detstatus == "pembelian" || $detstatus == "mutasi tambah"){
                    $total = (($detqty_lg * $detunit_qty ) + $detqty_sm )* $detprice_sm;
                        $it = array(
                             'product_d_id'=>$detid,
                             'product_id'=>$product_id,
                             'ref'=>$detref,
                             'batch'=>$detbatch,
                             'cur_qty_lg'=>$detqty_lg,
                             'cur_qty_sm'=>$detqty_sm,
                             'price_sm'=>$detprice_sm,
                             'total'=>$total,
                             'created_at'=>$detcreated_at,
                         );
                         $masukan->push($it);   
                         
                         $persediaan->push($it);
                }else if($detstatus == "penjualan" || $detstatus =="mutasi kurang"){
                    $total = (($detqty_lg * $detunit_qty ) + $detqty_sm )* $detprice_sm;
                        $it = array(
                             'product_d_id'=>$detid,
                             'product_id'=>$product_id,
                             'ref'=>$detref,
                             'batch'=>$detbatch,
                             'cur_qty_lg'=>$detqty_lg,
                             'cur_qty_sm'=>$detqty_sm,
                             'price_sm'=>$detprice_sm,
                             'total'=>$total,
                             'created_at'=>$detcreated_at,
                         );
                         $keluaran->push($it);
                    for($z=0; $z < count($persediaan); $z++){
                            $zprodid = $persediaan[$z]['product_id'];
                            $zid = $persediaan[$z]['product_d_id'];
                            $zref = $persediaan[$z]['ref'];
                            $zbatc = $persediaan[$z]['batch'];
                            $zqtylg = $persediaan[$z]['cur_qty_lg'];
                            $zqtysm = $persediaan[$z]['cur_qty_sm'];
                            $zpricesm = $persediaan[$z]['price_sm'];
                            $zcreated_at = $persediaan[$z]['created_at'];

                            if($detbatch == $zbatc){
                                $zqtylg = $zqtylg - $detqty_lg;
                                $zqtysm = $zqtysm - $detqty_sm; 
                                $total = (($zqtylg * $detunit_qty) + $zqtysm)* $zpricesm;
                            

                            $persediaanar = array(
                                'product_d_id'=>$zid,
                                'product_id'=>$zprodid,
                                'ref'=>$zref,
                                'batch'=>$zbatc,
                                'cur_qty_lg'=>$zqtylg,
                                'cur_qty_sm'=>$zqtysm,
                                'price_sm'=>$zpricesm,
                                'total'=>$total,
                                'created_at'=>$zcreated_at,
                            );
                            $persediaan[$z] = $persediaanar; 
                            }
                    }
                    $persediaann=$persediaan->filter(function($item){
                        return $item['cur_qty_lg'] != 0 || $item['cur_qty_sm'] != 0;
                    }); 
                    $persediaan = collect();
                    foreach ($persediaann as  $value) {
                        $persediaan->push($value);
                    } 
                } 

                $persediaaninput = clone $persediaan;
                $datadetail = array(
                    'date'=>$detcreated_at,
                    'ref'=>$detref,
                    'batch'=>$detbatch,
                    'masukan'=>$masukan,
                    'keluaran'=>$keluaran,
                    'persediaan'=>$persediaaninput,
                );
                $detailhpp->push($datadetail);

        }            
        return $this->success(null, 200, $detailhpp);

    }

    public function indexhpp(\Illuminate\Http\Request $request)
    {
        $tgl = $request->dateFrom;
        $tgl1 = $request->dateTo;
        $wh_id = auth()->user()->company->warehouse->pluck('wh_id')->toArray(); 

        $product1 = PRODUCT::with(['productDetails'=> function ($q) use ($tgl,$tgl1){
                            $q->where('created_at','<',$tgl);
                        }]) 
                        ->where('wh_id','=',$wh_id[0])->get();              

        $product2 = PRODUCT::with(['productDetails'=> function ($q) use ($tgl,$tgl1){
                            $q->where('created_at','>=',$tgl);
                            $q->where('created_at','<=',$tgl1);
                        }]) 
                        ->where('wh_id','=',$wh_id[0])->get();     
      

        $dataawalhpp = collect(); 
        $dataallhpp = collect();

        for($i = 0 ; $i < count($product1); $i++){ //proses cek data awal utk period sebelum $tgl
           $persediaan = collect();
           $totalunit_lg=0;
           $totalunit_sm=0;
           $totalprice=0;

           $product_id = $product1[$i]->product_id;
           $product_name = $product1[$i]->name; 
          
               $prod_det = $product1[$i]->productDetails;
                        for($d=0; $d < count($prod_det); $d++){
                                $det = $prod_det[$d];
                                $detstatus = $det->status;
                                $detref = $det->ref;
                                $detbatch = $det->batch;
                                $detqty_lg = $det->qty_lg;
                                $detqty_sm = $det->qty_sm;
                                $detprice_sm = $det->price_sm;

                                if($detstatus == "pembelian" || $detstatus == "mutasi tambah"){
                                     $persediaanar = array(
                                         'product_id'=>$product_id,
                                         'ref'=>$detref,
                                         'batch'=>$detbatch,
                                         'cur_qty_lg'=>$detqty_lg,
                                         'cur_qty_sm'=>$detqty_sm,
                                         'price_sm'=>$detprice_sm,
                                     );
                                     $persediaan->push($persediaanar);
                                }else if($detstatus == "penjualan" || $detstatus == "mutasi kurang"){
                                    for($x=0; $x < count($persediaan); $x++){
                                        $xbatch = $persediaan[$x]['batch'];
                                        $xqty_lg = $persediaan[$x]['cur_qty_lg'];
                                        $xqty_sm = $persediaan[$x]['cur_qty_sm'];
                                        
                                        if($detbatch == $xbatch){
                                            $xqty_lg = $xqty_lg - $detqty_lg;
                                            $xqty_sm = $xqty_sm - $detqty_sm;
                                            
                                        }
                                        $persediaanar = array(
                                            'product_id'=>$product_id,
                                            'ref'=>$detref,
                                            'batch'=>$detbatch,
                                            'cur_qty_lg'=>$xqty_lg,
                                            'cur_qty_sm'=>$xqty_sm,
                                            'price_sm'=>$detprice_sm,
                                        );
                                        $persediaan[$x] = $persediaanar; 
                                    }

                                    $persediaan=$persediaan->filter(function($item){
                                        return $item['cur_qty_lg'] != 0 || $item['cur_qty_sm'] != 0;
                                    }); 
                                }
                                
                        } 
            for($z =0 ; $z < count($persediaan) ; $z++){
                $zunit_lg = $persediaan[$z]['cur_qty_lg'];
                $zunit_sm = $persediaan[$z]['cur_qty_sm'];
                $zprice_sm = $persediaan[$z]['price_sm'];


                $totalunit_lg = $totalunit_lg+ $zunit_lg;
                $totalunit_sm = $totalunit_sm+ $zunit_sm;
                $totalprice = $totalprice + $zprice_sm; 
            }   

           $arhpp = array(
               'product_id'=>$product_id,
               'name'=>$product_name,
               'totalunit_lg'=>$totalunit_lg,
               'totalunit_sm'=>$totalunit_sm,
               'total_harga'=>$totalprice,
               'persediaan_akhir'=>$persediaan
           );
           $dataawalhpp->push($arhpp);
        } 
        //proses cek data hpp periode yang sedang di cari 
        for($c = 0; $c < count($product2); $c++){
           $persediaan = collect();
           $totalunit_lg=0;
           $totalunit_sm=0;
           $pricesm=0;
           $totalprice=0;
           

           $product_id = $product2[$c]->product_id;
           $product_name = $product2[$c]->name; 
           $unit_qty = $product2[$c]->unit_qty;
           $unit_lg = $product2[$c]->unit_lg;
           $unit_sm = $product2[$c]->unit_sm;

               $prod_det = $product2[$c]->productDetails;
                
                        for($d=0; $d < count($prod_det); $d++){

                                
                                $det = $prod_det[$d];
                                $detstatus = $det->status;
                                $detref = $det->ref;
                                $detbatch = $det->batch;
                                $detqty_lg = $det->qty_lg;
                                $detqty_sm = $det->qty_sm;
                                $detprice_sm = $det->price_sm;

                                if($detstatus == "pembelian" || $detstatus == "mutasi tambah"){
                                     $persediaanar = array(
                                         'product_id'=>$product_id,
                                         'ref'=>$detref,
                                         'batch'=>$detbatch,
                                         'cur_qty_lg'=>$detqty_lg,
                                         'cur_qty_sm'=>$detqty_sm,
                                         'price_sm'=>$detprice_sm,
                                     );
                                     $persediaan->push($persediaanar);
                                }else if($detstatus == "penjualan" || $detstatus == "mutasi kurang"){
                                    for($f=0; $f< count($dataawalhpp); $f++){ //proses pengecekan stock di periode sebelumnya
                                            $persediaanawal = $dataawalhpp[$f]['persediaan_akhir'];
                                                for($z=0; $z < count($persediaanawal); $z++){
                                                        $iprodid = $persediaanawal[$z]['product_id'];
                                                        $iref = $persediaanawal[$z]['ref'];
                                                        $ibatc = $persediaanawal[$z]['batch'];
                                                        $iqtylg = $persediaanawal[$z]['cur_qty_lg'];
                                                        $iqtysm = $persediaanawal[$z]['cur_qty_sm'];
                                                        $ipricesm = $persediaanawal[$z]['price_sm'];

                                                        if($detbatch == $ibatch){ //cari batch yang sama baru update 
                                                            $iqty_lg = $iqty_lg - $detqty_lg;
                                                            $iqty_sm = $iqty_sm - $detqty_sm;

                                                        $persediaanar = array(
                                                            'product_id'=>$iprodid,
                                                            'ref'=>$iref,
                                                            'batch'=>$ibatc,
                                                            'cur_qty_lg'=>$iqty_lg,
                                                            'cur_qty_sm'=>$iqty_sm,
                                                            'price_sm'=>$ipricesm,
                                                        );
                                                        $persediaanawal[$z] = $persediaanar; 
                                                    }
                                                }
                                            //hapus array persediaan awal stock yang habis
                                            $persediaanawal=$persediaanawal->filter(function($item){
                                                return $item['cur_qty_lg'] != 0 || $item['cur_qty_sm'] != 0;
                                            }); 
                                    }
                                    
                                    
                                    for($x=0; $x < count($persediaan); $x++){ //pengecekan stock di periode yang di cari
                                        $xbatch = $persediaan[$x]['batch'];
                                        $xref = $persediaan[$x]['ref'];
                                        $xqty_lg = $persediaan[$x]['cur_qty_lg'];
                                        $xqty_sm = $persediaan[$x]['cur_qty_sm'];
                                        
                                        if($detbatch == $xbatch){ //cari batch yang sama baru update 
                                            $xqty_lg = $xqty_lg - $detqty_lg;
                                            $xqty_sm = $xqty_sm - $detqty_sm;
                                            
                                        
                                        $persediaanar = array(
                                            'product_id'=>$product_id,
                                            'ref'=>$xref,
                                            'batch'=>$detbatch,
                                            'cur_qty_lg'=>$xqty_lg,
                                            'cur_qty_sm'=>$xqty_sm,
                                            'price_sm'=>$detprice_sm,
                                        );
                                        $persediaan[$x] = $persediaanar; 
                                        }
                                        
                                    }
                                

                                } 
                                        
                        } 
                  
            //hapus array persediaan awal stock yang habis
                $persediaan=$persediaan->filter(function($item){
                    return $item['cur_qty_lg'] != 0 || $item['cur_qty_sm'] != 0;
                });
            
            foreach ($persediaan as $val) {
                $zunit_lg = $val['cur_qty_lg'];
                $convertunit_lg = $zunit_lg * $unit_qty;
                $zunit_sm = $val['cur_qty_sm'];
                $zprice_sm = $val['price_sm'];

                $totalunit_lg = $totalunit_lg+ $zunit_lg;
                $totalunit_sm = $totalunit_sm+ $zunit_sm;
                $totalprice = $totalprice + ($zprice_sm * ($zunit_sm + $convertunit_lg)); 
            }  
           
 
            
           $arhpp = array(
               'product_id'=>$product_id,
               'name'=>$product_name,
               'unit_qty'=>$unit_qty,
               'unit_lg'=>$unit_lg,
               'unit_sm'=>$unit_sm,
               'totalunit_lg'=>$totalunit_lg,
               'totalunit_sm'=>$totalunit_sm,
               'total_harga'=>$totalprice,
               'persediaan_akhir'=>$persediaan
           );
           $dataallhpp->push($arhpp);


        }  
        

        $alldata = array(
            'data_awal'=>$dataawalhpp,
            'data_period'=>$dataallhpp,
        );  

        return $this->success(null, 200, $alldata);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $code = $request->code;
        if (empty($code)) {
            $code = $this->codeProduct([
                'wh_id' => $request->wh_id,
                'comp_code' => auth()->user()->company->code_alpha,
            ]);
        }
        \DB::beginTransaction();
        try {
            $product = $this->product->save($request->merge([
                'code' => $code,
                'unit_lg' => $request->unit['lg']['code'],
                'unit_sm' => !isset($request->unit['sm']) || $request->unit['sm']['code'] == 'null' ? $request->unit['lg']['code'] : $request->unit['sm']['code'],
                'unit_qty' => !isset($request->unit['sm']) || $request->unit['sm']['value'] == 'null' ? 1 : $request->unit['sm']['value'],
                'pack' => !isset($request->unit['pack']) || $request->unit['pack']['code'] == 'null' ? null : $request->unit['pack']['code'],
                'pack_qty' => !isset($request->unit['pack']) || $request->unit['pack']['value'] == 'null' ? null : $request->unit['pack']['value'],
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $product);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($product = $this->product->with(['productCategory', 'productDetails', 'warehouse'])->show($id)) {
            return $this->success(null, 200, $product);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function update(ProductRequest $request, $id)
    {
        $product = $this->product->show($id);
        \DB::beginTransaction();
        try{
            $product->update($request->merge([
                'id' => $id,
                'unit_lg' => $request->unit['lg']['code'],
                'unit_sm' => !isset($request->unit['sm']) || $request->unit['sm']['code'] == 'null' ? $request->unit['lg']['code'] : $request->unit['sm']['code'],
                'unit_qty' => !isset($request->unit['sm']) || $request->unit['sm']['value'] == 'null' ? 1 : $request->unit['sm']['value'],
                'pack' => !isset($request->unit['pack']) || $request->unit['pack']['code'] == 'null' ? null : $request->unit['pack']['code'],
                'pack_qty' => !isset($request->unit['pack']) || $request->unit['pack']['value'] == 'null' ? null : $request->unit['pack']['value'],
            ])->except(['wh_id']));
            \DB::commit();
            return $this->success(null, 200, $product);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->product->show($id);
        \DB::beginTransaction();
        try {
            if ($product->estimasiDetails->isEmpty() && $product->poProjectDetails->isEmpty() && $product->poPembelianDetails->isEmpty() && $product->penjualanDetails->isEmpty() && $product->productDetails->isEmpty()) {
                $product->delete();
            } else {
                return $this->fail('Transaction is available.', 422);
            }
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function historyPurchases(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'supplier_id' => 'required|integer|exists:suppliers,supplier_id',
            'product_id' => 'nullable|integer|exists:products,product_id',
            'product_limit' => 'nullable|integer',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $products = $this->product->with('productCategory')->all();
        $purchases = $this->pembelian->all(['filter' => 'supplier_id|'.$request->supplier_id.',status|approve']);
        $collects = collect();
        foreach ($products as $product) {
            $purchDetail = $purchases->flatMap->pembelianDetails->where('product_id', $product->product_id)->sortByDesc('pembelian_d_id')->take($request->product_limit ?? 5)->flatten();
            $history_price_lg = $purchDetail->unique('price_lg')->map(function ($item) {
                return [
                    'price' => $item['price_lg'],
                    'code' => $item['pembelian']['code'],
                    'date' => $item['pembelian']['updated_at']->format('d/m/Y'),
                ]; 
            })->all();
            $history_price_sm = $purchDetail->unique('price_sm')->map(function ($item) {
                return [
                    'price' => $item['price_sm'],
                    'code' => $item['pembelian']['code'],
                    'date' => $item['pembelian']['updated_at']->format('d/m/Y'),
                ]; 
            })->all();
            $product = collect($product)->merge(compact('history_price_lg', 'history_price_sm'))->all();
            $collects->push($product);
        }
        if ($request->product_id) {
            $collects = $collects->where('product_id', $request->product_id)->first();
        }
        return $this->success(null, 200, is_array($collects) ? $collects : $collects->toArray());
    }

    public function historySales(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'wh_id' => 'required|integer|exists:warehouses,wh_id',
            'customer_id' => 'required|integer|exists:customers,customer_id',
            'product_id' => 'nullable|integer|exists:products,product_id',
            'product_limit' => 'nullable|integer',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $products = $this->product->with('productCategory')->all(['filter' => 'wh_id|'.$request->wh_id]);
        $sales = $this->penjualan->all(['filter' => 'wh_id|'.$request->wh_id.',customer_id|'.$request->customer_id.',faktur|approved']);
        $collects = collect();
        foreach ($products as $product) {
            $saleDetail = $sales->flatMap->penjualanDetails->where('product_id', $product->product_id)->sortByDesc('penjualan_d_id')->take($request->product_limit ?? 5)->flatten();
            $history_price_lg = $saleDetail->unique('price_lg')->map(function ($item) {
                return [
                    'price' => $item['price_lg'],
                    'code' => $item['penjualan']['code'],
                    'date' => $item['penjualan']['updated_at']->format('d/m/Y'),
                ]; 
            })->all();
            $history_price_sm = $saleDetail->unique('price_sm')->map(function ($item) {
                return [
                    'price' => $item['price_sm'],
                    'code' => $item['penjualan']['code'],
                    'date' => $item['penjualan']['updated_at']->format('d/m/Y'),
                ]; 
            })->all();
            $product = collect($product)->merge(compact('history_price_lg', 'history_price_sm'))->all();
            $collects->push($product);
        }
        if ($request->product_id) {
            $collects = $collects->where('product_id', $request->product_id)->first();
        }
        return $this->success(null, 200, is_array($collects) ? $collects : $collects->toArray());
    }

    public function priceBatch(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'wh_id' => 'required|integer|exists:warehouses,wh_id',
            'product_id' => 'nullable|integer|exists:products,product_id',
            'product_limit' => 'nullable|integer',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $data = collect();
        $limit = $request->has('product_limit') ? $request->product_limit : 5;
        $prodDt = $this->productDt->getModel();
        $prodDt = $prodDt->whereIn('status', ['pembelian', 'mutasi tambah'])->take($limit)->orderBy('product_d_id', 'ASC')->get();
        $prodDt = $prodDt->where('product.wh_id', $request->wh_id)->flatten();
        if ($request->has('product_id')) {
            $prodDt = $prodDt->where('product_id', $request->product_id)->flatten();
        }
        $prodDt->each(function ($item) use ($data) {
            if ($item->cur_qty_lg > 0 || $item->cur_qty_sm > 0) {
                $args = $item->only(['status', 'ref', 'batch', 'unit_lg', 'unit_sm', 'cur_qty_lg', 'cur_qty_sm', 'price_lg', 'price_sm']);
                $args = array_merge($args, [
                    'product' => $item->product->only(['product_id', 'code', 'name', 'min_qty'])
                ]);
                $data->push($args);
            }
        });
        return $this->success(null, 200, $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $product_id
     * @return \Illuminate\Http\Response
     */    
    public function updateStock($product_id)
    {
        $product = $this->product->show($product_id);
        $price = $product->productDetails->where('status', 'pembelian')->flatten();
        $price_lg = $price->sortByDesc('price_lg')->first()->price_lg;
        $price_sm = $price->sortByDesc('price_sm')->first()->price_sm;
        $total_lg_qty = $product->productDetails->sum('cur_qty_lg'); 
        $total_sm_qty = $product->productDetails->sum('cur_qty_sm');
        if ($product->pack && $product->pack_qty) {
            $sm2lg = sm2lg($product->product_id, $total_sm_qty);
            $total_lg_qty = $total_lg_qty + $sm2lg['qty_lg']; 
            $total_sm_qty = $sm2lg['qty_sm'];
        }
        return $product->update(compact('total_lg_qty', 'total_sm_qty', 'price_lg', 'price_sm'));
    }
    
    public function codeProduct(array $request)
    {
        $now = \Carbon\Carbon::now();
        $model = $this->product->getModel();
        $format = 'PROD [CV] [MM][YY].[NO]';
        // =====
        $wh_id = isset($request['wh_id']) ? $request['wh_id'] : null;
        $comp_code = isset($request['comp_code']) ? $request['comp_code'] : null;
        // =====
        $code_MM = $now->format('m');
        $code_YY = $now->format('y');
        $code_CV = $comp_code;
        $noUrut = $model->where('wh_id', $wh_id)->whereMonth('created_at', $now->format('m'))->orderBy('product_id', 'DESC')->get();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('code')->first();
            preg_match('/PROD '.$code_CV.' (\d{4}).(\d{2})/', $noUrut, $match);
            $noUrut = $match[2] + 1;
            $noUrut = str_pad($noUrut, 2, '0', STR_PAD_LEFT);
        } else {
            $noUrut = str_pad(1, 2, '0', STR_PAD_LEFT);
        }
        $code_NO = $noUrut;
        
        return str_replace(
            array('[CV]', '[NO]', '[MM]', '[YY]'),
            array($code_CV, $code_NO, $code_MM, $code_YY),
            $format
        );
    }
}
