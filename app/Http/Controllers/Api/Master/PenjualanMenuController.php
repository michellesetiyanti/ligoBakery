<?php

namespace App\Http\Controllers\Api\Master;

use App\Models\Master\PenjualanMenu;
use App\Models\Master\PenjualanMenuDetail;
use App\Models\Master\MenuJual;
use App\Models\Auth\User;
use App\Models\Master\JenisPembayaran;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\Controller as ApiController;

class PenjualanMenuController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function initialize(\Illuminate\Http\Request $request)
    {
        $user_id = $request->userid;
        $count = $request->count;

        $penjualandata = PenjualanMenu::where('user_id',$user_id)->with(['penjualanDetails','jenisPembayaran'])->paginate($count);
        
        $datamenu = MenuJual::all();
        $jenispembayaran = JenisPembayaran::with(['categoryPembayaran','coa'])->get();
        

        $array = array(
            'penjualan_data'=>$penjualandata,
            'menu'=>$datamenu,
            'jenis_pembayaran'=>$jenispembayaran
        );

        return $this->success(null, 200, $array);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = $request->user_id;
        $userdata = User::where('user_id',$user_id)->with('company')->first();
        $companyid = $userdata->company->company_id;

        $jenis_pembayaran_id = $request->jenis_pembayaran_id;

        $datapayment = JenisPembayaran::where('jenis_pembayaran_id',$jenis_pembayaran_id)->first();
        $coa_code = $datapayment->coa_code;

        \DB::beginTransaction();
        try {
            $PJMenu = new PenjualanMenu;
            $PJMenu->user_id = $user_id;
            $PJMenu->jenis_pembayaran_id = $jenis_pembayaran_id;
            $PJMenu->date = $request->date;
            $PJMenu->nomor_penjualan_menu = $request->nomor_penjualan_menu;
            $PJMenu->note = $request->note;
            $PJMenu->subtotal = $request->subtotal;
            $PJMenu->tax = $request->tax;
            $PJMenu->discount = $request->discount;
            $PJMenu->grandtotal = $request->grandtotal;
            $PJMenu->save();

            $idPJ = $PJMenu->penjualan_menu_id;

            foreach ($request->penjualan_details as $key => $value) {
                $id_menu_jual = $value['id_menu_jual'];
                $qty = $value['qty'];


                $produksidet = new PenjualanMenuDetail;
                $produksidet->penjualan_menu_id = $idPJ;
                $produksidet->id_menu_jual = $id_menu_jual;
                $produksidet->note = $value['note'];
                $produksidet->qty = $qty;
                $produksidet->price = $value['price'];
                $produksidet->discount = $value['discount'];
                $produksidet->subtotal = $value['subtotal'];
                $produksidet->save();

                $datamenujual = MenuJual::where('id_menu_jual',$id_menu_jual)->first();
                $qtymenujual = $datamenujual->qty;
                $qtysisa = $qtymenujual - $qty;
                $array=array(
                    'qty'=> $qtysisa
                );
                MenuJual::where('id_menu_jual',$id_menu_jual)->update($array);

            }
            $cekpost = postpenjualan($companyid, "Cash", "Menu $idPJ", $request->subtotal, $request->tax,  $request->discount, $request->grandtotal);
            if($cekpost != "success"){
                return $cekpost;
            }
            $cekpost2 = postgl($companyid, $coa_code, "Menu $idPJ", "Penjualan Menu $idPJ", $request->grandtotal);
            if($cekpost2['status'] =="failed") {
                $array = array(
                    "msg"=>"failed post payment",
                    'data'=>$cekpost2
                );
                return $array;
            }
           
            \DB::commit();
            return $this->success("Data Add Successfully", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PenjualanMenu  $penjualanMenu
     * @return \Illuminate\Http\Response
     */
    public function show(PenjualanMenu $penjualanMenu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PenjualanMenu  $penjualanMenu
     * @return \Illuminate\Http\Response
     */
    public function edit(PenjualanMenu $penjualanMenu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PenjualanMenu  $penjualanMenu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $penjualan_menu_id = $request->penjualan_menu_id;
        $user_id = $request->user_id;
        $userdata = User::where('user_id',$user_id)->with('company')->first();
        $companyid = $userdata->company->company_id;

        $jenis_pembayaran_id = $request->jenis_pembayaran_id;
        $selisih = $request->selisih;

        $datapayment = JenisPembayaran::where('jenis_pembayaran_id',$jenis_pembayaran_id)->first();
        $coa_code = $datapayment->coa_code;

        \DB::beginTransaction();
        try {

            $arraypenjualan = array(
                'user_id'=>$user_id,
                'nomor_penjualan_menu'=>$request->nomor_penjualan_menu,
                'note'=>$request->note,
                'date'=>$request->date,
                'subtotal'=>$request->subtotal,
                'tax'=>$request->tax,
                'discount'=>$request->discount,
                'grandtotal'=>$request->grandtotal,
            );
            PenjualanMenu::where('penjualan_menu_id',$penjualan_menu_id)->update($arraypenjualan);
             
            $dataawal = PenjualanMenuDetail::where('penjualan_menu_id',$penjualan_menu_id)->get();
            foreach ($dataawal as $key => $value) { //balikan qty smua menu ke awal
                $id_menu_jual = $value->id_menu_jual;
                $qty = $value->qty;

                $datamenujual = MenuJual::where('id_menu_jual',$id_menu_jual)->first();
                $qtymenujual = $datamenujual->qty;
                $qtyawal = $qtymenujual + $qty;
                $array=array(
                    'qty'=> $qtyawal
                );
                MenuJual::where('id_menu_jual',$id_menu_jual)->update($array);
                
            }
            
            PenjualanMenuDetail::where('penjualan_menu_id',$penjualan_menu_id)->delete();    //hapus data lama

            foreach ($request->penjualan_details as $key => $value) { //kurangi dengan qty baru
                $id_menu_jual = $value['id_menu_jual'];
                $qty = $value['qty'];


                $produksidet = new PenjualanMenuDetail;
                $produksidet->penjualan_menu_id = $penjualan_menu_id;
                $produksidet->id_menu_jual = $id_menu_jual;
                $produksidet->note = $value['note'];
                $produksidet->qty = $qty;
                $produksidet->price = $value['price'];
                $produksidet->discount = $value['discount'];
                $produksidet->subtotal = $value['subtotal'];
                $produksidet->save();


               
                $datamenujual = MenuJual::where('id_menu_jual',$id_menu_jual)->first();
                $qtymenujual = $datamenujual->qty;
                $qtysisa = $qtymenujual - $qty;
                $array=array(
                    'qty'=> $qtysisa
                );
                MenuJual::where('id_menu_jual',$id_menu_jual)->update($array);
            }

            $cekrevisi = revisipostpenjualan($companyid, "cash", "Menu $penjualan_menu_id", $request->subtotal, $request->tax, $request->discount, $request->grandtotal);
            if($cekrevisi != "success"){
                return $cekpost;
            }

            if($selisih > 0){
                $cekpost2 = postgl($companyid, $coa_code, "Menu $penjualan_menu_id", "Penjualan Menu $penjualan_menu_id", $request->grandtotal);
                if($cekpost2['status'] =="failed") {
                    $array = array(
                        "msg"=>"failed post payment",
                        'data'=>$cekpost2
                    );
                    return $array;
                }
            }else{
                $cekpay = postpaid($companyid, $coa_code, $penjualan_menu_id, "Refund Menu $penjualan_menu_id", $selisih);
                if($cekpay['status'] =="failed") {
                    $array = array(
                        "msg"=>"failed post payment",
                        'data'=>$cekpay
                    );
                    return $array;
                }
            }
            

            
           
            \DB::commit();
            return $this->success("Edit Penjualan Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PenjualanMenu  $penjualanMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $penjualan_menu_id = $request->penjualan_menu_id;
        $user_id = $request->user_id;
        $userdata = User::where('user_id',$user_id)->with('company')->first();
        $companyid = $userdata->company->company_id;

        $datapenjualanmenu = PenjualanMenu::where('penjualan_menu_id',$penjualan_menu_id)->first();
        $subtotal = $datapenjualanmenu->subtotal;
        $tax = $datapenjualanmenu->tax;
        $discount = $datapenjualanmenu->discount;
        $grandtotal = $datapenjualanmenu->grandtotal;

        $jenis_pembayaran_id = $request->jenis_pembayaran_id;

        $datapayment = JenisPembayaran::where('jenis_pembayaran_id',$jenis_pembayaran_id)->first();
        $coa_code = $datapayment->coa_code;
        \DB::beginTransaction();
        try {
            
            $dataawal = PenjualanMenuDetail::where('penjualan_menu_id',$penjualan_menu_id)->get();
            foreach ($dataawal as $key => $value) { //balikan qty smua menu ke awal
                $id_menu_jual = $value->id_menu_jual;
                $qty = $value->qty;

                $datamenujual = MenuJual::where('id_menu_jual',$id_menu_jual)->first();
                $qtymenujual = $datamenujual->qty;
                $qtyawal = $qtymenujual + $qty;
                $array=array(
                    'qty'=> $qtyawal
                );
                MenuJual::where('id_menu_jual',$id_menu_jual)->update($array);
            }

            PenjualanMenu::where('penjualan_menu_id',$penjualan_menu_id)->delete();
            PenjualanMenuDetail::where('penjualan_menu_id',$penjualan_menu_id)->delete();

            $cekdeletpost = voidpostpenjualan($companyid, "cash", "Menu $penjualan_menu_id", $subtotal, $tax, $discount, $grandtotal);
            if($cekdeletpost != "success"){
                return $cekdeletpost;
            }

            $cekpay = postpaid($companyid, $coa_code, "Menu $penjualan_menu_id", "Refund Menu $penjualan_menu_id", $grandtotal);
            if($cekpay['status'] =="failed") {
                $array = array(
                    "msg"=>"failed post payment",
                    'data'=>$cekpay
                );
                return $array;
            }

            \DB::commit();
            return $this->success("Delete Penjualan Success", 200, null);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
