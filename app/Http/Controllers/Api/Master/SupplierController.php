<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Master\Supplier as SupplierRequest;
use App\Repositories\Master\Supplier as SupplierRepository;

class SupplierController extends ApiController
{
    protected $supplier;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->supplier = new SupplierRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $supplier = $this->supplier->with(['poPembelian', 'pembelian'])->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $supplier);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SupplierRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SupplierRequest $request)
    {
        \DB::beginTransaction();
        try {
            $code = $this->codeSupplier([
                'region' => $request->region,
                'code_type' => $request->code_type,
            ]);
            $supplier = $this->supplier->save($request->merge([
                'code' => $code,
                'currency_code' => $request->currency_code ?? 'IDR',
                'status' => $request->status ?? 'active'
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $supplier);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($supplier = $this->supplier->with(['poPembelian', 'pembelian'])->show($id)) {
            return $this->success(null, 200, $supplier);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SupplierRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function update(SupplierRequest $request, $id)
    {
        $supplier = $this->supplier->show($id);
        \DB::beginTransaction();
        try{
            $code = $supplier->code;
            if (empty($code)) {
                $code = $this->codeSupplier([
                    'region' => $request->region,
                    'code_type' => $request->code_type,
                ], $id);
            }
            $supplier->update($request->merge([
                'id' => $id,
                'code' => $code,
                'currency_code' => $request->currency_code ?? 'IDR',
                'status' => $request->status ?? 'active'
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $supplier);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = $this->supplier->show($id);
        \DB::beginTransaction();
        try {
            if ($supplier->poPembelian->isEmpty() && $supplier->pembelian->isEmpty()) {
                $supplier->delete();
            } else {
                $supplier->update(['status' => 'inactive']);
            }
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Import.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
            'currency_code' => 'sometimes|required|string',
            'attachment.*' => 'file|image|mimes:jpeg,png,jpg,doc,ppt,xls,pdf|max:2048',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        // Supplier
        $supplier = $this->supplier->getModel();
        $supplierByName = $supplier->where('name', $request->name)->first();
        if (empty($supplierByName)) {
            $supplierByName = $supplier->create([
                'name' => $request->name,
                'currency_code' => $request->currency_code ?? 'IDR',
            ]);
        }
        try {
            $supplierByName->import = $request->attachment;
            $supplierByName->save();
            return $this->success(null, 200, $supplierByName->import);
        } catch (\Exception $e) {
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    
    private function codeSupplier(array $request, $id = 0)
    {
        $now = \Carbon\Carbon::now();
        $auth = auth()->user();
        $model = $this->supplier->getModel();
        $format = '[CS]-[CV] [NO].[DS]';
        // =====
        // Kode Supplier
        // =============
        // SPL = Sipil
        // INT = Interior
        // ME = Mekanikal
        // PL = Plambing
        $type = isset($request['code_type']) ? $request['code_type'] : null;
        $district = isset($request['region']) ? $request['region'] : null;
        // =====
        $code_DS = $district;
        $code_CS = $type;
        $code_CV = $auth->company->code_alpha ?? str_pad($auth->company_id, 5, '0', STR_PAD_LEFT);
        $noUrut = $model->where('company_id', $auth->company_id)->where('supplier_id', '<>', $id)->where('region', $district)->orderBy('supplier_id', 'DESC')->get();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('code')->first();
            preg_match('/'.$code_CV.' (\d{3}).'.$code_DS.'/', $noUrut, $match);
            $noUrut = $match[1] + 1;
            $noUrut = str_pad($noUrut, strlen($noUrut) > 3 ? strlen($noUrut) : 3, '0', STR_PAD_LEFT);
        } else {
            $noUrut = str_pad(1, 3, '0', STR_PAD_LEFT);
        }
        $code_NO = $noUrut;
        
        return str_replace(
            array('[NO]', '[CS]', '[CV]', '[DS]'),
            array($code_NO, $code_CS, $code_CV, $code_DS),
            $format
        );
    }
}
