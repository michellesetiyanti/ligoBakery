<?php

namespace App\Http\Controllers\Api\Master;

use App\Models\Master\MenuJual;
use App\Models\Master\MenuSisaJual;
use App\Models\Master\MenuSisaJualDetail;
use Illuminate\Http\Request;
use App\Repositories\Master\MenuSisaJual as MenuSisaJualRepository;
use App\Http\Controllers\Api\Controller as ApiController;

class MenuSisaJualController extends ApiController
{
    protected $menusisajual;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->menusisajual = new MenuSisaJualRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(\Illuminate\Http\Request $request)
    {
         switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $sorting = 'company_id|DESC';
        $sorting = $request->sorting ? $request->sorting : $sorting;
        $menusisajual = $this->menusisajual->with(['companies', 'users','menuSisaDetails'])->all($request->merge(compact('filter', 'sorting'))->all());
        return $this->success(null, 200, $menusisajual);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid = auth()->user()->user_id;
        $company_id = auth()->user()->company_id;
        $date = $request->date;
        $id_menu_jual = $request->id_menu_jual;
        $qty = $request->qty;

         \DB::beginTransaction();
        try {
            $menusisa = new MenuSisaJual;
            $menusisa->company_id = $company_id;
            $menusisa->user_id = $userid;
            $menusisa->date = $date;
            $menusisa->status = 'open';
            $menusisa->save();

            $idmenusisajual = $menusisa->id_menu_sisa_jual;

            $menusisadetail = new MenuSisaJualDetail;
            $menusisadetail->id_menu_sisa_jual = $idmenusisajual;
            $menusisadetail->id_menu_jual = $id_menu_jual;
            $menusisadetail->qty = $qty;
            $menusisadetail->save();


            \DB::commit();
            return $this->success(null, 200, $menusisa);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function storedetails(Request $request)
    {
        $idmenusisajual = $request->id_menu_sisa_jual;
        $id_menu_jual = $request->id_menu_jual;
        $qty = $request->qty;

         \DB::beginTransaction();
        try {
           
            $cek = MenuSisaJualDetail::where('id_menu_sisa_jual',$idmenusisajual)->where('id_menu_jual',$id_menu_jual)->count();
            if($cek != 0){
                return $this->fail("Post Error", 500, "Data Input Tidak Boleh Double");
            }

            $menusisadetail = new MenuSisaJualDetail;
            $menusisadetail->id_menu_sisa_jual = $idmenusisajual;
            $menusisadetail->id_menu_jual = $id_menu_jual;
            $menusisadetail->qty = $qty;
            $menusisadetail->save();

            \DB::commit();
            return $this->success(null, 200, $menusisadetail);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\MenuSisaJual  $menuSisaJual
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MenuSisaJual::where('id_menu_sisa_jual',$id)->with(['companies', 'users','menuSisaDetails'])->first();
        return $this->success(null, 200, $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MenuSisaJual  $menuSisaJual
     * @return \Illuminate\Http\Response
     */
    public function edit(MenuSisaJual $menuSisaJual)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MenuSisaJual  $menuSisaJual
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
    {
        
    }

    public function updatedetails(Request $request, $id)
    {
        $qty = $request->qty;

         \DB::beginTransaction();
        try {
            if($qty <= 0){
                MenuSisaJualDetail::where('id_menu_sisa_jual_detail','=',$id)->delete();
            }else{
                $array = array(
                    'qty'=>$qty,
                );
                MenuSisaJualDetail::where('id_menu_sisa_jual_detail','=',$id)->update($array);
            }
          
            \DB::commit();
            return $this->success("Update Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function submit(Request $request, $id)
    {

         \DB::beginTransaction();
        try {

            $array = array(
                'status'=>'complete',
            );
            MenuSisaJual::where('id_menu_sisa_jual','=',$id)->update($array);

            $data = MenuSisaJual::where('id_menu_sisa_jual',$id)->with(['companies', 'users','menuSisaDetails'])->first();
            $details = $data->menuSisaDetails;
            foreach ($details as $key => $value) {
                
                $id_menu_jual = $value->id_menu_jual;
                $qty = $value->qty;
                
                $cekmenujual = MenuJual::where('id_menu_jual',$id_menu_jual)->first();
                $menuname = $cekmenujual->menu;
                $qtyawal = $cekmenujual->qty;
                
                $qtyakhir = $qtyawal-$qty;

                if($qtyakhir <= 0){
                    return $this->fail("Post Error", 500, "Data Menu $menuname Sudah nol");
                }

                $arrayup  = array(
                    'qty'=>$qtyakhir
                );
                MenuJual::where('id_menu_jual',$id_menu_jual)->update($arrayup);

            }

            \DB::commit();
            return $this->success("Data Submitted", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MenuSisaJual  $menuSisaJual
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {

            MenuSisaJualDetail::where('id_menu_sisa_jual','=',$id)->delete();
            MenuSisaJual::where('id_menu_sisa_jual','=',$id)->delete();

            \DB::commit();
            return $this->success("Delete Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function destroydetail($id)
    {
        \DB::beginTransaction();
        try {

            MenuSisaJualDetail::where('id_menu_sisa_jual_detail','=',$id)->delete();

            \DB::commit();
            return $this->success("Delete Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
