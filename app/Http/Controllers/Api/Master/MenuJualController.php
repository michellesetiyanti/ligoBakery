<?php

namespace App\Http\Controllers\Api\Master;

use App\Models\Master\MenuJual;
use App\Models\Master\Recipe;
use App\Models\Master\RecipeDetail;
use App\Repositories\Master\MenuJual as MenuJualRepository;
use App\Http\Controllers\Api\Controller as ApiController;
use Illuminate\Http\Request;

class MenuJualController extends ApiController
{
    protected $menujual;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->menujual = new MenuJualRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
         switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $sorting = 'company_id|DESC';
        $sorting = $request->sorting ? $request->sorting : $sorting;
        $menuJual = $this->menujual->with(['companies', 'recipes'])->all($request->merge(compact('filter', 'sorting'))->all());
        return $this->success(null, 200, $menuJual);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menuname = $request->menuname;
        $menuprice = $request->menuprice;

         \DB::beginTransaction();
        try {
            $menujual = new MenuJual;
            $menujual->menu = $menuname;
            $menujual->price = $menuprice;
            $menujual->save();

            $menuid = $menujual->id_menu_jual;

            \DB::commit();
            return $this->success(null, 200, $menuid);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function storerecipe(Request $request)
    {
        $idproduct = $request->idproduct;
        $idmenu = $request->idmenu;
        $qty = $request->qty;

         \DB::beginTransaction();
        try {
            $recipedetail = new RecipeDetail;
            $recipedetail->id_menu_jual = $idmenu;
            $recipedetail->product_id = $idproduct;
            $recipedetail->qty = $qty;
            $recipedetail->save();

            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MenuJual  $menuJual
     * @return \Illuminate\Http\Response
     */
    public function show(MenuJual $menuJual)
    {
        //
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MenuJual  $menuJual
     * @return \Illuminate\Http\Response
     */
    public function edit(MenuJual $menuJual)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MenuJual  $menuJual
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $menuname = $request->menuname;
         $menuprice = $request->menuprice;

         \DB::beginTransaction();
        try {

            $array = array(
                'menu'=>$menuname,
                'price'=>$menuprice,
            );
            MenuJual::where('id_menu_jual','=',$id)->update($array);

            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }


    public function updatedetails(Request $request, $id)
    {
        $qty = $request->qty;

         \DB::beginTransaction();
        try {

            $array = array(
                'qty'=>$qty,
            );
            RecipeDetail::where('id_recipe_details','=',$id)->update($array);

            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MenuJual  $menuJual
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $datamenu = MenuJual::where('id_menu_jual',$id)->first();
        $qty = $datamenu->qty;
         \DB::beginTransaction();
        try {

            if($qty != 0){
                $this->fail("Post Error",500,'Data Cant be Deleted!');
            }else{
                MenuJual::where('id_menu_jual','=',$id)->delete();
                RecipeDetail::where('id_menu_jual','=',$id)->delete();
            }

            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

     public function deletedetail($id)
    {

         \DB::beginTransaction();
        try {

           
            RecipeDetail::where('id_recipe_details','=',$id)->delete();

            \DB::commit();
            return $this->success(null, 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
