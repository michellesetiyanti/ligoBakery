<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Master\Customer as CustomerRequest;
use App\Repositories\Master\Customer as CustomerRepository;
use App\Models\Master\Customer as CUST;
use File;
use Illuminate\Support\Facades\Storage;

class CustomerController extends ApiController
{
    protected $customer;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->customer = new CustomerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $customer = $this->customer->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $customer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        \DB::beginTransaction();
        try {
            $customer = $this->customer->save($request->all());
            \DB::commit();
            return $this->success(null, 200, $customer);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($customer = $this->customer->show($id)) {
            return $this->success(null, 200, $customer);
        } else {
            return $this->notFound(null);
        }
    }
    
    public function getfile($id){
            $path = "public/customer/$id";  

            $cekfile = Storage::files($path);
            $ct = count($cekfile); 
            $allfile=[];
            for($i=0; $i < $ct ; $i++) {
                $name = $cekfile[$i];
                $total = strlen($name);
                $dt= substr($name , 7, $total);
                $allfile[$i]=$dt;   
            }
 
            $data = array(
                'status'=>"success",
                'message'=>"Data loaded successfully", 
                'file'=>$allfile, 
            );
            return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CustomerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function update(CustomerRequest $request, $id)
    {
        $customer = $this->customer->show($id);
        \DB::beginTransaction();
        try{
            $customer->update($request->merge([
                'id' => $id,
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $customer);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = $this->customer->show($id);
        \DB::beginTransaction();
        try {
            if ($customer->orders->isEmpty() || $customer->penjualan->isEmpty()) {
                $customer->delete();
            } else {
                $customer->update(['status' => 'inactive']);
            }
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Import.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
            'currency_code' => 'sometimes|required|string',
            'attachment.*' => 'file|image|mimes:jpeg,png,jpg,doc,ppt,xls,pdf|max:2048',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        // Customer
        $customer = $this->customer->getModel();
        $customerByName = $customer->where('name', $request->name)->first();
        if (empty($customerByName)) {
            $customerByName = $customer->create([
                'name' => $request->name,
                'currency_code' => $request->currency_code ?? 'IDR',
            ]);
        }
        try {
            $customerByName->import = $request->attachment;
            $customerByName->save();
            return $this->success(null, 200, $customerByName->import);
        } catch (\Exception $e) {
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    public function uploadktp(\Illuminate\Http\Request $request)
    {   
        $cust_id = $request->id; 
        $files = $request->file('ktpcust');
        $extension = $request->file('ktpcust')->extension(); 
        $cust= CUST::where('customer_id',$cust_id)->first();  

        // //custom name masing2 file
        $filename = "KTP-$cust_id.$extension";
        $path = "public/customer/$cust_id";
        //upload file 
        if (!file_exists($path)) {
            // path does not exist deletefilerab
            Storage::makeDirectory($path);
            $files->storeAs($path,$filename);
        }else{
            $files->storeAs($path,$filename); 
        }

        $cekfile = Storage::files($path);
        return $cekfile;
    }

    public function deletefile(\Illuminate\Http\Request $request)
    {   
        $dat = $request->pat;
        $path = "public/customer/$dat";
        Storage::delete($path);
       
    }
}
