<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Master\Tukang as TukangRequest;
use App\Repositories\Master\Tukang as TukangRepository;
use File;
use Illuminate\Support\Facades\Storage;

class TukangController extends ApiController
{
    protected $tukang;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->tukang = new TukangRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $tukang = $this->tukang->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $tukang);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TukangRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TukangRequest $request)
    {
        \DB::beginTransaction();
        try {
            $ttlFormat = '%CRADLE%, %BORN%';
            $ttl = str_replace(array(
                '%CRADLE%',
                '%BORN%'
            ), array(
                $request->cradle ?? 'NULL',
                $request->born ?? 'NULL',
            ), $ttlFormat);
            $induk_tukang = $this->codeTukang([
                'penempatan' => $request->penempatan,
                'born' => $request->born,
                'entry_at' => \Carbon\Carbon::createFromFormat('d/m/Y', $request->entry_at)->format('d-m-Y'),
            ]);
            $tukang = $this->tukang->save($request->merge([
                'upah' => $request->upah ?? 0,
                'ttl' => $ttl,
                'induk_tukang' => $induk_tukang,
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $tukang);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($tukang = $this->tukang->show($id)) {
            return $this->success(null, 200, $tukang);
        } else {
            return $this->notFound(null);
        }
    }

    public function getfile($id){
            $pathKTP = "public/tukang/$id/KTP";   

            $cekfile = Storage::files($pathKTP);
            $ct = count($cekfile); 
            $allfile=[];
            for($i=0; $i < $ct ; $i++) {
                $name = $cekfile[$i];
                $total = strlen($name);
                $dt= substr($name , 7, $total);
                $allfile[$i]=$dt;   
            } 
 
            $data = array(
                'status'=>"success",
                'message'=>"Data loaded successfully", 
                'fileKTP'=>$allfile,  
            );
            return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TukangRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function update(TukangRequest $request, $id)
    {
        $tukang = $this->tukang->show($id);
        \DB::beginTransaction();
        try{
            $ttlFormat = '%CRADLE%, %BORN%';
            $ttl = str_replace(array(
                '%CRADLE%',
                '%BORN%'
            ), array(
                $request->cradle ?? 'NULL',
                $request->born ?? 'NULL',
            ), $ttlFormat);
            $induk_tukang = $tukang->induk_tukang;
            if (empty($induk_tukang)) {
                $induk_tukang = $this->codeTukang([
                    'penempatan' => $request->penempatan,
                    'born' => $request->born,
                    'entry_at' => \Carbon\Carbon::createFromFormat('d/m/Y', $request->entry_at)->format('d-m-Y'),
                ], $id);
            }
            $tukang->update($request->merge([
                'id' => $id,
                'upah' => $request->upah ?? 0,
                'ttl' => $ttl,
                'induk_tukang' => $induk_tukang,
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $tukang);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tukang = $this->tukang->show($id);
        \DB::beginTransaction();
        try {
            $tukang->delete();
            \DB::commit();
            return $this->success(null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function uploadktp(\Illuminate\Http\Request $request)
    {   
        $id = $request->id; 
        $files = $request->file('ktptukang');
        $extension = $request->file('ktptukang')->extension(); 

        // //custom name masing2 file
        $filename = "KTP-$id.$extension";
        $path = "public/tukang/$id/KTP";
        //upload file 
        if (!file_exists($path)) {
            // path does not exist deletefilerab
            Storage::makeDirectory($path);
            $files->storeAs($path,$filename);
        }else{
            $files->storeAs($path,$filename); 
        }

        $cekfile = Storage::files($path);
        return $cekfile;
    }

    public function deletefile(\Illuminate\Http\Request $request)
    {   
        $dat = $request->pat;
        $path = "public/$dat";
        Storage::delete($path);
       
    }
    
    private function codeTukang(array $request, $id = 0)
    {
        $now = \Carbon\Carbon::now();
        $auth = auth()->user();
        $model = $this->tukang->getModel();
        $format = 'PHL [CV] [NO].[DS].[BM][BY].[JM][JY]';
        // =====
        $district = isset($request['penempatan']) ? $request['penempatan'] : null;
        $born = isset($request['born']) ? \Carbon\Carbon::createFromFormat("d-m-Y", $request['born']) : null;
        $join = isset($request['entry_at']) ? \Carbon\Carbon::createFromFormat("d-m-Y", $request['entry_at']) : null;
        // =====
        $code_CV = $auth->company->code_alpha ?? str_pad($auth->company_id, 5, '0', STR_PAD_LEFT);
        $code_DS = $district;
        $code_BM = empty($born) ? null : $born->format('m');
        $code_BY = empty($born) ? null : $born->format('y');
        $code_JM = empty($join) ? $now->format('m') : $join->format('m');
        $code_JY = empty($join) ? $now->format('y') : $join->format('y');
        $noUrut = $model->where('company_id', $auth->company_id)->where('tukang_id', '<>', $id)->where('penempatan', $district)->whereMonth('entry_at', empty($join) ? $now->format('m') : $join->format('m'))->orderBy('tukang_id', 'DESC')->get();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('induk_tukang')->first();
            preg_match('/PHL '.$code_CV.' (\d{2}).'.$code_DS.'.'.$code_BM.$code_BY.'/', $noUrut, $match);
            $noUrut = $match[1] + 1;
            $noUrut = str_pad($noUrut, 2, '0', STR_PAD_LEFT);
        } else {
            $noUrut = str_pad(1, 2, '0', STR_PAD_LEFT);
        }
        $code_NO = $noUrut;
        
        return str_replace(
            array('[NO]', '[CV]', '[DS]', '[BM]', '[BY]', '[JM]', '[JY]'),
            array($code_NO, $code_CV, $code_DS, $code_BM, $code_BY, $code_JM, $code_JY),
            $format
        );
    }
}
