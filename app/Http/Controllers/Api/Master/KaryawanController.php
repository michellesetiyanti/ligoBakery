<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Master\Karyawan as KaryawanRequest;
use App\Repositories\Master\Karyawan as KaryawanRepository;
use App\Models\Master\Karyawan as KARYAWAN;
use File;
use Illuminate\Support\Facades\Storage;

class KaryawanController extends ApiController
{
    protected $karyawan;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->karyawan = new KaryawanRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $karyawan = $this->karyawan->with('user')->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $karyawan);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  KaryawanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(KaryawanRequest $request)
    {
        \DB::beginTransaction();
        try {
            $ttlFormat = '%CRADLE%, %BORN%';
            $ttl = str_replace(array(
                '%CRADLE%',
                '%BORN%'
            ), array(
                $request->cradle ?? 'NULL',
                $request->born ?? 'NULL',
            ), $ttlFormat);
            $induk_karyawan = $this->codeKaryawan([
                'penempatan' => $request->penempatan,
                'born' => $request->born,
                'datejoin' => $request->datejoin,
                'status_karyawan' => $request->status_karyawan,
            ]);
            $karyawan = $this->karyawan->save($request->merge([
                'ttl' => $ttl,
                'emailperusahaan' => $request->email_perusahaan,
                'induk_karyawan' => $induk_karyawan,
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $karyawan);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($karyawan = $this->karyawan->with('user')->show($id)) {
            return $this->success(null, 200, $karyawan);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  KaryawanRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function update(KaryawanRequest $request, $id)
    {
        $karyawan = $this->karyawan->show($id);
        \DB::beginTransaction();
        try{
            $ttlFormat = '%CRADLE%, %BORN%';
            $ttl = str_replace(array(
                '%CRADLE%',
                '%BORN%'
            ), array(
                $request->cradle ?? 'NULL',
                $request->born ?? 'NULL',
            ), $ttlFormat);
            $induk_karyawan = $karyawan->induk_karyawan;
            if (empty($induk_karyawan)) {
                $induk_karyawan = $this->codeKaryawan([
                    'penempatan' => $request->penempatan,
                    'born' => $request->born,
                    'datejoin' => $request->datejoin,
                    'status_karyawan' => $request->status_karyawan,
                ], $id);
            }
            $karyawan->update($request->merge([
                'id' => $id,
                'ttl' => $ttl,
                'emailperusahaan' => $request->email_perusahaan,
                'induk_karyawan' => $induk_karyawan,
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $karyawan);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $karyawan = $this->karyawan->show($id);
        \DB::beginTransaction();
        try {
            if ($karyawan->penjualan->isEmpty()) {
                $karyawan->delete();
            } else {
                $karyawan->update(['status' => 'inactive']);
            }
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Import.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function photo(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
            'position' => 'required|string|min:2|max:20',
            'status' => 'required|string|in:active,inactive',
            'attachment.*' => 'file|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        $karyawan = $this->karyawan->all()->where('name', $request->name)->where('position', $request->position)->where('status', $request->status);
        if ($karyawan->isEmpty()) {
            $karyawan = $this->karyawan->save([
                'name' => $request->name,
                'position' => $request->position,
                'status' => $request->status,
            ]);
        } else {
            $karyawan = $karyawan->first();
        }
        try {
            $karyawan->photo = $request->attachment;
            $karyawan->save();
            return $this->success(null, 200, $karyawan->photo);
        } catch (\Exception $e) {
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function getfile($id){
            $pathKTP = "public/karyawan/$id/KTP";  
            $pathNPWP = "public/karyawan/$id/NPWP";  
            $pathijazah = "public/karyawan/$id/ijazah";  
            $pathfoto = "public/karyawan/$id/foto";  
            $pathmisc = "public/karyawan/$id/misc";  

            $cekfile = Storage::files($pathKTP);
            $ct = count($cekfile); 
            $allfile=[];
            for($i=0; $i < $ct ; $i++) {
                $name = $cekfile[$i];
                $total = strlen($name);
                $dt= substr($name , 7, $total);
                $allfile[$i]=$dt;   
            }

            $cekfilenpwp = Storage::files($pathNPWP);
            $ctnpwp = count($cekfilenpwp); 
            $allfilenpwp=[];
            for($i=0; $i < $ctnpwp ; $i++) {
                $name = $cekfilenpwp[$i];
                $total = strlen($name);
                $dt= substr($name , 7, $total);
                $allfilenpwp[$i]=$dt;   
            }

            $cekfileijazah= Storage::files($pathijazah);
            $ctijazah = count($cekfileijazah); 
            $allfileijazah=[];
            for($i=0; $i < $ctijazah ; $i++) {
                $name = $cekfileijazah[$i];
                $total = strlen($name);
                $dt= substr($name , 7, $total);
                $allfileijazah[$i]=$dt;   
            }

            $cekfilefoto= Storage::files($pathfoto);
            $ctfoto = count($cekfilefoto); 
            $allfilefoto=[];
            for($i=0; $i < $ctfoto ; $i++) {
                $name = $cekfilefoto[$i];
                $total = strlen($name);
                $dt= substr($name , 7, $total);
                $allfilefoto[$i]=$dt;   
            }

            $cekfilemisc= Storage::files($pathmisc);
            $ctmisc = count($cekfilemisc); 
            $allfilemisc=[];
            for($i=0; $i < $ctmisc ; $i++) {
                $name = $cekfilemisc[$i];
                $total = strlen($name);
                $dt= substr($name , 7, $total);
                $allfilemisc[$i]=$dt;   
            }
 
            $data = array(
                'status'=>"success",
                'message'=>"Data loaded successfully", 
                'fileKTP'=>$allfile, 
                'fileNPWP'=>$allfilenpwp, 
                'fileijazah'=>$allfileijazah, 
                'filefoto'=>$allfilefoto, 
                'filemisc'=>$allfilemisc, 
            );
            return $data;
    }

    public function uploadktp(\Illuminate\Http\Request $request)
    {   
        $id = $request->id; 
        $files = $request->file('ktpempl');
        $extension = $request->file('ktpempl')->extension(); 

        // //custom name masing2 file
        $filename = "KTP-$id.$extension";
        $path = "public/karyawan/$id/KTP";
        //upload file 
        if (!file_exists($path)) {
            // path does not exist deletefilerab
            Storage::makeDirectory($path);
            $files->storeAs($path,$filename);
        }else{
            $files->storeAs($path,$filename); 
        }

        $cekfile = Storage::files($path);
        return $cekfile;
    }

    public function uploadnpwp(\Illuminate\Http\Request $request)
    {   
        $id = $request->id; 
        $files = $request->file('npwpempl');
        $extension = $request->file('npwpempl')->extension(); 

        // //custom name masing2 file
        $filename = "NPWP-$id.$extension";
        $path = "public/karyawan/$id/NPWP";
        //upload file 
        if (!file_exists($path)) {
            // path does not exist deletefilerab
            Storage::makeDirectory($path);
            $files->storeAs($path,$filename);
        }else{
            $files->storeAs($path,$filename); 
        }

        $cekfile = Storage::files($path);
        return $cekfile;
    }
    public function uploadijazah(\Illuminate\Http\Request $request)
    {   
        $id = $request->id; 
        $files = $request->file('ijazahempl');
        $extension = $request->file('ijazahempl')->extension(); 

        // //custom name masing2 file
        $filename = "ijazah-$id.$extension";
        $path = "public/karyawan/$id/ijazah";
        //upload file 
        if (!file_exists($path)) {
            // path does not exist deletefilerab
            Storage::makeDirectory($path);
            $files->storeAs($path,$filename);
        }else{
            $files->storeAs($path,$filename); 
        }

        $cekfile = Storage::files($path);
        return $cekfile;
    }
    public function uploadfoto(\Illuminate\Http\Request $request)
    {   
        $id = $request->id; 
        $files = $request->file('fotoempl');
        $extension = $request->file('fotoempl')->extension(); 

        // //custom name masing2 file
        $filename = "foto-$id.$extension";
        $path = "public/karyawan/$id/foto";
        //upload file 
        if (!file_exists($path)) {
            // path does not exist deletefilerab
            Storage::makeDirectory($path);
            $files->storeAs($path,$filename);
        }else{
            $files->storeAs($path,$filename); 
        }

        $cekfile = Storage::files($path);
        return $cekfile;
    }
    public function uploadmisc(\Illuminate\Http\Request $request)
    {     
        $id = $request->id; 
        $files = $request->file('miscempl');
        $extension = $request->file('miscempl')->extension(); 
        $h = \Carbon\Carbon::now()->format('H');
        $i = \Carbon\Carbon::now()->format('i');
        $s = \Carbon\Carbon::now()->format('s');
        $date = \Carbon\Carbon::now()->format('Ymd');
        $val = rand(1,100);

        // //custom name masing2 file
        $filename = "misc-$id-"."$date$h$i$s$val.$extension";
        $path = "public/karyawan/$id/misc";
        //upload file 
        if (!file_exists($path)) {
            // path does not exist deletefilerab
            Storage::makeDirectory($path);
            $files->storeAs($path,$filename);
        }else{
            $files->storeAs($path,$filename); 
        }

        $cekfile = Storage::files($path);
        return $cekfile;
    }

    public function deletefile(\Illuminate\Http\Request $request)
    {   
        $dat = $request->pat;
        $path = "public/$dat";
        Storage::delete($path);
       
    }
    
    private function codeKaryawan(array $request, $id = 0)
    {
        $now = \Carbon\Carbon::now();
        $auth = auth()->user();
        $model = $this->karyawan->getModel();
        $format = 'BW [NO].[CV].[DS].[BM][BY].[JM][JY].[SI]';
        // =====
        $district = isset($request['penempatan']) ? $request['penempatan'] : null;
        $status = isset($request['status_karyawan']) ? $request['status_karyawan'] : null;
        $born = isset($request['born']) ? \Carbon\Carbon::createFromFormat("d-m-Y", $request['born']) : null;
        $join = isset($request['datejoin']) ? \Carbon\Carbon::createFromFormat("d-m-Y", $request['datejoin']) : null;
        // =====
        $code_CV = $auth->company->code_numb ?? str_pad($auth->company_id, 2, '0', STR_PAD_LEFT);
        $code_DS = $district;
        $code_BM = empty($born) ? null : $born->format('m');
        $code_BY = empty($born) ? null : $born->format('y');
        $code_JM = empty($join) ? $now->format('m') : $join->format('m');
        $code_JY = empty($join) ? $now->format('y') : $join->format('y');
        $noUrut = $model->where('company_id', $auth->company_id)->where('karyawan_id', '<>', $id)->where('penempatan', $district)->whereMonth('datejoin', empty($join) ? $now->format('m') : $join->format('m'))->orderBy('karyawan_id', 'DESC')->get();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('induk_karyawan')->first();
            preg_match('/(\d{3}).'.$code_CV.'.'.$code_DS.'.'.$code_BM.$code_BY.'/', $noUrut, $match);
            $noUrut = $match[1] + 1;
            $noUrut = str_pad($noUrut, 3, '0', STR_PAD_LEFT);
        } else {
            $noUrut = str_pad(1, 3, '0', STR_PAD_LEFT);
        }
        $code_NO = $noUrut;
        $code_SI = $status;
        
        return str_replace(
            array('[NO]', '[CV]', '[DS]', '[BM]', '[BY]', '[JM]', '[JY]', '[SI]'),
            array($code_NO, $code_CV, $code_DS, $code_BM, $code_BY, $code_JM, $code_JY, $code_SI),
            $format
        );
    }
}
