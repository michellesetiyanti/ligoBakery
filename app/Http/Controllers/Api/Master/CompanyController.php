<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request; 
use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Master\Company as CompanyRequest;
use App\Repositories\Master\Company as CompanyRepository;
use App\Models\Master\Company as CMP;
use App\Models\Accounting\ItemHppAwal as HPP;
Use App\Models\Master\Product as PRODUCT;

class CompanyController extends ApiController
{
    protected $company;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->company = new CompanyRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $sorting = 'company_id|DESC';
        $sorting = $request->sorting ? $request->sorting : $sorting;
        $company = $this->company->with(['parent', 'companies', 'warehouse'])->all($request->merge(compact('filter', 'sorting'))->all());
        return $this->success(null, 200, $company);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        \DB::beginTransaction();
        try {
            $user = $request->user;
            $email = $user['email'] ?? 'owner@email.com';
            $password = $user['password'] ?? 'owner123';
            $company = createCompany($request->merge([
                'parent_id' => $request->parent_id ?? null,
            ])->except('user', 'comp_type', 'coa_import'), true, $request->coa_import ?? false, compact('email', 'password'), $request->comp_type ?? 'agro');
            \DB::commit();
            return $this->success(null, 200, $company);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($company = $this->company->with(['parent', 'companies', 'warehouse'])->show($id)) {
            return $this->success(null, 200, $company);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CompanyRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function update(CompanyRequest $request, $id)
    {
        $company = $this->company->show($id);
        \DB::beginTransaction();
        try{
            $company->update($request->merge([
                'id' => $id,
                'parent_id' => $request->parent_id ?? null,
            ])->except('user', 'comp_type'));
            if ($company->warehouse->count() <= 1) {
                $company->warehouse()->update([
                    'name' => $company->name,
                    'address' => $company->address,
                    'phone' => $company->phone,
                    'currency_code' => $company->currency_code,
                    'npwp' => $company->npwp,
                    'taxable' => $company->taxable,
                    'taxable_rate' => $company->taxable_rate,
                    'status' => $company->status,
                ]);
            }
            \DB::commit();
            return $this->success(null, 200, $company);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
   
    public function updatelicense(Request $request, $id)
    { 
        \DB::beginTransaction();
        try {  
            $data = array( 
                'coa_penjualan' => $request->coa_penjualan,
                'coa_pembelian' => $request->coa_pembelian,
                'coa_hutang' => $request->coa_hutang,
                'coa_piutang' => $request->coa_piutang,
                'coa_pajak' => $request->coa_pajak,
                'coa_pajak_out' => $request->coa_pajak_out,
                'coa_discount' => $request->coa_discount,
                'coa_discount_buy' => $request->coa_discount_buy,
                'coa_ikhtisar_labarugi' => $request->coa_ikhtisar_labarugi,
                'coa_labarugi_ditahan' => $request->coa_labarugi_ditahan,
            ); 
            $cek = CMP::where('company_id', '=', $id)->update($data);
            \DB::commit();
            return $this->success("Company License Update Successfully", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function indexitemhpp(\Illuminate\Http\Request $request)
    { 
        \DB::beginTransaction();
        try {   
            $company_id = auth()->user()->company_id;
            $cek = HPP::where('company_id',$company_id)->with(['company','product'])->get();

            \DB::commit();
            return $this->success("Data Loaded Successfully", 200, $cek);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function storeitemhpp(\Illuminate\Http\Request $request)
    { 
        \DB::beginTransaction();
        try {   
            $company_id = auth()->user()->company_id;
            $product_id = $request->product_id;

            $cek = HPP::where('company_id',$company_id)->where('product_id',$product_id)->count();
            if($cek > 0){
                return $this->fail(null,500,"Product Sudah Pernah Di input, silahkan di Edit");
            }else{
                $product = PRODUCT::where('product_id',$product_id)->first();
                $qty_lg = $product->total_lg_qty;
                $qty_sm = $product->total_sm_qty;
                
                if($qty_lg != 0 || $qty_sm != 0){
                    $item = new HPP;
                    $item->product_id = $request->product_id;
                    $item->saldo_awal = $request->saldo;
                    $item->save();
                }else{
                    return $this->fail(null,500,"Stock Product Kosong, Harap lakukan pembelian / penyesuaian stock ");
                }

            } 

            \DB::commit();
            return $this->success("Company License Update Successfully", 200, $item);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function updateitemhpp(\Illuminate\Http\Request $request , $id)
    { 
        \DB::beginTransaction();
        try {   
            $company_id = auth()->user()->company_id;
            
            $data= array(
                'saldo_awal'=>$request->saldo
            );
            $update = HPP::where('id',$id)->update($data); 
            
            \DB::commit();
            return $this->success("Item HPP Update Successfully", 200, $update);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function deleteitemhpp($id)
    { 
        \DB::beginTransaction();
        try {   
             
            $company_id = auth()->user()->company_id;
            HPP::where('id',$id)->delete(); 
            
            \DB::commit();
            return $this->success("Item HPP Delete Successfully", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($id == auth()->user()->company_id) {
            return $this->fail('process_fail', 422, 'Not allowed to delete companies that are actively logged in');
        }
        $company = $this->company->show($id);
        \DB::beginTransaction();
        try {
            if ($company->companies->isEmpty() || $company->generalLedgers->flatMap->generalLedgerDetails->isEmpty()) {
                $company->delete();
            } else {
                $company->update(['status' => 'inactive']);
            }
            \DB::commit();
            return $this->success('process_success', 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail('process_fail', 500, $e->getMessage());
        }
    }
}
