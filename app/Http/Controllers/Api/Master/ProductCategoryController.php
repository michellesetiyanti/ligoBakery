<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Master\ProductCategory as ProductCategoryRequest;
use App\Repositories\Master\ProductCategory as ProductCategoryRepository;

class ProductCategoryController extends ApiController
{   
    protected $productCategory;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->productCategory = new ProductCategoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $productCategory = $this->productCategory->with('product')->all($request->all());
        return $this->success(null, 200, $productCategory);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCategoryRequest $request)
    {
        \DB::beginTransaction();
        try {
            $productCategory = $this->productCategory->save($request->all());
            \DB::commit();
            return $this->success(null, 200, $productCategory);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($productCategory = $this->productCategory->with('product')->show($id)) {
            return $this->success(null, 200, $productCategory);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProductCategoryRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductCategoryRequest $request, $id)
    {
        $productCategory = $this->productCategory->show($id);
        \DB::beginTransaction();
        try{
            $productCategory->fill($request->merge([
                'id' => $id,
            ])->all())->save();
            \DB::commit();
            return $this->success(null, 200, $productCategory);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productCategory = $this->productCategory->show($id);
        \DB::beginTransaction();
        try {
            if ($productCategory->product->isEmpty()) {
                $productCategory->delete();
            }
            \DB::commit();
            return $this->success(null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
