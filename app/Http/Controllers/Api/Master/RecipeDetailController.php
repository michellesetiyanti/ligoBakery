<?php

namespace App\Http\Controllers\Api\Master;

use App\Models\Master\RecipeDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\Controller as ApiController;

class RecipeDetailController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RecipeDetail  $recipeDetail
     * @return \Illuminate\Http\Response
     */
    public function show(RecipeDetail $recipeDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RecipeDetail  $recipeDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(RecipeDetail $recipeDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RecipeDetail  $recipeDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecipeDetail $recipeDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RecipeDetail  $recipeDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecipeDetail $recipeDetail)
    {
        //
    }
}
