<?php

namespace App\Http\Controllers\Api\Finance;

use Illuminate\Http\Request; 
use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Finance\Kas as KasRequest;
use App\Repositories\Finance\Kas as KasRepository;
use App\Models\Finance\Kas as Cash;
use App\Models\Accounting\Coa;
use App\Models\Accounting\GeneralLedger as GL;
use App\Models\Accounting\GeneralLedgerDetail as GLDetail;

class KasController extends ApiController
{
    protected $kas;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->kas = new KasRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(KasRequest $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $sorting = 'kas_id|ASC';
        $sorting = $request->sorting ? $request->sorting : $sorting;
        $kas = $this->kas->with(['from_coa','to_coa','company','user'])->all($request->merge(compact('filter', 'sorting'))->all());
           
        return $this->success(null, 200, $kas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KasRequest $request)
    {
        \DB::beginTransaction();
        try {
            $date=$request->date; 
            $description=$request->description;
            $type=$request->status;
            $from_coa=$request->from_coa;
            $to_coa=$request->to_coa;
            $amount=$request->amount;
            $user_id = auth()->user()->user_id;
            $company_id = auth()->user()->company_id;
            
            $count = Cash::where('company_id',$company_id)->count();
            $count = $count + 1;
            $ref = "CASH-".$count;
            
            $cekdana = GL::where('coa_code',$from_coa)->first();
            $coaName = $cekdana->coa_name;
            $balance = $cekdana->balance;

            if($balance <=$amount){
                return $this->fail("Posting Failed", 500, "Dana Pada $from_coa | $coaName tidak cukup" );
            }else{
                
            $cash=new Cash;  
            $cash->user_id = $user_id;
            $cash->company_id = $company_id;
            $cash->from_coa = $from_coa;
            $cash->to_coa = $to_coa;
            $cash->reference = $ref;
            $cash->description = $description;
            $cash->type = $type;
            $cash->nominal = $amount;
            $cash->entry_at = $date;
            $cash->save();  

            postgl($company_id, $to_coa, $ref, $description, $amount);
            postpaid($company_id, $from_coa, $ref, $description, $amount);  

            } 
            \DB::commit();
            return $this->success(null, 200, $cash);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($kas = $this->kas->show($id)) {
            return $this->success(null, 200, $kas);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CoaRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function update(KasRequest $request, $id)
    {
        $kas = Cash::where('kas_id',$id)->first();
        $ref=$kas->reference;
        $from_coa=$kas->from_coa;
        $to_coa=$kas->to_coa;

        $company_id = auth()->user()->company_id; 
       
        
        \DB::beginTransaction();
        try{
            delpostgl($company_id,$from_coa, $ref);
            delpostgl($company_id,$to_coa, $ref);

            //update data kas
            $date=$request->date; 
            $description=$request->description;
            $type=$request->status;
            $reqfrom_coa=$request->from_coa;
            $reqto_coa=$request->to_coa;
            $amount=$request->amount;
            $user_id = auth()->user()->user_id;

            $cekdana = GL::where('coa_code',$reqfrom_coa)->first();
            $coaName = $cekdana->coa_name;
            $balance = $cekdana->balance;

            if($balance <=$amount){
                return $this->fail("Posting Failed", 500, "Dana Pada $reqfrom_coa | $coaName tidak cukup" );
            }else{
                

            $data = array(
                'user_id'=>$user_id,
                'from_coa'=>$reqfrom_coa,
                'to_coa'=>$reqto_coa,
                'description'=>$description,
                'entry_at'=>$date,
                'type'=>$type,
                'nominal'=>$amount,
            );    
            $kas = Cash::where('kas_id',$id)->update($data); 

            //postgl ulang
            postgl($company_id, $reqto_coa, $ref, $description, $amount);
            postpaid($company_id, $reqfrom_coa, $ref, $description, $amount);

            }   
            \DB::commit();
            return $this->success(null, 200, $kas);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

     
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $companyid = auth()->user()->company_id; 
        $coaData = coa::where('coa_id',$id)->first();
        $code = $coaData->code;

        $glData = GL::where('coa_code',$code)->first();
        $glid = $glData->gl_id;
        $cek = GLDetail::where('gl_id',$glid)->count();

        \DB::beginTransaction();
        try {
            if ($cek == 0 ) {
                coa::where('coa_id',$id)->delete();
                GL::where('company_id',$companyid)->where('coa_code',$code)->delete();
            } else {
                return $this->fail(null, 500, "COA Can not be Deleted");  
            }
            \DB::commit();
            return $this->success(null);
        } catch(\Exception $e) {
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
