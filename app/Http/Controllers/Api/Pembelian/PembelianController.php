<?php

namespace App\Http\Controllers\Api\Pembelian;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Pembelian\Pembelian as PembelianRequest;
use App\Repositories\Pembelian\Pembelian as PembelianRepository;
use App\Repositories\Pembelian\PembelianDetail as PembelianDetailRepository;
use App\Repositories\Pembelian\PoPembelian as PoPembelianRepository;
use Illuminate\Http\Request;

class PembelianController extends ApiController
{
    protected $pembelian;
    protected $pembelianDetail;
    protected $po;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->pembelian = new PembelianRepository;
        $this->pembelianDetail = new PembelianDetailRepository;
        $this->po = new PoPembelianRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $pembelian = $this->pembelian->with(['pembelianDetails', 'poPembelian', 'toc', 'supplier', 'user'])->all($request->merge(compact('filter'))->all());
        $pembelian = $pembelian->whereNull('po_pro_id')->flatten();
        $args = [];
        foreach ($pembelian as $res) {
            $item = $res->makeHidden('pembelianDetails')->toArray();
            $item = array_merge($item, [
                'pembelian_details' => $res->pembelianDetails->where('revisi', '!=', 'delete')->flatten()
            ]);
            array_push($args, $item);
        }
        return $this->success(null, 200, $args);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  array  $request
     * @return
     */
    public function store(array $request)
    {
        $args = array(
            'code' => $request['code'],
            'due_date' => $request['due_date'],
            'ongkir' => $request['ongkir'],
            'ongkir_desc' => $request['ongkir_desc'],
            'biaya_lain' => $request['biaya_lain'],
            'biaya_lain_desc' => $request['biaya_lain_desc'],
            'total' => $request['total'],
            'payment' => $request['payment'],
        );
        if ($args['payment'] == 'unpaid') {
            $args['hutang_status'] = 'unpaid';
        }
        if (isset($request['supplier_id'])) {
            $args['supplier_id'] = $request['supplier_id'];
        }
        if (isset($request['company_id'])) {
            $args['company_id'] = $request['company_id'];
        }
        if (isset($request['po_pb_id'])) {
            $args['po_pb_id'] = $request['po_pb_id'];
        }
        if (isset($request['po_pro_id'])) {
            $args['po_pro_id'] = $request['po_pro_id'];
        }
        if (isset($request['status'])) {
            $args['status'] = $request['status'];
        }
        if (isset($request['approve_date'])) {
            $args['approve_date'] = $request['approve_date'];
        }
        try {
            $data = $this->pembelian->save($args);
            if (isset($request['poPembelianDetails']) && !empty($request['poPembelianDetails'])) {
                $details = collect($request['poPembelianDetails']);
            } else if (isset($request['poProjectDetails']) && !empty($request['poProjectDetails'])) {
                $details = collect($request['poProjectDetails']);
            } else {
                $details = collect();
            }
            if ($details->isNotEmpty()) {
                $details->each(function ($item) use ($data) {
                    $data->pembelianDetails()->create($item->only(['product_id', 'unit_lg', 'unit_sm', 'qty_lg', 'qty_sm', 'price_lg', 'price_sm', 'tax', 'discount', 'amount']));
                });
            }
            return ['success' => $data];
        } catch (\Throwable $th) {
            return ['error' => $th->errorInfo[2]];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pembelian = $this->pembelian->with(['pembelianDetails', 'poPembelian', 'toc', 'supplier', 'user'])->show($id);
        $args = $pembelian->makeHidden('pembelianDetails')->toArray();
        $args = array_merge($args, [
            'pembelian_details' => $pembelian->pembelianDetails->where('revisi', '!=', 'delete')->flatten()
        ]);
        return $this->success(null, 200, $args);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  array  $request
     * @param  int  $id
     * @return
     */
    public function update(array $request, $id)
    {
        $args = array(
            'code' => $request['code'],
            'due_date' => $request['due_date'],
            'ongkir' => $request['ongkir'],
            'ongkir_desc' => $request['ongkir_desc'],
            'biaya_lain' => $request['biaya_lain'],
            'biaya_lain_desc' => $request['biaya_lain_desc'],
            'total' => $request['total'],
            'payment' => $request['payment'],
        );
        if ($args['payment'] == 'unpaid') {
            $args['hutang_status'] = 'unpaid';
        }
        if (isset($request['supplier_id'])) {
            $args['supplier_id'] = $request['supplier_id'];
        }
        if (isset($request['company_id'])) {
            $args['company_id'] = $request['company_id'];
        }
        if (isset($request['po_pb_id'])) {
            $args['po_pb_id'] = $request['po_pb_id'];
        }
        if (isset($request['po_pro_id'])) {
            $args['po_pro_id'] = $request['po_pro_id'];
        }
        if (isset($request['status'])) {
            $args['status'] = $request['status'];
        }
        if (isset($request['approve_date'])) {
            $args['approve_date'] = $request['approve_date'];
        }
        $data = $this->pembelian->show($id);
        try {
            $data->update($args);
            if (isset($request['poPembelianDetails']) && !empty($request['poPembelianDetails'])) {
                $details = collect($request['poPembelianDetails']);
            } else if (isset($request['poProjectDetails']) && !empty($request['poProjectDetails'])) {
                $details = collect($request['poProjectDetails']);
            } else {
                $details = collect();
            }
            if ($details->isNotEmpty()) {
                $data->pembelianDetails()->whereNotIn('product_id', $details->pluck('product_id'))->delete();
                $details->each(function ($item) use ($data) {
                    if ($detail = $data->pembelianDetails->where('product_id', $item->product_id)->first()) {
                        $detail->update($item->only(['qty_lg', 'qty_sm', 'price_lg', 'price_sm', 'tax', 'discount', 'amount']));
                    } else {
                        $data->pembelianDetails()->create($item->only(['product_id', 'unit_lg', 'unit_sm', 'qty_lg', 'qty_sm', 'price_lg', 'price_sm', 'tax', 'discount', 'amount']));
                    }
                });
            }
            return ['success' => $data];
        } catch (\Throwable $th) {
            return ['error' => $th->errorInfo[2]];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $pembelian = $this->pembelian->show($id);
        \DB::beginTransaction();
        try {
            if (glByDesc($pembelian->company_id, 'Pembelian '.$pembelian->code)->count() > 0) {
                $approveDate = \Carbon\Carbon::parse($pembelian->approve_date);
                if (Closing::where('company_id', $pembelian->company_id)->where('bulan', $approveDate->format('m'))->where('tahun', $approveDate->format('y'))->where('status', 1)->exists()) {
                    return $this->fail('Accounting closure already exists', 422);
                }
                if ($request->missing('notes')) {
                    return $this->fail(trans('validation.required', ['attribute' => 'notes']), 422);
                }
                $products = $pembelian->pembelianDetails->map->product->map->productDetails->flatten();
                $productBeli = $products->where('status', 'pembelian')->where('ref', $pembelian->code)->flatten();
                if ($products->whereNotIn('status', ['pembelian'])->whereIn('batch', $productBeli->pluck('batch'))->count() > 0) {
                    return $this->fail('stock is already used', 422);
                }
                $productBeli->each(function ($item) use ($pembelian) {
                    $args = $item->only(['product_id', 'status', 'batch', 'unit_lg', 'unit_sm', 'price_lg', 'price_sm']);
                    $item->create(array_merge($args, [
                        'ref' => $pembelian->code.' [void]',
                        'qty_lg' => $item->cur_qty_lg,
                        'qty_sm' => $item->cur_qty_sm,
                    ]));
                    $item->update([
                        'cur_qty_lg' => 0,
                        'cur_qty_sm' => 0,
                    ]);
                    app(\App\Http\Controllers\Api\Master\ProductController::class)->updateStock($args['product_id']);
                });
                $pembelian->update([
                    'status' => 'void',
                    'notes' => $request->notes,
                ]);
                if ($pembelian->hutang) {
                    $pembelian->hutang->delete();
                }
                // Post GL
                voidpostpembelian($pembelian->company_id, $pembelian->payment == 'paid' ? 'cash' : 'credit',  $pembelian->code, $pembelian->dpp, $pembelian->tax, $pembelian->discount, $pembelian->total);
                $message = 'Pembelian\'s status has been void';
            } else {
                $pembelian->delete();
                $message = 'Pembelian\'s status has been delete';
            }
            \DB::commit();
            return $this->success($message, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    
    public function codeInv(\Illuminate\Http\Request $request, $toJson = 0)
    {
        $now = \Carbon\Carbon::now();
        $auth = auth()->user();
        $model = $this->pembelian->getModel();
        $format = 'INV [CV]-[NO] %lpar%[WO]%rpar% [TN].[TT]-[MM]/[YY]';
        // =====
        $company_id = $request->has('company_id') ? $request->company_id : null;
        $wo = $request->has('code_wo') ? $request->code_wo : null;
        $term_numb = $request->has('term_numb') ? $request->term_numb : null;
        $term_total = $request->has('term_total') ? $request->term_total : null;
        // =====
        $company = $auth->company;
        if ($company_id) {
            $company = \App\Models\Master\Company::find($company_id);
        }
        if (empty($wo)) {
            $format = str_replace(' %lpar%[WO]%rpar% ', ' ', $format);
        } else {
            $format = str_replace(array('[WO]', '%lpar%', '%rpar%'), array($wo, '[', ']'), $format);
        }
        if (empty($term_numb) || empty($term_total)) {
            $format = str_replace(array('[TN].', '[TT]-'), '', $format);
        } else {
            $term_numb = str_pad($term_numb, 2, '0', STR_PAD_LEFT);
            $term_total = str_pad($term_total, 2, '0', STR_PAD_LEFT);
            $format = str_replace(array('[TN]', '[TT]'), array($term_numb, $term_total), $format);
        }
        $code_CV = $company->code_alpha ?? str_pad($company->company_id, 5, '0', STR_PAD_LEFT);
        $code_MM = $now->format('m');
        $code_YY = $now->format('y');
        $noUrut = $model->where('company_id', $company->company_id)->whereMonth('created_at', $now->format('m'))->orderBy('pembelian_id', 'DESC')->get();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('code')->first();
            preg_match('/INV '.$code_CV.'-(\d{3})/', $noUrut, $match);
            $noUrut = empty($match) ? 1 : $match[1] + 1;
            $noUrut = str_pad($noUrut, 3, '0', STR_PAD_LEFT);
        } else {
            $noUrut = str_pad(1, 3, '0', STR_PAD_LEFT);
        }
        $code_NO = $noUrut;
        
        $code = str_replace(
            array('[NO]', '[CV]', '[MM]', '[YY]'),
            array($code_NO, $code_CV, $code_MM, $code_YY),
            $format
        );        
        if ($toJson > 0) return $code;
        return $this->success(null, 200, compact('code'));
    }
}
