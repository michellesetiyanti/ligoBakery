<?php

namespace App\Http\Controllers\Api\Pembelian;

use App\Models\Accounting\Closing;
use App\Http\Controllers\Api\Controller as ApiController;
use App\Repositories\Pembelian\Pembelian as BeliRepository;
use App\Repositories\Pembelian\PembelianDetail as BeliDetailRepository;
use App\Repositories\Master\ProductDetail as ProductDetailRepository;
use Illuminate\Http\Request;

class RevisiPembelianController extends ApiController
{
    protected $beli;
    protected $beliDetail;
    protected $productDetail;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->beli = new BeliRepository;
        $this->beliDetail = new BeliDetailRepository;
        $this->productDetail = new ProductDetailRepository;
    }
    
    public function status(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'no_faktur' => 'required|string|exists:pembelian,code',
            'notes' => 'required|string',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $beli = $this->beli->all()->where('code', $request->no_faktur)->first();
        if (empty($beli)) {
            return $this->fail('not_available.', 422);
        }
        $approveDate = \Carbon\Carbon::parse($beli->approve_date);
        if (Closing::where('company_id', $beli->company_id)->where('bulan', $approveDate->format('m'))->where('tahun', $approveDate->format('y'))->where('status', 1)->exists()) {
            return $this->fail('Accounting closure already exists', 422);
        }
        $productDetail = $this->productDetail->getModel();
        $productBeli = $productDetail->where('status', 'pembelian')->where('ref', $beli->code)->get();
        if ($productDetail->whereNotIn('status', ['pembelian'])->whereIn('batch', $productBeli->pluck('batch'))->count() > 0) {
            return $this->fail('stock_used', 422);
        }
        \DB::beginTransaction();
        try {
            $beli->update([
                'status' => 'revisi',
                'notes' => $request->notes,
            ]);
            \DB::commit();
            return $this->success(null, 200, ['status' => $beli->status]);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function detailUpdate(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'execute' => 'required|string|in:general,agro',
            'qty_lg' => 'sometimes|nullable|integer|required_if:execute,general',
            'qty_sm' => 'sometimes|nullable|integer|required_if:execute,general',
            'qty_pack' => 'sometimes|nullable|integer|required_if:execute,agro',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $beliDetail = $this->beliDetail->show($id);
        $beli = $this->beli->show($beliDetail->pembelian_id);
        // Check Stock
        $stock = $beliDetail->product->productDetails->where('status', 'pembelian')->where('ref', $beli->code)->flatten();
        if ($beliDetail->product->productDetails->whereNotIn('status', ['pembelian'])->whereIn('batch', $stock->pluck('batch'))->count() > 0) {
            return $this->fail('stock_used', 422);
        }
        \DB::beginTransaction();
        try {
            if ($request->execute == 'agro') {
                $qty_sm = $request->qty_pack / $beliDetail->product->pack_qty;
                if (is_float($qty_sm)) {
                    return $this->fail('fail_convert', 422, [
                        'qty_order' => $request->qty_pack,
                        'qty_pack' => $beliDetail->product->pack_qty,
                    ]);
                }
                // check po qty
                $poQty = ($beliDetail->po_qty_lg * $beliDetail->product->unit_qty) + $beliDetail->po_qty_sm;
                $poQty = $poQty * $beliDetail->product->pack_qty;
                if ($request->qty_pack > $poQty) {
                    return $this->fail('qty_bigger_than', 422, [
                        'stock_order' => $request->qty_pack,
                        'stock_po' => $poQty,
                    ]);
                }
                $stockBeli = $stock->first();
                if ($request->qty_pack > $stockBeli->volume['stock']['cur_qty']) {
                    return $this->fail('stock_used', 422);
                }
                $sm2lg = sm2lg($beliDetail->product_id, $qty_sm);
                $amount = $qty_sm * calcAmount($beliDetail->price_sm, $beliDetail->tax);
                $beliDetail->update([
                    'qty_lg' => $sm2lg['qty_lg'],
                    'qty_sm' => $sm2lg['qty_sm'],
                    'amount' => $amount - $beliDetail->discount,
                    'revisi' => 'edit',
                ]);
            } else {
                // check po qty
                if ($request->qty_lg > $beliDetail->po_qty_lg || $request->qty_sm > $beliDetail->po_qty_sm) {
                    return $this->fail('qty_bigger_than', 422, [
                        'stock_order_lg' => $request->qty_lg,
                        'stock_order_sm' => $request->qty_sm,
                        'stock_po_lg' => $beliDetail->po_qty_lg,
                        'stock_po_sm' => $beliDetail->po_qty_sm,
                    ]);
                }
                $stockBeli = $stock->first();
                if ($request->qty_lg > $stockBeli->cur_qty_lg || $request->qty_sm > $stockBeli->cur_qty_sm) {
                    return $this->fail('stock_used', 422);
                }
                $amount = $request->qty_lg * calcAmount($beliDetail->price_lg, $beliDetail->tax);
                $amount+= $request->qty_sm * calcAmount($beliDetail->price_sm, $beliDetail->tax);
                $beliDetail->update([
                    'qty_lg' => $request->qty_lg,
                    'qty_sm' => $request->qty_sm,
                    'amount' => $amount - $beliDetail->discount,
                    'revisi' => 'edit',
                ]);
            }
            $total = $beli->pembelianDetails->where('revisi', '!=', 'delete')->sum('amount');
            $total+= $beli->ongkir + $beli->biaya_lain;
            $beli->update(['total' => $total]);
            \DB::commit();
            return $this->success(null, 200, $beliDetail);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function detailDelete($id)
    {
        $beliDetail = $this->beliDetail->show($id);
        $beli = $this->beli->show($beliDetail->pembelian_id);
        \DB::beginTransaction();
        try {
            $beliDetail->update(['revisi' => 'delete']);
            $total = $beli->pembelianDetails->where('revisi', '!=', 'delete')->sum('amount');
            $total+= $beli->ongkir + $beli->biaya_lain;
            $beli->update(['total' => $total]);
            \DB::commit();
            return $this->success(null, 200, $beliDetail);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function submit($id)
    {
        $beli = $this->beli->show($id);
        \DB::beginTransaction();
        try {
            $total = $beli->pembelianDetails->where('revisi', '!=', 'delete')->sum('amount');
            $total+= $beli->ongkir + $beli->biaya_lain;
            $beli->update(['total' => $total, 'status' => 'submit']);
            \DB::commit();
            return $this->success(null, 200, $beli);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function approve($id)
    {
        $beli = $this->beli->show($id);
        if (!in_array($beli->status, ['revisi', 'submit'])) {
            return $this->fail('fail_process', 422, ['status' => $beli->status]);
        }
        // Check Stock
        $stockDetail = $this->productDetail->getModel();
        $stockBeli = $stockDetail->where('status', 'pembelian')->where('ref', $beli->code)->get();
        if ($stockDetail->whereNotIn('status', ['pembelian'])->whereIn('batch', $stockBeli->pluck('batch'))->count() > 0) {
            return $this->fail('stock_used', 422);
        }
        \DB::beginTransaction();
        try {
            // match stock edit
            $editIds = $beli->pembelianDetails->where('revisi', 'edit')->flatten();
            $stockEdit = $stockBeli->whereIn('product_id', $editIds->pluck('product_id'))->flatten();
            $stockEdit->each(function ($item) use ($stockDetail, $beli) {
                $beliDetail = $beli->pembelianDetails->where('product_id', $item->product_id)->first();
                $beliDetail->update(['revisi' => null]);
                $args = $item->only(['product_id', 'status', 'batch', 'unit_lg', 'unit_sm', 'price_lg', 'price_sm']);
                $args = array_merge($args, [
                    'ref' => $item->ref.' [revisi]',
                    'qty_lg' => $item->cur_qty_lg - $beliDetail->qty_lg,
                    'qty_sm' => $item->cur_qty_sm - $beliDetail->qty_sm,
                ]);
                if ($args['qty_lg'] > 0 || $args['qty_sm'] > 0) {
                    $stockDetail->create($args);
                }
                $item->update([
                    'cur_qty_lg' => $item->cur_qty_lg - $args['qty_lg'],
                    'cur_qty_sm' => $item->cur_qty_sm - $args['qty_sm'],
                ]);
                app(\App\Http\Controllers\Api\Master\ProductController::class)->updateStock($item->product_id);
            });
            // match stock delete
            $delIds = $beli->pembelianDetails->where('revisi', 'delete')->flatten();
            $stockDel = $stockBeli->whereIn('product_id', $delIds->pluck('product_id'))->flatten();
            $stockDel->each(function ($item) use ($stockDetail, $beli) {
                $beliDetail = $beli->pembelianDetails->where('product_id', $item->product_id)->first();
                $beliDetail->delete();
                $args = $item->only(['product_id', 'status', 'batch', 'unit_lg', 'unit_sm', 'qty_lg', 'qty_sm', 'price_lg', 'price_sm']);
                $args = array_merge($args, ['ref' => $item->ref.' [revisi]']);
                $stockDetail->create($args);
                $item->update([
                    'cur_qty_lg' => 0,
                    'cur_qty_sm' => 0,
                ]);
                app(\App\Http\Controllers\Api\Master\ProductController::class)->updateStock($item->product_id);
            });
            if (glByDesc($beli->company_id, 'Pembelian '.$beli->code)->count() > 0) {
                revisipostpembelian($beli->company_id, $beli->payment == 'paid' ? 'cash' : 'credit',  $beli->code, $beli->dpp, $beli->tax, $beli->discount, $beli->total);
            }
            $hutang_status = $beli->hutang_status;
            if ($beli->hutang) {
                $beli->hutang->update([
                    'total' => $beli->total,
                    'balance' => $beli->total - $beli->hutang->paid_off,
                ]);
                $beli->hutang->hutangDetails->first()->update($beli->hutang->only(['total', 'balance']));
                if ($beli->total == $beli->hutang->paid_off) {
                    $hutang_status = 'paid';
                }
            }
            $beli->update([
                'status' => 'approve',
                'hutang_status' => $hutang_status,
            ]);
            \DB::commit();
            return $this->success(null, 200, $beli);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function reject(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'rejected' => 'required|boolean',
            'notes' => 'required|string',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $beli = $this->beli->show($id);
        if (in_array($beli->status, ['approve', 'void'])) {
            return $this->fail('fail_process', 422, ['status' => $beli->status]);
        }
        \DB::beginTransaction();
        try {
            $status = $request->boolean('rejected') ? 'reject' : 'revisi';
            if ($status == 'revisi') {
                $restDetail = $beli->pembelianDetails->whereNotNull('revisi')->flatten();
                $restDetail->each(function ($item) use ($beli) {
                    $beliDetail = $beli->pembelianDetails->where('product_id', $item->product_id)->first();
                    if ($beli->po_pb_id) {
                        $poDetail = $beli->poPembelian->poPembelianDetails->where('product_id', $item->product_id)->first();
                        $args = $poDetail->only(['qty_lg', 'qty_sm', 'amount']);
                    }
                    if ($beli->po_pro_id) {
                        $poDetail = $beli->poProject->poProjectDetails->where('product_id', $item->product_id)->first();
                        $args = $poDetail->only(['qty_lg', 'qty_sm', 'amount']);
                    }
                    $beliDetail->update(array_merge($args, ['revisi' => null]));
                });
            }
            $total = $beli->pembelianDetails->where('revisi', '!=', 'delete')->sum('amount');
            $total+= $beli->ongkir + $beli->biaya_lain;
            $beli->update([
                'total' => $total,
                'status' => $status,
                'notes' => $request->notes,
            ]);
            \DB::commit();
            return $this->success(null, 200, $beli);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
