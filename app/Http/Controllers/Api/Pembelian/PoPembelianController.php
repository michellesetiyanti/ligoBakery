<?php

namespace App\Http\Controllers\Api\Pembelian;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Pembelian\PoPembelian as PoPembelianRequest;
use App\Repositories\Master\Product as ProductRepository;
use App\Repositories\Master\Supplier as SupplierRepository;
use App\Repositories\Master\Warehouse as WarehouseRepository;
use App\Repositories\Pembelian\PoPembelian as PoPembelianRepository;
use App\Repositories\Project\Order as OrderRepository;

class PoPembelianController extends ApiController
{
    protected $order;
    protected $product;
    protected $po;
    protected $supplier;
    protected $warehouse;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->order = new OrderRepository;
        $this->product = new ProductRepository;
        $this->po = new PoPembelianRepository;
        $this->supplier = new SupplierRepository;
        $this->warehouse = new WarehouseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $wh_id = implode(':', auth()->user()->company->warehouse->pluck('wh_id')->toArray());
                $filter = 'wh_id|'.$wh_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $po = $this->po->with(['poPembelianDetails', 'toc', 'pembelian', 'supplier', 'user', 'warehouse'])->all($request->merge(compact('filter'))->all());
        $po = $po->whereNull('po_pro_id')->flatten();
        return $this->success(null, 200, $po);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PoPembelianRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PoPembelianRequest $request)
    {
        if (empty(auth()->user()->company->coa_pembelian) || empty(auth()->user()->company->coa_hutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $warehouse = $this->warehouse->show($request->wh_id);
        $coa_code = $request->missing('coa_code') || $request->coa_code == 'null' ? null : $request->coa_code;
        $due_date = $request->missing('due_date') || $request->due_date == 'null' ? null : $request->due_date;
        if ($due_date) {
            $due_date = \Carbon\Carbon::now()->add($due_date, 'day')->format('d/m/Y');
        }
        $model = $this->po->getModel();
        \DB::beginTransaction();
        try {
            $po = $model->where('name', $request->name)->first();
            if (empty($po)) {
                $po = $model->create($request->merge([
                    'company_id' => $warehouse->company_id,
                    'code' => $request->supplier_inv,
                    'total' => $request->ongkir + $request->biaya_lain,
                    'due_date' => $due_date,
                    'payment' => $request->payment == 'cash' ? 'paid' : 'unpaid',
                    'coa_code' => $coa_code
                ])->except('details'));
            }
            $po->update($request->merge([
                'code' => $request->supplier_inv,
                'total' => $po->poPembelianDetails->sum('amount') + $request->ongkir + $request->biaya_lain,
                'due_date' => $due_date,
                'payment' => $request->payment == 'cash' ? 'paid' : 'unpaid',
                'coa_code' => $coa_code,
                'status' => 'pending',
            ])->except(['name', 'details']));
            \DB::commit();
            return $this->success(null, 200, $po);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $po = $this->po->with(['poPembelianDetails', 'toc', 'pembelian', 'supplier', 'user', 'warehouse'])->show($id);
        
        return $this->success(null, 200, $po);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PoPembelianRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PoPembelianRequest $request, $id)
    {
        if (empty(auth()->user()->company->coa_pembelian) || empty(auth()->user()->company->coa_hutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $warehouse = $this->warehouse->show($request->wh_id);
        $coa_code = $request->missing('coa_code') || $request->coa_code == 'null' ? null : $request->coa_code;
        $due_date = $request->missing('due_date') || $request->due_date == 'null' ? null : $request->due_date;
        if ($due_date) {
            $due_date = \Carbon\Carbon::now()->add($due_date, 'day')->format('d/m/Y');
        }
        $po = $this->po->show($id);
        \DB::beginTransaction();
        try {
            $po->update($request->merge([
                'code' => $request->supplier_inv,
                'total' => $po->poPembelianDetails->sum('amount') + $request->ongkir + $request->biaya_lain,
                'due_date' => $due_date,
                'payment' => $request->payment == 'cash' ? 'paid' : 'unpaid',
                'coa_code' => $coa_code,
                'status' => 'pending',
            ])->except(['name', 'details']));
            \DB::commit();
            return $this->success(null, 200, $po);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $po = $this->po->show($id);
        if (in_array($po->status, ['close'])) {
            return $this->fail('fail_status', 422, ['status' => $po->status]);
        }
        \DB::beginTransaction();
        try {
            $po->delete();
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Do Action the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function doAction(\Illuminate\Http\Request $request, $id)
    {

        $validator = \Validator::make($request->all(), [
            'details' => 'sometimes|required|array',
            'details.*.id' => 'integer|exists:po_pembelian_details,po_pb_d_id',
            'execute' => 'required|string|in:rejected,void,approved',
            'rejected' => 'sometimes|required|boolean',
            'notes' => 'sometimes|required|string',
            'approved' => 'sometimes|required|array',
            'approved.supplier_inv' => 'sometimes|nullable|string',
            'approved.payment' => 'sometimes|required|string|in:cash,credit',
            // 'approved.due_date' => 'sometimes|nullable|required_if:approved.payment,credit'.(auth()->user()->company->code_numb && auth()->user()->company->code_alpha ? '' : '|date_format:"d/m/Y"'),
            'approved.coa_code' => 'sometimes|nullable|string|required_if:approved.payment,cash',
            'approved.ongkir' => 'sometimes|nullable|digits_between:1,20',
            'approved.ongkir_desc' => 'sometimes|nullable|string',
            'approved.biaya_lain' => 'sometimes|nullable|digits_between:1,20',
            'approved.biaya_lain_desc' => 'sometimes|nullable|string',
        ]);
        if ($validator->fails()) {
            return $this->fail('fail_validation', 422, $validator->errors());
        }
        $po = $this->po->show($id);
        \DB::beginTransaction();
        try {
            $ids = [];
            if ($request->details) {
                foreach ($request->details as $detail) {
                    array_push($ids, $detail['id']);
                }
            }
            switch ($request->execute) {
                case 'rejected':
                    if ($request->has('rejected')) {
                        $status = $request->boolean('rejected') ? 'reject' : 'open';
                        $po->update(['status' => $status, 'notes' => $request->notes]);
                    } else {
                        $errors = $validator->errors()->add('rejected', trans('validation.required', ['attribute' => 'rejected']));
                        return $this->fail('fail_validation', 422, $errors);
                    }
                break;
                case 'void':
                    $po->update(['status' => 'void', 'notes' => $request->notes]);
                break;
                case 'approved':
                    $approved = $request->approved;
                    if (!in_array($po->status, ['pending'])) {
                        return $this->fail('fail_process', 422, ['status' => $po->status]);
                    }
                    $suppInv = $approved['supplier_inv'];
                    if ($request->missing('approved.supplier_inv') || in_array($suppInv, ['null', null, ''])) {
                        $reqSuppInv = new \Illuminate\Http\Request(['company_id' => $po->company_id]);
                        $suppInv = app(\App\Http\Controllers\Api\Pembelian\PembelianController::class)->codeInv($reqSuppInv, 1);
                    }
                    $checkInv = $this->po->getModel()->where('code', $suppInv)->get();
                    if ($checkInv->count() > 1) {
                        return $this->fail("Duplicate entry $suppInv", 422, $checkInv);
                    }
                    // PO Details
                    $po->poPembelianDetails()->whereNotIn('po_pb_d_id', $ids)->delete();
                    $coa_code = $approved['coa_code'] && $approved['coa_code'] == 'null' ? null : $approved['coa_code'];
                    $due_date = $approved['due_date'] && $approved['due_date'] == 'null' ? null : $approved['due_date'];
                    if ($due_date) {
                        $due_date = \Carbon\Carbon::now()->add($due_date, 'day')->format('d/m/Y');
                    }
                    $po->update([
                        'code' => $suppInv,
                        'ongkir' => $approved['ongkir'] ?? $po->ongkir,
                        'ongkir_desc' => ($approved['ongkir_desc'] == 'null') ? null : $approved['ongkir_desc'],
                        'biaya_lain' => $approved['biaya_lain'] ?? $po->biaya_lain,
                        'biaya_lain_desc' => ($approved['biaya_lain_desc'] == 'null') ? null : $approved['biaya_lain_desc'],
                        'total' => $po->poPembelianDetails()->sum('amount') + ($approved['ongkir'] ?? $po->ongkir) + ($approved['biaya_lain'] ?? $po->biaya_lain),
                        'due_date' => $due_date,
                        'payment' => $approved['payment'] ? ($approved['payment'] == 'cash') ? 'paid' : 'unpaid' : $po->payment,
                        'coa_code' => $coa_code,
                        'signed' => authExecute(),
                        'status' => 'close',
                    ]);
                    // Pembelian
                    $purch = $po->only('po_pb_id', 'company_id', 'supplier_id', 'code', 'due_date', 'ongkir', 'ongkir_desc', 'biaya_lain', 'biaya_lain_desc', 'total', 'payment', 'status', 'poPembelianDetails');
                    $purch = array_merge($purch, [
                        'status' => 'approve',
                        'approve_date' => \Carbon\Carbon::now()->toDateString()
                    ]);
                    $purch = app(\App\Http\Controllers\Api\Pembelian\PembelianController::class)->store($purch);
                    if (isset($purch['error'])) {
                        return $this->fail($purch['error'], 422);
                    }
                    $pembelian = $purch['success'];
                    // Check payment
                    if ($pembelian->payment == 'paid') {
                        $approveDate = \Carbon\Carbon::parse($pembelian->approve_date)->format('d/m/Y');
                        $message = postpaid($pembelian->company_id, $po->coa_code, $pembelian->code, $approveDate, $pembelian->total);
                        if ($message['status'] === 'failed') {
                            return $this->fail($message['errors'], 422);
                        }
                    } else {
                        prosesHutang([
                            "company_id" => $pembelian->company_id,
                            "pembelian_id" => $pembelian->pembelian_id,
                            "total" => $pembelian->total,
                            "paid_off" => ($pembelian->payment == 'paid') ? $pembelian->total : 0,
                            "balance" => ($pembelian->payment == 'unpaid') ? $pembelian->total : 0,
                        ], $pembelian->company->coa_hutang);
                    }
                    // Product                        
                    $product = ['status' => 'pembelian', 'ref' => $pembelian->code];
                    foreach ($pembelian->pembelianDetails as $detail) {
                        $product = array_merge($product, [
                            'unit_lg' => $detail->product->unit_lg,
                            'unit_sm' => $detail->product->unit_sm,
                            'qty_lg' => $detail->qty_lg,
                            'qty_sm' => $detail->qty_sm,
                            'cur_qty_lg' => $detail->qty_lg,
                            'cur_qty_sm' => $detail->qty_sm,
                            'price_lg' => $detail->price_lg,
                            'price_sm' => $detail->price_sm,
                        ]);
                        $detail->product->productDetails()->create($product);
                        app(\App\Http\Controllers\Api\Master\ProductController::class)->updateStock($detail->product_id);
                    }
                    // Post GL
                    postpembelian($pembelian->company_id, ($pembelian->payment == 'paid') ? 'cash' : 'credit',  $pembelian->code, $pembelian->dpp, $pembelian->tax, $pembelian->discount, $pembelian->total);
                break;
            }
            \DB::commit();
            return $this->success('success_process', 200, ['status' => $po->status]);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail('fail_process', 500, $e->getMessage());
        }
    }
    
    public function codePo(\Illuminate\Http\Request $request, $toJson = 0)
    {
        $now = \Carbon\Carbon::now();
        $auth = auth()->user();
        $model = $this->po->getModel();
        $code = 'PO-[CV] [NO] %lpar%[WO]%rpar% [PO]-[MM]/[YY]';
        // Kode CV
        $code = str_replace('[CV]', $auth->company->code_alpha, $code);
        // No. Urut
        $noUrut = $model->where('company_id', $auth->company_id)->whereMonth('created_at', $now->format('m'))->orderBy('po_pb_id', 'DESC')->get();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('name')->first();
            preg_match('/PO-'.$auth->company->code_alpha.' (\d{3})/', $noUrut, $match);
            $noUrut = $match[1] + 1;
        } else {
            $noUrut = 1;
        }
        $noUrut = str_pad($noUrut, strlen($noUrut) > 3 ? $noUrut : 3, '0', STR_PAD_LEFT);
        $code = str_replace('[NO]', $noUrut, $code);
        // No. WO || TOTAL PO DALAM SPK
        if ($request->has('code_spk')) {
            $wo = $this->order->getModel()->where('code_spk', $request->code_spk)->pluck('work_order')->first();
            $po = $model->where('company_id', $auth->company_id)->where('code_spk', $request->code_spk)->count() + 1;
            $po = str_pad($po, 2, '0', STR_PAD_LEFT);
            $code = str_replace(array('[WO]', '[PO]', '%lpar%', '%rpar%'), array($wo, $po, '[', ']'), $code);
        } else {
            $code = str_replace(' %lpar%[WO]%rpar% [PO]', '', $code);
        }
        // Bulan Terbit || Tahun Terbit
        $code = str_replace(array('[MM]', '[YY]'), array($now->format('m'), $now->format('y')), $code);
        
        if ($toJson > 0) return $code;
        return $this->success(null, 200, compact('code'));
    }
}
