<?php

namespace App\Http\Controllers\Api\Pembelian;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Pembelian\PoPembelian as PoPembelianRequest;
use App\Repositories\Master\Product as ProductRepository;
use App\Repositories\Master\Supplier as SupplierRepository;
use App\Repositories\Master\Warehouse as WarehouseRepository;
use App\Repositories\Pembelian\PoPembelian as PoPembelianRepository;
use App\Repositories\Pembelian\PoPembelianDetail as PoPembelianDetailRepository;

class PoPembelianDetailController extends ApiController
{
    protected $po;
    protected $poDetail;
    protected $product;
    protected $supplier;
    protected $warehouse;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->po = new PoPembelianRepository;
        $this->poDetail = new PoPembelianDetailRepository;
        $this->product = new ProductRepository;
        $this->supplier = new SupplierRepository;
        $this->warehouse = new WarehouseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $poDetail = $this->poDetail->with(['poPembelian', 'product'])->all($request->all());
        return $this->success(null, 200, $poDetail);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PoPembelianRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PoPembelianRequest $request)
    {
        if (empty(auth()->user()->company->coa_pembelian) || empty(auth()->user()->company->coa_hutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $warehouse = $this->warehouse->show($request->wh_id);
        $supplier = $this->supplier->show($request->supplier_id);
        $product = $this->product->all()->where('wh_id', $request->wh_id)->where('product_id', $request->details['product_id'])->first();
        if (empty($product)) {
            return $this->fail('product_dont_exist', 422);
        }
        $coa_code = $request->missing('coa_code') || $request->coa_code == 'null' ? null : $request->coa_code;
        $due_date = $request->missing('due_date') || $request->due_date == 'null' ? null : $request->due_date;
        if ($due_date) {
            $due_date = \Carbon\Carbon::now()->add($due_date, 'day')->format('d/m/Y');
        }
        $model = $this->po->getModel();
        \DB::beginTransaction();
        try {
            $payment = $request->payment == 'cash' ? 'paid' : 'unpaid';
            $po = $model->where('name', $request->name)->first();
            if (empty($po)) {
                $po = $model->create($request->merge([
                    'company_id' => $warehouse->company_id,
                    'code' => $request->supplier_inv,
                    'total' => $request->ongkir + $request->biaya_lain,
                    'due_date' => $due_date,
                    'payment' => $payment,
                    'coa_code' => ($payment == 'paid') ? $coa_code : null,
                ])->except('details'));
            }
            if ($po->poPembelianDetails()->where('product_id', $product->product_id)->exists()) {
                return $this->fail('product_exist', 422, $po->poPembelianDetails->where('product_id', $product->product_id)->first());
            }
            $amount = $request->details['qty_lg'] * calcAmount($request->details['price_lg'], $request->details['tax']);
            $amount+= $request->details['qty_sm'] * calcAmount($request->details['price_sm'], $request->details['tax']);
            $poDetail = $po->poPembelianDetails()->create([
                'product_id' => $product->product_id,
                'unit_lg' => $product->unit_lg,
                'unit_sm' => $product->unit_sm,
                'qty_lg' => $request->details['qty_lg'],
                'qty_sm' => $request->details['qty_sm'],
                'price_lg' => $request->details['price_lg'],
                'price_sm' => $request->details['price_sm'],
                'tax' => $request->details['tax'],
                'discount' => $request->details['discount'],
                'amount' => $amount - $request->details['discount'],
            ]);
            $po->update($request->merge([
                'code' => $request->supplier_inv,
                'total' => $po->poPembelianDetails->sum('amount') + $request->ongkir + $request->biaya_lain,
                'due_date' => $due_date,
                'payment' => $payment,
                'coa_code' => ($payment == 'paid') ? $coa_code : null,
            ])->except(['name', 'details']));
            \DB::commit();
            return $this->success('success_process', 200, $poDetail);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail('fail_process', 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $po = $this->po->all();
        if ($poDetail = $po->flatMap->poPembelianDetails->where('po_pb_d_id', $id)->first()) {
            return $this->success(null, 200, $poDetail);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PoPembelianRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PoPembelianRequest $request, $id)
    {
        if (empty(auth()->user()->company->coa_pembelian) || empty(auth()->user()->company->coa_hutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $warehouse = $this->warehouse->show($request->wh_id);
        $supplier = $this->supplier->show($request->supplier_id);
        $product = $this->product->all()->where('wh_id', $request->wh_id)->where('product_id', $request->details['product_id'])->first();
        if (empty($product)) {
            return $this->fail('product_dont_exist', 422);
        }
        $coa_code = $request->missing('coa_code') || $request->coa_code == 'null' ? null : $request->coa_code;
        $due_date = $request->missing('due_date') || $request->due_date == 'null' ? null : $request->due_date;
        if ($due_date) {
            $due_date = \Carbon\Carbon::now()->add($due_date, 'day')->format('d/m/Y');
        }
        $poDetail = $this->poDetail->show($id);
        $po = $this->po->show($poDetail->po_pb_id);
        \DB::beginTransaction();
        try {
            $amount = $request->details['qty_lg'] * calcAmount($request->details['price_lg'], $request->details['tax']);
            $amount+= $request->details['qty_sm'] * calcAmount($request->details['price_sm'], $request->details['tax']);
            $poDetail->update([
                'qty_lg' => $request->details['qty_lg'],
                'qty_sm' => $request->details['qty_sm'],
                'price_lg' => $request->details['price_lg'],
                'price_sm' => $request->details['price_sm'],
                'tax' => $request->details['tax'],
                'discount' => $request->details['discount'],
                'amount' => $amount - $request->details['discount'],
            ]);
            $po->update($request->merge([
                'code' => $request->supplier_inv,
                'total' => $po->poPembelianDetails->sum('amount') + $request->ongkir + $request->biaya_lain,
                'due_date' => $due_date,
                'payment' => $request->payment == 'cash' ? 'paid' : 'unpaid',
                'coa_code' => $coa_code,
            ])->except(['name', 'details']));
            \DB::commit();
            return $this->success('success_process', 200, $poDetail);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail('fail_process', 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
        $poDetail = $this->poDetail->show($id);
        $po = $this->po->show($poDetail->po_pb_id);
        if (!in_array($po->status, ['open', 'pending'])) {
            return $this->fail('fail_status', 422, ['status' => $po->status]);
        }
        \DB::beginTransaction();
        try {
            $poDetail->delete();
            if ($po->poPembelianDetails()->exists()) {
                $po->update(['total' => $po->poPembelianDetails->sum('amount') + $po->ongkir + $po->biaya_lain]);
            } else {
                $po->update(['total' => $po->ongkir + $po->biaya_lain]);
            }
            \DB::commit();
            return $this->success('success_process', 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail('fail_process', 500, $e->getMessage());
        }
    }
}
