<?php

namespace App\Http\Controllers\Api\Pembelian;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Repositories\Pembelian\Hutang as HutangRepository;
use Illuminate\Http\Request;

class HutangController extends ApiController
{
    protected $hutang;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth', ['except' => ['store', 'update', 'destroy']]);
        $this->hutang = new HutangRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $hutang = $this->hutang->with(['hutangDetails', 'pembelian'])->all($request->merge(compact('filter'))->all());
        $hutang = $hutang->where('pembelian.status', 'approve')->flatten();
        return $this->success(null, 200, $hutang);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($hutang = $this->hutang->with(['hutangDetails', 'pembelian'])->show($id)) {
            return $this->success(null, 200, $hutang);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Do Action the specified resource from storage.
     *
     * @param  HutangRequest $request
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function doAction(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'date' => 'required|date_format:"d/m/Y"',
            'coa_code' => 'required|string|exists:coa,code',
            'amount' => 'required|digits_between:1,20',
            'execute' => 'required|string|in:pay',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        $hutang = $this->hutang->show($id);
        \DB::beginTransaction();
        try {
            switch ($request->execute) {
                case 'pay':
                    $hutangDetails = $hutang->hutangDetails->first();
                    if (($hutangDetails->paid_off + $request->amount) <= $hutangDetails->total) {
                        $hutangDetails->update([
                            'coa_code' => $request->coa_code,
                            'paid_off' => $hutangDetails->paid_off + $request->amount,
                            'balance' => $hutangDetails->balance - $request->amount,
                        ]);
                        // Balance Paid Off Hutang
                        $hutang->update([
                            'paid_off' => $hutang->paid_off + $request->amount,
                            'balance' => $hutang->balance - $request->amount,
                        ]);
                        // Post GL
                        $postpaid = postpaid($hutang->company_id, $request->coa_code, $hutang->pembelian->code, $request->date, $request->amount);
                        if ($postpaid['status'] === 'failed') {
                            return $this->fail('GL '.$postpaid['message'], 422, $postpaid['errors']);
                        }
                        $message = $postpaid['message'];
                    } else {
                        $message = sprintf('Hutang\'s balance is "%s".', $hutangDetails->balance);
                        return $this->fail($postpaid['message'], 422);
                    }
                    if ($hutang->total == $hutang->paid_off) {
                        $hutang->pembelian->update(['hutang_status' => 'paid']);
                    }
                break;
            }
            \DB::commit();
            return $this->success($message, 200, $hutang);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
