<?php

namespace App\Http\Controllers\Api\Pembelian\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Pembelian\Pembelian as PembelianRequest;
use App\Repositories\Pembelian\Pembelian as PembelianRepository;
use App\Repositories\Pembelian\PembelianDetail as PembelianDetailRepository;
use App\Repositories\Pembelian\PoPembelian as PoPembelianRepository;
use Illuminate\Http\Request;

class PembelianController extends ApiController
{
    protected $pembelian;
    protected $pembelianDetail;
    protected $po;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->pembelian = new PembelianRepository;
        $this->pembelianDetail = new PembelianDetailRepository;
        $this->po = new PoPembelianRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $pembelian = $this->pembelian->with(['pembelianDetails', 'poPembelian', 'poProject', 'user'])->all($request->merge(compact('filter'))->all());
        $pembelian = $pembelian->whereNotNull('po_pro_id')->flatten();
        $args = [];
        foreach ($pembelian as $res) {
            $item = $res->makeHidden('pembelianDetails')->toArray();
            $item = array_merge($item, [
                'pembelian_details' => $res->pembelianDetails->where('revisi', '!=', 'delete')->flatten()
            ]);
            array_push($args, $item);
        }
        
        return $this->success(null, 200, $args);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  array  $request
     * @return
     */
    public function store(array $request)
    {
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pembelian = $this->pembelian->with(['pembelianDetails', 'poPembelian', 'poProject', 'user'])->show($id);
        $args = $pembelian->makeHidden('pembelianDetails')->toArray();
        $args = array_merge($args, [
            'pembelian_details' => $pembelian->pembelianDetails->where('revisi', '!=', 'delete')->flatten()
        ]);

        return $this->success(null, 200, $args);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  array  $request
     * @param  int  $id
     * @return
     */
    public function update(array $request, $id)
    {
        // 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // 
    }
}
