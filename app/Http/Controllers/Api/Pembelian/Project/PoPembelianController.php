<?php

namespace App\Http\Controllers\Api\Pembelian\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Pembelian\PoPembelian as PoPembelianRequest;
use App\Repositories\Auth\User as UserRepository;
use App\Repositories\Master\Product as ProductRepository;
use App\Repositories\Master\Warehouse as WarehouseRepository;
use App\Repositories\Pembelian\PoPembelian as PoPembelianRepository;
use App\Repositories\Project\Order as OrderRepository;
use Illuminate\Http\Request;

class PoPembelianController extends ApiController
{
    protected $order;
    protected $product;
    protected $po;
    protected $user;
    protected $warehouse;

    /**
     * Instance constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->order = new OrderRepository;
        $this->product = new ProductRepository;
        $this->po = new PoPembelianRepository;
        $this->user = new UserRepository;
        $this->warehouse = new WarehouseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        switch (auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
                break;
            default:
                $wh_id = implode(':', auth()->user()->company->warehouse->pluck('wh_id')->toArray());
                $filter = 'wh_id|' . $wh_id;
                $filter = $request->filter ? $filter . ';' . $request->filter : $filter;
                break;
        }
        $sorting = $request->sorting ? $request->sorting : 'po_pb_id|DESC';
        $po = $this->po->with(['poPembelianDetails', 'poProject', 'order', 'pembelian', 'user', 'warehouse'])->all($request->merge(compact('filter', 'sorting'))->all());
        $po = $po->whereNotNull('po_pro_id')->flatten();
        return $this->success(null, 200, $po);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $due_date = $request->missing('due_date') || $request->due_date == 'null' ? null : $request->due_date;
        if ($due_date) {
            $due_date = \Carbon\Carbon::now()->add($due_date, 'day')->format('d/m/Y');
        }
        $name = app(\App\Http\Controllers\Api\Pembelian\PoPembelianController::class)->codePo($request, 1);
        $model = $this->po->getModel();
        $po = $model->create([
            'po_pro_id' => $request->po_pro_id,
            'company_id' => $request->company_id,
            'wh_id' => $request->wh_id,
            'code_spk' => $request->code_spk,
            'name' => $name,
            'ongkir' => $request->ongkir,
            'ongkir_desc' => $request->ongkir_desc,
            'biaya_lain' => $request->biaya_lain,
            'biaya_lain_desc' => $request->biaya_lain_desc,
            'total' => $request->total,
            'payment' => $request->payment == 'cash' ? 'paid' : 'unpaid',
            'due_date' => $due_date,
            'coa_code' => $request->coa_code,
            'status' => 'pending',
        ]);
        if ($request->has('details')) {
            foreach ($request->details as $detail) {
                $product = $this->product->getModel();
                $product = $product->where('wh_id', $request->wh_id)->where('name', $detail['product']['name'])->where('unit_lg', $detail['product']['unit_lg'])->where('unit_sm', $detail['product']['unit_sm'])->first();
                if (empty($product)) {
                    $reqProd = new Request([
                        'wh_id' => $request->wh_id,
                        'product_c_id' => $detail['product']['product_c_id'],
                        'code' => null,
                        'name' => $detail['product']['name'],
                        'description' => $detail['product']['description'],
                        'unit' => [
                            'lg' => [
                                'code' => $detail['product']['unit_lg']
                            ],
                            'sm' => [
                                'code' => $detail['product']['unit_sm'],
                                'value' => $detail['product']['unit_qty']
                            ],
                        ],
                        'min_qty' => $detail['product']['min_qty'],
                    ]);
                    $product = app(\App\Http\Controllers\Api\Master\ProductController::class)->store($reqProd);
                }
                $po->poPembelianDetails()->create([
                    'product_id' => $product->product_id,
                    'unit_lg' => $product->unit_lg,
                    'unit_sm' => $product->unit_sm,
                    'qty_lg' => $detail['qty_lg'],
                    'qty_sm' => $detail['qty_sm'],
                    'price_lg' => $detail['price_lg'],
                    'price_sm' => $detail['price_sm'],
                    'tax' => $detail['tax'],
                    'discount' => $detail['discount'],
                    'amount' => $detail['amount'],
                ]);
            }
        }

        $code = new Request([
            'company_id' => $po->company_id,
            'code_wo' => $po->code_spk,
            'term_numb' => $request->to_termin ? substr($request->to_termin, 7) : 0,
            'term_total' => $po->order->orderTermins->count(),
        ]);
        $code = app(\App\Http\Controllers\Api\Pembelian\PembelianController::class)->codeInv($code, 1);
        $po->update(compact('code'));

        return $po;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $po = $this->po->with(['poPembelianDetails', 'poProject', 'order', 'pembelian', 'user', 'warehouse'])->show($id);
        
        return $this->success(null, 200, $po);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // 
    }
    
    public function approve(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'auto_inv' => 'required|in:yes,no',
            'invoice' => 'nullable|required_if:auto_inv,no',
            'payment' => 'required|in:cash,credit',
            'due_date' => 'nullable|required_if:payment,credit',
            'coa_code' => 'nullable|required_if:payment,cash',
            'ongkir' => 'nullable|digits_between:1,20',
            'ongkir_desc' => 'nullable',
            'biaya_lain' => 'nullable|digits_between:1,20',
            'biaya_lain_desc' => 'nullable',
        ]);
        if ($validator->fails()) {
            return $this->fail('fail_validation', 422, $validator->errors());
        }
        $model = $this->po->getModel();
        $po = $model->find($id);
        \DB::beginTransaction();
        try {
            if (!in_array($po->status, ['pending'])) {
                return $this->fail('fail_process', 422, ['status' => $po->status]);
            }
            // PO Details
            $ongkir = $request->ongkir ?? $po->ongkir;
            $biayaLain = $request->biaya_lain ?? $po->biaya_lain;
            $coaCode = $request->coa_code && $request->coa_code == 'null' ? null : $request->coa_code;
            $dueDate = $request->due_date && $request->due_date == 'null' ? null : $request->due_date;
            if ($dueDate) {
                $dueDate = $po->created_at->add($dueDate, 'day')->format('d/m/Y');
            }
            $invoice = $request->invoice;
            if ($request->auto_inv == 'yes') {
                $invoice = new Request([
                    'company_id' => $po->company_id,
                    'code_wo' => $po->code_spk,
                ]);
                $invoice = app(\App\Http\Controllers\Api\Pembelian\PembelianController::class)->codeInv($invoice, 1);
            } else {
                if ($model->where('code', $invoice)->count() > 1) {
                    return $this->fail("Duplicate entry $invoice", 422);
                }
            }
            $po->update([
                'code' => $invoice,
                'ongkir' => $ongkir,
                'ongkir_desc' => $request->ongkir_desc == 'null' ? null : $request->ongkir_desc,
                'biaya_lain' => $biayaLain,
                'biaya_lain_desc' => $request->biaya_lain_desc == 'null' ? null : $request->biaya_lain_desc,
                'total' => $po->poPembelianDetails()->sum('amount') + $ongkir + $biayaLain,
                'due_date' => $dueDate,
                'payment' => $request->payment ? ($request->payment == 'cash') ? 'paid' : 'unpaid' : $po->payment,
                'coa_code' => $coaCode,
                'status' => 'close',
            ]);
            $po->order->update(['limit_cost' => $po->order->limit_cost - $po->total]);
            // Pembelian
            $purch = $po->only('po_pb_id', 'po_pro_id', 'company_id', 'code', 'due_date', 'ongkir', 'ongkir_desc', 'biaya_lain', 'biaya_lain_desc', 'total', 'payment', 'status', 'poPembelianDetails');
            $purch = array_merge($purch, [
                'approve_date' => \Carbon\Carbon::now()->toDateString(),
                'signed' => authExecute(),
                'status' => 'approve',
            ]);
            $purch = app(\App\Http\Controllers\Api\Pembelian\PembelianController::class)->store($purch);
            if (isset($purch['error'])) {
                return $this->fail($purch['error'], 422);
            }
            $pembelian = $purch['success'];
            // Product
            $product = ['status' => 'pembelian', 'ref' => $pembelian->code];
            foreach ($pembelian->pembelianDetails as $detail) {
                $product = array_merge($product, [
                    'unit_lg' => $detail->product->unit_lg,
                    'unit_sm' => $detail->product->unit_sm,
                    'qty_lg' => $detail->qty_lg,
                    'qty_sm' => $detail->qty_sm,
                    'cur_qty_lg' => $detail->qty_lg,
                    'cur_qty_sm' => $detail->qty_sm,
                    'price_lg' => $detail->price_lg,
                    'price_sm' => $detail->price_sm,
                ]);
                $detail->product->productDetails()->create($product);
                app(\App\Http\Controllers\Api\Master\ProductController::class)->updateStock($detail->product_id);
            }
            // Post GL & Check payment
            postpembelian($pembelian->company_id, ($pembelian->payment == 'paid') ? 'cash' : 'credit',  $pembelian->code, $pembelian->dpp, $pembelian->tax, $pembelian->discount, $pembelian->total);
            if ($pembelian->payment == 'paid') {
                $approveDate = \Carbon\Carbon::parse($pembelian->approve_date)->format('d/m/Y');
                $message = postpaid($pembelian->company_id, $po->coa_code, $pembelian->code, $approveDate, $pembelian->total);
                if ($message['status'] === 'failed') {
                    return $this->fail($message['errors'], 422);
                }
            } else {
                prosesHutang([
                    "company_id" => $pembelian->company_id,
                    "pembelian_id" => $pembelian->pembelian_id,
                    "total" => $pembelian->total,
                    "paid_off" => ($pembelian->payment == 'paid') ? $pembelian->total : 0,
                    "balance" => ($pembelian->payment == 'unpaid') ? $pembelian->total : 0,
                ], $pembelian->company->coa_hutang);
            }
            \DB::commit();
            return $this->success('success_process', 200, ['status' => $po->status]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->fail('fail_process', 500, $e->getMessage());
        }
    }
    
    public function reject(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'rejected' => 'required|boolean',
        ]);
        if ($validator->fails()) {
            return $this->fail('fail_validation', 422, $validator->errors());
        }
        $status = $request->boolean('rejected') ? 'reject' : 'open';
        $po = $this->po->show($id);
        $po->update(['status' => $status, 'notes' => $request->notes]);
        
        return $this->success(null, 200, $po);
    }
    
    public function void($id)
    {
        $po = $this->po->show($id);
        $po->update([
            'status' => 'void',
            'notes' => 'Void by '.auth()->user()->username,
        ]);
        
        return $this->success(null, 200, $po);
    }
}
