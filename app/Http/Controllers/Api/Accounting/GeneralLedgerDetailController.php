<?php

namespace App\Http\Controllers\Api\Accounting;

use Illuminate\Http\Request; 
use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Accounting\GeneralLedgerDetail as GeneralLedgerDetailRequest;
use App\Repositories\Accounting\GeneralLedgerDetail as GeneralLedgerDetailRepository;
use App\Models\Accounting\GeneralLedgerDetail as GLDetail;
use App\Models\Accounting\Coa as COA;
use App\Models\Accounting\GeneralLedger as GL; 

class GeneralLedgerDetailController extends ApiController
{
    protected $generalLedgerDetail;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->generalLedgerDetail = new GeneralLedgerDetailRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(GeneralLedgerDetailRequest $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $generalLedgerDetail = $this->generalLedgerDetail->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $generalLedgerDetail);
    }

    public function gldet(GeneralLedgerDetailRequest $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.','.$request->filter : $filter;
            break;
        }
        $company_id = auth()->user()->company_id;
        $generalLedgerDetail = $this->generalLedgerDetail->all($request->merge(compact('filter'))->all()); 
        

            $glid = $generalLedgerDetail[0]->gl_id;

            $glData = GL::where('gl_id',$glid)->where('company_id',$company_id)->first();
            $startBalance = $glData->start_balance;
            $code = $glData->coa_code;

            $CoaData= Coa::where('code',$code)->where('company_id',$company_id)->first();
            $normal = $CoaData->normal_balance;

            $data = collect();
            $balance = $startBalance;

            foreach ($generalLedgerDetail as $dt) {
                $gl_d_id = $dt->gl_d_id;
                $gl_id = $dt->gl_id;
                $reference = $dt->reference;
                $description = $dt->description;
                $debit = $dt->debit;
                $credit = $dt->credit;
                $created_at = $dt->created_at;
                $updated_at = $dt->updated_at;
                
                if($normal == "D"){
                    $balance = $balance + $debit - $credit;
                }else{
                    $balance = $balance + $credit - $debit; 
                }

                $array = array(
                    'gl_d_id' => $gl_d_id,
                    'gl_id' => $gl_id,
                    'reference' => $reference,
                    'description' => $description,
                    'debit' => $debit,
                    'credit' => $credit,
                    'balance' => $balance,
                    'created_at' => $created_at,
                    'updated_at' => $updated_at,
                );
                $data->push($array); 
            }
        return $this->success(null, 200, $data);
        
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function searchdata(Request $request)
    {
        $id = $request->id;
        $datefrom = $request->date_from;
        $dateto = $request->date_to;
        $from = \Carbon\Carbon::createFromFormat('d/m/Y', $datefrom)->toDateString();
        $to = \Carbon\Carbon::createFromFormat('d/m/Y', $dateto)->toDateString();

        
        $GLData = GL::where('gl_id',$id)->first();
        $coa_code = $GLData->coa_code;
        $debit = $GLData->debit;
        $credit = $GLData->credit;
        $start_balance = $GLData->start_balance;
        $total_balance = $GLData->balance; 

        $CoaData= COA::where('code',$coa_code)->first();
        $normal_balance = $CoaData->normal_balance;
             

        $before_debit = GLDetail::where('gl_id','=',$id)
                        ->where("created_at",'<' , $from)
                        ->sum('debit');
        $before_credit = GLDetail::where('gl_id','=',$id)
                        ->where("created_at",'<' , $from)
                        ->sum('credit');
        if($normal_balance == "D"){
            $before_balance = $start_balance + $before_debit - $before_credit;
        }else{ 
            $before_balance = $start_balance + $before_credit - $before_debit;
        }                

        $data = GLDetail::where('gl_id','=',$id)
                        ->whereBetween("created_at", [$from, $to])
                        ->orderby('gl_d_id','ASC')
                        ->get();    
        
        $datagldetail = collect();           
        $bal = $before_balance;    
        foreach ($data as $dt) {
            $gl_d_id = $dt->gl_d_id;
            $gl_id = $dt->gl_id;
            $reference = $dt->reference;
            $description = $dt->description;
            $debit = $dt->debit;
            $credit = $dt->credit;
            $created_at = $dt->created_at;
            $updated_at = $dt->updated_at;
            
            if($normal_balance == "D"){
                $bal = $bal + $debit - $credit;
            }else{
                $bal = $bal + $credit - $debit; 
            }

            $array = array(
                'gl_d_id' => $gl_d_id,
                'gl_id' => $gl_id,
                'reference' => $reference,
                'description' => $description,
                'debit' => $debit,
                'credit' => $credit,
                'balance' => $bal,
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            );
            $datagldetail->push($array); 
        }                
                        
                        
        $data_debit = GLDetail::where('gl_id','=',$id)
                        ->whereBetween("created_at", [$from, $to])
                        ->sum('debit');
        $data_credit = GLDetail::where('gl_id','=',$id)
                        ->whereBetween("created_at", [$from, $to])
                        ->sum('credit');
        $total_debit = $before_debit + $data_debit;
        $total_credit = $before_credit + $data_credit;

        if($normal_balance == "D"){
            $total_balance = $start_balance+ $total_debit - $total_credit;
        }else{ 
            $total_balance =  $start_balance+ $total_credit - $total_debit;
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Data loaded successfully',
            'before_balance' => $before_balance, 
            'total_debit' => $total_debit, 
            'total_credit' => $total_credit, 
            'total_balance' => $total_balance, 
            'data' => $datagldetail,
        ]);
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}