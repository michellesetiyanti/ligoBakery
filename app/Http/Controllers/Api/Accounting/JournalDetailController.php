<?php

namespace App\Http\Controllers\Api\Accounting;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Accounting\JournalDetail as JournalDetailRequest;
use App\Repositories\Accounting\JournalDetail as JournalDetailRepository;
use App\Models\Accounting\JournalDetail as jDModel;
use App\Models\Accounting\GeneralLedger as GL;
use App\Models\Accounting\GeneralLedgerDetail as GLDetail;
use App\Models\Accounting\Journal;
use App\Models\Accounting\Coa;
use Carbon\Carbon;

class JournalDetailController extends ApiController
{
    protected $jD;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->jD = new JournalDetailRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $journalDetail = $this->jD->with(['user', 'coa'])->all($request->all()); 
        return $this->success(null, 200, $journalDetail);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JournalDetailRequest $request)
    {
        $company_id = auth()->user()->company_id;
        \DB::beginTransaction();
        try {
            $coa_code= $request->coa_code;
            $journal_id = $request->journal_id;
            $description = $request->description;
            $status = $request->status;
            $amount = $request->amount;
            $userid = auth()->user()->user_id;    
            $date = Carbon::now();
            $date=date_create($date);
            $date= date_format($date,"Y-m-d"); 

            $jData = Journal::where("journal_id",$journal_id)->first();
            $jCode = $jData->code;
            $jDebit = $jData->debit;
            $jCredit = $jData->credit; 
            
            
            $journalDetail = new jDModel;
            $journalDetail->user_id = $userid;
            $journalDetail->journal_id = $journal_id;
            $journalDetail->coa_code= $coa_code;
            $journalDetail->description=$description;
            $journalDetail->entry_at=$date;
            if($status == "debit"){
                $debit= $amount;
                $credit= 0;

                $journalDetail->debit=$amount;
                $jDebit = $jDebit+$amount;
            }else{
                $debit= 0;
                $credit= $amount;

                $jCredit = $jCredit+$amount;
                $journalDetail->credit=$amount;
            } 
            $journalDetail->save();

            $data = array(
                'debit' => $jDebit,
                'credit' => $jCredit,
              );
            
          
            $cek = Journal::where('journal_id','=',$journal_id)->update($data);             


            \DB::commit();
            return $this->success(trans('general.journal')." ".trans('general.create.success', ['name' => $journalDetail->name]), 200, $journalDetail);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JournalDetailRequest $request, $id)
    {
        $company_id = auth()->user()->company_id;
        $remark = $request->remark;
        $amount = $request->amount;  
        $status = $request->status;  
        $a =  jDModel::where('journal_d_id','=',$id)->first();
        $coa_code = $a->coa_code;
        $journal_id = $a->journal_id;
        $dbitAwal = $a->debit;
        $creditAwal = $a->credit;

        $dtjournal = Journal::where('journal_id','=',$journal_id)->first();
        $code_journal = $dtjournal->code;
        $dtDebitAwal = $dtjournal->debit;
        $dtCreditAwal = $dtjournal->credit;

        $dtDebit = $dtDebitAwal - $dbitAwal;
        $dtCredit = $dtCreditAwal - $creditAwal;

        \DB::beginTransaction();
        try { 

            if($status == "credit"){
                $debit = 0;
                $credit = $amount;
                $data = array(
                'description' => $remark,
                'credit' => $amount,
              ); 
              
            }else{
                $debit = $amount;
                $credit = 0;
                $data = array(
                'description' => $remark,
                'debit' => $amount,
              );
            }  
            jDModel::where('journal_d_id','=',$id)->update($data);   
            
            if($status == "debit"){ $dtDebit =$dtDebit + $amount;  }else{ $dtCredit =$dtCredit + $amount; }
            $data = array(
                'debit' => $dtDebit,
                'credit' => $dtCredit,
              );
            
            Journal::where('journal_id','=',$journal_id)->update($data);


            \DB::commit();
            return $this->success("Journal Updated Successfully", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company_id = auth()->user()->company_id;

        $journalDetail = $this->jD->show($id);
        \DB::beginTransaction();
        try { 
            $journal_id = $journalDetail->journal_id;
            $coa_code = $journalDetail->coa_code;
            $debit = $journalDetail->debit;
            $credit = $journalDetail->credit;

            $cekJournal = Journal::where('journal_id',$journal_id)->first();
            $journalCode = $cekJournal->code;
            $totalCredit = $cekJournal->credit;
            $totalDebit = $cekJournal->debit;

            $totalDebit = $totalDebit-$debit;
            $totalCredit = $totalCredit-$credit;

            $data = array(
                'debit' => $totalDebit,
                'credit' => $totalCredit,
              );
            
            Journal::where('journal_id','=',$journal_id)->update($data); 


            $journalDetail->delete(); 
            \DB::commit();
            return $this->success(trans('general.journal')." ".trans('general.delete.success', ['name' => $journalDetail->journal_d_id]));
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}