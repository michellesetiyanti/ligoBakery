<?php

namespace App\Http\Controllers\Api\Accounting;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Accounting\Adjustment as AdjustmentRequest;
use App\Repositories\Accounting\Adjustment as AdjustmentRepository; 
use App\Repositories\Master\Product as ProductRepository;
use App\Repositories\Master\ProductDetail as ProductDetailRepository;
use App\Models\Master\Product;
use App\Models\Master\ProductDetail as PD;
use App\Models\Accounting\Adjustment as Adjust;
use App\Models\Accounting\AdjustmentDetail as AdjustDetail;

class AdjustmentController extends ApiController
{
    protected $adjustment;
    protected $product;
    protected $productDetail;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->adjustment = new AdjustmentRepository;
        $this->product = new ProductRepository;
        $this->productDetail = new ProductDetailRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'wh_id|'.auth()->user()->company->warehouse->wh_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }   
        $adjustment = $this->adjustment->with(['adjustmentDetails', 'warehouse', 'user'])->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $adjustment);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AdjustmentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\Illuminate\Http\Request $request)
    {    
        \DB::beginTransaction();
        try { 
            $userid = auth()->user()->userid;
            $company_id = auth()->user()->company_id;
            $adjustid = $request->adjust_id;
            $whid = $request->wh_id;
            $productid = $request->product_id;
            $description = $request->description;
            $entrydate = $request->entrydate;
            $qtylg = $request->qtylg;
            $qtysm = $request->qtysm;
            $pricesm = $request->pricesm;


            if($adjustid == null){
                $cek = Adjust::where('wh_id',$whid)->count();
                $cek = $cek + 1;
                $ref = "ADJUST-".$cek;

                $adjust = new Adjust;
                $adjust->user_id = $userid;
                $adjust->company_id = $company_id;
                $adjust->wh_id = $whid;
                $adjust->date_entry = $entrydate;
                $adjust->reference = $ref;
                $adjust->description = $description;
                $adjust->status = 'open';
                $adjust->save();

                $adjustid = $adjust->adjust_id;
            }else{
                $dtadjust = array(
                    'wh_id'=>$whid,
                    'date_entry'=>$entrydate,
                    'description'=>$description,
                );
                Adjust::where('adjust_id',$adjustid)->update($dtadjust);
            }


            //cek double product 
            $cekprod = AdjustDetail::where('product_id',$productid)->where('adjust_id',$adjustid)->count();
            if($cekprod >0){
                return $this->fail('Post Error', 500, 'Product Double');
            }

            $adjustDetail = new AdjustDetail;
            $adjustDetail->adjust_id = $adjustid;
            $adjustDetail->product_id = $productid;
            $adjustDetail->qty_lg = $qtylg;
            $adjustDetail->qty_sm = $qtysm;
            $adjustDetail->price_sm = $pricesm;
            $adjustDetail->save();

            
            $data = array(
                'adjustment_detail'=>$adjustDetail, 
            );

            \DB::commit();
            return $this->success(null, 200, $data);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($adjustment = $this->adjustment->with(['adjustmentDetails', 'warehouse', 'user'])->show($id)) {
            return $this->success(null, 200, $adjustment);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AdjustmentRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\Illuminate\Http\Request $request, $id)
    {
        $adjustmentDetail = AdjustDetail::where('adjust_d_id',$id)->first();
        \DB::beginTransaction();
        try{
            $qtylg = $request->qtyLg;
            $qtysm = $request->qtysm;
            $pricesm = $request->pricesm;

            $data = array(
                'qty_lg'=>$qtylg,
                'qty_sm'=>$qtysm,
                'price_sm'=>$pricesm,
            );
            $update = AdjustDetail::where('adjust_d_id',$id)->update($data);
            \DB::commit();
            return $this->success(null, 200, $update);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function submitadjustment($id)
    {
        \DB::beginTransaction();
        try{
            $data = array(
                'status'=>'pending',
            );
            $update = Adjust::where('adjust_id',$id)->update($data);
            \DB::commit();
            return $this->success(null, 200, $update);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        \DB::beginTransaction();
        try { 

            $cek = Adjust::where('adjust_id',$id)->delete();
            \DB::commit();
            return $this->success("Adjustment successfully Deleted",200,$cek);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Do Action the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function cekMutasi(\Illuminate\Http\Request $request){

                         $adjust_qty_lg =  $request->qty_lg;
                         if($adjust_qty_lg == "undefined" ){ $adjust_qty_lg = 0;}
                        $adjust_qty_sm =  $request->qty_sm;
                         if($adjust_qty_sm == "undefined" ){ $adjust_qty_sm = 0;}
                        $product_id = $request->product_id;
                        
                        $productById = Product::where('product_id', $product_id)->first();
                        $unit_qty = $productById->unit_qty; 
                        $unit_lg = $productById->unit_lg; 
                        $unit_sm = $productById->unit_sm; 
                        $price_lg = $productById->price_lg; 
                        $price_sm = $productById->price_sm; 

                        

                        $totalcur_qty_lg =  $productById->total_lg_qty;
                        $totalcur_qty_sm =  $productById->total_sm_qty;
                        $cek1 =  ($totalcur_qty_lg * $unit_qty) + $totalcur_qty_sm ;

                        $total_sm_qty = $adjust_qty_sm % $unit_qty; 
                        $total_lg_qty = (($adjust_qty_sm - $total_sm_qty)/$unit_qty)+$adjust_qty_lg;  
                        $cek2 =  ($total_lg_qty * $unit_qty) + $total_sm_qty;
                        
                        if($totalcur_qty_lg == null && $totalcur_qty_sm == null){
                            return "Mutasi Tambah";
                        }else if($cek1 < $cek2){
                            return "Mutasi Tambah";
                        }else{
                            return "Mutasi Kurang";
                        } 
     }

    public function doAction($id)
    {
         
        $adjustment = Adjust::where('adjust_id',$id)->with(['adjustmentDetails', 'warehouse', 'user'])->first();
        $reference = $adjustment->reference;
        \DB::beginTransaction();
        try { 
            
            foreach ($adjustment->adjustmentDetails as $detail) {
                        $adjust_qty_lg =  $detail->qty_lg;
                        $adjust_qty_sm =  $detail->qty_sm;
                        $adjust_price_sm =  $detail->price_sm;
                        $productById = Product::where('product_id', $detail->product_id)->first();
                        $unit_qty = $productById->unit_qty; 
                        $unit_lg = $productById->unit_lg; 
                        $unit_sm = $productById->unit_sm; 
                        $price_lg = $productById->price_lg; 
                        $price_sm = $productById->price_sm; 

                        $totalcur_qty_lg =  $productById->total_lg_qty;
                        $totalcur_qty_sm =  $productById->total_sm_qty;
                        $cek1 =  ($totalcur_qty_lg * $unit_qty) + $totalcur_qty_sm ;

                        $total_sm_qty = $adjust_qty_sm % $unit_qty; 
                        $total_lg_qty = (($adjust_qty_sm - $total_sm_qty)/$unit_qty)+$adjust_qty_lg;  
                        $cek2 =  ($total_lg_qty * $unit_qty) + $total_sm_qty ; 
                        

                        if($totalcur_qty_lg == null && $totalcur_qty_sm == null){
                            $totalmt = PD::where('status','mutasi%')->count();
                            $totalmt = $totalmt + 1;

                            $pd = new PD;
                            $pd->product_id = $detail->product_id;
                            $pd->status = "mutasi tambah";
                            $pd->ref = $reference;
                            $pd->batch = "MT-$totalmt";
                            $pd->unit_lg = $unit_lg;
                            $pd->unit_sm = $unit_sm;
                            $pd->qty_lg = $total_lg_qty;
                            $pd->qty_sm = $total_sm_qty;
                            $pd->cur_qty_lg = $total_lg_qty;
                            $pd->cur_qty_sm = $total_sm_qty;
                            $pd->price_lg = $price_lg;
                            $pd->price_sm = $adjust_price_sm;
                            $pd->save(); 

                        }else if($cek1 > $cek2){ //current lebih banyak dari input(barang system kurang dari qty inputan)
                            $datapd = PD::where('product_id', $detail->product_id)->where('status','!=','penjualan')->orderBy('product_d_id','ASC')->get();

                            $x = 1;
                            foreach ($datapd as $dtpd) {
                                $ddproductid = $dtpd->product_d_id;
                                $ddbatch = $dtpd->batch;
                                $ddunit_lg = $dtpd->unit_lg;
                                $ddunit_sm = $dtpd->unit_sm;
                                $ddqty_lg = $dtpd->qty_lg;
                                $ddqty_sm = $dtpd->qty_sm;
                                $ddcur_qty_lg = $dtpd->cur_qty_lg;
                                $ddcur_qty_sm = $dtpd->cur_qty_sm;
                                $ddprice_lg = $dtpd->price_lg;
                                $ddprice_sm = $dtpd->price_sm;
                            

                                $ddtotal = ($ddqty_lg * $unit_qty) + $ddqty_sm ;
                                $ddcurtotal = ($ddcur_qty_lg * $unit_qty) + $ddcur_qty_sm ;

                                if($ddcurtotal > 0){ //check current batch is empty or no 
                                     
                                    if($cek2 < 0){
                                        break;
                                    }else if($cek2 >0){  
                                            
                                        if($cek2 < $ddcurtotal){
                                            $realjmlh = $ddcurtotal - $cek2;
                                            $realqtysm = $realjmlh % $unit_qty; 
                                            $realqtylg = ($realjmlh - $realqtysm)/$unit_qty;   

                                            $pdqtysm = $cek2 % $unit_qty ; 
                                            $pdqtylg = ($cek2 - $pdqtysm)/$unit_qty;   
 
                                        }else{
                                            $realqtysm = 0;
                                            $pdqtysm = $ddqty_sm;
                                            $realqtylg = 0;
                                            $pdqtylg = $ddqty_lg;
                                        }
                                        $dataDetail = array(
                                            'cur_qty_lg'=>$realqtylg,
                                            'cur_qty_sm'=>$realqtysm,
                                        ); 
                                        PD::where('product_d_id',$ddproductid)->update($dataDetail); 

                                        $pd = new PD;
                                        $pd->product_id = $detail->product_id;
                                        $pd->status = "mutasi kurang";
                                        $pd->ref = $reference;
                                        $pd->batch = $ddbatch;
                                        $pd->unit_lg = $ddunit_lg;
                                        $pd->unit_sm = $ddunit_sm;
                                        $pd->qty_lg = $pdqtylg;
                                        $pd->qty_sm = $pdqtysm;
                                        $pd->cur_qty_lg = 0;
                                        $pd->cur_qty_sm = 0;
                                        $pd->price_lg = $ddprice_lg;
                                        $pd->price_sm = $ddprice_sm;
                                        $pd->save(); 

                                     
                                    }    
                                }

                                $cek2 = $cek2 - $ddcurtotal;
                            }  //endforeach    

                        }else if($cek1 < $cek2){ //current kurang dari qty input (barang lebih dari system)
                            $datapd = PD::where('product_id', $detail->product_id)->where('status','!=','penjualan')->orderBy('product_d_id','ASC')->get();
                            $cek2 = $cek2 - $cek1 ;
 
                            $real_sm_qty = $cek2 % $unit_qty; 
                            $real_lg_qty = ($cek2 - $real_sm_qty)/$unit_qty;  

                            $totalmt = PD::where('status','mutasi%')->count();
                            $totalmt = $totalmt + 1;

                            $pd = new PD;
                            $pd->product_id = $detail->product_id;
                            $pd->status = "mutasi tambah";
                            $pd->ref = $reference;
                            $pd->batch = "MT-$totalmt";
                            $pd->unit_lg = $unit_lg;
                            $pd->unit_sm = $unit_sm;
                            $pd->qty_lg = $real_lg_qty;
                            $pd->qty_sm = $real_sm_qty;
                            $pd->cur_qty_lg =  $real_lg_qty;
                            $pd->cur_qty_sm = $real_sm_qty;
                            $pd->price_lg = $price_lg;
                            $pd->price_sm = $adjust_price_sm;
                            $pd->save(); 
                              
                        } 
                //update total qty product
                $dataDetail = array(
                    'total_lg_qty'=>$total_lg_qty,
                    'total_sm_qty'=>$total_sm_qty,
                );
                Product::where('product_id',$detail->product_id)->update($dataDetail); 

                $data = array('status'=>'complete');
                Adjust::where('adjust_id',$id)->update($data); 
            } 
            
           
 
            \DB::commit();
            return $this->success(null, 200, $adjustment);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Import.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'wh_id' => 'required|integer|exists:warehouses,wh_id',
            'reference' => 'required|string',
            'attachment.*' => 'file|image|mimes:jpeg,png,jpg,doc,ppt,xls,pdf|max:2048',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        // Adjustment
        $adjustment = $this->adjustment->all()->where('wh_id', $request->wh_id)->where('reference', $request->reference);
        if ($adjustment->isEmpty()) {
            $adjustment = $this->adjustment->save($request->except('attachment'));
        } else {
            $adjustment = $adjustment->first();
        }
        try {
            $adjustment->import = $request->attachment;
            $adjustment->save();
            return $this->success(null, 200, $adjustment->import);
        } catch (\Exception $e) {
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
