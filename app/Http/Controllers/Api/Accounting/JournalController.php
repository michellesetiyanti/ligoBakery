<?php

namespace App\Http\Controllers\Api\Accounting;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Accounting\Journal as JournalRequest;
use App\Repositories\Accounting\Journal as JournalRepository;
use App\Models\Accounting\Journal;
use App\Models\Accounting\JournalDetail;

class JournalController extends ApiController
{
    protected $journal;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->journal = new JournalRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(JournalRequest $request)
    {
        $journal = $this->journal->with('user')->all($request->all());
        $journal->where('company_id', auth()->user()->company_id);
        return $this->success(null, 200, $journal);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JournalRequest $request)
    {
        \DB::beginTransaction();
        try {
            $journalname= $request->name;
            $journaldate = $request->date;
            $journaldesc = $request->description;
            $userid = auth()->user()->user_id;    
            $company_id = auth()->user()->company_id;
            $year = substr($journaldate,2,2);

            $cektotal = Journal::where('company_id','=',$company_id)->count(); 
            $code =  $cektotal+1; 
            
            $journal = new Journal;
            $journal->user_id = $userid;
            $journal->company_id = $company_id;
            $journal->code= "JR-".$year."-".$code;
            $journal->name=$journalname;
            $journal->entry_at=$journaldate;
            $journal->debit=0;
            $journal->credit=0;
            $journal->status="Open";
            $journal->description=$journaldesc;
            $journal->save();
            \DB::commit();
            return $this->success(trans('general.coa')." ".trans('general.create.success', ['name' => $journal->name]), 200, $journal);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($generalLedger = $this->generalLedger->show($id)) {
            return $this->success(null, 200, $generalLedger);
        } else {
            return $this->notFound(trans('general.coa')." ".trans('general.not_found'));
        }
    }
    
    public function complete(JournalRequest $request)
    {
        $journalid = $request->journalid; 
        $dataJournal = Journal::where('journal_id',$journalid)->first();
        $jCode = $dataJournal->code;
         $company_id = auth()->user()->company_id;

        \DB::beginTransaction();
        try { 
            $countJDetail = JournalDetail::where('journal_id','=',$journalid)->count();
            $JDetail = JournalDetail::where('journal_id','=',$journalid)->get();
            for($i=0; $i < $countJDetail; $i++){
                $coa_code = $JDetail[$i]->coa_code;
                $description = $JDetail[$i]->description;
                $debit = $JDetail[$i]->debit;
                $credit = $JDetail[$i]->credit;

                $a = postgljournal($company_id, $coa_code, $jCode, $description, $debit, $credit); 
                if($a != "Success"){
                    return "Failed $a";
                }
            } 

            $data = array(
                'status' => "Complete"
              ); 
            $cek = Journal::where('journal_id','=',$journalid)->update($data);

            \DB::commit();
            return $this->success("Journal Completed", 200,null );
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }


       
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $journal = $this->journal->show($id);
        \DB::beginTransaction();
        try { 
            $journalstatus = $journal->status;
            if($journalstatus != "Open"){
                return $this->fail(null, 500, "Journal Already Complete Can't Be Delete ");
            }else{
                $cek = JournalDetail::where("journal_id",$id)->count();
                if($cek != 0){
                    return $this->fail(null, 500, "Please Empty The Post In This Journal!");
                }else{
                    $journal->delete();  
                }
                
            }

            \DB::commit();
            return $this->success(trans('general.journal')." ".trans('general.delete.success', ['name' => $journal->journal_id]));
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}