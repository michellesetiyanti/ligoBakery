<?php

namespace App\Http\Controllers\Api\Accounting;

use Illuminate\Http\Request; 

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Accounting\GeneralLedger as GeneralLedgerRequest;
use App\Repositories\Accounting\GeneralLedger as GeneralLedgerRepository;
use App\Models\Accounting\GeneralLedgerDetail as GLDetail;
use App\Models\Accounting\Coa as COA;
use App\Models\Accounting\PersediaanBarang ;
use App\Models\Accounting\Closing as CLOSING;
use App\Models\Accounting\Journal as JOUR;
use App\Models\Accounting\JournalDetail as JOURDetail;
use App\Models\Accounting\GeneralLedger as GLL;
use App\Models\Master\Company;
use App\User;
Use \Carbon\Carbon;

class GeneralLedgerController extends ApiController
{
    protected $generalLedger;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->generalLedger = new GeneralLedgerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $generalLedger = $this->generalLedger->all($request->merge(compact('filter'))->all()); 
        return $this->success(null, 200, $generalLedger);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($generalLedger = $this->generalLedger->with('coa')->show($id)) {
            return $this->success(null, 200, $generalLedger);
        } else {
            return $this->notFound(null);
        }
    }

    public function showlabarugi(Request $request)
    {
        $company_id = auth()->user()->company_id;
        $company = Company::where('company_id',$company_id)->first();
        $coa_penjualan = $company->coa_penjualan;
        $coa_pembelian = $company->coa_pembelian;

        $head_penjualan = COA::where('code',$coa_penjualan)->where('company_id',$company_id)->first();
        $fromcode= $head_penjualan->fromcode;

        $data = collect();


        $penjualan = COA::where('fromcode',$fromcode)->where('company_id',$company_id)->get();
        $penjualan= GLL::where('coa_code','LIKE',$fromcode.'%')->where('company_id',$company_id)
                          ->get();
        $totalpenjualan= GLL::where('coa_code','LIKE',$fromcode.'%')->where('company_id',$company_id)
                          ->sum('balance');
        $pendapatanluar = GLL::where('coa_code','LIKE',"7.01%")->where('company_id',$company_id)->get();
        $totalpendapatanluar = GLL::where('coa_code','LIKE',"7.01%")->where('company_id',$company_id)->sum('balance');
       
        $pembelian = GLL::where('coa_code','LIKE',"5.03%")->where('company_id',$company_id)->get();
        $totalpembelian = GLL::where('coa_code','LIKE',"5.03%")->where('company_id',$company_id)->sum('balance');
        
                          
        $pbd = GLL::where('coa_code','=',$coa_pembelian)->where('company_id',$company_id)
                          ->first();

        $jmlbeban = COA::where('fromcode','6')->where('company_id',$company_id)
                        ->where('level','2')->count();
        $databeban = COA::where('fromcode','6')->where('company_id',$company_id)
                        ->where('level','2')->get(); 
        $totalbeban = 0;
        for($i = 1; $i<= $jmlbeban; $i++){
                $text = 'beban'.$i;
                $a = $i-1;  

                $fc = $databeban[$a]->code;
                $name = $databeban[$a]->name;
                $bb = GLL::where('coa_code','LIKE',$fc.'%')->where('company_id',$company_id)
                          ->get(); 
                $ttl = GLL::where('coa_code','LIKE',$fc.'%')->where('company_id',$company_id)
                              ->sum('balance'); 
                $totalbeban = $totalbeban + $ttl;      
                          $array = array(
                                'name'=> $name,
                                'data'=> $bb,
                                'total'=>$ttl, 
                                );        
                $data->push($array); 
        }

        $bebanluar = GLL::where('coa_code','LIKE',"8.01%")->where('company_id',$company_id)->get();
        $totalbebanluar = GLL::where('coa_code','LIKE',"8.01%")->where('company_id',$company_id)->sum('balance');

        $bebanpajak = GLL::where('coa_code','LIKE',"9.01%")->where('company_id',$company_id)->get();
        $totalbebanpajak = GLL::where('coa_code','LIKE',"9.01%")->where('company_id',$company_id)->sum('balance');
 
       
        return response()->json([
            'status' => 'success',
            'message' => 'Data loaded successfully',
            'penjualan' => $penjualan,  
            'totalpenjualan' => $totalpenjualan,  
            'pendapatanluar'=>$pendapatanluar,
            'totalpendapatanluar'=>$totalpendapatanluar, 
            'pembelian'=>$pembelian,
            'totalpembelian'=>$totalpembelian,
            'pbd' => $pbd,  
            'beban' => $data,  
            'totalbeban' => $totalbeban,  
            'bebanluar' => $bebanluar,  
            'totalbebanluar' => $totalbebanluar,  
            'bebanpajak' => $bebanpajak,  
            'totalbebanpajak' => $totalbebanpajak,  
        ]);
    }

    public function showlabarugiperiod(Request $request)
    {

        $date_start = $request->dateFrom;
        $date_end = $request->dateTo;
        
        $date = Carbon::parse($date_end); 
        $bulan = $date->format('m');
        $bulan = $bulan -1;
        $tahun = $date->format('y'); 
        $date = $date->format('Y-m-d');




        $company_id = auth()->user()->company_id;
        $company = Company::where('company_id',$company_id)->first();
        $coa_penjualan = $company->coa_penjualan;
        $coa_pembelian = $company->coa_pembelian;

        $head_penjualan = COA::where('code',$coa_penjualan)->where('company_id',$company_id)->first();
        $fromcode= $head_penjualan->fromcode;

        $pbd = PersediaanBarang::where('bulan','=',$bulan)->where('tahun','=',$tahun)->where('company_id',$company_id)->first();

        $data = collect(); 

        $countpenjualan = GLL::where('coa_code','LIKE',$fromcode.'%')->where('company_id',$company_id)
                          ->count(); 
        $penjualan= GLL::where('coa_code','LIKE',$fromcode.'%')->where('company_id',$company_id)
                          ->get(); 
            $datapenjualan = collect();
            $datatotalpenjualan = 0;
            for($i = 1; $i <= $countpenjualan; $i++ )
            {
                $a = $i - 1;
                $jglid = $penjualan[$a]->gl_id;
                $jcode = $penjualan[$a]->coa_code;
                $jname = $penjualan[$a]->coa_name;
                $jdebit = GLDetail::where('gl_id',$jglid)
                                    ->where('company_id',$company_id)
                                    ->where('created_at','>=',$date_start)
                                    ->where('created_at','<=',$date_end)->sum('debit');
                $jcredit = GLDetail::where('gl_id',$jglid)
                                    ->where('company_id',$company_id)
                                    ->where('created_at','>=',$date_start)
                                    ->where('created_at','<=',$date_end)->sum('credit');
                $jstart = $penjualan[$a]->start_balance;
                $ccoa = COA::where('code',$jcode)->first();
                $def = $ccoa->normal_balance;
                if($def =="D"){
                    $jbal = ($jdebit + $jstart )- $jcredit;
                }else{
                    $jbal = ($jcredit + $jstart) - $jdebit;

                } 
                $datatotalpenjualan = $datatotalpenjualan + $jbal;

                $array = array(
                                'gl_id'=> $jglid,
                                'coa_code'=> $jcode,
                                'coa_name'=>$jname, 
                                'debit'=>$jdebit, 
                                'credit'=>$jcredit, 
                                'start_balance'=>$jstart,    
                                'balance'=>$jbal,    
                                );        
                $datapenjualan->push($array); 
            }
        $pembelian = GLL::where('coa_code','=',$coa_pembelian)->where('company_id',$company_id)->first();
        $datapembelian = collect();
        $datatotalpembelian = 0;
                $pembelianglid = $pembelian->gl_id;
                $pembeliancode = $pembelian->coa_code;
                $pembelianname = $pembelian->coa_name;
                $pembeliandebit = GLDetail::where('gl_id',$pembelianglid)
                                    ->where('company_id',$company_id)
                                    ->where('created_at','>=',$date_start)
                                    ->where('created_at','<=',$date_end)->sum('debit');
                $pembeliancredit = GLDetail::where('gl_id',$pembelianglid)
                                    ->where('company_id',$company_id)
                                    ->where('created_at','>=',$date_start)
                                    ->where('created_at','<=',$date_end)->sum('credit');
                $pembelianstart = $pembelian->start_balance;
                $ccoa = COA::where('code',$pembeliancode)->first();
                $def = $ccoa->normal_balance;
                if($def =="D"){
                    $pembelianbal = ($pembeliandebit + $pembelianstart )- $pembeliancredit;
                }else{
                    $pembelianbal = ($pembeliancredit + $pembelianstart) - $pembeliandebit;

                } 
                $datatotalpembelian = $datatotalpembelian + $pembelianbal;

                $array = array(
                                'gl_id'=> $pembelianglid,
                                'coa_code'=> $pembeliancode,
                                'coa_name'=>$pembelianname, 
                                'debit'=>$pembeliandebit, 
                                'credit'=>$pembeliancredit, 
                                'start_balance'=>$pembelianstart,    
                                'balance'=>$pembelianbal,    
                                );        
                $datapembelian->push($array); 

        $countpendapatanluar = GLL::where('coa_code','LIKE',"7.01%")->where('company_id',$company_id)->count();                    
        $pendapatanluar = GLL::where('coa_code','LIKE',"7.01%")->where('company_id',$company_id)->get();
            $datapendapatanluar = collect();
            $datatotalpendapatanluar = 0;
            for($i = 1; $i <= $countpendapatanluar; $i++ ){
                $a = $i - 1;
                
                $pdpLuarglid = $pendapatanluar[$a]->gl_id;
                $pdpLuarcode = $pendapatanluar[$a]->coa_code;
                $pdpLuarname = $pendapatanluar[$a]->coa_name;
                $pdpLuardebit = GLDetail::where('gl_id',$pdpLuarglid)
                                    ->where('company_id',$company_id)
                                    ->where('created_at','>=',$date_start)
                                    ->where('created_at','<=',$date_end)->sum('debit');
                $pdpLuarcredit = GLDetail::where('gl_id',$pdpLuarglid)
                                    ->where('company_id',$company_id)
                                    ->where('created_at','>=',$date_start)
                                    ->where('created_at','<=',$date_end)->sum('credit');
                $pdpLuarstart = $pendapatanluar[$a]->start_balance;
                $ccoa = COA::where('code',$pdpLuarcode)->first();
                $def = $ccoa->normal_balance;
                if($def =="D"){
                    $pdpLuarbal = ($pdpLuardebit + $pdpLuarstart )- $pdpLuarcredit;
                }else{
                    $pdpLuarbal = ($pdpLuarcredit + $pdpLuarstart) - $pdpLuardebit;

                } 
                $datatotalpendapatanluar = $datatotalpendapatanluar + $pdpLuarbal;

                $array = array(
                                'gl_id'=> $pdpLuarglid,
                                'coa_code'=> $pdpLuarcode,
                                'coa_name'=>$pdpLuarname, 
                                'debit'=>$pdpLuardebit, 
                                'credit'=>$pdpLuarcredit, 
                                'start_balance'=>$pdpLuarstart,    
                                'balance'=>$pdpLuarbal,    
                                );        
                $datapendapatanluar->push($array); 
            }


        $jmlbeban = COA::where('fromcode','6')->where('company_id',$company_id)
                        ->where('level','2')->count();
        $databeban = COA::where('fromcode','6')->where('company_id',$company_id)
                        ->where('level','2')->get(); 
        $totalbeban = 0;
        for($i = 1; $i<= $jmlbeban; $i++){
                $text = 'beban'.$i;
                $a = $i-1;  

                $fc = $databeban[$a]->code;
                $name = $databeban[$a]->name;
                $def = $databeban[$a]->normal_balance;
                $bb = GLL::where('coa_code','LIKE',$fc.'%')->where('company_id',$company_id)
                          ->get(); 
                $cbb = GLL::where('coa_code','LIKE',$fc.'%')->where('company_id',$company_id)->count();        
                $databb = collect();
                $ttl = 0;
                for($i = 1; $i<= $cbb; $i++){
                    $a = $i-1;
                    $bbglid = $bb[$a]->gl_id;
                    $bbcode = $bb[$a]->coa_code;
                    $bbname = $bb[$a]->coa_name;
                    $bbdebit = GLDETAIL::where('gl_id',$bbglid)->where('company_id',$company_id)->where('created_at',$date_start)->where('created_at',$date_end)->sum('debit');
                    $bbcredit = GLDETAIL::where('gl_id',$bbglid)->where('company_id',$company_id)->where('created_at',$date_start)->where('created_at',$date_end)->sum('credit');;
                    $bbstart = $bb[$a]->start_balance;
                    if($def =="D"){
                        $bbbal = ($bbdebit + $bbstart )- $bbcredit;
                    }else{
                        $bbbal = ($bbcredit + $bbstart) - $bbdebit; 
                    } 
                    $ttl = $ttl + $bbbal;
                    $array = array(
                                'gl_id'=> $bbglid,
                                'coa_code'=> $bbcode,
                                'coa_name'=>$bbname, 
                                'debit'=>$bbdebit, 
                                'credit'=>$bbcredit, 
                                'start_balance'=>$bbstart, 
                                'balance'=>$bbbal, 
                                );        
                    $databb->push($array); 

                }   
                $totalbeban = $totalbeban + $ttl;      
                          $array = array(
                                'name'=> $name,
                                'data'=> $databb,
                                'total'=>$ttl, 
                                );        
                $data->push($array); 
        }

        $countbebanluar = GLL::where('coa_code','LIKE',"8.01%")->where('company_id',$company_id)->count();
        $bebanluar = GLL::where('coa_code','LIKE',"8.01%")->where('company_id',$company_id)->get();
            $databebanluar = collect();
            $datatotalbebanluar = 0;
            for($i = 1; $i <= $countbebanluar; $i++ ){
                $a = $i - 1;
                
                $bebanLuarglid = $bebanluar[$a]->gl_id;
                $bebanLuarcode = $bebanluar[$a]->coa_code;
                $bebanLuarname = $bebanluar[$a]->coa_name;
                $bebanLuardebit = GLDetail::where('gl_id',$bebanLuarglid)
                                    ->where('company_id',$company_id)
                                    ->where('created_at','>=',$date_start)
                                    ->where('created_at','<=',$date_end)->sum('debit');
                $bebanLuarcredit = GLDetail::where('gl_id',$bebanLuarglid)
                                    ->where('company_id',$company_id)
                                    ->where('created_at','>=',$date_start)
                                    ->where('created_at','<=',$date_end)->sum('credit');
                $bebanLuarstart = $bebanluar[$a]->start_balance;
                $ccoa = COA::where('code',$bebanLuarcode)->first();
                $def = $ccoa->normal_balance;
                if($def =="D"){
                    $bebanLuarbal = ($bebanLuardebit + $bebanLuarstart )- $bebanLuarcredit;
                }else{
                    $bebanLuarbal = ($bebanLuarcredit + $bebanLuarstart) - $bebanLuardebit;

                } 
                $datatotalbebanluar = $datatotalbebanluar + $bebanLuarbal;

                $array = array(
                                'gl_id'=> $bebanLuarglid,
                                'coa_code'=> $bebanLuarcode,
                                'coa_name'=>$bebanLuarname, 
                                'debit'=>$bebanLuardebit, 
                                'credit'=>$bebanLuarcredit, 
                                'start_balance'=>$bebanLuarstart,    
                                'balance'=>$bebanLuarbal,    
                                );        
                $databebanluar->push($array); 
            }

        $countbebanpajak =  GLL::where('coa_code','LIKE',"9.01%")->where('company_id',$company_id)->count();
        $bebanpajak = GLL::where('coa_code','LIKE',"9.01%")->where('company_id',$company_id)->get();
            $databebanpajak = collect();
            $datatotalbebanpajak = 0;
            for($i = 1; $i <= $countbebanpajak; $i++ ){
                $a = $i - 1;
                
                $bebanPajakglid = $bebanpajak[$a]->gl_id;
                $bebanPajakcode = $bebanpajak[$a]->coa_code;
                $bebanPajakname = $bebanpajak[$a]->coa_name;
                $bebanPajakdebit = GLDetail::where('gl_id',$bebanPajakglid)
                                    ->where('company_id',$company_id)
                                    ->where('created_at','>=',$date_start)
                                    ->where('created_at','<=',$date_end)->sum('debit');
                $bebanPajakcredit = GLDetail::where('gl_id',$bebanPajakglid)
                                    ->where('company_id',$company_id)
                                    ->where('created_at','>=',$date_start)
                                    ->where('created_at','<=',$date_end)->sum('credit');
                $bebanPajakstart = $bebanpajak[$a]->start_balance;
                $ccoa = COA::where('code',$bebanPajakcode)->first();
                $def = $ccoa->normal_balance;
                if($def =="D"){
                    $bebanPajakbal = ($bebanPajakdebit + $bebanPajakstart )- $bebanPajakcredit;
                }else{
                    $bebanPajakbal = ($bebanPajakcredit + $bebanPajakstart) - $bebanPajakdebit;

                } 
                $datatotalbebanpajak = $datatotalbebanpajak + $bebanPajakbal;

                $array = array(
                                'gl_id'=> $bebanPajakglid,
                                'coa_code'=> $bebanPajakcode,
                                'coa_name'=>$bebanPajakname, 
                                'debit'=>$bebanPajakdebit, 
                                'credit'=>$bebanPajakcredit, 
                                'start_balance'=>$bebanPajakstart,    
                                'balance'=>$bebanPajakbal,    
                                );        
                $databebanpajak->push($array); 
            }
        
        return response()->json([
            'status' => 'success',
            'message' => 'Data loaded successfully',
            'penjualan' => $datapenjualan,  
            'totalpenjualan' => $datatotalpenjualan,
            'pendapatanluar'=>$datapendapatanluar,  
            'totalpendapatanluar'=>$datatotalpendapatanluar,
            'pembelian'=>$datapembelian,
            'totalpembelian'=>$datatotalpembelian,  
            'pbd'=>$pbd,
            'beban' => $data,  
            'totalbeban' => $totalbeban,
            'bebanluar'=>$databebanluar,
            'totalbebanluar'=>$datatotalbebanluar,
            'bebanpajak'=>$databebanpajak,
            'totalbebanpajak'=>$datatotalbebanpajak  
        ]); 
    }

    public function indexclosing(){
         $data = CLOSING::all();
         return $this->success(null, 200, $data);
    }
    public function closingbulanan(Request $request){
        $date_start = $request->dateFrom;
        $date_end = $request->dateTo; 
        $company_id = auth()->user()->company_id;
        $totalpbd = 0;

        $pbd = app(\App\Http\Controllers\Api\Master\ProductController::class)->indexhpp($request);
        $pbd = json_encode($pbd);
        $pbd = json_decode($pbd, true);
        
        for($i =0; $i< count($pbd); $i++){
            $totalharga = $pbd['original']['data']['data_period'][10]['total_harga'];
            $totalpbd = $totalpbd + $totalharga;
        } 
        
        $datacompany = COMPANY::where('company_id',$company_id)->first();
        $coaikhtisar = $datacompany->coa_ikhtisar_labarugi;
        $coalabarugi = $datacompany->coa_labarugi_ditahan;

        $date = Carbon::parse($date_end); 
        $month = $date->format('M');
        $bulan = $date->format('m');
        $yearr = $date->format('Y'); 
        $tahun = $date->format('y'); 
        $date = $date->format('Y-m-d'); 
        
        $cekclosing = CLOSING::where('bulan',$bulan)->where('tahun',$tahun)->count();
        if($cekclosing != 0){
            return $this->fail(null, 200, "Closing Sudah Pernah di jalankan");
        } 
        
        
        $cekpbd = PersediaanBarang::where('company_id',$company_id)->where('bulan','=',$bulan)->where('tahun','=',$tahun)->count();
        if($cekpbd ==0){
            PersediaanBarang::create([
                'bulan'=>$bulan,
                'tahun'=>$tahun,
                'jumlah'=>$totalpbd
            ]); 
        }
        

        $coa = COA::where('fromcode','>','4')->where('company_id',$company_id)->where('level',3)
                          ->get(); 
        
        \DB::beginTransaction();
        try {
            
            $journalname= $request->name;
            $journaldesc = $request->description;
            $userid = auth()->user()->user_id;    
            $company_id = auth()->user()->company_id;
            $year = substr($date,2,2);

            $cektotal = JOUR::where('company_id','=',$company_id)->count(); 
            $code =  $cektotal+1; 
            
            //proses create journal closing
            $journal = new JOUR;
            $journal->user_id = $userid;
            $journal->company_id = $company_id;
            $journal->code= "JR-".$year."-".$code;
            $journal->name="Closing $month $yearr";
            $journal->entry_at=$date;
            $journal->debit=0;
            $journal->credit=0;
            $journal->status="Open";
            $journal->description="Closing $month $yearr";
            $journal->save();

            $journal_id = $journal->journal_id;

            foreach ($coa as $list) {
                $coa_code = $list->code;
                $coa_name = $list->name;
                $default_balance = $list->normal_balance;

                $jData = JOUR::where("journal_id",$journal_id)->first();
                $jCode = $jData->code;
                $jDebit = $jData->debit;
                $jCredit = $jData->credit; 

                $gll = GLL::where('coa_code',$coa_code)->where('company_id',$company_id)->first();
                $gl_id = $gll->gl_id;
                $start_bal = $gll->start_balance;

                $amountDebit = GLDetail::where('gl_id',$gl_id)->where('company_id',$company_id)->where('created_at','>',$date_start)->where('created_at','<',$date_end)->sum('debit');
                
                $amountCredit = GLDetail::where('gl_id',$gl_id)->where('company_id',$company_id)->where('created_at','>',$date_start)->where('created_at','<',$date_end)->sum('credit');
                if($default_balance == "D"){
                    $valbal = ($amountDebit + $start_bal) - $amountCredit; 
                }else{
                    $valbal = ($amountCredit + $start_bal) - $amountDebit; 
                } 
                if($valbal != 0){ 
                    //posting coa list
                    $journalDetail = new JOURDetail;
                    $journalDetail->user_id = $userid;
                    $journalDetail->journal_id = $journal_id;
                    $journalDetail->coa_code= $coa_code;
                    $journalDetail->description="Closing $coa_name $month-$year";
                    $journalDetail->entry_at=$date;
                    if($default_balance == "D"){
                        $debit= 0;
                        $credit= $amountDebit;

                        $journalDetail->credit=$amountDebit;
                        $jCredit = $jCredit+$amountDebit;
                    }else{
                        $debit= $amountCredit;
                        $credit= 0;

                        $jDebit = $jDebit+$amountCredit;
                        $journalDetail->debit=$amountCredit;
                    } 
                    $journalDetail->save();
                    
                    $data = array(
                        'debit' => $jDebit,
                        'credit' => $jCredit,
                    ); 

                    $cek = JOUR::where('journal_id','=',$journal_id)->update($data);       
                    postglclosing($company_id, $coa_code, $jCode, "Closing $coa_name $month-$year", $debit, $credit,$date_end); //post ke COA yang di tutup
                    
                    //posting coa ikhtisar
                    $journalDetail = new JOURDetail;
                    $journalDetail->user_id = $userid;
                    $journalDetail->journal_id = $journal_id;
                    $journalDetail->coa_code= '3204';
                    $journalDetail->description="Closing $coa_name $month-$year";
                    $journalDetail->entry_at=$date;

                    if($default_balance == "D"){
                        $debit= $amountDebit;
                        $credit= 0;

                        $jDebit = $jDebit+$amountDebit;
                        $journalDetail->debit=$amountDebit;
                    }else{
                        $debit= 0;
                        $credit= $amountCredit; 
                        
                        $journalDetail->credit=$amountCredit;
                        $jCredit = $jCredit+$amountCredit;
                    } 
                    $journalDetail->save();

                    $data = array(
                        'debit' => $jDebit,
                        'credit' => $jCredit,
                    ); 
                
                    $cek = JOUR::where('journal_id','=',$journal_id)->update($data);
                    
                    postglclosing($company_id, $coaikhtisar , $jCode, "Closing $coa_name $month-$year", $debit, $credit,$date_end); //Post Ke Ikhtisar Laba Rugi
                } 
  
            } 
            $cekjournal = JOUR::where('journal_id','=',$journal_id)->with(['journalDetails'])->first();

            $dataikhtisar = GLL::where('coa_code', $coaikhtisar)->where("company_id",$company_id)->first();
            $balance = $dataikhtisar->balance;
            $descikhtisar ="Closing";
            if($balance >0){
                
                $descikhtisar = "Laba $month-$year";
                postglclosing($company_id, $coaikhtisar, $jCode, "Posting Balik Ikhtisar $month-$year", $balance, 0,$date_end); //posting closing ke iktthisar
                // return "Laba";
            }else{
                $balan = $balance * -1;
                $descikhtisar = "Rugi Dari Ikthisar Laba Rugi $month-$year";
                postglclosing($company_id, $coaikhtisar, $jCode, "Posting Balik Ikhtisar $month-$year", 0, $balan,$date_end); //posting closing ke iktthisar
                // return "Rugi";
            }
            
            //cek coalabarugi di tahan
            $ceklabarugiditahan = COA::where('code','=',$coalabarugi)->first();
            $normal_balance = $ceklabarugiditahan->normal_balance;
            if($normal_balance == "K"){
                postglclosing($company_id, $coalabarugi, $jCode, $descikhtisar, 0, $balance,$date_end); //posting nilai ikthisar ke laba rugi di tahan

            }else{
                postglclosing($company_id, $coalabarugi, $jCode, $descikhtisar, $balance, 0,$date_end); //posting nilai ikthisar ke laba rugi di tahan
            }

            $dataikhtisar = GLL::where('coa_code', $coalabarugi)->where("company_id",$company_id)->first();
            
            $cekclosedata = CLOSING::where('bulan',$bulan)->where('tahun',$year)->count();
            if($cekclosedata != 0 ){
                $closedata = CLOSING::where('bulan',$bulan)->where('tahun',$year)->first();
                $closing_id = $closedata->closing_id;
                $updateclose = array(
                    'journal_code'=>$jCode,
                    'status'=>true,
                );
                CLOSING::where('closing_id',$closing_id)->update($updateclose);
            }else{
                 $close = CLOSING::create([
                    'journal_code' => $jCode,
                    'bulan' => $bulan,
                    'tahun' => $year,
                    'status' => true,
                ]);   
            }
             

            $completejournal = array(
                'status'=>'Complete'
            );
            JOUR::where('company_id',$company_id)->where('code',$jCode)->update($completejournal);


            \DB::commit();
            return "success";
        } catch(\Exception $e) {
            \DB::rollback();
            return "failed $e";
        } 
    }

    public function openclosing(Request $request){

        $journal_code = $request->journal_code;
        $company_id = auth()->user()->company_id;

        $datacompany = COMPANY::where('company_id',$company_id)->first();
        $coaikhtisar = $datacompany->coa_ikhtisar_labarugi;
        $coalabarugi = $datacompany->coa_labarugi_ditahan;

        $data_journal = JOUR::where('code',$journal_code)->where('company_id',$company_id)->first();
        $journal_id = $data_journal->journal_id;

        $total_debit_ikhtisar= JOURDetail::where('journal_id',$journal_id)->where('coa_code',$coaikhtisar)->sum('debit');
        $total_credit_ikhtisar = JOURDetail::where('journal_id',$journal_id)->where('coa_code',$coaikhtisar)->sum('credit');

        $balance_ikhtisar = $total_credit_ikhtisar - $total_debit_ikhtisar;
        

        \DB::beginTransaction();
        try{

            if($balance_ikhtisar < 0){
                $balan = $balance_ikhtisar * -1;
            }else{
                $balan = $balance_ikhtisar;
            }
            $t = delpostgl2($company_id,$coaikhtisar, $journal_code,0,$balan); //hapus inputan ikhtisar terakhir
            if($t != "success"){
                return $t;
            }
           
            $t = delpostgl2($company_id,$coalabarugi, $journal_code,0,$balance_ikhtisar); //hapus inputan laba rugi di tahan
            if($t != "success"){
                return $t;
            }

            $cekdatadetail = JOURDetail::where('journal_id',$journal_id)->get();
            $jmlhcekdatadetail = JOURDetail::where('journal_id',$journal_id)->count();
            for($x= 0; $x<$jmlhcekdatadetail ; $x++){
                $coaCode = $cekdatadetail[$x]->coa_code;
                $debit = $cekdatadetail[$x]->debit;
                $credit = $cekdatadetail[$x]->credit;

                $t = delpostgl2($company_id,$coaCode, $journal_code,$debit,$credit); 
                 if($t != "success"){
                    return $t;
                }
            }  
            

            JOURDetail::where('journal_id',$journal_id)->delete();
            JOUR::where('code',$journal_code)->delete();

            $status = array(
                'journal_code'=>null,
                'status'=> false
            );
            CLOSING::where('journal_code',$journal_code)->update($status);

        
            \DB::commit();
            return "success";
        } catch(\Exception $e) {
            \DB::rollback();
            return "failed $e";
        } 
    }

    



    public function showneraca(Request $request){
        $company_id = auth()->user()->company_id;
        $company = Company::where('company_id',$company_id)->first();


        $jmlitemneraca = COA::where('level','2')->where('company_id',$company_id)
                        ->where('fromcode','<=','4')->count();
        $dataitemneraca = COA::where('level','2')->where('company_id',$company_id)
                        ->where('fromcode','<=','4')
                        ->orderBy('code','asc')->get(); 
        $data = collect();
        $totaldata = 0;
        $data2 = collect();
        $totaldata2 = 0;

        for($i = 1; $i<= $jmlitemneraca; $i++){
                $a = $i-1;  

                $fc = $dataitemneraca[$a]->code;
                $name = $dataitemneraca[$a]->name; 

                $bb = GLL::where('coa_code','LIKE',$fc.'%')->where('company_id',$company_id)
                          ->get(); 
                $ttl = GLL::where('coa_code','LIKE',$fc.'%')->where('company_id',$company_id)
                              ->sum('balance');       

                $array = array(
                            'name'=> $name,
                            'data'=> $bb,
                            'total'=>$ttl, 
                            ); 
                if($a <= 7){
                    $totaldata = $totaldata + $ttl;
                    $data->push($array); 

                }else{
                    $totaldata2 = $totaldata2 + $ttl;
                   $data2->push($array); 

                }                   

        }
 

        return response()->json([
            'status' => 'success',
            'message' => 'Data loaded successfully', 
            'data1' => $data,  
            'totaldata1' => $totaldata,  
            'data2' => $data2,  
            'totaldata2' => $totaldata2,  
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatestartbalance(Request $request, $id)
    {
        $company_id = auth()->user()->company_id;

        \DB::beginTransaction();
        try {
            $amount  = $request->amount;

            $cekdata = GLL::where('gl_id',$id)->where('company_id',$company_id)->first();
            $coa_code = $cekdata->coa_code;
            $debit = $cekdata->debit;
            $credit = $cekdata->credit;
            
            $cekCoa = Coa::where('code',$coa_code)->where('company_id',$company_id)->first();
            $normal_bal = $cekCoa->normal_balance;

            if($normal_bal =="D"){
               $totalbalance = $amount + $debit - $credit;

            }else{
                $totalbalance = $amount + $credit - $debit;  
            }

            $data = array(
                "start_balance"=>$amount,
                "balance"=>$totalbalance
            );
            $gl = GLL::where('gl_id',$id)->where('company_id',$company_id)->update($data);
 
            \DB::commit();
            return $this->success("Update Start Balance Success", 200, $gl);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}