<?php

namespace App\Http\Controllers\Api\Accounting;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Accounting\Adjustment as AdjustmentRequest;
use App\Repositories\Accounting\Adjustment as AdjustmentRepository;
use App\Repositories\Accounting\AdjustmentDetail as AdjustmentDetailRepository;
use App\Models\Accounting\AdjustmentDetail as AdjustDetail;

class AdjustmentDetailController extends ApiController
{
    protected $adjustment;
    protected $adjustmentDetail;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->adjustment = new AdjustmentRepository;
        $this->adjustmentDetail = new AdjustmentDetailRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $adjustmentDetail = $this->adjustmentDetail->with(['adjustment', 'product'])->all($request->all());
        return $this->success(null, 200, $adjustmentDetail);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AdjustmentDetailRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdjustmentRequest $request)
    {
        \DB::beginTransaction();
        try {
            $adjustment = $this->adjustment->all()->where('wh_id', $request->wh_id)->where('reference', $request->reference);
            if ($adjustment->isEmpty()) {
                $adjustment = $this->adjustment->save($request->except('details'));
            } else {
                $adjustment = $adjustment->first();
            }
            if ($request->details) {
                $adjustment->adjustmentDetails()->create([
                    'product_id' => $request->details['product_id'],
                    'unit_lg' => $request->details['unit_lg'],
                    'unit_sm' => $request->details['unit_sm'],
                    'qty_lg' => $request->details['qty_lg'],
                    'qty_sm' => $request->details['qty_sm'],
                    'type' => $request->details['type'],
                ]);
            }
            \DB::commit();
            return $this->success(null, 200, $adjustment->adjustmentDetails()->where('adjust_id', $adjustment->adjust_id)->latest('adjust_id')->first());
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $adjustment = $this->adjustment->with('adjustmentDetails')->all();
        if ($adjustmentDetail = $adjustment->flatMap->adjustmentDetails->where('adjust_d_id', $id)->first()) {
            return $this->success(null, 200, $adjustmentDetail);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AdjustmentDetailRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\Illuminate\Http\Request $request, $id)
    {
        $adjustmentDetail = AdjustDetail::where('adjust_d_id',$id)->first();
        \DB::beginTransaction();
        try{
            $qtylg = $request->qtylg;
            $qtysm = $request->qtysm;

            $data = array(
                'qty_lg'=>$qtylg,
                'qty_sm'=>$qtysm,
            );
            $update = AdjustDetail::where('adjust_d_id',$id)->update($data);
            \DB::commit();
            return $this->success(null, 200, $update);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try{ 
            $update = AdjustDetail::where('adjust_d_id',$id)->delete();
            \DB::commit();
            return $this->success(null, 200, $update);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
