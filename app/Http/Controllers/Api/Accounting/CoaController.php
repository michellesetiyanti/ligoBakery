<?php

namespace App\Http\Controllers\Api\Accounting;

use Illuminate\Http\Request; 
use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Accounting\Coa as CoaRequest;
use App\Repositories\Accounting\Coa as CoaRepository;
use App\Models\Accounting\Coa;
use App\Models\Accounting\GeneralLedger as GL;
use App\Models\Accounting\GeneralLedgerDetail as GLDetail;

class CoaController extends ApiController
{
    protected $coa;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->coa = new CoaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $sorting = 'code|ASC';
        $sorting = $request->sorting ? $request->sorting : $sorting;
        $coa = $this->coa->all($request->merge(compact('filter', 'sorting'))->all());
        return $this->success(null, 200, $coa);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CoaRequest $request)
    {
        \DB::beginTransaction();
        try {
            $company_id=$request->company_id; 
            $name=$request->name;
            $level=$request->level;
            $normal=$request->normal_balance;
            
            
            //auto generate code Account 3 level  1,11,1101
            if($level>1){ 
                $fromcode = $request->fromcode;
                if($fromcode==""){
                    return redirect('/registercoa')->with('info',"Please Select FromCode");  
                }
            }
            
            $coa=new coa;

            if($level>=2){ 
            $coa->name = $name;
            $cektotal = coa::where('fromcode','=',$fromcode)
                    ->where('level','=',$level)->count();
            $code = $cektotal + 1; 
            if($code<10){ $coa->code = $fromcode.".0".$code;  }else{ $coa->code = $fromcode.".".$code; }
            $coa->level = $level;
            $coa->fromcode = $fromcode; 
                if($level==3){ //pengecekan utk data parent
                    $lenght = strlen($fromcode);
                    $cekparent = substr($fromcode,0,1);
                    $ceklast = substr($fromcode,1,$lenght);
                    $parent = $cekparent."".$ceklast;
                }else{
                    if($code<10){ $parent = $fromcode.".0".$code;  }else{ $parent = $fromcode.".".$code; }
                }
            $coa->parent = $parent; 
            $coa->save();   

            }else{

            $coa->name = $name;
            $cektotal = coa::where('level','=','1')->count();
            $code = $cektotal + 1;
            $coa->company_id = $company_id;
            $coa->code = $code;
            $coa->level = $level;
            $coa->fromcode = $code;
            $coa->parent = $code;
            $coa->save();

            } 


            // $coa = $this->coa->save($request->all()); 
            \DB::commit();
            return $this->success(null, 200, $coa);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($coa = $this->coa->show($id)) {
            return $this->success(null, 200, $coa);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CoaRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function update(CoaRequest $request, $id)
    {
        $coa = $this->coa->show($id);
        $company_id = $coa->company_id;
        $coa_code = $coa->code;
        \DB::beginTransaction();
        try{
            $coa->fill($request->merge([
                'id' => $id,
            ])->all());
            $coa->save();

            $array = array(
                'coa_name'=>$request->name
            );
            GL::where('coa_code',$coa_code)->where('company_id',$company_id)->update($array);

            \DB::commit();
            return $this->success(null, 200, $coa);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function generategl(Request $request)
    {   
        $companyid = auth()->user()->company_id;
        \DB::beginTransaction();
        try{
            $coa = coa::where('company_id',$companyid)->where('level','3')->get();
            $total =  coa::where('company_id',$companyid)->where('level','3')->count();

            foreach ($coa as $c){ 
                $code = $c->code;
                $coa_name = $c->name;

                $cek = GL::where('company_id',$companyid)->where('coa_code',$code)->count();    
                if($cek == 0 ){
                    $gl=new GL; 
                    $gl->company_id = $companyid; 
                    $gl->coa_code = $code; 
                    $gl->coa_name = $coa_name; 
                    $gl->debit = 0; 
                    $gl->credit = 0; 
                    $gl->start_balance = 0; 
                    $gl->balance = 0; 
                    $gl->save();   
                }
            }

            \DB::commit();
            return  "Generate Success";
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $companyid = auth()->user()->company_id; 
        $coaData = coa::where('coa_id',$id)->first();
        $code = $coaData->code;

       
        

        \DB::beginTransaction();
        try {
            $cekGL =  GL::where('coa_code',$code)->count();
            if($cekGL > 0){
                $glData = GL::where('coa_code',$code)->first();
                $glid = $glData->gl_id;
                $cek = GLDetail::where('gl_id',$glid)->count();
                
                if ($cek == 0 ) {
                coa::where('coa_id',$id)->delete();
                    GL::where('company_id',$companyid)->where('coa_code',$code)->delete();
                } else {
                    return $this->fail(null, 500, "COA Can not be Deleted");  
                }
            }else{
                coa::where('coa_id',$id)->delete(); 
            }
            
            \DB::commit();
            return $this->success(null);
        } catch(\Exception $e) {
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
