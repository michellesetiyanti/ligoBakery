<?php

namespace App\Http\Controllers\Api\Accounting;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Accounting\PenyusutanAset as PenyusutanAsetRequest;
use App\Repositories\Accounting\PenyusutanAset as PenyusutanAsetRepository;
use App\Models\Accounting\PenyusutanAset as PA;

class PenyusutanAsetController extends ApiController
{
    protected $penyusutanAset;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->penyusutanAset = new PenyusutanAsetRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $data = $this->penyusutanAset->with(['input_by','coa_code','coa_penyusutan','coa_beban','coa_beban','coa_pengeluaran'])->all($request->all());
        $data->where('company_id', auth()->user()->company_id);
        return $this->success(null, 200, $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PenyusutanAsetRequest $request)
    {
        \DB::beginTransaction();
        try {  

            $codeAset= $request->code_aset;
            $dateBuy = $request->date_buy;
            $dateUse = $request->date_use;
            $name = $request->name;
            $desc = $request->description;
            $coaCode = $request->coa_code;
            $coaPenyusutan = $request->coa_penyusutan;
            $coaBeban = $request->coa_beban;
            $bulan = $request->bulan;
            $coaPengeluaran = $request->coa_pengeluaran;
            $keteranganPengeluaran = $request->keterangan_pengeluaran;
            $qty = $request->qty;
            $amount = $request->amount;
            $input_by = auth()->user()->user_id;    
            $company_id = auth()->user()->company_id; 
            
            $pa = new PA;
            $pa->input_by = $input_by;
            $pa->company_id = $company_id;
            $pa->code_aset= $codeAset;
            $pa->date_buy=$dateBuy;
            $pa->date_use=$dateUse;
            $pa->name=$name;
            $pa->description=$desc;
            $pa->coa_code=$coaCode;
            $pa->coa_penyusutan=$coaPenyusutan;
            $pa->coa_beban=$coaBeban;
            $pa->umur_bulan=$bulan;
            $pa->bulan_tersisa=$bulan;
            $pa->coa_pengeluaran=$coaPengeluaran;
            $pa->keterangan_pengeluaran=$keteranganPengeluaran;
            $pa->qty=$qty;
            $pa->amount=$amount;
            $pa->save();
            \DB::commit();
            return $this->success("Aset Added Succesfully", 200, $pa);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($pa = $this->penyusutanAset->show($id)) {
            return $this->success(null, 200, $pa);
        } else {
            return $this->notFound("Aset ".trans('general.not_found'));
        }
    }
    
     
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PenyusutanAsetRequest $request, $id)
    {
        \DB::beginTransaction();
        try{
            $codeAset= $request->code_aset;
            $dateBuy = $request->date_buy;
            $dateUse = $request->date_use;
            $name = $request->name;
            $desc = $request->description;
            $coaCode = $request->coa_code;
            $coaPenyusutan = $request->coa_penyusutan;
            $coaBeban = $request->coa_beban;
            $bulan = $request->bulan;
            $coaPengeluaran = $request->coa_pengeluaran;
            $keteranganPengeluaran = $request->keterangan_pengeluaran;
            $qty = $request->qty;
            $amount = $request->amount;
            $input_by = auth()->user()->user_id;    
            $company_id = auth()->user()->company_id; 
            
            
            $data = array(
                'input_by' => $input_by,
                'code_aset' => $codeAset,
                'date_buy' => $dateBuy,
                'date_use' => $dateUse,
                'name' => $name,
                'description' => $desc,
                'coa_code' => $coaCode,
                'coa_penyusutan' => $coaPenyusutan,
                'coa_beban' => $coaBeban,
                'umur_bulan' => $bulan,
                'coa_pengeluaran' => $coaPengeluaran,
                'keterangan_pengeluaran' => $keteranganPengeluaran,
                'qty' =>$qty,
                'amount' => $amount
              ); 
            $cek = PA::where('aset_id','=',$id)->update($data);

            \DB::commit();
            return $this->success("Penyusutan Aset Update Successfully ".trans('general.update.success'), 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pa = PA::where('aset_id','=',$id)->first(); 
        \DB::beginTransaction();
        try { 
            $bulanTersisa = $pa->bulan_tersisa;
            $umurBulan = $pa->umur_bulan;
            if($umurBulan != $bulanTersisa){
                return $this->fail(null, 500, "Aset Yang Sudah di Closing Tidak bisa Dihapus");
            }else{
                PA::where('aset_id',$id)->delete(); 
            }

            \DB::commit();
            return $this->success("Aset ".trans('general.delete.success', ['name' => $pa->code_aset]));
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
