<?php

namespace App\Http\Controllers\Api\Penjualan;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Penjualan\DeliveryOrder as DeliveryOrderRequest;
use App\Repositories\Penjualan\DeliveryOrder as DeliveryOrderRepository;
use App\Repositories\Penjualan\Penjualan as PenjualanRepository;

class DeliveryOrderController extends ApiController
{
    protected $do;
    protected $penjualan;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth', ['except' => ['update', 'destroy']]);
        $this->do = new DeliveryOrderRepository;
        $this->penjualan = new PenjualanRepository;
    }

    
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $do = $this->do->with(['deliveryOrderDetails', 'penjualan'])->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $do);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DeliveryOrderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeliveryOrderRequest $request)
    {
        $jual = $this->penjualan->show($request->penjualan_id);
        if (!in_array($jual->faktur, ['approved'])) {
            return $this->fail('fail_status', 422, ['status' => $jual->faktur]);
        }
        $model = $this->do->getModel();
        \DB::beginTransaction();
        try {
            $work_order = $jual->order->work_order ?? null;
            $penempatan = $request->missing('penempatan') || ($request->input('penempatan') == 'null') ? null : $request->input('penempatan');
            if ($work_order && empty($penempatan)) {
                return $this->fail(trans('validation.required', ['attribute' => 'penempatan']), 422);
            }
            $code = $this->codeDo([
                'penempatan' => $penempatan,
                'work_order' => $work_order,
            ]);
            $signed = $request->input('signed', null);
            $do = $jual->deliveryOrders()->create($request->merge([
                'company_id' => $jual->company_id,
                'code' => $code,
                'note' => $jual->order->code_spk ?? null,
                'penempatan' => $penempatan,
                'trans_type' => $jual->po_pro_id ? 'project' : null,
                'signed' => $signed,
            ])->except('products'));
            foreach ($request->products as $product) {
                $os_do = $jual->os_do->where('product_id', $product['id'])->first();
                if ($product['qty_lg'] == 0 || $product['qty_sm'] == 0) {
                }
                if ($product['qty_lg'] > $os_do['qty_lg'] || $product['qty_sm'] > $os_do['qty_sm']) {
                    return $this->fail('qty_bigger_than', 422, [
                        'stock_order_lg' => $product['qty_lg'],
                        'stock_order_sm' => $product['qty_sm'],
                        'stock_prod_lg' => $os_do['qty_lg'],
                        'stock_prod_sm' => $os_do['qty_sm'],
                    ]);
                }
                if ($product['qty_lg'] > $os_do['product']['total_lg_qty'] || $product['qty_sm'] > $os_do['product']['total_sm_qty']) {
                    return $this->fail('stock_not_enough', 422, [
                        'stock_order_lg' => $product['qty_lg'],
                        'stock_order_sm' => $product['qty_sm'],
                        'stock_prod_lg' => $os_do['product']['total_lg_qty'],
                        'stock_prod_sm' => $os_do['product']['total_sm_qty'],
                    ]);
                }
                $doDetail = $do->deliveryOrderDetails()->create([
                    'product_id' => $product['id'],
                    'unit_lg' => $os_do['product']['unit_lg'],
                    'unit_sm' => $os_do['product']['unit_sm'],
                    'delv_qty_lg' => $product['qty_lg'],
                    'delv_qty_sm' => $product['qty_sm'],
                ]);
                if ($request->confirmed == 'no') {
                    $doDetail->update([
                        'recv_qty_lg' => $doDetail->delv_qty_lg,
                        'recv_qty_sm' => $doDetail->delv_qty_sm
                    ]);
                    if ($os_do['status'] == 'complete') {
                        $jual->penjualanDetails->where('product_id', $product['id'])->update(['do_status' => 'complete']);
                    }
                    // fifo
                    fifoStock(
                        $jual->code,
                        $doDetail->product_id,
                        ['lg' => $doDetail->recv_qty_lg, 'sm' => $doDetail->recv_qty_sm]
                    );
                }
            }
            \DB::commit();
            return $this->success(null, 200, $do);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($do = $this->do->with(['deliveryOrderDetails', 'penjualan'])->show($id)) {
            return $this->success(null, 200, $do);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\Illuminate\Http\Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Do Action the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function doAction(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'penjualan_id' => 'required|integer|exists:penjualan,penjualan_id',
            'delivery' => 'required|array',
            'delivery.*.id' => 'integer|exists:delivery_orders,do_id',
            'delivery.*.product_id' => 'integer|exists:products,product_id',
            'delivery.*.recv_qty_lg' => 'integer',
            'delivery.*.recv_qty_sm' => 'integer',
            'delivery.*.note' => 'nullable|string',
            'execute' => 'required|string|in:received',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        $jual = $this->penjualan->show($request->penjualan_id);
        \DB::beginTransaction();
        try {
            switch ($request->execute) {
                case 'received':                    
                    for ($i = 0; $i < count($request->delivery); $i++) {
                        $doDetail = $jual->deliveryOrders->flatMap->deliveryOrderDetails->where('do_id', $request->delivery[$i]['id'])->where('product_id', $request->delivery[$i]['product_id'])->first();
                        $jualDetail = $jual->penjualanDetails->where('product_id', $request->delivery[$i]['product_id'])->first();
                        if ($request->delivery[$i]['recv_qty_lg'] > $doDetail->delv_qty_lg || $request->delivery[$i]['recv_qty_sm'] > $doDetail->delv_qty_sm) {
                            return $this->fail('qty_bigger_than', 422, [
                                'stock_recv_lg' => $request->delivery[$i]['recv_qty_lg'],
                                'stock_recv_sm' => $request->delivery[$i]['recv_qty_sm'],
                                'stock_delv_lg' => $doDetail->delv_qty_lg,
                                'stock_delv_sm' => $doDetail->delv_qty_sm,
                            ]);
                        }
                        $doDetail->update([
                            'recv_qty_lg' => $request->delivery[$i]['recv_qty_lg'],
                            'recv_qty_sm' => $request->delivery[$i]['recv_qty_sm'],
                            'note' => $request->delivery[$i]['note'],
                        ]);
                        // fifo
                        fifoStock(
                            $jual->code,
                            $doDetail->product_id,
                            ['lg' => $doDetail->recv_qty_lg, 'sm' => $doDetail->recv_qty_sm]
                        );
                    }
                break;
            }
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    
    private function codeDo(array $request)
    {
        $now = \Carbon\Carbon::now();
        $auth = auth()->user();
        $model = $this->do->getModel();
        $format = 'SJ [CV]-[NO].[DS] [WO] [DD]/[MM]/[YY]';
        // =====
        $district = isset($request['penempatan']) ? $request['penempatan'] : null;
        $wo = isset($request['work_order']) ? $request['work_order'] : null;
        // =====
        $code_CV = $auth->company->code_alpha ?? str_pad($auth->company_id, 5, '0', STR_PAD_LEFT);
        $code_DS = $district ?? '00';
        $code_DD = $now->format('d');
        $code_MM = $now->format('m');
        $code_YY = $now->format('y');
        $noUrut = $model->where('company_id', $auth->company_id)->where('penempatan', $district)->whereMonth('created_at', $now->format('m'))->orderBy('do_id', 'DESC')->get();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('code')->first();
            preg_match('/SJ '.$code_CV.'-(\d{3}).'.$code_DS.'/', $noUrut, $match);
            $noUrut = $match[1] + 1;
            $noUrut = str_pad($noUrut, strlen($noUrut) > 3 ? strlen($noUrut) : 3, '0', STR_PAD_LEFT);
        } else {
            $noUrut = str_pad(1, 3, '0', STR_PAD_LEFT);
        }
        $code_NO = $noUrut;
        if ($wo) {        
            return str_replace(
                array('[NO]', '[CV]', '[DS]', '[WO]', '[DD]', '[MM]', '[YY]'),
                array($code_NO, $code_CV, $code_DS, $wo, $code_DD, $code_MM, $code_YY),
                $format
            );
        } else {
            $format = str_replace(' [WO] ', ' ', $format);
            return str_replace(
                array('[NO]', '[CV]', '[DS]', '[DD]', '[MM]', '[YY]'),
                array($code_NO, $code_CV, $code_DS, $code_DD, $code_MM, $code_YY),
                $format
            );
        }
    }
}
