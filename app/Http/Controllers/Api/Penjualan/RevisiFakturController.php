<?php

namespace App\Http\Controllers\Api\Penjualan;

use App\Models\Accounting\Closing;
use App\Http\Controllers\Api\Controller as ApiController;
use App\Repositories\Penjualan\Penjualan as PenjualanRepository;
use App\Repositories\Penjualan\PenjualanDetail as PenjualanDetailRepository;
use App\Repositories\Master\ProductDetail as ProductDetailRepository;
use Illuminate\Http\Request;

class RevisiFakturController extends ApiController
{
    protected $penjualan;
    protected $penjualanDetail;
    protected $productDetail;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth', ['except' => ['store']]);
        $this->penjualan = new PenjualanRepository;
        $this->penjualanDetail = new PenjualanDetailRepository;
        $this->productDetail = new ProductDetailRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : 'faktur|actived';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id.',faktur|actived';
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $penjualan = $this->penjualan->with(['customer', 'penjualanDetails', 'deliveryOrders', 'piutang', 'karyawan'])->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $penjualan);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($penjualan = $this->penjualan->with(['customer', 'penjualanDetails', 'deliveryOrders', 'piutang', 'karyawan'])->show($id)) {
            return $this->success(null, 200, $penjualan);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'execute' => 'required|string|in:faktur',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $penjualan = $this->penjualan->show($id);
        if (!in_array($penjualan->faktur, ['actived'])) {
            return $this->fail('fail_status', 422, ['status' => $penjualan->faktur]);
        }
        \DB::beginTransaction();
        try {
            $penjualan->update(['faktur' => 'unactived']);
            \DB::commit();
            return $this->success(null, 200, $penjualan);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return app(\App\Http\Controllers\Api\Penjualan\PenjualanController::class)->destroy($id);
    }

    /**
     * Do Action the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function revisi(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'no_faktur' => 'required|string|exists:penjualan,code',
            'notes' => 'required|string',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $jual = $this->penjualan->all()->where('code', $request->no_faktur)->first();
        if (empty($jual)) {
            return $this->fail('not_available.', 422);
        }
        $approveDate = \Carbon\Carbon::parse($jual->approve_date);
        if (Closing::where('company_id', $jual->company_id)->where('bulan', $approveDate->format('m'))->where('tahun', $approveDate->format('y'))->where('status', 1)->exists()) {
            return $this->fail('Accounting closure already exists', 422);
        }
        \DB::beginTransaction();
        try {
            if ($jual->do_status == 'complete') {
                return $this->fail('do_complete', 422, $jual->os_do);
            }
            $jual->update([
                'faktur' => 'actived',
                'notes' => $request->notes,
            ]);
            $jual->penjualanDetails->each(function ($item) {
                $item->update([
                    'rev_qty_lg' => $item->qty_lg,
                    'rev_qty_sm' => $item->qty_sm,
                ]);
            });
            \DB::commit();
            return $this->success('revisi_actived', 200, ['status' => $jual->faktur]);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Do Action the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function doAction(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'execute' => 'required|string|in:rejected,void,approved',
            'rejected' => 'sometimes|required|boolean',
            'notes' => 'sometimes|required|string',
            'approved' => 'sometimes|required|array',
            'approved.payment' => 'sometimes|nullable|string|in:cash,credit',
            'approved.due_date' => 'sometimes|nullable|required_if:payment,credit',
            'approved.coa_code' => 'sometimes|nullable|string|required_if:payment,cash',
            'approved.ongkir' => 'sometimes|nullable|digits_between:1,20',
            'approved.ongkir_desc' => 'sometimes|nullable|string',
            'approved.biaya_lain' => 'sometimes|nullable|digits_between:1,20',
            'approved.biaya_lain_desc' => 'sometimes|nullable|string',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $penjualan = $this->penjualan->show($id);
        \DB::beginTransaction();
        try {
            switch ($request->execute) {
                case 'rejected':
                    if (in_array($penjualan->faktur, ['void'])) {
                        return $this->fail('fail_status', 422, ['status' => $penjualan->faktur]);
                    }
                    if ($request->missing('rejected')) {
                        $errors = $validator->errors()->add('rejected', trans('validation.required', ['attribute' => 'rejected']));
                        return $this->fail('fail_validation', 422, $errors);
                    }
                    $faktur = $request->boolean('rejected') ? 'reject' : 'unverified';
                    if (glByDesc($penjualan->company_id, 'Penjualan '.$penjualan->code)->count() > 0) {
                        $this->revisiReject($penjualan->penjualan_id, $request->boolean('rejected'));
                        $faktur = $request->boolean('rejected') ? 'reject' : 'actived';
                    }
                    $penjualan->update(['faktur' => $faktur, 'notes' => $request->notes]);
                break;
                case 'void':
                    if (in_array($penjualan->faktur, ['void'])) {
                        return $this->success('success_process', 200, ['status' => $penjualan->faktur]);
                    }
                    if (in_array($penjualan->faktur, ['unverified', 'pending'])) {
                        app(\App\Http\Controllers\Api\Penjualan\PenjualanController::class)->destroy($penjualan->penjualan_id);
                    }
                    if ($penjualan->deliveryOrders()->exists()) {
                        return $this->fail('do_shipping', 422, $penjualan->os_do);
                    }
                    if ($request->missing('notes')) {
                        $errors = $validator->errors()->add('notes', trans('validation.required', ['attribute' => 'notes']));
                        return $this->fail('fail_validation', 422, $errors);
                    }
                    $penjualan->update([
                        'faktur' => 'void',
                        'notes' => $request->notes,
                    ]);
                    if ($penjualan->piutang()->exists()) {
                        $penjualan->piutang()->delete();
                    }
                    if (glByDesc($penjualan->company_id, 'Penjualan '.$penjualan->code)->count() > 0) {
                        $approveDate = \Carbon\Carbon::parse($penjualan->approve_date);
                        if (Closing::where('company_id', $penjualan->company_id)->where('bulan', $approveDate->format('m'))->where('tahun', $approveDate->format('y'))->where('status', 1)->exists()) {
                            return $this->fail('Accounting closure already exists', 422);
                        }
                        $this->revisiVoid($penjualan->penjualan_id);
                        voidpostpenjualan($penjualan->company_id, $penjualan->status == 'paid' ? 'cash' : 'credit', $penjualan->code, $penjualan->dpp, $penjualan->tax, $penjualan->discount, $penjualan->total);
                    }
                break;
                case 'approved':
                    if (!in_array($penjualan->faktur, ['pending', 'actived', 'unactived'])) {
                        return $this->fail('fail_status', 422, ['status' => $penjualan->faktur]);
                    }
                    $statusPay = $request->approved['payment'] ? ($request->approved['payment'] == 'cash') ? 'paid' : 'unpaid' : $penjualan->status;
                    $coa_code = $request->approved['coa_code'] && $request->approved['coa_code'] == 'null' ? null : $request->approved['coa_code'];
                    $due_date = $request->approved['due_date'] && $request->approved['due_date'] == 'null' ? null : $request->approved['due_date'];
                    if ($due_date && auth()->user()->company->code_numb && auth()->user()->company->code_alpha) {
                        $due_date = \Carbon\Carbon::now()->add($due_date, 'day')->format('d/m/Y');
                    }
                    $penjualan->update([
                        'due_date' => $due_date,
                        'ongkir' => $request->approved['ongkir'] ?? $penjualan->ongkir,
                        'ongkir_desc' => $request->approved['ongkir_desc'] == 'null' ? null : $request->approved['ongkir_desc'],
                        'biaya_lain' => $request->approved['biaya_lain'] ?? $penjualan->biaya_lain,
                        'biaya_lain_desc' => $request->approved['biaya_lain_desc'] == 'null' ? null : $request->approved['biaya_lain_desc'],
                        'status' => $statusPay,
                        'coa_code' => $statusPay == 'paid' ? $coa_code : $penjualan->company->coa_piutang,
                        'piutang_status' => $statusPay == 'paid' ? null : 'unpaid',
                        'signed' => authExecute(),
                    ]);
                    // Post GL
                    if (in_array($penjualan->faktur, ['actived', 'unactived'])) {
                        $this->revisiApprove($penjualan->penjualan_id);
                        revisipostpenjualan($penjualan->company_id, $penjualan->status == 'paid' ? 'cash' : 'credit', $penjualan->code, $penjualan->dpp, $penjualan->tax, $penjualan->discount, $penjualan->total);
                        $piutang_status = $penjualan->piutang_status;
                        if ($penjualan->piutang()->exists()) {
                            $piutang = $penjualan->piutang->first();
                            $piutang->update([
                                'total' => $penjualan->total,
                                'balance' => $penjualan->total - $piutang->paid_off,
                            ]);
                            $piutang->piutangDetails->first()->update($piutang->only(['total', 'balance']));
                            if ($penjualan->total == $piutang->paid_off) {
                                $piutang_status = 'paid';
                            }
                        }
                        $penjualan->update([
                            'faktur' => 'approved',
                            'piutang_status' => $piutang_status,
                        ]);
                    } else {
                        $penjualan->update([
                            'faktur' => 'approved',                        
                            'approve_date' => \Carbon\Carbon::now()->toDateString(),
                        ]);
                        postpenjualan($penjualan->company_id, $penjualan->status == 'paid' ? 'cash' : 'credit', $penjualan->code, $penjualan->dpp, $penjualan->tax, $penjualan->discount, $penjualan->total);
                        // Check payment cash 
                        if ($penjualan->status == 'paid') {
                            $approveDate = \Carbon\Carbon::parse($penjualan->approve_date)->format('d/m/Y');
                            $message = postgl($penjualan->company_id, $penjualan->coa_code, $penjualan->code, $penjualan->code.' '.$approveDate, $penjualan->total);
                            if ($message['status'] === 'failed') {
                                return $this->fail($message['message'], 422, $message['data'] ?? []);
                            }
                        } else {
                            prosesPiutang([
                                "company_id" => $penjualan->company_id,
                                "penjualan_id" => $penjualan->penjualan_id,
                                "total" => $penjualan->total,
                                "paid_off" => $penjualan->status == 'paid' ? $penjualan->total : 0,
                                "balance" => $penjualan->status == 'unpaid' ? $penjualan->total : 0,
                            ], $penjualan->company->coa_piutang);
                        }
                    }
                break;
            }
            \DB::commit();
            return $this->success('success_process', 200, ['status' => $penjualan->faktur]);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * revisiApprove
     *
     * @param  int $id
     * @return
     */
    private function revisiApprove(int $id)
    {
        $jual = $this->penjualan->show($id);
        $modelDetail = $this->penjualanDetail->getModel();
        // Check Stock
        $stockDetail = $this->productDetail->getModel();
        $stockJual = $stockDetail->where('status', 'penjualan')->where('ref', $jual->code)->get();
        // match stock edit
        $editIds = $modelDetail->where('revisi', 'edit')->get();
        $stockEdit = $stockJual->whereIn('product_id', $editIds->pluck('product_id'))->flatten();
        $stockEdit->each(function ($item) use ($stockDetail, $editIds) {
            $jualItem = $editIds->where('product_id', $item->product_id)->first();
            $args = $item->only(['product_id', 'status', 'batch', 'unit_lg', 'unit_sm', 'price_lg', 'price_sm']);
            $args = array_merge($args, [
                'ref' => $item->ref.' [revisi]',
                'qty_lg' => $item->cur_qty_lg - $jualItem->qty_lg,
                'qty_sm' => $item->cur_qty_sm - $jualItem->qty_sm,
            ]);
            if ($args['qty_lg'] > 0 || $args['qty_sm'] > 0) {
                $stockDetail->create($args);
            }
            $item->update([
                'cur_qty_lg' => $item->cur_qty_lg - $args['qty_lg'],
                'cur_qty_sm' => $item->cur_qty_sm - $args['qty_sm'],
            ]);
            app(\App\Http\Controllers\Api\Master\ProductController::class)->updateStock($item->product_id);
        });
        $editIds->map->update(['revisi' => null]);
        // match stock delete
        $delIds = $modelDetail->where('revisi', 'delete')->get();
        $stockDel = $stockJual->whereIn('product_id', $delIds->pluck('product_id'))->flatten();
        $stockDel->each(function ($item) use ($stockDetail, $delIds) {
            $args = $item->only(['product_id', 'status', 'batch', 'unit_lg', 'unit_sm', 'qty_lg', 'qty_sm', 'price_lg', 'price_sm']);
            $args = array_merge($args, ['ref' => $item->ref.' [revisi]']);
            $stockDetail->create($args);
            $item->update([
                'cur_qty_lg' => 0,
                'cur_qty_sm' => 0,
            ]);
            app(\App\Http\Controllers\Api\Master\ProductController::class)->updateStock($item->product_id);
        });
        $delIds->map->delete();
        return $jual;
    }

    /**
     * revisiReject
     *
     * @param  int $id
     * @param  bool $rejected
     * @return
     */
    private function revisiReject(int $id, bool $rejected)
    {
        $jual = $this->penjualan->show($id);
        if ($rejected == false) {
            $restDetail = $jual->penjualanDetails->whereNotNull('revisi')->flatten();
            $restDetail->each(function ($item) use ($jual) {
                $jualDetail = $jual->penjualanDetails->where('product_id', $item->product_id)->first();
                if ($item->product->pack && $item->product->pack_qty) {
                    $qty_sm = ($jualDetail->rev_qty_lg * $item->product->unit_qty) + $jualDetail->rev_qty_sm;
                    $amount = $qty_sm * calcAmount($item->price_sm, $item->tax);
                }
                $jualDetail->update([
                    'qty_lg' => $jualDetail->rev_qty_lg,
                    'qty_sm' => $jualDetail->rev_qty_sm,
                    'amount' => $amount,    
                    'revisi' => null,                
                ]);
            });
        }
        return $jual;
    }

    /**
     * revisiReject
     *
     * @param  int $id
     * @param  bool $rejected
     * @return
     */
    private function revisiVoid(int $id)
    {
        $jual = $this->penjualan->show($id);
        // Check Stock
        $stockDetail = $this->productDetail->getModel();
        $stockJual = $stockDetail->where('status', 'penjualan')->where('ref', $jual->code)->get();
        // match stock void
        $stockVoid = $stockJual->whereIn('product_id', $jual->penjualanDetails->pluck('product_id'))->flatten();
        $stockVoid->each(function ($item) use ($stockDetail) {
            $args = $item->only(['product_id', 'status', 'batch', 'unit_lg', 'unit_sm', 'price_lg', 'price_sm']);
            $args = array_merge($args, [
                'ref' => $item->ref.' [void]',
                'qty_lg' => $item->cur_qty_lg,
                'qty_sm' => $item->cur_qty_sm,
            ]);
            $stockDetail->create($args);
            $item->update([
                'cur_qty_lg' => 0,
                'cur_qty_sm' => 0,
            ]);
            app(\App\Http\Controllers\Api\Master\ProductController::class)->updateStock($item->product_id);
        });
        return $jual;
    }
}
