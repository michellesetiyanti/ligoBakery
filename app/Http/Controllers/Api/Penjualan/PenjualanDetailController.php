<?php

namespace App\Http\Controllers\Api\Penjualan;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Penjualan\Penjualan as PenjualanRequest;
use App\Repositories\Master\Product as ProductRepository;
use App\Repositories\Master\Warehouse as WarehouseRepository;
use App\Repositories\Penjualan\Penjualan as PenjualanRepository;
use App\Repositories\Penjualan\PenjualanDetail as PenjualanDetailRepository;

class PenjualanDetailController extends ApiController
{
    protected $penjualan;
    protected $penjualanDetail;
    protected $product;
    protected $warehouse;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->penjualan = new PenjualanRepository;
        $this->penjualanDetail = new PenjualanDetailRepository;
        $this->product = new ProductRepository;
        $this->warehouse = new WarehouseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $penjualanDetail = $this->penjualanDetail->with(['penjualan', 'product'])->all($request->all());
        return $this->success(null, 200, $penjualanDetail);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PenjualanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PenjualanRequest $request)
    {
        if (empty(auth()->user()->company->coa_penjualan) || empty(auth()->user()->company->coa_piutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $warehouse = $this->warehouse->show($request->wh_id);
        $product = $this->product->all()->where('wh_id', $request->wh_id)->where('product_id', $request->details['product_id'])->first();
        if (empty($product)) {
            return $this->fail('product_dont_exist', 422);
        }
        $coa_code = $request->missing('coa_code') || $request->coa_code == 'null' ? null : $request->coa_code;
        $due_date = $request->missing('entry_at') || $request->entry_at == 'null' ? null : $request->entry_at;
        if ($due_date) {
            $due_date = \Carbon\Carbon::now()->add($due_date, 'day')->format('d/m/Y');
        }
        $model = $this->penjualan->getModel();
        \DB::beginTransaction();
        try {
            $jual = $model->where('code', $request->no_faktur)->first();
            if (empty($jual)) {
                $jual = $model->create($request->merge([
                    'company_id' => $warehouse->company_id,
                    'code' => $request->no_faktur,
                    'due_date' => $due_date,
                    'status' => $request->payment == 'cash' ? 'paid' : 'unpaid',
                    'coa_code' => $coa_code,
                ])->except('details'));
            }
            if ($jual->penjualanDetails()->where('product_id', $product->product_id)->exists()) {
                return $this->fail('product_exist', 422, $jual->penjualanDetails->where('product_id', $product->product_id)->first());
            }
            if ($request->details['qty_lg'] > $product->stock('lg') || $request->details['qty_sm'] > $product->stock('sm')) {
                return $this->fail('qty_bigger_than', 422, [
                    'stock_order_lg' => $request->details['qty_lg'],
                    'stock_order_sm' => $request->details['qty_sm'],
                    'stock_prod_lg' => $product->stock('lg'),
                    'stock_prod_sm' => $product->stock('sm'),
                ]);
            }
            $amount = $request->details['qty_lg'] * calcAmount($request->details['price_lg'], $request->details['tax']);
            $amount+= $request->details['qty_sm'] * calcAmount($request->details['price_sm'], $request->details['tax']);
            $jualDetail = $jual->penjualanDetails()->create([
                'product_id' => $product->product_id,
                'unit_lg' => $product->unit_lg,
                'unit_sm' => $product->unit_sm,
                'qty_lg' => $request->details['qty_lg'],
                'qty_sm' => $request->details['qty_sm'],
                'price_lg' => $request->details['price_lg'],
                'price_sm' => $request->details['price_sm'],
                'tax' => $request->details['tax'],
                'discount' => $request->details['discount'],
                'amount' => $amount - $request->details['discount'],
            ]);
            $jual->update($request->merge([
                'due_date' => $due_date,
                'status' => $request->payment == 'cash' ? 'paid' : 'unpaid',
                'coa_code' => $coa_code,
            ])->except(['no_faktur', 'details']));
            \DB::commit();
            return $this->success('success_process', 200, $jualDetail);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail('fail_process', 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penjualan = $this->penjualan->all();
        if ($penjualanDetail = $penjualan->flatMap->penjualanDetails->where('penjualan_d_id', $id)->first()) {
            return $this->success(null, 200, $penjualanDetail);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PenjualanRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PenjualanRequest $request, $id)
    {
        if (empty(auth()->user()->company->coa_penjualan) || empty(auth()->user()->company->coa_piutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $product = $this->product->all()->where('wh_id', $request->wh_id)->where('product_id', $request->details['product_id'])->first();
        if (empty($product)) {
            return $this->fail('product_dont_exist', 422);
        }
        $coa_code = $request->missing('coa_code') || $request->coa_code == 'null' ? null : $request->coa_code;
        $due_date = $request->missing('entry_at') || $request->entry_at == 'null' ? null : $request->entry_at;
        if ($due_date) {
            $due_date = \Carbon\Carbon::now()->add($due_date, 'day')->format('d/m/Y');
        }  
        $jualDetail = $this->penjualanDetail->show($id);
        $jual = $this->penjualan->show($jualDetail->penjualan_id);
        \DB::beginTransaction();
        try {
            $amount = $request->details['qty_lg'] * calcAmount($request->details['price_lg'], $request->details['tax']);
            $amount+= $request->details['qty_sm'] * calcAmount($request->details['price_sm'], $request->details['tax']);
            $jualDetail->update([
                'qty_lg' => $request->details['qty_lg'],
                'qty_sm' => $request->details['qty_sm'],
                'price_lg' => $request->details['price_lg'],
                'price_sm' => $request->details['price_sm'],
                'tax' => $request->details['tax'],
                'discount' => $request->details['discount'],
                'amount' => $amount - $request->details['discount'],
            ]);
            $jual->update($request->merge([
                'due_date' => $due_date,
                'status' => $request->payment == 'cash' ? 'paid' : 'unpaid',
                'coa_code' => $coa_code,
            ])->except(['no_faktur', 'details']));
            \DB::commit();
            return $this->success('success_process', 200, $jualDetail);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail('fail_process', 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jualDetail = $this->penjualanDetail->show($id);
        $jual = $this->penjualan->show($jualDetail->penjualan_id);
        if (in_array($jual->faktur, ['approved', 'void'])) {
            return $this->fail('fail_status', 422, ['status' => $jual->faktur]);
        }
        \DB::beginTransaction();
        try {
            if (in_array($jual->faktur, ['actived', 'unactived'])) {
                $jualDetail->update(['revisi' => 'delete']);
            } else {
                $jualDetail->delete();
            }
            \DB::commit();
            return $this->success('success_process', 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail('fail_process', 500, $e->getMessage());
        }
    }
}
