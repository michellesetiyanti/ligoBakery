<?php

namespace App\Http\Controllers\Api\Penjualan;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Repositories\Penjualan\Piutang as PiutangRepository;
use Illuminate\Http\Request;

class PiutangController extends ApiController
{
    protected $piutang;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth', ['except' => ['store', 'update', 'destroy']]);
        $this->piutang = new PiutangRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $piutang = $this->piutang->with(['piutangDetails', 'penjualan'])->all($request->merge(compact('filter'))->all());
        $piutang = $piutang->where('penjualan.faktur', 'approved')->flatten();
        return $this->success(null, 200, $piutang);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($piutang = $this->piutang->with(['piutangDetails', 'penjualan'])->show($id)) {
            return $this->success(null, 200, $piutang);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Do Action the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function doAction(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'coa_code' => 'required|string|exists:coa,code',
            'amount' => 'required|digits_between:1,20',
            'execute' => 'required|string|in:pay',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        $piutang = $this->piutang->show($id);
        \DB::beginTransaction();
        try {
            switch ($request->execute) {
                case 'pay':
                    $piutangDetails = $piutang->piutangDetails->first();
                    if (($piutangDetails->paid_off + $request->amount) <= $piutangDetails->total) {
                        $piutangDetails->update([
                            'coa_code' => $request->coa_code,
                            'paid_off' => $piutangDetails->paid_off + $request->amount,
                            'balance' => $piutangDetails->balance - $request->amount,
                        ]);
                        // Balance Paid Off Piutang
                        $piutang->update([
                            'paid_off' => $piutang->paid_off + $request->amount,
                            'balance' => $piutang->balance - $request->amount,
                        ]);
                        // Post GL
                        $status = 'success';
                        $message = postgl($piutangDetails->company_id, $request->coa_code, $piutang->penjualan->code, \Carbon\Carbon::now()->format('d/m/Y').' '.$piutang->penjualan->code, $request->amount);
                    } else {
                        $status = 'fail';
                        $message = sprintf(
                            'Piutang\'s balance is "%s".',
                            number_format($piutangDetails->balance, 2)
                        );
                    }
                    if ($piutang->paid_off == $piutang->total) {
                        // Update status penjualan
                        $piutang->penjualan->update(['piutang_status' => 'paid']);
                    }
                break;
            }
            \DB::commit();
            switch ($status) {
                case 'success':
                    return $this->success($message, 200, $piutang);
                break;
                case 'fail':
                    return $this->fail($message, 422);
                break;
            }
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
