<?php

namespace App\Http\Controllers\Api\Penjualan\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Penjualan\Penjualan as PenjualanRequest;
use App\Repositories\Master\Warehouse as WarehouseRepository;
use App\Repositories\Penjualan\Penjualan as PenjualanRepository;
use Illuminate\Http\Request;

class PenjualanController extends ApiController
{
    protected $penjualan;
    protected $warehouse;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->penjualan = new PenjualanRepository;
        $this->warehouse = new WarehouseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $penjualan = $this->penjualan->with(['customer', 'penjualanDetails', 'deliveryOrders', 'piutang', 'karyawan', 'penagihan', 'denda'])->all($request->merge(compact('filter'))->all());
        $penjualan = $penjualan->whereNotNull('po_pro_id')->flatten();
        if ($do_status = $request->do_status) {
            $penjualan = collect($penjualan)->where('do_status', $do_status)->flatten();
        }
        $args = [];
        foreach ($penjualan as $res) {
            $item = $res->makeHidden('penjualanDetails')->toArray();
            $item = array_merge($item, [
                'penjualan_details' => $res->penjualanDetails->where('revisi', '!=', 'delete')->flatten()
            ]);
            array_push($args, $item);
        }
        return $this->success(null, 200, $args);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $due_date = $request->missing('due_date') || $request->due_date == 'null' ? null : $request->due_date;
        if ($due_date) {
            $due_date = \Carbon\Carbon::now()->add($due_date, 'day')->format('d/m/Y');
        }
        $code = app(\App\Http\Controllers\Api\Penjualan\PenjualanController::class)->codeFaktur($request, 1);
        $model = $this->penjualan->getModel();
        $jual = $model->create([
            'po_pro_id' => $request->po_pro_id,
            'company_id' => $request->company_id,
            'wh_id' => $request->wh_id,
            'code' => $code,
            'code_spk' => $request->code_spk,
            'ongkir' => $request->ongkir,
            'ongkir_desc' => $request->ongkir_desc,
            'biaya_lain' => $request->biaya_lain,
            'biaya_lain_desc' => $request->biaya_lain_desc,
            'status' => $request->payment == 'cash' ? 'paid' : 'unpaid',
            'due_date' => $due_date,
            'coa_code' => $request->coa_code,
            'faktur' => 'pending',
        ]);
        if ($request->has('details')) {
            foreach ($request->details as $detail) {
                $jual->penjualanDetails()->create([                    
                    'product_id' => $detail['product_id'],
                    'unit_lg' => $detail['unit_lg'],
                    'unit_sm' => $detail['unit_sm'],
                    'qty_lg' => $detail['qty_lg'],
                    'qty_sm' => $detail['qty_sm'],
                    'price_lg' => $detail['price_lg'],
                    'price_sm' => $detail['price_sm'],
                    'tax' => $detail['tax'],
                    'discount' => $detail['discount'],
                    'amount' => $detail['amount'],
                ]);
            }
        }        
        return $jual;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $jual = $this->penjualan->with(['customer', 'penjualanDetails', 'deliveryOrders', 'piutang', 'karyawan', 'penagihan', 'denda'])->show($id);
        $args = $jual->makeHidden('penjualanDetails')->toArray();
        $args = array_merge($args, [
            'penjualan_details' => $jual->penjualanDetails->where('revisi', '!=', 'delete')->flatten()
        ]);
        return $this->success(null, 200, $args);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PenjualanRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PenjualanRequest $request, $id)
    {
        // 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // 
    }
    
    public function approve(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'payment' => 'required|in:cash,credit',
            'due_date' => 'nullable|required_if:payment,credit',
            'coa_code' => 'nullable|required_if:payment,cash',
            'ongkir' => 'nullable|digits_between:1,20',
            'ongkir_desc' => 'nullable',
            'biaya_lain' => 'nullable|digits_between:1,20',
            'biaya_lain_desc' => 'nullable',
        ]);
        if ($validator->fails()) {
            return $this->fail('fail_validation', 422, $validator->errors());
        }
        $model = $this->penjualan->getModel();
        $jual = $model->find($id);
        \DB::beginTransaction();
        try {
            if (!in_array($jual->faktur, ['pending', 'actived', 'unactived'])) {
                return $this->fail('fail_process', 422, ['status' => $jual->faktur]);
            }
            // PO Details
            $statusPay = $request->payment ? ($request->payment == 'cash') ? 'paid' : 'unpaid' : $penjualan->status;
            $ongkir = $request->ongkir ?? $jual->ongkir;
            $biayaLain = $request->biaya_lain ?? $jual->biaya_lain;
            $coaCode = $request->coa_code && $request->coa_code == 'null' ? null : $request->coa_code;
            $dueDate = $request->due_date && $request->due_date == 'null' ? null : $request->due_date;
            if ($dueDate) {
                $dueDate = $jual->created_at->add($dueDate, 'day')->format('d/m/Y');
            }
            $jual->update([
                'ongkir' => $ongkir,
                'ongkir_desc' => $request->ongkir_desc == 'null' ? null : $request->ongkir_desc,
                'biaya_lain' => $biayaLain,
                'biaya_lain_desc' => $request->biaya_lain_desc == 'null' ? null : $request->biaya_lain_desc,                
                'status' => $statusPay,
                'due_date' => $dueDate,
                'coa_code' => $coaCode,
                'faktur' => 'approved',
                'approve_date' => \Carbon\Carbon::now()->toDateString(),
                'signed' => authExecute(),
                'piutang_status' => $statusPay == 'paid' ? null : 'unpaid',
            ]);
            // Post GL & Check payment
            postpenjualan($jual->company_id, $jual->status == 'paid' ? 'cash' : 'credit', $jual->code, $jual->dpp, $jual->tax, $jual->discount, $jual->total);
            if ($jual->status == 'paid') {
                $approveDate = \Carbon\Carbon::parse($jual->approve_date)->format('d/m/Y');
                $message = postgl($jual->company_id, $jual->coa_code, $jual->code, $jual->code.' '.$approveDate, $jual->total);
                if ($message['status'] === 'failed') {
                    return $this->fail($message['message'], 422, $message['data'] ?? []);
                }
            } else {
                prosesPiutang([
                    "company_id" => $jual->company_id,
                    "penjualan_id" => $jual->penjualan_id,
                    "total" => $jual->total,
                    "paid_off" => $jual->status == 'paid' ? $jual->total : 0,
                    "balance" => $jual->status == 'unpaid' ? $jual->total : 0,
                ], $jual->company->coa_piutang);
            }
            \DB::commit();
            return $this->success('success_process', 200, ['status' => $jual->faktur]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->fail('fail_process', 500, $e->getMessage());
        }
    }
    
    public function reject(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'rejected' => 'required|boolean',
        ]);
        if ($validator->fails()) {
            return $this->fail('fail_validation', 422, $validator->errors());
        }
        $faktur = $request->boolean('rejected') ? 'reject' : 'unverified';
        $jual = $this->penjualan->show($id);
        $jual->update(['faktur' => $faktur, 'notes' => $request->notes]);
        
        return $this->success('success_process', 200, $jual);
    }
    
    public function void($id)
    {
        $jual = $this->penjualan->show($id);
        $jual->update([
            'faktur' => 'void',
            'notes' => 'Void by '.auth()->user()->username,
        ]);
        
        return $this->success('success_process', 200, $jual);
    }
}
