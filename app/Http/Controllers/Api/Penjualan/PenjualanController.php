<?php

namespace App\Http\Controllers\Api\Penjualan;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Penjualan\Penjualan as PenjualanRequest;
use App\Repositories\Master\Company as CompanyRepository;
use App\Repositories\Master\Warehouse as WarehouseRepository;
use App\Repositories\Penjualan\Penjualan as PenjualanRepository;
use App\Repositories\Project\Order as OrderRepository;

class PenjualanController extends ApiController
{
    protected $company;
    protected $order;
    protected $penjualan;
    protected $warehouse;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->company = new CompanyRepository;
        $this->order = new OrderRepository;
        $this->penjualan = new PenjualanRepository;
        $this->warehouse = new WarehouseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $penjualan = $this->penjualan->with(['customer', 'penjualanDetails', 'deliveryOrders', 'piutang', 'karyawan', 'penagihan', 'denda'])->all($request->merge(compact('filter'))->all());
        $penjualan = $penjualan->whereNull('po_pro_id')->flatten();
        if ($do_status = $request->do_status) {
            $penjualan = collect($penjualan)->where('do_status', $do_status)->flatten();
        }
        $args = [];
        foreach ($penjualan as $res) {
            $item = $res->makeHidden('penjualanDetails')->toArray();
            $item = array_merge($item, [
                'penjualan_details' => $res->penjualanDetails->where('revisi', '!=', 'delete')->flatten()
            ]);
            array_push($args, $item);
        }
        return $this->success(null, 200, $args);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PenjualanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PenjualanRequest $request)
    {
        if (empty(auth()->user()->company->coa_penjualan) || empty(auth()->user()->company->coa_piutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $warehouse = $this->warehouse->show($request->wh_id);
        $coa_code = $request->missing('coa_code') || $request->coa_code == 'null' ? null : $request->coa_code;
        $due_date = $request->missing('entry_at') || $request->entry_at == 'null' ? null : $request->entry_at;
        if ($due_date) {
            $due_date = \Carbon\Carbon::now()->add($due_date, 'day')->format('d/m/Y');
        }
        $model = $this->penjualan->getModel();
        \DB::beginTransaction();
        try {
            $jual = $model->where('code', $request->no_faktur)->first();
            if (empty($jual)) {
                $jual = $model->create($request->merge([
                    'company_id' => $warehouse->company_id,
                    'code' => $request->no_faktur,
                    'due_date' => $due_date,
                    'status' => $request->payment == 'cash' ? 'paid' : 'unpaid',
                    'coa_code' => $coa_code,
                ])->except('details'));
            }
            $jual->update($request->merge([
                'due_date' => $due_date,
                'status' => $request->payment == 'cash' ? 'paid' : 'unpaid',
                'coa_code' => $coa_code,
                'faktur' => 'pending',
            ])->except(['no_faktur', 'details']));
            \DB::commit();
            return $this->success(null, 200, $jual);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $jual = $this->penjualan->with(['customer', 'penjualanDetails', 'deliveryOrders', 'piutang', 'karyawan', 'penagihan', 'denda'])->show($id);
        $args = $jual->makeHidden('penjualanDetails')->toArray();
        $args = array_merge($args, [
            'penjualan_details' => $jual->penjualanDetails->where('revisi', '!=', 'delete')->flatten()
        ]);
        return $this->success(null, 200, $args);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PenjualanRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PenjualanRequest $request, $id)
    {
        if (empty(auth()->user()->company->coa_penjualan) || empty(auth()->user()->company->coa_piutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $coa_code = $request->missing('coa_code') || $request->coa_code == 'null' ? null : $request->coa_code;
        $due_date = $request->missing('entry_at') || $request->entry_at == 'null' ? null : $request->entry_at;
        if ($due_date) {
            $due_date = \Carbon\Carbon::now()->add($due_date, 'day')->format('d/m/Y');
        }
        $jual = $this->penjualan->show($id);
        if (in_array($jual->faktur, ['approved', 'reject', 'void'])) {
            return $this->fail('fail_status', 422, ['status' => $jual->faktur]);
        }
        \DB::beginTransaction();
        try {
            $jual->update($request->merge([
                'due_date' => $due_date,
                'status' => $request->payment == 'cash' ? 'paid' : 'unpaid',
                'coa_code' => $coa_code,
                'faktur' => 'pending',
            ])->except(['details', 'customer_id']));
            \DB::commit();
            return $this->success(null, 200, $jual);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jual = $this->penjualan->show($id);
        if (in_array($jual->faktur, ['approved', 'void'])) {
            return $this->fail('fail_status', 422, ['status' => $jual->faktur]);
        }
        \DB::beginTransaction();
        try {
            $jual->delete();
            \DB::commit();
            return $this->success('success_process', 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail('fail_process', 500, $e->getMessage());
        }
    }
    
    public function codeFaktur(\Illuminate\Http\Request $request, $toJson = 0)
    {
        $now = \Carbon\Carbon::now();
        $auth = auth()->user();
        $model = $this->penjualan->getModel();
        $code = 'SLS-[CV] [NO] %lpar%[WO]%rpar% [PO]-[MM]/[YY]';
        // =====
        $code_spk = $request->has('code_spk') ? $request->code_spk : null;
        $company = $auth->company;
        if ($request->has('company_id')) {
            $company = $this->company->show($request->company_id);
        }
        // =====
        // Kode CV
        $code_CV = $company->code_alpha ?? str_pad($company->company_id, 5, '0', STR_PAD_LEFT);
        $code = str_replace('[CV]', $code_CV, $code);
        // No. Urut
        $noUrut = $model->where('company_id', $company->company_id)->whereMonth('created_at', $now->format('m'))->orderBy('penjualan_id', 'DESC')->get();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('code')->first();
            preg_match('/SLS-'.$code_CV.' (\d{3})/', $noUrut, $match);
            $noUrut = empty($match) ? 1 : $match[1] + 1;
        } else {
            $noUrut = 1;
        }
        $noUrut = str_pad($noUrut, strlen($noUrut) > 3 ? $noUrut : 3, '0', STR_PAD_LEFT);
        $code = str_replace('[NO]', $noUrut, $code);
        // No. WO || TOTAL PO DALAM SPK
        if ($code_spk) {
            $wo = $this->order->getModel()->where('code_spk', $code_spk)->pluck('work_order')->first();
            $po = $model->where('company_id', $company->company_id)->where('code_spk', $code_spk)->count() + 1;
            $po = str_pad($po, 2, '0', STR_PAD_LEFT);
            $code = str_replace(array('[WO]', '[PO]', '%lpar%', '%rpar%'), array($wo, $po, '[', ']'), $code);
        } else {
            $code = str_replace(' %lpar%[WO]%rpar% [PO]', '', $code);
        }
        // Bulan Terbit || Tahun Terbit
        $code = str_replace(array('[MM]', '[YY]'), array($now->format('m'), $now->format('y')), $code);
                
        if ($toJson > 0) return $code;
        return $this->success(null, 200, compact('code'));
    }
}
