<?php

namespace App\Http\Controllers\Api\Report;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Repositories\Pembelian\Hutang as HutangRepository;
use App\Repositories\Pembelian\Pembelian as PembelianRepository;

class HutangController extends ApiController
{
    protected $hutang;
    protected $pembelian;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->hutang = new HutangRepository;
        $this->pembelian = new PembelianRepository;
    }

    public function hutang(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'supplier' => 'nullable|integer|exists:suppliers,supplier_id',
            'status' => 'nullable|string|in:all,paid,unpaid',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $search = [];
        if ($request->supplier) {
            $search = array_merge($search, ['supplier_id' => $request->supplier]);
        }
        if ($request->status) {
            switch ($request->status) {
                case 'paid':
                    $search = array_merge($search, [['balance', '=', '0']]);
                break;
                case 'unpaid':
                    $search = array_merge($search, [['balance', '<>', '0']]);
                break;
                default:
                    $search = array_merge($search, [['balance']]);
                break;
            }
        }
        $output = collect();
        $pembelian = $this->pembelian->all(['filter' => $filter, 'search' => collect($search)->only('supplier_id')->toArray()]);
        $pembelian = $pembelian->whereNull('po_pro_id')->where('status', 'approve')->flatten();
        $hutang = $this->hutang->all($request->merge([
            'filter' => 'pembelian_id|'.$pembelian->pluck('pembelian_id')->implode(':'),
            'search' => collect($search)->except('supplier_id')->toArray(),
        ])->all());
        foreach ($hutang as $htg) {
            $table['date'] = $htg->created_at->format('d/m/Y');
            $table['faktur'] = $htg->pembelian->code;
            $table['expired'] = $htg->pembelian->due_date->format('d/m/Y');
            $table['supplier']['id'] = $htg->pembelian->supplier->supplier_id;
            $table['supplier']['name'] = $htg->pembelian->supplier->name;
            $table['total'] = $htg->total;
            $table['paid_off'] = $htg->paid_off;
            $table['balance'] = $htg->balance;
            $output->push($table);
        }
        return $this->success(['total' => [
            'hutang' => $pembelian->where('payment', 'unpaid')->sum('total'),
            'hutang_terbayar' => $output->sum('paid_off'),
            'sisa_hutang' => $pembelian->where('payment', 'unpaid')->sum('total') - $output->sum('paid_off'),
        ]], 200, $output);
    }
    
    public function hutangTerbayar(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'supplier' => 'nullable|integer|exists:suppliers,supplier_id',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $search = [];
        if ($request->supplier) {
            $search = array_merge($search, ['supplier_id' => $request->supplier]);
        }
        $output = collect();
        $pembelian = $this->pembelian->all(['filter' => $filter, 'search' => collect($search)->only('supplier_id')->toArray()]);
        $pembelian = $pembelian->whereNull('po_pro_id')->where('status', 'approve')->flatten();
        $hutang = $this->hutang->all($request->merge([
            'filter' => 'pembelian_id|'.$pembelian->pluck('pembelian_id')->implode(':'),
        ])->all())->where('paid_off', '>', '0')->flatten();
        foreach ($hutang as $htg) {
            $table['date'] = $htg->created_at->format('d/m/Y');
            $table['faktur'] = $htg->pembelian->code;
            $table['expired'] = $htg->pembelian->due_date->format('d/m/Y');
            $table['supplier']['id'] = $htg->pembelian->supplier->supplier_id;
            $table['supplier']['name'] = $htg->pembelian->supplier->name;
            $table['total'] = $htg->total;
            $table['paid_off'] = $htg->paid_off;
            $table['balance'] = $htg->balance;
            $output->push($table);
        }
        return $this->success(['total' => [
            'hutang' => $pembelian->where('payment', 'unpaid')->sum('total'),
            'hutang_terbayar' => $output->sum('paid_off'),
            'sisa_hutang' => $pembelian->where('payment', 'unpaid')->sum('total') - $output->sum('paid_off'),
        ]], 200, $output);
    }
}
