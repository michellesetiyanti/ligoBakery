<?php

namespace App\Http\Controllers\Api\Report;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Repositories\Master\Company as CompanyRepository;
use App\Repositories\Master\Product as ProductRepository;
use App\Repositories\Master\ProductDetail as ProductDetailRepository;

class StockController extends ApiController
{
    protected $company;
    protected $product;
    protected $productDetail;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->company = new CompanyRepository;
        $this->product = new ProductRepository;
        $this->productDetail = new ProductDetailRepository;
    }
    
    public function jumlahStock(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'search' => 'nullable|string',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $wh_id = implode(':', auth()->user()->company->warehouse->pluck('wh_id')->toArray());
                $filter = 'wh_id|'.$wh_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }      
        switch ($request->exists('company_id')) {
            case true:
                $company_id = $request->company_id;
            break;
            default:
                $company_id = auth()->user()->company_id;
            break;
        }
        $products = $this->product->all($request->merge(compact('filter'))->all()); 
        $products = $products->where('warehouse.company_id', $company_id)->flatten();
        $output = collect();
        foreach ($products as $product) {
            $table['warehouse'] = $product->warehouse;
            $table['code'] = $product->code;
            $table['name'] = $product->name;
            $table['category'] = $product->productCategory->name;
            $table['stock']['unit']['lg'] = $product->unit_lg;
            $table['stock']['unit']['sm'] = $product->unit_sm;
            $table['stock']['unit']['qty'] = $product->unit_qty;
            $table['stock']['pack']['name'] = $product->pack;
            $table['stock']['pack']['qty'] = $product->pack_qty;
            $table['stock']['qty']['lg'] = $product->total_lg_qty;
            $table['stock']['qty']['sm'] = $product->total_sm_qty;                         
            $table['stock']['volume'] = $product->volume;
            $output->push($table);
        }
        return $this->success(null, 200, $output);
    }
    
    public function mutasiStock(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'company_id' => 'nullable|integer|exists:companies,company_id',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch ($request->exists('company_id')) {
            case true:
                $company_id = $request->company_id;
            break;
            default:
                $company_id = auth()->user()->company_id;
            break;
        }
        $sorting = 'product_d_id|DESC';
        $sorting = $request->sorting ? $request->sorting : $sorting;
        $result = $this->productDetail->all($request->merge(compact('sorting'))->all());
        $result = $result->where('product.warehouse.company_id', $company_id)->flatten();
        $output = collect();
        foreach ($result as $res) {
            $table['warehouse'] = $res->product->warehouse;
            $table['product'] = $res->product->only(['code', 'name', 'unit_lg', 'unit_sm', 'pack']);
            $table['status'] = $res->status;
            $table['ref'] = $res->ref;
            $table['batch'] = $res->batch;
            $table['qty_in']['lg'] = 0;
            $table['qty_in']['sm'] = 0;
            $table['qty_in']['to_pack'] = 0;
            $table['qty_out']['lg'] = 0;
            $table['qty_out']['sm'] = 0;
            $table['qty_out']['to_pack'] = 0;
            preg_match('/\brevisi\b/i', $res->ref, $revisi);
            preg_match('/\bvoid\b/i', $res->ref, $void);
            if (in_array($res->status, ['mutasi-tambah', 'pembelian'])) {
                if ($revisi || $void) {
                    $table['qty_out']['lg'] = $res->qty_lg;
                    $table['qty_out']['sm'] = $res->qty_sm;
                    $table['qty_out']['to_pack'] = $res->volume['stock']['qty'];
                } else {
                    $table['qty_in']['lg'] = $res->qty_lg;
                    $table['qty_in']['sm'] = $res->qty_sm;
                    $table['qty_in']['to_pack'] = $res->volume['stock']['qty'];
                }
            }
            if (in_array($res->status, ['mutasi-kurang', 'penjualan'])) {
                if ($revisi || $void) {
                    $table['qty_in']['lg'] = $res->qty_lg;
                    $table['qty_in']['sm'] = $res->qty_sm;
                    $table['qty_in']['to_pack'] = $res->volume['stock']['qty'];
                } else {
                    $table['qty_out']['lg'] = $res->qty_lg;
                    $table['qty_out']['sm'] = $res->qty_sm;
                    $table['qty_out']['to_pack'] = $res->volume['stock']['qty'];
                }
            }
            $output->push($table);
        }
        return $this->success(null, 200, $output);
    }
}
