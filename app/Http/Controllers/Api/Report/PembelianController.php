<?php

namespace App\Http\Controllers\Api\Report;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Repositories\Pembelian\Pembelian as PembelianRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PembelianDetailsExport;
use App\Exports\EFakturPembelianExport;

class PembelianController extends ApiController
{
    protected $pembelian;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->pembelian = new PembelianRepository;
    }
    
    public function pembelian(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'user' => 'nullable|integer|exists:users,user_id',
            'supplier' => 'nullable|integer|exists:suppliers,supplier_id',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $search = [];
        if ($request->user) {
            $search['user_id'] = $request->user;
        }
        if ($request->supplier) {
            $search['supplier_id'] = $request->supplier;
        }
        $output = collect();
        $pembelian = $this->pembelian->all($request->merge(compact('filter', 'search'))->all());
        $pembelian = $pembelian->whereNull('po_pro_id')->flatten();
        foreach ($pembelian as $beli) {
            $table['user']['id'] = $beli->user->user_id;
            $table['user']['name'] = $beli->user->karyawan->name ?? $beli->user->username;
            $table['name'] = $beli->code;
            $table['supplier']['id'] = $beli->supplier->supplier_id;
            $table['supplier']['name'] = $beli->supplier->name;
            $table['faktur'] = $beli->poPembelian->name;
            $table['status'] = $beli->status;
            $table['payment'] = $beli->hutang ? 'credit' : 'cash';
            $table['total'] = $beli->total;
            $output->push($table);
        }
        return $this->success(['total' => [
            'pembelian' => $output->sum('total'),
        ]], 200, $output);
    }
    
    public function pembelianDetail(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'user' => 'nullable|integer|exists:users,user_id',
            'supplier' => 'nullable|integer|exists:suppliers,supplier_id',
            'product' => 'nullable|integer|exists:products,product_id',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $search = [];
        if ($request->user) {
            $search['user_id'] = $request->user;
        }
        if ($request->supplier) {
            $search['supplier_id'] = $request->supplier;
        }
        $pembelian = $this->pembelian->all($request->merge(compact('filter', 'search'))->except('product'));
        $pembelian = $pembelian->whereNull('po_pro_id')->flatten();
        $pembelianDetails = $pembelian->flatMap->pembelianDetails->groupBy('pembelian_id');
        if ($request->product) {
            $pembelianDetails = $pembelian->flatMap->pembelianDetails->where('product_id', $request->product)->groupBy('pembelian_id');
        }
        $output = collect();
        foreach ($pembelianDetails as $beli_id => $beliDetails) {
            $pembelianById = $this->pembelian->getModel()->find($beli_id);
            $beliDetails = $pembelianById->pembelianDetails->flatten();
            $product = [];
            for ($i=0; $i < $beliDetails->count(); $i++) {
                $product[$i]['id'] = $beliDetails[$i]->product->product_id;
                $product[$i]['code'] = $beliDetails[$i]->product->code;
                $product[$i]['description'] = $beliDetails[$i]->product->name;
                $product[$i]['unit']['lg'] = $beliDetails[$i]->product->unit_lg;
                $product[$i]['unit']['sm'] = $beliDetails[$i]->product->unit_sm;
                $product[$i]['qty']['lg'] = $beliDetails[$i]->qty_lg;
                $product[$i]['qty']['sm'] = $beliDetails[$i]->qty_sm;
                $product[$i]['price']['lg'] = $beliDetails[$i]->price_lg;
                $product[$i]['price']['sm'] = $beliDetails[$i]->price_sm;
                $product[$i]['amount'] = $beliDetails[$i]->amount;
                // Add pack code/qty
                $product[$i]['volume'] = $beliDetails[$i]->volume;
            }
            $table['company']['id'] = $pembelianById->company->company_id;
            $table['company']['name'] = $pembelianById->company->name;
            $table['company']['address'] = $pembelianById->company->address;
            $table['company']['npwp'] = $pembelianById->company->npwp;
            $table['user']['id'] = $pembelianById->user->user_id;
            $table['user']['name'] = $pembelianById->user->karyawan->name ?? $pembelianById->user->username;
            $table['date'] = $pembelianById->created_at->format('d/m/Y');
            $table['faktur'] = $pembelianById->poPembelian->name;
            $table['supplier']['id'] = $pembelianById->supplier->supplier_id;
            $table['supplier']['name'] = $pembelianById->supplier->name;
            $table['payment'] = $pembelianById->hutang ? 'credit' : 'cash';
            $table['status'] = $pembelianById->hutang && $pembelianById->hutang->balance > 0 ? 'uncomplete' : 'complete';
            $table['due_date'] = $pembelianById->due_date ? $pembelianById->due_date->format('d/m/Y') : null;
            $table['over_due'] = $pembelianById->over_due;
            $table['product'] = $product;
            $table['tax'] = $pembelianById->tax;
            $table['discount'] = $pembelianById->discount;
            $table['total'] = $pembelianById->total;
            $table['pay_off'] = $pembelianById->hutang ? $pembelianById->hutang->paid_off : $pembelianById->total;
            $output->push($table);
        }
        return $this->success(['total' => [
            'pembelian' => $output->sum('total'),
            'pembelian_terbayar' => $output->sum('pay_off'),
        ]], 200, $output);
    }

    public function createExcelPembelianDetail(\Illuminate\Http\Request $request)
    { 
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'user' => 'nullable|integer|exists:users,user_id',
            'supplier' => 'nullable|integer|exists:suppliers,supplier_id',
            'product' => 'nullable|integer|exists:products,product_id',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $search = [];
        if ($request->user) {
            $search['user_id'] = $request->user;
        }
        if ($request->supplier) {
            $search['supplier_id'] = $request->supplier;
        }
        $pembelian = $this->pembelian->all($request->merge(compact('filter', 'search'))->except('product'));
        $pembelian = $pembelian->whereNull('po_pro_id')->flatten();
        $pembelianDetails = $pembelian->flatMap->pembelianDetails->groupBy('pembelian_id');
        if ($request->product) {
            $pembelianDetails = $pembelian->flatMap->pembelianDetails->where('product_id', $request->product)->groupBy('pembelian_id');
        }
        $output = collect();
        foreach ($pembelianDetails as $beli_id => $beliDetails) {
            $pembelianById = $this->pembelian->getModel()->find($beli_id);
            $beliDetails = $pembelianById->pembelianDetails->flatten();
            $product = [];
            for ($i=0; $i < $beliDetails->count(); $i++) {
                $product[$i]['id'] = $beliDetails[$i]->product->product_id;
                $product[$i]['code'] = $beliDetails[$i]->product->code;
                $product[$i]['description'] = $beliDetails[$i]->product->name;
                $product[$i]['unit']['lg'] = $beliDetails[$i]->product->unit_lg;
                $product[$i]['unit']['sm'] = $beliDetails[$i]->product->unit_sm;
                $product[$i]['qty']['lg'] = $beliDetails[$i]->qty_lg;
                $product[$i]['qty']['sm'] = $beliDetails[$i]->qty_sm;
                $product[$i]['price']['lg'] = $beliDetails[$i]->price_lg;
                $product[$i]['price']['sm'] = $beliDetails[$i]->price_sm;
                $product[$i]['amount'] = $beliDetails[$i]->amount;
                // Add pack code/qty
                $product[$i]['volume'] = $beliDetails[$i]->volume;
            }
            $table['user']['id'] = $pembelianById->user->user_id;
            $table['user']['name'] = $pembelianById->user->karyawan->name ?? $pembelianById->user->username;
            $table['date'] = $pembelianById->created_at->format('d/m/Y');
            $table['faktur'] = $pembelianById->poPembelian->name;
            $table['supplier']['id'] = $pembelianById->supplier->supplier_id;
            $table['supplier']['name'] = $pembelianById->supplier->name;
            $table['payment'] = $pembelianById->hutang ? 'credit' : 'cash';
            $table['status'] = $pembelianById->hutang && $pembelianById->hutang->balance > 0 ? 'uncomplete' : 'complete';
            $table['due_date'] = $pembelianById->due_date ? $pembelianById->due_date->format('d/m/Y') : null;
            $table['over_due'] = $pembelianById->over_due;
            $table['product'] = $product;
            $table['tax'] = $pembelianById->tax;
            $table['discount'] = $pembelianById->discount;
            $table['total'] = $pembelianById->total;
            $table['pay_off'] = $pembelianById->hutang ? $pembelianById->hutang->paid_off : $pembelianById->total;
            $output->push($table);
        }
        session()->put('detail',$output);
        ob_end_clean(); // this
        ob_start(); // and this
       return Excel::download(new PembelianDetailsExport, 'ReportPembelianDetail.xlsx');
    }

    public function createExcelEFakturPembelian(\Illuminate\Http\Request $request)
    { 
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'user' => 'nullable|integer|exists:users,user_id',
            'supplier' => 'nullable|integer|exists:suppliers,supplier_id',
            'product' => 'nullable|integer|exists:products,product_id',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $search = [];
        if ($request->user) {
            $search['user_id'] = $request->user;
        }
        if ($request->supplier) {
            $search['supplier_id'] = $request->supplier;
        }
        $pembelian = $this->pembelian->all($request->merge(compact('filter', 'search'))->except('product'));
        $pembelian = $pembelian->whereNull('po_pro_id')->flatten();
        $pembelianDetails = $pembelian->flatMap->pembelianDetails->groupBy('pembelian_id');
        if ($request->product) {
            $pembelianDetails = $pembelian->flatMap->pembelianDetails->where('product_id', $request->product)->groupBy('pembelian_id');
        }
        $output = collect();
        foreach ($pembelianDetails as $beli_id => $beliDetails) {
            $pembelianById = $this->pembelian->getModel()->find($beli_id);
            $beliDetails = $pembelianById->pembelianDetails->flatten();
            $product = [];
            for ($i=0; $i < $beliDetails->count(); $i++) {
                $product[$i]['id'] = $beliDetails[$i]->product->product_id;
                $product[$i]['code'] = $beliDetails[$i]->product->code;
                $product[$i]['description'] = $beliDetails[$i]->product->name;
                $product[$i]['unit']['lg'] = $beliDetails[$i]->product->unit_lg;
                $product[$i]['unit']['sm'] = $beliDetails[$i]->product->unit_sm;
                $product[$i]['qty']['lg'] = $beliDetails[$i]->qty_lg;
                $product[$i]['qty']['sm'] = $beliDetails[$i]->qty_sm;
                $product[$i]['price']['lg'] = $beliDetails[$i]->price_lg;
                $product[$i]['price']['sm'] = $beliDetails[$i]->price_sm;
                $product[$i]['amount'] = $beliDetails[$i]->amount;
                // Add pack code/qty
                $product[$i]['volume'] = $beliDetails[$i]->volume;
            }
            $table['company']['id'] = $pembelianById->company->company_id;
            $table['company']['name'] = $pembelianById->company->name;
            $table['company']['address'] = $pembelianById->company->address;
            $table['company']['npwp'] = $pembelianById->company->npwp;
            $table['user']['id'] = $pembelianById->user->user_id;
            $table['user']['name'] = $pembelianById->user->karyawan->name ?? $pembelianById->user->username;
            $table['date'] = $pembelianById->created_at->format('d/m/Y');
            $table['faktur'] = $pembelianById->poPembelian->name;
            $table['supplier']['id'] = $pembelianById->supplier->supplier_id;
            $table['supplier']['name'] = $pembelianById->supplier->name;
            $table['payment'] = $pembelianById->hutang ? 'credit' : 'cash';
            $table['status'] = $pembelianById->hutang && $pembelianById->hutang->balance > 0 ? 'uncomplete' : 'complete';
            $table['due_date'] = $pembelianById->due_date ? $pembelianById->due_date->format('d/m/Y') : null;
            $table['over_due'] = $pembelianById->over_due;
            $table['product'] = $product;
            $table['tax'] = $pembelianById->tax;
            $table['ongkir'] = $pembelianById->ongkir;
            $table['biaya_lain'] = $pembelianById->biaya_lain;
            $table['discount'] = $pembelianById->discount;
            $table['total'] = $pembelianById->total;
            $table['pay_off'] = $pembelianById->hutang ? $pembelianById->hutang->paid_off : $pembelianById->total;
            $output->push($table);
        }
        $faktur1 = $request->faktur1;
        $faktur2 = $request->faktur2;
        $faktur3 = $request->faktur3;
        session()->put('detail',$output);
        session()->put('efaktur2',$faktur1);
        session()->put('efaktur3',$faktur2);
        session()->put('efaktur4',$faktur3);

        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new EFakturPembelianExport, 'documents-efakturpembelian.csv');
    }
}
