<?php

namespace App\Http\Controllers\Api\Report;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Repositories\Penjualan\Penjualan as PenjualanRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PenjualanDetailsExport;
use App\Exports\EFakturExport;

class PenjualanController extends ApiController
{
    protected $penjualan;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->penjualan = new PenjualanRepository;
    }
    
    public function penjualan(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'user' => 'nullable|integer|exists:users,user_id',
            'customer' => 'nullable|integer|exists:customers,customer_id',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $search = [];
        if ($request->user) {
            $search['user_id'] = $request->user;
        }
        if ($request->customer) {
            $search['customer_id'] = $request->customer;
        }
        $output = collect();
        $penjualan = $this->penjualan->with(['penjualanDetails', 'piutang', 'customer', 'user'])->all($request->merge(compact('filter', 'search'))->all());
        $penjualan = $penjualan->whereNull('po_pro_id')->flatten();
        foreach ($penjualan as $jual) {
            $table['user']['id'] = $jual->user->user_id;
            $table['user']['name'] = $jual->user->karyawan->name ?? $jual->user->username;
            $table['date'] = $jual->created_at->format('d/m/Y');
            $table['faktur'] = $jual->code;
            $table['customer']['id'] = $jual->customer->customer_id;
            $table['customer']['name'] = $jual->customer->name;
            $table['payment'] = $jual->piutang ? 'credit' : 'cash';
            $table['total'] = $jual->total;
            // Type Transaksi Agro
            $table['trans_type'] = $jual->trans_type;
            $output->push($table);
        }
        return $this->success(['total' => [
            'penjualan' => $output->sum('total'),
        ]], 200, $output);
    }
    
    public function penjualanDetail(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'user' => 'nullable|integer|exists:users,user_id',
            'customer' => 'nullable|integer|exists:customers,customer_id',
            'product' => 'nullable|integer|exists:products,product_id',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $search = [];
        if ($request->user) {
            $search['user_id'] = $request->user;
        }
        if ($request->customer) {
            $search['customer_id'] = $request->customer;
        }
        $output = collect();
        $penjualan = $this->penjualan->all($request->merge(compact('filter', 'search'))->except('product'));
        $penjualan = $penjualan->whereNull('po_pro_id')->flatten();
        $penjualanDetails = $penjualan->flatMap->penjualanDetails->groupBy('penjualan_id');
        if ($request->product) {
            $penjualanDetails = $penjualan->flatMap->penjualanDetails->where('product_id', $request->product)->groupBy('penjualan_id');
        }
        foreach ($penjualanDetails as $jual_id => $jualDetails) {
            $penjualanById = $this->penjualan->getModel()->find($jual_id);
            $jualDetails = $penjualanById->penjualanDetails->flatten();
            $product = [];
            for ($i=0; $i < $jualDetails->count(); $i++) {
                $product[$i]['id'] = $jualDetails[$i]->product->product_id;
                $product[$i]['code'] = $jualDetails[$i]->product->code;
                $product[$i]['description'] = $jualDetails[$i]->product->name;
                $product[$i]['unit']['lg'] = $jualDetails[$i]->product->unit_lg;
                $product[$i]['unit']['sm'] = $jualDetails[$i]->product->unit_sm;
                $product[$i]['unit_qty'] = $jualDetails[$i]->product->unit_qty;
                $product[$i]['qty']['lg'] = $jualDetails[$i]->qty_lg;
                $product[$i]['qty']['sm'] = $jualDetails[$i]->qty_sm;
                $product[$i]['price']['lg'] = $jualDetails[$i]->price_lg;
                $product[$i]['price']['sm'] = $jualDetails[$i]->price_sm;
                $product[$i]['tax'] = $jualDetails[$i]->tax;
                $product[$i]['discount'] = $jualDetails[$i]->discount;
                $product[$i]['amount'] = $jualDetails[$i]->amount;
                // Add pack code/qty
                $product[$i]['volume'] = $jualDetails[$i]->volume;
            }
            $table['user']['id'] = $penjualanById->user->user_id;
            $table['user']['name'] = $penjualanById->user->karyawan->name ?? $penjualanById->user->username;
            $table['sales']['id'] = $penjualanById->karyawan->karyawan_id ?? null;
            $table['sales']['name'] = $penjualanById->karyawan->name ?? 'office';
            $table['date'] = $penjualanById->created_at->format('d/m/Y');
            $table['faktur'] = $penjualanById->code;
            $table['ongkir'] = $penjualanById->ongkir;
            $table['biaya_lain'] = $penjualanById->biaya_lain;
            $table['customer']['id'] = $penjualanById->customer->customer_id;
            $table['customer']['name'] = $penjualanById->customer->name;
            $table['customer']['npwp'] = $penjualanById->customer->npwp;
            $table['customer']['address'] = $penjualanById->customer->address;
            $table['customer']['pkp_name'] = $penjualanById->customer->pkp_name;
            $table['customer']['pkp_address'] = $penjualanById->customer->pkp_address;
            $table['payment'] = $penjualanById->piutang->isNotEmpty() ? 'credit' : 'cash';
            $table['status'] = $penjualanById->piutang->isNotEmpty() && $penjualanById->piutang->first()->balance > 0 ? 'uncomplete' : 'complete';
            $table['product'] = $product;
            $table['tax'] = $penjualanById->tax;
            $table['discount'] = $penjualanById->discount;
            $table['total'] = $penjualanById->total;
            $table['pay_off'] = $penjualanById->piutang->isNotEmpty() ? $penjualanById->piutang->first()->paid_off : $penjualanById->total;
            // Type Transaksi Agro
            $table['trans_type'] = $penjualanById->trans_type;
            $output->push($table);
        }
        return $this->success(['total' => [
            'penjualan' => $output->sum('total'),
            'penjualan_terbayar' => $output->sum('pay_off'),
        ]], 200, $output);
    }

    public function createExcelPenjualanDetail(\Illuminate\Http\Request $request)
    { 
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'user' => 'nullable|integer|exists:users,user_id',
            'customer' => 'nullable|integer|exists:customers,customer_id',
            'product' => 'nullable|integer|exists:products,product_id',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $search = [];
        if ($request->user) {
            $search['user_id'] = $request->user;
        }
        if ($request->customer) {
            $search['customer_id'] = $request->customer;
        }
        $output = collect();
        $penjualan = $this->penjualan->all($request->merge(compact('filter', 'search'))->except('product'));
        $penjualan = $penjualan->whereNull('po_pro_id')->flatten();
        $penjualanDetails = $penjualan->flatMap->penjualanDetails->groupBy('penjualan_id');
        if ($request->product) {
            $penjualanDetails = $penjualan->flatMap->penjualanDetails->where('product_id', $request->product)->groupBy('penjualan_id');
        }
        foreach ($penjualanDetails as $jual_id => $jualDetails) {
            $penjualanById = $this->penjualan->getModel()->find($jual_id);
            $jualDetails = $penjualanById->penjualanDetails->flatten();
            $product = [];
            for ($i=0; $i < $jualDetails->count(); $i++) {
                $product[$i]['id'] = $jualDetails[$i]->product->product_id;
                $product[$i]['code'] = $jualDetails[$i]->product->code;
                $product[$i]['description'] = $jualDetails[$i]->product->name;
                $product[$i]['unit']['lg'] = $jualDetails[$i]->product->unit_lg;
                $product[$i]['unit']['sm'] = $jualDetails[$i]->product->unit_sm;
                $product[$i]['unit_qty'] = $jualDetails[$i]->product->unit_qty;
                $product[$i]['qty']['lg'] = $jualDetails[$i]->qty_lg;
                $product[$i]['qty']['sm'] = $jualDetails[$i]->qty_sm;
                $product[$i]['price']['lg'] = $jualDetails[$i]->price_lg;
                $product[$i]['price']['sm'] = $jualDetails[$i]->price_sm;
                $product[$i]['tax'] = $jualDetails[$i]->tax;
                $product[$i]['discount'] = $jualDetails[$i]->discount;
                $product[$i]['amount'] = $jualDetails[$i]->amount;
                // Add pack code/qty
                $product[$i]['volume'] = $jualDetails[$i]->volume;
            }
            $table['user']['id'] = $penjualanById->user->user_id;
            $table['user']['name'] = $penjualanById->user->karyawan->name ?? $penjualanById->user->username;
            $table['sales']['id'] = $penjualanById->karyawan->karyawan_id ?? null;
            $table['sales']['name'] = $penjualanById->karyawan->name ?? 'office';
            $table['date'] = $penjualanById->created_at->format('d/m/Y');
            $table['faktur'] = $penjualanById->code;
            $table['ongkir'] = $penjualanById->ongkir;
            $table['biaya_lain'] = $penjualanById->biaya_lain;
            $table['customer']['id'] = $penjualanById->customer->customer_id;
            $table['customer']['name'] = $penjualanById->customer->name;
            $table['customer']['npwp'] = $penjualanById->customer->npwp;
            $table['customer']['address'] = $penjualanById->customer->address;
            $table['customer']['pkp_name'] = $penjualanById->customer->pkp_name;
            $table['customer']['pkp_address'] = $penjualanById->customer->pkp_address;
            $table['payment'] = $penjualanById->piutang->isNotEmpty() ? 'credit' : 'cash';
            $table['status'] = $penjualanById->piutang->isNotEmpty() && $penjualanById->piutang->first()->balance > 0 ? 'uncomplete' : 'complete';
            $table['product'] = $product;
            $table['tax'] = $penjualanById->tax;
            $table['discount'] = $penjualanById->discount;
            $table['total'] = $penjualanById->total;
            $table['pay_off'] = $penjualanById->piutang->isNotEmpty() ? $penjualanById->piutang->first()->paid_off : $penjualanById->total;
            // Type Transaksi Agro
            $table['trans_type'] = $penjualanById->trans_type;
            $output->push($table);
        }
        session()->put('detail',$output);
        ob_end_clean(); // this
        ob_start(); // and this
       return Excel::download(new PenjualanDetailsExport, 'ReportPenjualanDetail.xlsx');
    }

    public function createExcelEFaktur(\Illuminate\Http\Request $request)
    { 
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'user' => 'nullable|integer|exists:users,user_id',
            'customer' => 'nullable|integer|exists:customers,customer_id',
            'product' => 'nullable|integer|exists:products,product_id',
            'faktur1' => 'nullable',
            'faktur2' => 'nullable',
            'faktur3' => 'nullable',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $search = [];
        if ($request->user) {
            $search['user_id'] = $request->user;
        }
        if ($request->customer) {
            $search['customer_id'] = $request->customer;
        }
        $output = collect();
        $penjualan = $this->penjualan->all($request->merge(compact('filter', 'search'))->except('product'));
        $penjualan = $penjualan->whereNull('po_pro_id')->flatten();
        $penjualanDetails = $penjualan->flatMap->penjualanDetails->groupBy('penjualan_id');
        if ($request->product) {
            $penjualanDetails = $penjualan->flatMap->penjualanDetails->where('product_id', $request->product)->groupBy('penjualan_id');
        }
        foreach ($penjualanDetails as $jual_id => $jualDetails) {
            $penjualanById = $this->penjualan->getModel()->find($jual_id);
            $jualDetails = $penjualanById->penjualanDetails->flatten();
            $product = [];
            for ($i=0; $i < $jualDetails->count(); $i++) {
                $product[$i]['id'] = $jualDetails[$i]->product->product_id;
                $product[$i]['code'] = $jualDetails[$i]->product->code;
                $product[$i]['description'] = $jualDetails[$i]->product->name;
                $product[$i]['unit']['lg'] = $jualDetails[$i]->product->unit_lg;
                $product[$i]['unit']['sm'] = $jualDetails[$i]->product->unit_sm;
                $product[$i]['unit_qty'] = $jualDetails[$i]->product->unit_qty;
                $product[$i]['qty']['lg'] = $jualDetails[$i]->qty_lg;
                $product[$i]['qty']['sm'] = $jualDetails[$i]->qty_sm;
                $product[$i]['price']['lg'] = $jualDetails[$i]->price_lg;
                $product[$i]['price']['sm'] = $jualDetails[$i]->price_sm;
                $product[$i]['tax'] = $jualDetails[$i]->tax;
                $product[$i]['discount'] = $jualDetails[$i]->discount;
                $product[$i]['amount'] = $jualDetails[$i]->amount;
                // Add pack code/qty
                $product[$i]['volume'] = $jualDetails[$i]->volume;
            }
            $table['user']['id'] = $penjualanById->user->user_id;
            $table['user']['name'] = $penjualanById->user->karyawan->name ?? $penjualanById->user->username;
            $table['sales']['id'] = $penjualanById->karyawan->karyawan_id ?? null;
            $table['sales']['name'] = $penjualanById->karyawan->name ?? 'office';
            $table['date'] = $penjualanById->created_at->format('d/m/Y');
            $table['faktur'] = $penjualanById->code;
            $table['ongkir'] = $penjualanById->ongkir;
            $table['biaya_lain'] = $penjualanById->biaya_lain;
            $table['customer']['id'] = $penjualanById->customer->customer_id;
            $table['customer']['name'] = $penjualanById->customer->name;
            $table['customer']['npwp'] = $penjualanById->customer->npwp;
            $table['customer']['address'] = $penjualanById->customer->address;
            $table['customer']['pkp_name'] = $penjualanById->customer->pkp_name;
            $table['customer']['pkp_address'] = $penjualanById->customer->pkp_address;
            $table['payment'] = $penjualanById->piutang->isNotEmpty() ? 'credit' : 'cash';
            $table['status'] = $penjualanById->piutang->isNotEmpty() && $penjualanById->piutang->first()->balance > 0 ? 'uncomplete' : 'complete';
            $table['product'] = $product;
            $table['tax'] = $penjualanById->tax;
            $table['discount'] = $penjualanById->discount;
            $table['total'] = $penjualanById->total;
            $table['pay_off'] = $penjualanById->piutang->isNotEmpty() ? $penjualanById->piutang->first()->paid_off : $penjualanById->total;
            // Type Transaksi Agro
            $table['trans_type'] = $penjualanById->trans_type;
            $output->push($table);
        }
        $faktur1 = $request->faktur1;
        $faktur2 = $request->faktur2;
        $faktur3 = $request->faktur3;
        session()->put('detail',$output);
        session()->put('efaktur2',$faktur1);
        session()->put('efaktur3',$faktur2);
        session()->put('efaktur4',$faktur3);

        ob_end_clean(); // this
        ob_start(); // and this
        return Excel::download(new EFakturExport, 'documents-efaktur.csv');
    }
}
