<?php

namespace App\Http\Controllers\Api\Report;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Repositories\Penjualan\Penjualan as PenjualanRepository;
use App\Repositories\Penjualan\Piutang as PiutangRepository;

class PiutangController extends ApiController
{
    protected $penjualan;
    protected $piutang;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->penjualan = new PenjualanRepository;
        $this->piutang = new PiutangRepository;
    }
    
    public function piutang(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'customer' => 'nullable|integer|exists:customers,customer_id',
            'status' => 'nullable|string|in:all,paid,unpaid',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $search = [];
        if ($request->customer) {
            $search = array_merge($search, ['customer_id' => $request->customer]);
        }
        if ($request->status) {
            switch ($request->status) {
                case 'paid':
                    $search = array_merge($search, [['balance', '=', '0']]);
                break;
                case 'unpaid':
                    $search = array_merge($search, [['balance', '<>', '0']]);
                break;
                default:
                    $search = array_merge($search, [['balance']]);
                break;
            }
        }
        $output = collect();
        $penjualan = $this->penjualan->all(['filter' => $filter, 'search' => collect($search)->only('customer_id')->toArray()]);
        $penjualan = $penjualan->whereNull('po_pro_id')->where('faktur', 'approved')->flatten();
        $piutang = $this->piutang->all($request->merge([
            'filter' => 'penjualan_id|'.$penjualan->pluck('penjualan_id')->implode(':'),
            'search' => collect($search)->except('customer_id')->toArray(),
        ])->all());
        foreach ($piutang as $ptg) {
            $table['id'] = $ptg->piutang_id;
            $table['date'] = $ptg->created_at->format('d/m/Y');
            $table['faktur'] = $ptg->penjualan->code;
            $table['expired'] = $ptg->penjualan->due_date->format('d/m/Y');
            $table['customer']['id'] = $ptg->penjualan->customer->customer_id;
            $table['customer']['name'] = $ptg->penjualan->customer->name;
            $table['total'] = $ptg->total;
            $table['paid_off'] = $ptg->paid_off;
            $table['balance'] = $ptg->balance;
            // Type Transaksi Agro
            $table['trans_type'] = $ptg->penjualan->trans_type;
            $output->push($table);
        }
        return $this->success(['total' => [
            'piutang' => $penjualan->where('status', 'unpaid')->sum('total'),
            'piutang_terbayar' => $output->sum('paid_off'),
            'sisa_piutang' => $penjualan->where('status', 'unpaid')->sum('total') - $output->sum('paid_off'),
        ]], 200, $output);
    }
    
    public function piutangTerbayar(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'date_from' => 'nullable|date_format:"d/m/Y"',
            'date_to' => 'nullable|date_format:"d/m/Y"',
            'customer' => 'nullable|integer|exists:customers,customer_id',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $search = [];
        if ($request->customer) {
            $search = array_merge($search, ['customer_id' => $request->customer]);
        }
        $output = collect();
        $penjualan = $this->penjualan->all(['filter' => $filter, 'search' => collect($search)->only('customer_id')->toArray()]);
        $penjualan = $penjualan->whereNull('po_pro_id')->where('faktur', 'approved')->flatten();
        $piutang = $this->piutang->all($request->merge([
            'filter' => 'penjualan_id|'.$penjualan->pluck('penjualan_id')->implode(':'),
        ])->all())->where('paid_off', '>', '0')->flatten();
        foreach ($piutang as $ptg) {
            $table['id'] = $ptg->piutang_id;
            $table['date'] = $ptg->created_at->format('d/m/Y');
            $table['faktur'] = $ptg->penjualan->code;
            $table['expired'] = $ptg->penjualan->due_date->format('d/m/Y');
            $table['customer']['id'] = $ptg->penjualan->customer->customer_id;
            $table['customer']['name'] = $ptg->penjualan->customer->name;
            $table['total'] = $ptg->total;
            $table['paid_off'] = $ptg->paid_off;
            $table['balance'] = $ptg->balance;
            // Type Transaksi Agro
            $table['trans_type'] = $ptg->penjualan->trans_type;
            $output->push($table);
        }
        return $this->success(['total' => [
            'piutang' => $penjualan->where('status', 'unpaid')->sum('total'),
            'piutang_terbayar' => $output->sum('paid_off'),
            'sisa_piutang' => $penjualan->where('status', 'unpaid')->sum('total') - $output->sum('paid_off'),
        ]], 200, $output);
    }
}
