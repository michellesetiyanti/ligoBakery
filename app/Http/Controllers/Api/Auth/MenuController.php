<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Auth\Menu as MenuRequest;
use App\Repositories\Auth\Menu as MenuRepository;

class MenuController extends ApiController
{
    protected $menu;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth', ['except' => ['store', 'destroy']]);
        $this->menu = new MenuRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $menu = $this->menu->with('submenu')->all($request->all());
        return $this->success(null, 200, $menu);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($menu = $this->menu->with('submenu')->show($id)) {
            return $this->success(null, 200, $menu);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  MenuRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuRequest $request, $id)
    {
        $menu = $this->menu->with(['submenu'])->show($id);
        \DB::beginTransaction();
        try{
            $menu->fill($request->merge([
                'id' => $id,
            ])->all());
            $menu->save();
            if ($request->submenu) {
                foreach ($request->submenu as $submenu) {
                    $menu->parent()->fill(array_merge($submenu, ['parent_id' => $menu->menu_id]))->save();
                }
            }
            \DB::commit();
            return $this->success(null, 200, $menu->where('menu_id', $menu->menu_id)->with(['submenu'])->get());
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
