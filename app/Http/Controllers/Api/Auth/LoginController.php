<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Repositories\Auth\User as UserRepository;
use Illuminate\Http\Request;
use App\Models\Master\Company as COMP;
use App\Models\Master\MenuJual;
use App\Models\Master\JenisPembayaran;

class LoginController extends ApiController
{
    protected $user;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth', ['except' => ['login', 'listcompany', 'getCompany']]);
        $this->user = new UserRepository;
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'identity' => 'required|string',
            'password' => 'required|string',
            'company_id' => 'sometimes|required|integer|exists:companies,company_id',
        ]);
        if ($validator->fails()) {
            return $this->fail('There was an error with validation.', 422, $validator->errors());
        }
        $identity = filter_var($request->identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        $credentials = [
            $identity => $request->identity,
            'password' => $request->password,
            'status' => 'active',
        ];
        // Check User
        $checkIdentity = $this->user->getModel()->with(['company', 'karyawan','role' ])->where($identity, $credentials[$identity])->where('status', $credentials['status'])->get();
        if ($checkIdentity->count() > 1 && $request->missing('company_id')) {
            return $this->fail(trans('validation.required', ['attribute' => 'company_id']), 422);
        }
        if ($request->exists('company_id')) {
            $credentials = array_merge($credentials, ['company_id' => $request->company_id]);
        }
        try {
            // Check Login
            if (!$token = \EOGAuth::attempt($credentials)) {
                return $this->unauthorized(null);
            }
            // Get user authenticated
            $user = $checkIdentity->find(auth()->id());
            $menu = MenuJual::all();
            $jenispembayaran = JenisPembayaran::with(['categoryPembayaran','coa'])->get();
            $token = auth()->tokenById($user->user_id);
            return $this->success(null, 200, compact('token', 'user','menu','jenispembayaran'))->withCookie(\Cookie::make("Bearer", $token));
        } catch (\EOGException $e) {
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    public function listcompany(Request $request)
    { 
        $company = COMP::all();
        return $this->success(null, 200, $company);
    }
    
    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        try {
            $token = \Cookie::forget('Bearer');
            \EOGAuth::parseToken()->invalidate($token);
            return $this->success('Logout Success', 200,null)->withCookie($token);
        } catch (\EOGException $e) {
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    
    public function getCompany(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required|string|email',
        ]);
        if ($validator->fails()) {
            return $this->fail('validation', 422, $validator->errors());
        }
        $model = $this->user->getModel();
        $user = $model->where('email', $request->email)->where('status', 'active')->get();
        $companies = $user->map->company->flatten();
        return $this->success('company_id', 200, $companies->map->only(['company_id', 'name'])->toArray());
    }
}
