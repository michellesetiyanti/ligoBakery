<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Auth\User as UserRequest;
use App\Repositories\Auth\User as UserRepository;
use App\Repositories\Auth\Role as RoleRepository;
use App\Repositories\Master\Karyawan as KaryawanRepository;

class UserController extends ApiController
{
    protected $user;
    protected $karyawan;

    /**
     * Instance constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->user = new UserRepository;
        $this->karyawan = new KaryawanRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $roles = (new RoleRepository)->getModel()->all()->pluck('code')->toArray();
        switch(auth()->user()->type) {
            case "owner":
                $diff = array_diff($roles, ['owner']);
                $filter = 'type|'.implode(':', $diff);
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
            case "superadmin":
                $diff = array_diff($roles, ['owner', 'superadmin']);
                $filter = 'type|'.implode(':', $diff);
                $filter = $filter.';company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
            default:
                $filter = 'type|'.auth()->user()->type;
                $filter = $filter.';company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $user = $this->user->with(['company', 'karyawan', 'role'])->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $username = $request->username;
        if (empty($username)) {
            $emply = $this->karyawan->show($request->karyawan_id);
            $born = \Carbon\Carbon::parse($emply->born);
            $username = \Str::limit(\Str::slug($emply->name, ''), 4, $born->format('d').$born->format('m'));
        }
        \DB::beginTransaction();
        try {
            $user = $this->user->save($request->merge(compact('username'))->all());
            \DB::commit();
            return $this->success(null, 200, $user->with(['company', 'karyawan', 'role'])->where('user_id', $user->user_id)->first());
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($user = $this->user->with(['company', 'karyawan', 'role'])->show($id)) {
            return $this->success(null, 200, $user);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = $this->user->show($id);
        \DB::beginTransaction();
        try {
            $user->update($request->merge([
                'password' => $request->password
            ])->except([$request->password ? '' : 'password', 'username']));
            \DB::commit();
            return $this->success(null, 200, $user);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->user->show($id);
        \DB::beginTransaction();
        try {
            if ($user->customers->isEmpty() || $user->suppliers->isEmpty() || $user->vendors->isEmpty() || $user->tukang->isEmpty() || $user->products->isEmpty()) {
                $user->delete();
            } else {
                $user->update(['status' => 'inactive']);
            }
            \DB::commit();
            return $this->success(null);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
