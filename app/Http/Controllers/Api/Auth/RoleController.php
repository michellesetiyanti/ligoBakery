<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Auth\Role as RoleRequest;
use App\Repositories\Auth\Role as RoleRepository;
use App\Repositories\Auth\Menu as MenuRepository;
use App\Models\Auth\Menu as MENU;
use App\Models\Auth\MenuDetail as MENUDETAIL;
use App\Models\Auth\RoleMenu as ROLEMENU;

class RoleController extends ApiController
{
    protected $role;
    protected $menu;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->role = new RoleRepository;
        $this->menu = new MenuRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $role = $this->role->with('roleMenus')->all($request->all());
        return $this->success(null, 200, $role);
    }

    public function indexmenu(\Illuminate\Http\Request $request)
    {
        $role = MENUDETAIL::with(['parent'])->orderBy('menu_id','ASC')->get();
        return $this->success(null, 200, $role);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\Illuminate\Http\Request $request)
    {
        \DB::beginTransaction();
        try{
            $role_id = $request->role_id;    
            $list_menu = $request->list_menu; 

            $role=new ROLEMENU;   
            $role->role_id = $role_id;
            $role->access = $list_menu;  
            $role->save();  

            \DB::commit();
            return $this->success(null, 200, $role);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $head = MENU::all();
        // return $head;
        $datarole = ROLEMENU::where('role_id', $id)->first();
        $access = $datarole->access;
        // return $access;
        
        $list = collect();

        $home['url'] = '/home';
        $home['name'] = 'Home';
        $home['slug'] = 'home';
        $home['icon'] = 'HomeIcon';
        $home['showw'] = true;
        $list->push($home); 

        foreach ($head as $data) {
            $menu_id = $data->menu_id;
            $name = $data->name;
            $icon = $data->icon;
            $showw = $data->showw;
            
            $table['name'] = $name;
            $table['icon'] = $icon;
            $table['showw'] = false;
            $table['submenu'] = collect();

            foreach($access as $listrole) {
                $d_id = $listrole->menu_d_id;
                $idhead = $listrole->menu_id;
                $d_url = $listrole->url;
                $d_slug = $listrole->slug;
                $d_name = $listrole->name;
                $d_readd = $listrole->readd;
                $d_createe = $listrole->createe;
                $d_updatee = $listrole->updatee;
                $d_deletee = $listrole->deletee;

                if($idhead == $menu_id){
                    $submenu['url'] = $d_url;
                    $submenu['name'] = $d_name;
                    $submenu['slug'] = $d_slug;
                    $submenu['readd'] = $d_readd;
                    $submenu['createe'] = $d_createe;
                    $submenu['updatee'] = $d_updatee;
                    $submenu['deletee'] = $d_deletee;
                    
                    if($d_readd == true){
                        $table['showw'] = true;
                    }
                     
                    $table['submenu']->push($submenu); 
                } 
            } 
            $list->push($table);    
        } 
        return $this->success(null, 200, $list); 
         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RoleRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\Illuminate\Http\Request $request, $id)
    {
        \DB::beginTransaction();
        try{ 
            $list_menu = $request->list_menu;

            $ar = array(
                'access'=> $list_menu
            );
            $role = ROLEMENU::where('role_m_id',$id)->update($ar); 
            
            \DB::commit();
            return $this->success("Update Success", 200,$list_menu);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = $this->role->with(['roleMenus', 'users'])->show($id);
        \DB::beginTransaction();
        try {
            if ($role->users->isEmpty()) {
                $role->delete();
            } else {
                $role->update(['status' => 'inactive']);
            }
            \DB::commit();
            return $this->success(null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
