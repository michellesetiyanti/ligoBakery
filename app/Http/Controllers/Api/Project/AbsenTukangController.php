<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\AbsenTukang as AbsenTukangRequest;
use App\Repositories\Finance\UpahTukang as UpahTukangRepository;
use App\Repositories\Master\Tukang as TukangRepository;
use App\Repositories\Project\Order as OrderRepository;
use App\Repositories\Project\UpahHarian as UpahHarianRepository;
use App\Models\Finance\UpahTukang as UPAHTUKANG;
use App\Models\Finance\upahTukangDetail as UPAHTUKANGDETAIL;

class AbsenTukangController extends ApiController
{
    protected $order;
    protected $tukang;
    protected $upahHarian;
    protected $upahTukang;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->order = new OrderRepository;
        $this->tukang = new TukangRepository;
        $this->upahHarian = new UpahHarianRepository;
        $this->upahTukang = new UpahTukangRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $upahTukang = $this->upahTukang->with('upahTukangDetails')->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $upahTukang);
    }
    
    public function indexapprove(\Illuminate\Http\Request $request)
    {
         
        $upahtukang = UPAHTUKANG::where('approved','!=',null)->with('upahTukangDetails')->get();
        return $this->success(null, 200, $upahtukang);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AbsenTukangRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AbsenTukangRequest $request)
    {
        $datefrom = \Carbon\Carbon::createFromFormat('d/m/Y', $request->date_from);
        $dateto = \Carbon\Carbon::createFromFormat('d/m/Y', $request->date_to);
        $periode = $datefrom->format('d/m/Y').' - '.$dateto->format('d/m/Y');
        if ($datefrom->dayOfWeek != \Carbon\Carbon::FRIDAY || $dateto->dayOfWeek != \Carbon\Carbon::THURSDAY) {
            return $this->fail('Incorrect select day of week', 422, [
                'data_from' => $datefrom->dayName,
                'data_to' => $dateto->dayName
            ]);
        }
        if ($datefrom->diffInDays($dateto->add(1, 'day')) > 7) {
            return $this->fail('Max 7 day', 422);
        }
        $model = $this->upahTukang->getModel();
        $modelDay = $this->upahHarian->getModel();
        \DB::beginTransaction();
        try {
            $upahTkg = $model->where('work_order', $request->work_order)->where('upah_tkg_id', $request->upah_tkg_id)->first();
            $upahTkg->update($request->merge([
                'periode' => $periode,
                'nominal' => $upahTkg->upahTukangDetails->sum('amount'),
                'balance' => $upahTkg->upahTukangDetails->sum('amount'),
                // 'approved' => auth()->user()->isSuperAdmin() ? json_encode(authExecute()) : null,
            ])->except('work_order', 'absent'));
            if ($upahTkg->upahTukangDetails->isNotEmpty()) {
                foreach ($upahTkg->upahTukangDetails as $upahTkgDt) {
                    $description = "$upahTkg->location [$upahTkg->periode] $upahTkg->description";
                    $modelDay->create([
                        'company_id' => $upahTkg->company_id,
                        'code' => $upahTkg->work_order,
                        'name' => $upahTkgDt->tukang->name,
                        'description' => $description,
                        'upah' => $upahTkgDt->amount,
                        // 'approved' => auth()->user()->isSuperAdmin() ? json_encode(authExecute()) : null,
                    ]);
                }
            }
            if (auth()->user()->isSuperAdmin()) {
                $doAction = $this->doAction(new \Illuminate\Http\Request(['execute' => 'approved']), $upahTkg->upah_tkg_id);
                $doAction = json_decode($doAction->content(), true);
                if ($doAction['status'] == 'failed') {
                    return $this->fail($doAction['message'], 500, $doAction['errors']);
                }
            }
            \DB::commit();
            return $this->success(null, 200, $upahTkg);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($upahTukang = $this->upahTukang->with('upahTukangDetails')->show($id)) {
            return $this->success(null, 200, $upahTukang);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AbsenTukangRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AbsenTukangRequest $request, $id)
    {
        $datefrom = \Carbon\Carbon::createFromFormat('d/m/Y', $request->date_from);
        $dateto = \Carbon\Carbon::createFromFormat('d/m/Y', $request->date_to);
        $periode = $datefrom->format('d/m/Y').' - '.$dateto->format('d/m/Y');
        if ($datefrom->dayOfWeek != \Carbon\Carbon::FRIDAY || $dateto->dayOfWeek != \Carbon\Carbon::THURSDAY) {
            return $this->fail('Incorrect select day of week', 422, [
                'data_from' => $datefrom->dayName,
                'data_to' => $dateto->dayName
            ]);
        }
        if ($datefrom->diffInDays($dateto->add(1, 'day')) > 7) {
            return $this->fail('Max 7 day', 422);
        }
        $upahTkg = $this->upahTukang->show($id);
        $modelDay = $this->upahHarian->getModel();
        \DB::beginTransaction();
        try {
            $upahTkg->update($request->merge([
                'periode' => $periode,
                'nominal' => $upahTkg->upahTukangDetails->sum('amount'),
                'balance' => $upahTkg->upahTukangDetails->sum('amount'),
            ])->except('work_order', 'absent'));
            if ($upahTkg->upahTukangDetails->isNotEmpty()) {
                foreach ($upahTkg->upahTukangDetails as $upahTkgDt) {
                    $description = "$upahTkg->location [$upahTkg->periode] $upahTkg->description";
                    $upahTkgDay = $modelDay->where('name', $upahTkgDt->tukang->name)->where('description', $description)->first();
                    if (empty($upahTkgDay)) {
                        $modelDay->create([
                            'company_id' => $upahTkg->company_id,
                            'code' => $upahTkg->work_order,
                            'name' => $upahTkgDt->tukang->name,
                            'description' => $description,
                            'upah' => $upahTkgDt->amount,
                            // 'approved' => auth()->user()->isSuperAdmin() ? json_encode(authExecute()) : null,
                        ]);
                    } else {
                        $upahTkgDay->update([
                            'name' => $upahTkgDt->tukang->name,
                            'description' => $description,
                            'upah' => $upahTkgDt->amount,
                            // 'approved' => auth()->user()->isSuperAdmin() ? json_encode(authExecute()) : null,
                        ]);
                    }
                }
            }
            if (auth()->user()->isSuperAdmin()) {
                $doAction = $this->doAction(new \Illuminate\Http\Request(['execute' => 'approved']), $upahTkg->upah_tkg_id);
                $doAction = json_decode($doAction->content(), true);
                if ($doAction['status'] == 'failed') {
                    return $this->fail($doAction['message'], 422, $doAction['errors']);
                }
            }
            \DB::commit();
            return $this->success(null, 200, $upahTkg);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $modelDay = $this->upahHarian->getModel();
        $upahTkg = $this->upahTukang->show($id);
        \DB::beginTransaction();
        try {
            $description = $upahTkg->location.' ['.$upahTkg->periode.'] '.$upahTkg->description;
            if (empty($upahTkg->approved)) {
                $modelDay->where('code', $upahTkg->work_order)->where('description', $description)->delete();
                $upahTkg->delete();
            }
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Do Action the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function doAction(\Illuminate\Http\Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'execute' => 'required|string|in:approved',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $upahTkg = $this->upahTukang->show($id);
        $description = $upahTkg->location.' ['.$upahTkg->periode.'] '.$upahTkg->description;
        
        $modelDay = $this->upahHarian->getModel();
        $upahDay = $modelDay->where('code', $upahTkg->work_order)->where('description', $description)->get();

        \DB::beginTransaction();
        try {
            switch ($request->execute) {
                case 'approved':
                    $upahTkg->update([
                        'nominal' => $upahTkg->upahTukangDetails->sum('amount'),
                        'balance' => $upahTkg->upahTukangDetails->sum('amount'),
                        'approved' => json_encode(authExecute())
                    ]);
                    foreach ($upahTkg->upahTukangDetails as $upahTkgDt) {
                        $upahDay->where('name', $upahTkgDt->tukang->name)->first()->update([
                            'upah' => $upahTkgDt->amount,
                            'approved' => json_encode(authExecute())
                        ]);
                    }
                    $response = authExecute();
                break;
                default:
                    $response = null;
                break;
            }

            // if ($response) {
            //     $total = UPAHTUKANGDETAIL::where('upah_tkg_id', $id)->sum("amount");
            //     $update = array(
            //         'nominal' => $total,
            //         'balance' => $total
            //     );
            //     UPAHTUKANG::where('upah_tkg_id', $id)->update($update);
            // }
            
            \DB::commit();
            return $this->success(null, 200, $response);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
