<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use Illuminate\Http\Request;
use App\Models\Master\Workshop as WORKSHOP; 
use App\Models\Master\WorkshopDetail as WORKSHOPDETAIL; 
use App\Models\Master\Warehouse as WAREHOUSE; 
use App\Models\Master\Product as PRODUCT; 
use App\Models\Project\Order as ORDER;


class WorkshopController extends ApiController
{
    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth'); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_id = auth()->user()->company_id;
        $data = WORKSHOP::where('company_id',$company_id)->with(['order'])->get();
        return $this->success(null, 200, $data);
    }

    public function indexpending()
    {
        $company_id = auth()->user()->company_id;
        $data = WORKSHOP::where('company_id',$company_id)->with(['order'])->where('status','pending')->get();
        return $this->success(null, 200, $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $company_id = auth()->user()->company_id;

        $count = WORKSHOP::where('company_id',$company_id)->count();
        $count = $count + 1;
        $workshop_code = "WSHP-$count";

        $list = ORDER::where('company_id',$company_id)->where('to_workshop','=',"true")->get();
        $wh = WAREHOUSE::where('company_id',$company_id)->first();
        $wh_id = $wh->wh_id;

        $products = PRODUCT::where('wh_id',$wh_id)->get();

        $array = array(
            'status' => 'success',
            'message' => 'Data loaded successfully',
            'code' =>  $workshop_code,
            'list' => $list,
            'products' => $products,
        );
        return $array;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $user_id = auth()->user()->user_id;
        $company_id = auth()->user()->company_id;
        $order_id = $request->order;
        $product_id = $request->product;
        $qty_lg = $request->qty_lg;
        $qty_sm = $request->qty_sm;

        $count = WORKSHOP::where('company_id',$company_id)->count();
        $count = $count + 1;
        $workshop_code = "WSHP-$count";

        $wh = WAREHOUSE::where('company_id',$company_id)->first();
        $wh_id = $wh->wh_id;


        \DB::beginTransaction();
        try {
            $data = PRODUCT::where('product_id',$product_id)->where('wh_id',$wh_id)->first();
            $unit_lg = $data->unit_lg;
            $unit_sm = $data->unit_sm; 
            $price_lg = $data->price_lg;
            $price_sm = $data->price_sm; 
            $unit_qty = $data->unit_qty; 

            // $qty_smm = $qty_sm % $unit_qty;
            $qty_smm = $qty_sm;
            // $qty_lg = (($qty_sm - $qty_smm )/$unit_qty) + $qty_lg ; 
            $qty_lg = 0;
            $amount = ($qty_lg * $price_lg) + ($qty_smm * $price_sm);

            $c = new WORKSHOP;
            $c->user_id = $user_id;
            $c->company_id = $company_id;
            $c->order_id = $order_id;
            $c->workshop_code = $workshop_code;
            $c->total = $amount;
            $c->save();

            $workshop_id = $c->workshop_id;

            $create = new WORKSHOPDETAIL;
            $create->user_id = $user_id;
            $create->wh_id = $wh_id;
            $create->workshop_id = $workshop_id;
            $create->product_id = $product_id;
            $create->unit_lg = $unit_lg;
            $create->unit_sm = $unit_sm;
            $create->qty_lg = $qty_lg;
            $create->qty_sm = $qty_smm;
            $create->price_lg = $price_lg;
            $create->price_sm = $price_sm;
            $create->amount = $amount;
            $create->save(); 

            $ret = WORKSHOP::where('workshop_id',$workshop_id)->with(['order','workshopDetails'])->first();
            
            \DB::commit();
            return $this->success(null, 200, $ret);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company_id = auth()->user()->company_id;

        $data = WORKSHOP::where('workshop_id',$id)->with(['order','workshopDetails'])->first();
        $workshop_code = $data->workshop_code;

        $list = ORDER::where('company_id',$company_id)->where('to_workshop','=',"true")->get();
        $wh = WAREHOUSE::where('company_id',$company_id)->first();
        $wh_id = $wh->wh_id;

        $products = PRODUCT::where('wh_id',$wh_id)->get();

        $data = WORKSHOP::where('workshop_id',$id)->with(['order','workshopDetails'])->first(); 

        $array = array(
            'status' => 'success',
            'message' => 'Data loaded successfully',
            'data' =>  $data,
            'list' => $list,
            'products' => $products,
        );
        return $array;
    }

    public function submit($id)
    {
        \DB::beginTransaction();
        try {
            
            $array = array(
                'status'=>'pending'
            );
            WORKSHOP::where('workshop_id',$id)->update($array);
            
            \DB::commit();
            return $this->success("Submit Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function confirm($id)
    {
        \DB::beginTransaction();
        try {
            
            $array = array(
                'status'=>'complete'
            );
            WORKSHOP::where('workshop_id',$id)->update($array);

            $data = WORKSHOP::where('workshop_id',$id)->first();
            $order_id = $data->order_id;
            $total = $data->total;

            $order = ORDER::where('order_id',$order_id)->first();
            $limit_cost = $order->limit_cost;

            if($limit_cost < $total){
               return $this->fail("Post Failed", 500, "Limit Cost Project Kurang");
            }else{
                $limit_cost = $limit_cost - $total ;

                $update = array(
                    'limit_cost'=>$limit_cost,
                );
                ORDER::where('order_id',$order_id)->update($update);
            } 
            \DB::commit();
            return $this->success("Confirm Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function reject($id)
    {
        \DB::beginTransaction();
        try {
            
            $array = array(
                'status'=>'rejected'
            );
            WORKSHOP::where('workshop_id',$id)->update($array);
            
            \DB::commit();
            return $this->success("Reject Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            WORKSHOPDETAIL::where('workshop_id',$id)->delete(); 
            WORKSHOP::where('workshop_id',$id)->delete(); 
           
            
            \DB::commit();
            return $this->success("Delete Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
