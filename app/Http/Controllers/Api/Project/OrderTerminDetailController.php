<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\OrderTerminDetail as OrderTerminDetailRequest;
use App\Repositories\Project\Order as OrderRepository;
use App\Repositories\Project\OrderTerminDetail as OrderTerminDetailRepository;
use App\Models\Project\Order as ORDER;
use App\Models\Project\OrderTermin as TERMIN;
use App\Models\Project\OrderTerminDetail as TERMINDETAIL;

class OrderTerminDetailController extends ApiController
{
    protected $order;
    protected $terminDetail;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->order = new OrderRepository;
        $this->terminDetail = new OrderTerminDetailRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $terminDetail = $this->terminDetail->with(['order', 'orderTermin'])->all($request->all());
        return $this->success(null, 200, $terminDetail);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  OrderTerminDetailRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\Illuminate\Http\Request $request)
    {
        \DB::beginTransaction();
        try { 
            $termin_id = $request->termin_id;        
            $order_id = $request->order_id;        
            $description = $request->description;        
            $qty = $request->qty;        
            $price = $request->price; 

            $total = $qty * $price;

            $detail = new TERMINDETAIL;
            $detail->order_id=$order_id;
            $detail->order_t_id=$termin_id;
            $detail->description=$description;
            $detail->qty=$qty;
            $detail->price=$price;
            $detail->total=$total;
            $detail->save();

            $grandtotal = TERMINDETAIL::where('order_t_id',$termin_id)->sum('total');

            $cek = TERMIN::where('order_t_id',$termin_id)->first();
            $amount = $cek->amount;
            $termin_total = $amount + $grandtotal;

            $ar = array(
                'termin_total' =>$termin_total,
                'amount_detail' =>$grandtotal,
                'balance' =>$termin_total,
            );
            TERMIN::where('order_t_id',$termin_id)->update($ar);

            $data = TERMINDETAIL::where('order_t_id',$termin_id)->get();

            $array = array(
            'status'=>'success',
            'message'=>'Success Delete',
            'data'=>$data,
            'total'=>$grandtotal,
            );


            \DB::commit();
            return $array;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($terminDetail = $this->terminDetail->with(['order', 'orderTermin'])->show($id)) {
            return $this->success(null, 200, $terminDetail);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  OrderTerminDetailRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\Illuminate\Http\Request $request, $id)
    {
       
        \DB::beginTransaction();
        try {
            $description = $request->description;
            $qty = $request->qty;
            $price = $request->price;
            $total = $qty * $price; 

            $cek = TERMINDETAIL::where('order_t_d_id',$id)->first();
            $termin_id = $cek->order_t_id;
            

            $ar = array(
                'description' =>$description,
                'qty' =>$qty,
                'price' =>$price,
                'total' =>$total,
            );
            TERMINDETAIL::where('order_t_d_id',$id)->update($ar);

            $grandtotal = TERMINDETAIL::where('order_t_id',$termin_id)->sum('total');
            $cek = TERMIN::where('order_t_id',$termin_id)->first();
            $amount = $cek->amount;
            $termin_total = $amount + $grandtotal;

            $ar = array(
                'termin_total' =>$termin_total,
                'amount_detail' =>$grandtotal,
                'balance' =>$termin_total,
            );
            TERMIN::where('order_t_id',$termin_id)->update($ar);

            $data = TERMINDETAIL::where('order_t_id',$termin_id)->get(); 

            $array = array(
            'status'=>'success',
            'message'=>'Success Update',
            'data'=>$data,
            'total'=>$grandtotal,
            );

            \DB::commit();
            return $array;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {

            $data = TERMINDETAIL::where('order_t_d_id',$id)->first();
            $termin_id = $data->order_t_id;

            TERMINDETAIL::where('order_t_d_id',$id)->delete();

            $grandtotal = TERMINDETAIL::where('order_t_id',$termin_id)->sum('total');

            $cek = TERMIN::where('order_t_id',$termin_id)->first();
            $amount = $cek->amount;
            $termin_total = $amount + $grandtotal;

            $ar = array(
                'termin_total' =>$termin_total,
                'amount_detail' =>$grandtotal,
                'balance' =>$termin_total,
            );
            TERMIN::where('order_t_id',$termin_id)->update($ar);

            $data = TERMINDETAIL::where('order_t_id',$termin_id)->get();  

            $array = array(
            'status'=>'success',
            'message'=>'Success Delete',
            'data'=>$data,
            'total'=>$grandtotal,
            );

            \DB::commit();
            return $array;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
