<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\Order as OrderRequest;
use App\Repositories\Finance\Invoice as InvoiceRepository;
use App\Repositories\Master\Customer as CustomerRepository;
use App\Repositories\Project\Order as OrderRepository;
use App\Models\Project\Order as ORDER;
use App\Models\Master\Company as COMPANY;
use App\Models\Project\OrderTermin as TERMIN;
use File;
use Illuminate\Support\Facades\Storage;

class OrderController extends ApiController
{
    protected $customer;
    protected $invoice;
    protected $order;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->customer = new CustomerRepository;
        $this->invoice = new InvoiceRepository;
        $this->order = new OrderRepository;
    }

    /**
     * Display a listing of the resource.
     *`
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $order = $this->order->with(['orderTermins', 'estimasi', 'customer', 'invoices'])->all($request->all());
        
        return $this->success(null, 200, $order);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  OrderRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(\Illuminate\Http\Request $request)
    {
        \DB::beginTransaction();
        try {
            $company_id = auth()->user()->company_id;
            $datacompany = COMPANY::where('company_id',$company_id)->first();
            $comp_code = $datacompany->code_numb;

            $spkCode = $request->spkCode;
            $customer = $request->customer;
            $offer_id = $request->penawaran;
            $workshop = $request->workshop;
            $workorder = $request->workorder;
            $date = $request->date;
            $address = $request->address;
            $durasi = $request->durasi;
            $totalProject = $request->totalProject;
            $persenProfit = $request->persenProfit;
            $amountProfit = $request->amountProfit;
            $persenCost = $request->persenCost;
            $amountCost = $request->amountCost;
            $disc = $request->disc;
            $tax = $request->tax;
            $termin = $request->termin;
            $amountTermin = $request->amountTermin; 

              
            $spkCode= $this->codeSpk(new \Illuminate\Http\Request(['comp_code'=>$comp_code,'code_wo'=>$workorder ,'trans_type'=>'piutang']),1);

            if($amountTermin > $totalProject){
                return $this->fail("Post Failed", 500, "Total Biaya Yang Di Tagih Melebihi Total Nilai Project");  
            }

            $ord=new ORDER;   
            $ord->company_id = $company_id;
            $ord->code_spk = $spkCode;
            $ord->customer_id = $customer;
            $ord->to_workshop = $workshop;
            $ord->offer_id = $offer_id;
            $ord->work_order = $workorder;
            $ord->entry_at = $date;
            $ord->address = $address;
            $ord->time_work = $durasi;
            $ord->total_project = $totalProject;
            $ord->profit = $persenProfit;
            $ord->amount_profit = $amountProfit;
            $ord->cost = $persenCost;
            $ord->cost_project = $amountCost;
            $ord->limit_cost = $amountCost;
            $ord->discount = 0;
            $ord->tax = $tax; 
            $ord->realisasi_total = $totalProject; 
            $ord->realisasi_profit = $amountProfit; 
            $ord->realisasi_cost = $amountCost; 
            $ord->status = 'open'; 
            $ord->save();  

            $order_id = $ord->order_id;

            $term=new TERMIN;   
            $term->order_id = $order_id; 
            $term->name = "Termin-1";
            $term->amount = $amountTermin; 
            $term->termin_total = $amountTermin; 
            $term->balance = $amountTermin; 
            $term->save();  

            $term = TERMIN::where('order_id',$order_id)->get();

            $data = array(
                'order'=>$ord,
                'termin'=>$term,
                'total_termin'=>$amountTermin,
            );
           
            \DB::commit();
            return $this->success("success post", 200, $data);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        if ($order = $this->order->with(['orderTermins', 'estimasi', 'customer', 'invoices'])->show($id)) {
            $order_id = $order->order_id;
            $cektotal = TERMIN::where("order_id",$id)->sum('amount');
            $path = "public/$order_id/adendum"; 
            $pathrab = "public/$order_id/rab";  

            $cekfile = Storage::files($path);
            $ct = count($cekfile); 
            $allfile=[];
            $allfilerab=[];
            for($i=0; $i < $ct ; $i++) {
                $name = $cekfile[$i];
                $total = strlen($name);
                $dt= substr($name , 7, $total);
                $allfile[$i]=$dt;  
            }

            $cekfilerab = Storage::files($pathrab); 
            $ctrab = count($cekfilerab); 
            for($i=0; $i < $ctrab ; $i++) {
                $name = $cekfilerab[$i];
                $total = strlen($name);
                $dt= substr($name , 7, $total);
                $allfilerab[$i]=$dt;  
            }

            $data = array(
                'status'=>"success",
                'message'=>"Data loaded successfully",
                'data'=>$order,
                'file'=>$allfile,
                'rab'=>$allfilerab,
                'total_termin'=>$cektotal,
            );
            return $data;
        } else {
            return $this->notFound(null);
        }
    }

    public function submit($id)
    {
        \DB::beginTransaction();
        try {  

            $cekadendum =  TERMIN::where("order_id",$id)->where('balance','!=',0)->count();
            if($cekadendum < 3){
                $adendum = "true";
            }else{
                $adendum = "false"; 
            }  
           
            $ar = array(
                'status'=>'pending',
                'adendum'=>$adendum
            );
            ORDER::where('order_id',$id)->update($ar);

            \DB::commit();
            return $this->success("Update Success", 200,null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }    
    }

    public function confirm($id)
    {
        \DB::beginTransaction();
        try {  

           
            $ar = array(
                'status'=>'progress'
            );
            ORDER::where('order_id',$id)->update($ar);

            \DB::commit();
            return $this->success("Update Success", 200,null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }    
    }

    public function reject($id)
    {
        \DB::beginTransaction();
        try {  

           
            $ar = array(
                'status'=>'rejected'
            );
            ORDER::where('order_id',$id)->update($ar);

            \DB::commit();
            return $this->success("Update Success", 200,null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  OrderRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\Illuminate\Http\Request $request, $id)
    {

        \DB::beginTransaction();
        try {
            $company_id = auth()->user()->company_id;
            $order_id = $id;
            $spkCode = $request->spkCode;
            $customer = $request->customer;
            $workshop = $request->workshop;
            $workorder = $request->workorder;
            $date = $request->date;
            $address = $request->address;
            $durasi = $request->durasi;
            $totalProject = $request->totalProject;
            $persenProfit = $request->persenProfit;
            $amountProfit = $request->amountProfit;
            $persenCost = $request->persenCost;
            $amountCost = $request->amountCost;
            $disc = $request->disc;
            $tax = $request->tax; 
 
            $data = array(
                'company_id'=>$company_id,
                'code_spk'=>$spkCode,
                'customer_id'=>$customer,
                'to_workshop'=>$workshop=="true" ? true:'false',
                'work_order'=>$workorder,
                'entry_at'=>$date,
                'address'=>$address,
                'time_work'=>$durasi,
                'total_project'=>$totalProject,
                'profit'=>$persenProfit,
                'amount_profit'=>$amountProfit,
                'cost'=>$persenCost,
                'cost_project'=>$amountCost,
                'limit_cost'=>$amountCost,
                'discount'=>$disc,
                'tax'=>$tax,
                'realisasi_total'=>$totalProject,
                'realisasi_profit'=>$amountProfit,
                'realisasi_cost'=>$amountCost,
                
            );
             ORDER::where('order_id',$order_id)->update($data);
             
            \DB::commit();
            return $this->success("Update Success", 200, $data);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            $data = ORDER::where('order_id',$id)->first();
            $status = $data->status;
            if($status !="open"){
                return $this->fail("Status Is Not Open",500,null);
            }else{
                ORDER::where('order_id',$id)->delete();
                TERMIN::where('order_id',$id)->delete(); 
                Storage::deleteDirectory("public/$id");
            }
            
            
            \DB::commit();
            return $this->success();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function adendum(\Illuminate\Http\Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $pekerjaan = $request->pekerjaan;
            $nominal = $request->nominal;

            $data = ORDER::where('order_id',$id)->first();
            $limit_cost = $data->limit_cost;
            $realisasi_profit = $data->realisasi_profit;
            $realisasi_cost = $data->realisasi_cost;

            if($pekerjaan == "Pekerjaan Lebih"){
                $real_cost = $realisasi_cost - $nominal;
                $real_profit = $nominal + $realisasi_profit;

                $update = array(
                'jenis_pekerjaan'=>$pekerjaan,    
                'nominal'=>$nominal,    
                'realisasi_profit'=>$real_profit,
                'realisasi_cost'=>$real_cost,
            );
            }else{ 
                if($limit_cost < $nominal ){
                   $nominal_sm = $nominal - $limit_cost ;
                   $limit_cost = 0;
                   $real_cost = $limit_cost;
                   $real_profit = $realisasi_profit - $nominal_sm; 
                   $real_total = $realisasi_total - $nominal;

                }else{
                   $limit_cost = $limit_cost - $nominal ; 
                   $real_total = $realisasi_total - $nominal;
                   $real_cost = $realisasi_cost - $nominal ; 
                   $real_profit = $realisasi_profit;
                } 

                $update = array(
                    'jenis_pekerjaan'=>$pekerjaan,    
                    'nominal'=>$nominal,   
                    'limit_cost'=>$limit_cost,
                    'realisasi_total'=>$real_total,
                    'realisasi_profit'=>$real_profit,
                    'realisasi_cost'=>$real_cost,
                );
            } 
            
            ORDER::where('order_id',$id)->update($update); 
            
            \DB::commit();
            return $this->success();
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Import.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'customer' => 'required|array',
            'customer.name' => 'required|string',
            'customer.currency_code' => 'required|string',
            'customer.address' => 'nullable|string',
            'customer.phone' => 'nullable|string',
            'work_order' => 'required|string',
            'entry_at' => 'required|date_format:"d/m/Y"',
            'attachment.*' => 'file|image|mimes:jpeg,png,jpg,doc,ppt,xls,pdf|max:2048',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 500, $validator->errors());
        }
        // Customer
        $customer = $this->customer->all()->where('name', $request->customer['name']);
        if ($customer->isEmpty()) {
            $customer = $this->customer->save([
                'name' => $request->customer['name'],
                'currency_code' => $request->customer['currency_code'],
                'address' => $request->customer['address'],
                'phone' => $request->customer['phone'],
            ]);
        } else {
            $customer = $customer->first();
        }
        // Order
        $order = $this->order->all()->where('work_order', $request->work_order);
        if ($order->isEmpty()) {
            $order = $this->order->save([
                'customer_id' => $customer->customer_id,
                'work_order' => $request->work_order,
                'entry_at' => $request->entry_at,
            ]);
        } else {
            $order = $order->first();
        }
        try {
            $order->import = $request->attachment;
            $order->save();
            return $this->success(null, 200, $order->import);
        } catch (\Exception $e) {
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    //adendum
    public function uploadrab(\Illuminate\Http\Request $request)
    {   
        $order_id = $request->order_id; 
        $files = $request->file('filerab');
        $extension = $request->file('filerab')->extension(); 
        $order= ORDER::where('order_id',$order_id)->first();
        $h = \Carbon\Carbon::now()->format('H');
        $i = \Carbon\Carbon::now()->format('i');
        $s = \Carbon\Carbon::now()->format('s');
        $date = \Carbon\Carbon::now()->format('Ymd');
        $val = rand(1,100);
        
        // //custom name masing2 file
        $filename = "AD-$order_id-"."$date$h$i$s$val.$extension";
        $path = "public/$order_id/adendum";
        //upload file 
        if (!file_exists($path)) {
            // path does not exist deletefilerab
            Storage::makeDirectory($path);
            $files->storeAs($path,$filename);
        }else{
            $files->storeAs($path,$filename); 
        }

        $cekfile = Storage::files($path);
        return $cekfile;
    }

    public function deletefilerab(\Illuminate\Http\Request $request)
    {   
        $dat = $request->pat;
        $path = "public/$dat";
        Storage::delete($path);
       
    }

    //rab
    public function uploadrab2(\Illuminate\Http\Request $request)
    {   
        $order_id = $request->spkId; 
        $files = $request->file('filerab');
        $extension = $request->file('filerab')->extension(); 
        $order= ORDER::where('order_id',$order_id)->first();
        $h = \Carbon\Carbon::now()->format('H');
        $i = \Carbon\Carbon::now()->format('i');
        $s = \Carbon\Carbon::now()->format('s');
        $date = \Carbon\Carbon::now()->format('Ymd');
        $val = rand(1,100);
        
        // //custom name masing2 file
        $filename = "RAB-$order_id-"."$date$h$i$s$val.$extension";
        $path = "public/$order_id/rab";
        //upload file 
        if (!file_exists($path)) {
            // path does not exist deletefilerab
            Storage::makeDirectory($path);
            $files->storeAs($path,$filename);
        }else{
            $files->storeAs($path,$filename); 
        }

        $cekfile = Storage::files($path);
        return $cekfile;
    }

    public function deletefilerab2(\Illuminate\Http\Request $request)
    {   
        $dat = $request->pat;
        $path = "public/$dat";
        Storage::delete($path);
       
    }
    
    public function codeSpk(\Illuminate\Http\Request $request, $toJson = 0)
    {
        $now = \Carbon\Carbon::now();
        $model = $this->order->getModel();
        $format = 'SPK [CV]-[UP]-[NO] %lpar%[WO]%rpar% [MM]/[YY]';
        // =====
        $cv = $request->has('comp_code') ? $request->comp_code : null;
        $wo = $request->has('code_wo') ? $request->code_wo : null;
        $up = $request->has('trans_type') ? $request->trans_type : null;
        // =====
        switch ($up) {
            case 'piutang':
                $code_UP = 'I';
            break;
            case 'hutang':
                $code_UP = 'II';
            break;
            default:
                $code_UP = 'H/P';
            break;
        }
        $code_CV = $cv;
        $code_WO = $wo;
        $code_MM = $now->format('m');
        $code_YY = $now->format('y');
        $noUrut = $model->whereMonth('created_at', $now->format('m'))->orderBy('order_id', 'DESC')->get();
        $noUrut = $noUrut->where('company.code_alpha', $cv)->flatten();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('code_spk')->first();
            preg_match('/SPK '.$code_CV.'-(?:H\/P|I{0,3})-(\d{3})/', $noUrut, $match);
            $noUrut = $match[1] + 1;
            $noUrut = str_pad($noUrut, 3, '0', STR_PAD_LEFT);
        } else {
            $noUrut = str_pad(1, 3, '0', STR_PAD_LEFT);
        }
        $code_NO = $noUrut;
        
        $code = str_replace(
            array('[NO]', '[CV]', '[WO]', '[UP]', '[MM]', '[YY]', '%lpar%', '%rpar%'),
            array($code_NO, $code_CV, $code_WO, $code_UP, $code_MM, $code_YY, '[', ']'),
            $format
        );      
        if ($toJson > 0) return $code;
        return $this->success(null, 200, compact('code'));
    }

    
}
