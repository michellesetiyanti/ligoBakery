<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\Borongan as BoronganRequest;
use App\Repositories\Finance\BoronganKerja as BoronganKerjaRepository;
use App\Repositories\Finance\UpahBorongan as UpahBoronganRepository;
use App\Repositories\Project\Borongan as BoronganRepository;
use App\Repositories\Project\Order as OrderRepository;
use App\Models\Project\Order as ORDER;
use App\Models\Project\Borongan as BORONGAN;
use App\Models\Project\BoronganKerja as BORONGANDETAIL;

class BoronganDetailController extends ApiController
{
    protected $borongan;
    protected $boronganKerja;
    protected $order;
    protected $upahBorongan;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->borongan = new BoronganRepository;
        $this->boronganKerja = new BoronganKerjaRepository;
        $this->order = new OrderRepository;
        $this->upahBorongan = new UpahBoronganRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $boronganDetails = $this->boronganKerja->with('borongan')->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $boronganDetails);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BoronganRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\Illuminate\Http\Request $request)
    {
        \DB::beginTransaction();
        try {
             
            $borongan_id=$request->borongan_id;
            $amount=$request->amount;
            $progress=$request->progress;
            $description=$request->description2;

            $detail = new BORONGANDETAIL;
            $detail->borongan_id=$borongan_id;
            $detail->amount=$amount;
            $detail->progress=$progress;
            $detail->description=$description;
            $detail->save();

            $total = BORONGANDETAIL::where('borongan_id',$borongan_id)->sum('amount');

           

            $cek = BORONGANDETAIL::where('borongan_id',$borongan_id)->with(['borongan'])->get(); 


            \DB::commit();
            return $this->success(null, 200, $cek);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($boronganDetail = $this->boronganKerja->with('borongan')->show($id)) {
            return $this->success(null, 200, $boronganDetail);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BoronganRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\Illuminate\Http\Request $request, $id)
    {
        $boronganDetail = BORONGANDETAIL::where('borongan_d_id',$id)->first();
        $borongan_id = $boronganDetail->borongan_id;
        \DB::beginTransaction();
        try {
                $update = array(
                    'description' => $request->description2,
                    'progress' => $request->progress,
                    'amount' => $request->amount,
                );
                BORONGANDETAIL::where('borongan_d_id',$id)->update($update);

                $total = BORONGANDETAIL::where('borongan_id',$borongan_id)->sum('amount');


                $cek = BORONGANDETAIL::where('borongan_id',$borongan_id)->with(['borongan'])->get(); 
            \DB::commit();
            return $this->success(null, 200, $cek);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function payborongan(\Illuminate\Http\Request $request, $id){
        \DB::beginTransaction();
        try {
                $company_id = auth()->user()->company_id;
                $coa_code = $request->coa_code;

                $data = BORONGANDETAIL::where('borongan_d_id',$id)->first();
                $borongan_id = $data->borongan_id;
                $desc_detail = $data->description;
                $progress = $data->progress;
                $amount = $data->amount;
                $desc = "Payment Borongan $desc_detail $progress%";

                $databorongan = BORONGAN::where('borongan_id',$borongan_id)->first();
                $totalhutang =$databorongan->nominal;
                $desc_borongan = $databorongan->description;
                

                $update = array(
                    'user_pay' => auth()->user()->user_id,
                    'date_pay' => $request->date,
                );
                BORONGANDETAIL::where('borongan_d_id',$id)->update($update);

                $post = postpaid($company_id, $coa_code, $desc_borongan, $desc, $amount);
                if($post['status'] =="failed"){
                    return $this->fail("Post Failed", 500, $post['errors']);
                }
            
                $total = BORONGANDETAIL::where('borongan_id',$borongan_id)->where('user_pay','!=',null)->sum('amount');
                $sisa = $totalhutang - $total;

                if($sisa == 0 ){
                    $data = array('paid'=>$total, 'balance'=>$sisa ,'status'=>'complete'); 
                }else{
                    $data = array('paid'=>$total, 'balance'=>$sisa); 
                }

                BORONGAN::where('borongan_id',$borongan_id)->update($data); 

                $cek = BORONGANDETAIL::where('borongan_id',$borongan_id)->with(['borongan'])->get(); 
            \DB::commit();
            return $this->success(null, 200, $cek);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            $cek = BORONGANDETAIL::where('borongan_d_id',$id)->first();
            $borongan_id = $cek->borongan_id;
            BORONGANDETAIL::where('borongan_d_id',$id)->delete();
            $total = BORONGANDETAIL::where('borongan_id',$borongan_id)->sum('amount');


            $cek = BORONGANDETAIL::where('borongan_id',$borongan_id)->with(['borongan'])->get();
            \DB::commit();
            return $this->success(null, 200, $cek);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
