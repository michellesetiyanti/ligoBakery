<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\Kasbon as KasbonRequest;
use App\Repositories\Finance\Kasbon as KasbonRepository;
use App\Repositories\Finance\KasbonDetail as KasbonDetailRepository;
use App\Repositories\Project\Order as OrderRepository;

class KasbonDetailController extends ApiController
{
    protected $kasbon;
    protected $kasbonDetail;
    protected $order;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->kasbon = new KasbonRepository;
        $this->kasbonDetail = new KasbonDetailRepository;
        $this->order = new OrderRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $kasbonDetails = $this->kasbonDetail->with('kasbon')->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $kasbonDetails);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  KasbonRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KasbonRequest $request)
    {
        $model = $this->kasbon->getModel();
        $code = $request->input('code');
        if (empty($code)) {
            $reqCode = new \Illuminate\Http\Request($request->only('code_spk'));
            $code = app(\App\Http\Controllers\Api\Project\KasbonController::class)->codeKasbon($reqCode);
        }
        $kasbon = $model->where('code', $code)->first();
        \DB::beginTransaction();
        try {
            if (empty($kasbon)) {
                $kasbon = $model->create($request->merge(compact('code'))->all());
            }
            $kasbonDetail = $kasbon->kasbonDetails()->create([
                'company_id' => $kasbon->company_id,
                'name' => $request->input('detail.person'),
                'description' => $request->input('detail.description'),
                'nominal' => $request->input('detail.nominal'),
            ]);
            $kasbon->update([
                'nominal' => $kasbon->kasbonDetails->sum('nominal')
            ]);
            \DB::commit();
            return $this->success(null, 200, $kasbonDetail);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($kasbonDetail = $this->kasbonDetail->with('kasbon')->show($id)) {
            return $this->success(null, 200, $kasbonDetail);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  KasbonRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KasbonRequest $request, $id)
    {
        if ($request->missing('code') && empty($request->input('code'))) {
            return $this->fail(trans('validation.required', ['attribute' => 'code']), 422);
        }
        $model = $this->kasbon->getModel();
        $kasbon = $model->where('code', $request->input('code'))->first();
        $kasbonDetail = $kasbon->kasbonDetails->where('kasbon_d_id', $id)->first();
        \DB::beginTransaction();
        try {
            $kasbonDetail->update([
                'name' => $request->input('detail.person'),
                'description' => $request->input('detail.description'),
                'nominal' => $request->input('detail.nominal'),
            ]);
            $kasbon->update([
                'nominal' => $kasbon->kasbonDetails->sum('nominal')
            ]);
            \DB::commit();
            return $this->success(null, 200, $kasbonDetail);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kasbon = $this->kasbon->all();
        $kasbonDetail = $kasbon->flatMap->kasbonDetails->where('kasbon_d_id', $id)->first();
        \DB::beginTransaction();
        try {
            if ($kasbonDetail->kasbon->approved) {
                return $this->fail('Transaction status has been approved', 422);
            }
            $kasbonDetail->delete();
            $kasbonDetail->kasbon->update([
                'nominal' => $kasbonDetail->kasbon->kasbonDetails->sum('nominal')
            ]);
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
