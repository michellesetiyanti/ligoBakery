<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\Estimasi as EstimasiRequest;
use App\Repositories\Master\Product as ProductRepository;
use App\Repositories\Project\Estimasi as EstimasiRepository;
use App\Repositories\Project\Order as OrderRepository;
use App\Models\Project\Order as ORDER;
use App\Models\Project\OrderTermin as TERMIN;
use App\Models\Project\OrderTerminDetail as TERMINDETAIL;
use App\Models\Project\Estimasi as ESTIMASI;
use App\Models\Project\EstimasiDetail as ESTIMASIDETAIL;
use App\Models\Master\Product as PRODUCT;
use App\Models\Master\ProductDetail as PRODUCTDETAIL;
use App\Models\Project\LimitOrder as LIMITORDER;



class EstimasiController extends ApiController
{
    protected $estimasi;
    protected $order;
    protected $product;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth', ['except' => ['update', 'destroy']]);
        $this->estimasi = new EstimasiRepository;
        $this->order = new OrderRepository;
        $this->product = new ProductRepository;
    }
   
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $estimasi = $this->estimasi->with(['estimasiDetails', 'order'])->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $estimasi);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EstimasiRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\Illuminate\Http\Request $request)
    { 
        \DB::beginTransaction();
        try {
            $user_id = auth()->user()->user_id;
            $company_id = auth()->user()->company_id;
            $order_id = $request->order_id;
            $wh_id = $request->wh_id;
            $product_id = $request->product_id;
            $date = $request->date;
            $qty_lg = $request->qty_lg;
            $qty_sm = $request->qty_sm; 
            $qty_lg = 0;
            
            $est = new ESTIMASI;
            $est->user_id = $user_id;
            $est->company_id = $company_id;
            $est->order_id = $order_id;
            $est->wh_id = $wh_id;
            $est->entry_at = $date;  
            $est->approved = "open";  
            $est->save();

            $estimasi_id = $est->estimasi_id;

            $dataproduct = PRODUCT::where('product_id',$product_id)->first();
            $unit_qty = $dataproduct->unit_qty;
            $unit_lg = $dataproduct->unit_lg;
            $unit_sm = $dataproduct->unit_sm;
            
            $coun = PRODUCTDETAIL::where('product_id',$product_id)->orderBy('product_d_id','DESC')->count();
            if($coun > 0){
                $dataproductdetail= PRODUCTDETAIL::where('product_id',$product_id)->orderBy('product_d_id','DESC')->first();
                $price_lg = $dataproductdetail->price_lg;
                $price_sm = $dataproductdetail->price_sm;
            }else{
                $price_lg = $dataproduct->price_lg;
                $price_sm = $dataproduct->price_sm;   
            }

            // $qty_sm_m = ($qty_sm % $unit_qty); 
            // $qty_lg = (($qty_sm - $qty_sm_m)/$unit_qty) + $qty_lg;
            $amount =  ($qty_sm*$price_sm);

            $detail = new ESTIMASIDETAIL;
            $detail->estimasi_id = $estimasi_id;
            $detail->product_id = $product_id;
            $detail->unit_lg = $unit_lg;
            $detail->unit_sm = $unit_sm;
            $detail->qty_lg = $qty_lg;
            $detail->qty_sm = $qty_sm;
            $detail->price_lg = $price_lg;
            $detail->price_sm = $price_sm;
            $detail->amount = $amount; 
            $detail->save();

            $update = array('total'=>$amount);
            ESTIMASI::where('estimasi_id',$estimasi_id)->update($update);

            $estimasi = ESTIMASI::where('estimasi_id',$estimasi_id)->with(['estimasiDetails'])->first();

            $array = array(
                'status'=>'Success',
                'message'=>'Success Create Estimasi',
                'data'=>$estimasi,
            ); 

            \DB::commit();
            return $array;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cek = ESTIMASI::where('order_id',$id)->where('approved','=','open')->count();

        if($cek >0){
            $estimasi = ESTIMASI::where('order_id',$id)->where('approved','=','open')->with(['estimasiDetails','warehouse','order'])->first();

            $data = array(
                'status'=>"success",
                'message'=>"Data loaded successfully",
                'estimasi'=>true,
                'data'=>$estimasi,
            );
            return $data;

        }else{
            $order = ORDER::where('order_id',$id)->first();
            $data = array(
                'status'=>"success",
                'message'=>"Data loaded successfully",
                'estimasi'=>false,
                'data'=>$order,
            );
            return $data;
        } 
    }

    public function showpending($id)
    {
        $cek = ESTIMASI::where('order_id',$id)->where('approved','=','pending')->count();

        if($cek >0){
            $estimasi = ESTIMASI::where('order_id',$id)->where('approved','=','pending')->with(['estimasiDetails','warehouse','order'])->first();

            $data = array(
                'status'=>"success",
                'message'=>"Data loaded successfully",
                'estimasi'=>true,
                'data'=>$estimasi,
            );
            return $data;

        }else{
            $order = ORDER::where('order_id',$id)->first();
            $data = array(
                'status'=>"success",
                'message'=>"Data loaded successfully",
                'estimasi'=>false,
                'data'=>$order,
            );
            return $data;
        } 
    }
    public function showtotal($id)
    {
        $cek = LIMITORDER::where('order_id',$id)->count();

        if($cek >0){
            $estimasi = LIMITORDER::where('order_id',$id)->with(['product','order'])->get();

            $data = array(
                'status'=>"success",
                'message'=>"Data loaded successfully",
                'data'=>$estimasi,
            );
            return $data;

        } else{
            $data = array(
                'status'=>"success",
                'message'=>"There's No Data",
                'data'=>null,
            );
            return $data;
        }

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EstimasiRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\Illuminate\Http\Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $date= $request->date;
            $wh_id= $request->wh_id;

            $data = ESTIMASI::where('estimasi_id',$id)->first();
            $order_id = $data->order_id;
            $oldwh_id = $data->wh_id;

            if($wh_id != $oldwh_id){
                ESTIMASIDETAIL::where('estimasi_id',$id)->delete();
                $update= array(
                    'entry_at'=>$date,
                    'wh_id'=>$wh_id,
                    'total'=>0,
                );
            }else{
                $update= array(
                    'entry_at'=>$date,
                );
            } 
            ESTIMASI::where('estimasi_id',$id)->update($update);

            $estimasi = ESTIMASI::where('order_id',$order_id)->where('approved','=','open')->with(['estimasiDetails','warehouse','order'])->first();

            $data = array(
                'status'=>"success",
                'message'=>"Data Updated Successfully",
                'data'=>$estimasi,
            );

            \DB::commit();
            return $data;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function submit($id){
        \DB::beginTransaction();
        try { 
            $data = ESTIMASI::where('estimasi_id',$id)->first();
            $order_id = $data->order_id;
                $update= array(
                    'approved'=>'pending',
                ); 
            ESTIMASI::where('estimasi_id',$id)->update($update);

            $update2 = array(
                    'status_estimasi'=>'pending',
                ); 
            ORDER::where('order_id',$order_id)->update($update2);
 
            \DB::commit();
            return $this->success("Success Submit Estimasi", 200, null);;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        } 
    }

    public function confirm($id){
        \DB::beginTransaction();
        try { 
            $data = ESTIMASI::where('estimasi_id',$id)->first();
            $entry_at = $data->entry_at;
            $order_id = $data->order_id;
            $total = $data->total;
                $update= array(
                    'approved'=>'complete',
                ); 
            ESTIMASI::where('estimasi_id',$id)->update($update);


            $dataorder = ORDER::where('order_id',$order_id)->first();
            $totalestimasi = $dataorder->amount_estimasi;

            $totalestimasi = $totalestimasi + $total;

            $update2 = array(
                    'amount_estimasi'=> $totalestimasi,
                    'status_estimasi'=>'complete',
                ); 
            ORDER::where('order_id',$order_id)->update($update2);

            $detail = ESTIMASIDETAIL::where('estimasi_id',$id)->get();
            foreach($detail as $dt){
                $product_id = $dt->product_id;
                $qty_lg = $dt->qty_lg;
                $qty_sm = $dt->qty_sm;
                $price_lg = $dt->price_lg;
                $price_sm = $dt->price_sm;

                $ceklimit = LIMITORDER::where('order_id',$order_id)->where('product_id',$product_id)->count(); 

                if($ceklimit > 0){
                    $dtlimit = LIMITORDER::where('order_id',$order_id)->where('product_id',$product_id)->first();
                    $idlimit = $dtlimit->limit_id;
                    $oldqty_lg = $dtlimit->qty_lg;
                    $oldqty_sm = $dtlimit->qty_sm; 

                    $dataproduct = PRODUCT::where('product_id',$product_id)->first();
                    $unit_qty = $dataproduct->unit_qty;

                    $totalqty_sm = ($oldqty_sm + $qty_sm) % $unit_qty;
                    $totalqty_lg = ((($oldqty_sm + $qty_sm) - $totalqty_sm)/$unit_qty) + $oldqty_lg + $qty_lg;


                    $updatelimit = array(
                        'qty_lg'=>$totalqty_lg,
                        'qty_sm'=>$totalqty_sm,
                    );
                    LIMITORDER::where('limit_id',$idlimit)->update($updatelimit);

                }else{ 
                    $limit = new LIMITORDER;
                    $limit->order_id = $order_id;
                    $limit->product_id = $product_id;
                    $limit->qty_lg = $qty_lg;
                    $limit->qty_sm = $qty_sm;
                    $limit->price_lg = $price_lg;
                    $limit->price_sm = $price_sm;
                    $limit->entry_at = $entry_at;
                    $limit->save();
                }

            }
 
            \DB::commit();
            return $this->success("Success Confirm Estimasi", 200, null);;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        } 
    }

    public function reject($id){
        \DB::beginTransaction();
        try { 
            $data = ESTIMASI::where('estimasi_id',$id)->first();
            $order_id = $data->order_id;
                $update= array(
                    'approved'=>'open',
                ); 
            ESTIMASI::where('estimasi_id',$id)->update($update);

            $update2 = array(
                    'status_estimasi'=>'open',
                ); 
            ORDER::where('order_id',$order_id)->update($update2);
 
            \DB::commit();
            return $this->success("Success Submit Estimasi", 200, null);;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
