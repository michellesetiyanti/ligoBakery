<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\Ro as RoRequest;
use App\Repositories\Master\Product as ProductRepository;
use App\Repositories\Master\Supplier as SupplierRepository;
use App\Repositories\Master\Warehouse as WarehouseRepository;
use App\Repositories\Pembelian\PoProject as PoProjectRepository;
use App\Repositories\Pembelian\PoProjectDetail as PoProjectDetailRepository;
use App\Repositories\Project\Estimasi as EstimasiRepository;
use App\Repositories\Project\Order as OrderRepository;

class RoDetailController extends ApiController
{
    protected $estimasi;
    protected $order;
    protected $po;
    protected $poDetail;
    protected $product;
    protected $supplier;
    protected $warehouse;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->estimasi = new EstimasiRepository;
        $this->order = new OrderRepository;
        $this->po = new PoProjectRepository;
        $this->poDetail = new PoProjectDetailRepository;
        $this->product = new ProductRepository;
        $this->supplier = new SupplierRepository;
        $this->warehouse = new WarehouseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $poDetail = $this->poDetail->with(['product', 'poProject'])->all($request->all());
        return $this->success(null, 200, $poDetail);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoRequest $request)
    {
        if (empty(auth()->user()->company->coa_pembelian) || empty(auth()->user()->company->coa_hutang) || empty(auth()->user()->company->coa_penjualan) || empty(auth()->user()->company->coa_piutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $warehouse = $this->warehouse->show($request->wh_id);
        $product = $this->product->all()->where('wh_id', $request->wh_id)->where('product_id', $request->details['product_id'])->first();
        if (empty($product)) {
            return $this->fail('product_dont_exist', 422);
        }
        $order = $this->order->all()->where('code_spk', $request->code_spk)->first();
        $est = $order->estimasi->where('wh_id', $request->wh_id)->where('approved', 'complete')->flatten();
        if ($est->isEmpty()) {
            return $this->fail('Please complete estimation', 422);
        }
        $model = $this->po->getModel();
        \DB::beginTransaction();
        try {
            $code = $request->code && $request->code == 'null' ? null : $request->code;
            if (empty($code)) {
                $code = app(RoController::class)->codeRo($request->only('code_spk'));
            }
            $po = $model->where('code', $code)->first();
            if (empty($po)) {
                $po = $model->create($request->merge([
                    'code' => $code,
                    'total' => $request->ongkir + $request->biaya_lain,
                ])->except(['supplier_id', 'details']));
            }
            if ($po->poProjectDetails()->where('product_id', $product->product_id)->exists()) {
                return $this->fail('product_exist', 422, $po->poProjectDetails->where('product_id', $product->product_id)->first());
            }
            $osEst = app(RoController::class)->osEst([
                'code_spk' => $request->code_spk,
                'product_id' => $request->details['product_id'],
            ]);
            if ($request->details['qty_lg'] > $osEst->sum('qty_lg') || $request->details['qty_sm'] > $osEst->sum('qty_sm')) {
                return $this->fail('Exceeded the estimate limit', 422, [
                    'po_qty_lg' => $request->details['qty_lg'],
                    'po_qty_sm' => $request->details['qty_sm'],
                    'est_qty_lg' => $osEst->sum('qty_lg'),
                    'est_qty_sm' => $osEst->sum('qty_sm'),
                ]);
            }
            $amount = $request->details['qty_lg'] * calcAmount($request->details['price_lg'], $request->details['tax']);
            $amount+= $request->details['qty_sm'] * calcAmount($request->details['price_sm'], $request->details['tax']);
            $poDetail = $po->poProjectDetails()->create([
                'product_id' => $product->product_id,
                'unit_lg' => $product->unit_lg,
                'unit_sm' => $product->unit_sm,
                'qty_lg' => $request->details['qty_lg'],
                'qty_sm' => $request->details['qty_sm'],
                'price_lg' => $request->details['price_lg'],
                'price_sm' => $request->details['price_sm'],
                'tax' => $request->details['tax'],
                'discount' => $request->details['discount'],
                'amount' => $amount - $request->details['discount'],
            ]);
            $po->update($request->merge([
                'total' => $po->poProjectDetails->sum('amount') + $request->ongkir + $request->biaya_lain,
            ])->except(['code', 'supplier_id', 'details']));
            \DB::commit();
            return $this->success('success_process', 200, $poDetail);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail('fail_process', 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $po = $this->po->all();
        if ($poDetail = $po->flatMap->poProjectDetails->where('po_pro_d_id', $id)->first()) {
            return $this->success(null, 200, $poDetail);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RoRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoRequest $request, $id)
    {
        if (empty(auth()->user()->company->coa_pembelian) || empty(auth()->user()->company->coa_hutang) || empty(auth()->user()->company->coa_penjualan) || empty(auth()->user()->company->coa_piutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $warehouse = $this->warehouse->show($request->wh_id);
        $product = $this->product->all()->where('wh_id', $request->wh_id)->where('product_id', $request->details['product_id'])->first();
        if (empty($product)) {
            return $this->fail('product_dont_exist', 422);
        }        
        $order = $this->order->all()->where('code_spk', $request->code_spk)->first();
        $est = $order->estimasi->where('wh_id', $request->wh_id)->where('approved', 'complete')->flatten();
        if ($est->isEmpty()) {
            return $this->fail('Please complete estimation', 422);
        }
        $poDetail = $this->poDetail->show($id);
        $po = $this->po->show($poDetail->po_pro_id);
        \DB::beginTransaction();
        try {
            $osEst = app(RoController::class)->osEst([
                'code_spk' => $request->code_spk,
                'product_id' => $request->details['product_id'],
            ]);
            $osEstQtyLg = $osEst->sum('qty_lg') + $poDetail->qty_lg;
            $osEstQtySm = $osEst->sum('qty_sm') + $poDetail->qty_sm;
            if ($request->details['qty_lg'] > $osEstQtyLg || $request->details['qty_sm'] > $osEstQtySm) {
                return $this->fail('Exceeded the estimate limit', 422, [
                    'po_qty_lg' => $request->details['qty_lg'],
                    'po_qty_sm' => $request->details['qty_sm'],
                    'est_qty_lg' => $osEstQtyLg,
                    'est_qty_sm' => $osEstQtySm,
                ]);
            }
            $amount = $request->details['qty_lg'] * calcAmount($request->details['price_lg'], $request->details['tax']);
            $amount+= $request->details['qty_sm'] * calcAmount($request->details['price_sm'], $request->details['tax']);
            $poDetail->update([
                'qty_lg' => $request->details['qty_lg'],
                'qty_sm' => $request->details['qty_sm'],
                'price_lg' => $request->details['price_lg'],
                'price_sm' => $request->details['price_sm'],
                'tax' => $request->details['tax'],
                'discount' => $request->details['discount'],
                'amount' => $amount - $request->details['discount'],
            ]);
            $po->update($request->merge([
                'total' => $po->poProjectDetails->sum('amount') + $request->ongkir + $request->biaya_lain,
            ])->except(['code', 'supplier_id', 'details']));
            \DB::commit();
            return $this->success(null, 200, $poDetail);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $poDetail = $this->poDetail->show($id);
        \DB::beginTransaction();
        try {
            if (in_array($poDetail->poProject->status, ['close'])) {
                return $this->fail('fail_status', 422, ['status' => $poDetail->poProject->status]);
            } else {
                $poDetail->delete();
            }
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
