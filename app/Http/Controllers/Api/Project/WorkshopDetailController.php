<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use Illuminate\Http\Request;
use App\Models\Master\Product as PRODUCT; 
use App\Models\Master\Warehouse as WAREHOUSE; 
use App\Models\Master\Workshop as WORKSHOP;
use App\Models\Master\WorkshopDetail as WORKSHOPDETAIL;

class WorkshopDetailController extends ApiController
{
    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth'); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_id = auth()->user()->company_id;
        $data = WORKSHOP::where('company_id',$company_id)->get();
        return $this->success(null, 200, $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = auth()->user()->user_id;
        $company_id = auth()->user()->company_id;
        $workshop_id = $request->workshop_id;
        $product_id = $request->product;
        // $qty_lg = $request->qty_lg;
        $qty_sm = $request->qty_sm; 
        $qty_lg = 0;

        $wh = WAREHOUSE::where('company_id',$company_id)->first();
        $wh_id = $wh->wh_id;


        \DB::beginTransaction();
        try {
            $data = PRODUCT::where('product_id',$product_id)->where('wh_id',$wh_id)->first();
            $unit_lg = $data->unit_lg;
            $unit_sm = $data->unit_sm; 
            $price_lg = $data->price_lg;
            $price_sm = $data->price_sm; 
            $unit_qty = $data->unit_qty; 

            // $qty_smm = $qty_sm % $unit_qty;
            // $qty_lg = (($qty_sm - $qty_smm )/$unit_qty) + $qty_lg ; 
            // $amount = ($qty_lg * $price_lg) + ($qty_sm * $price_sm);
            
            $qty_smm = $qty_sm;
            $qty_lg = 0;
            $amount = ($qty_lg * $price_lg) + ($qty_smm * $price_sm);

            $coun = WORKSHOPDETAIL::where('workshop_id',$workshop_id)->where('product_id',$product_id)->count();
            if($coun > 0){
                return $this->fail("Post Failed", 500, "Double Product Input");
            }

            $create = new WORKSHOPDETAIL;
            $create->user_id = $user_id;
            $create->wh_id = $wh_id;
            $create->workshop_id = $workshop_id;
            $create->product_id = $product_id;
            $create->unit_lg = $unit_lg;
            $create->unit_sm = $unit_sm;
            $create->qty_lg = $qty_lg;
            $create->qty_sm = $qty_smm;
            $create->price_lg = $price_lg;
            $create->price_sm = $price_sm;
            $create->amount = $amount;
            $create->save(); 

            $total = WORKSHOPDETAIL::where('workshop_id',$workshop_id)->sum('amount');

            $update =array(
                'total'=>$total,
            );
            WORKSHOP::where('workshop_id',$workshop_id)->update($update); 

            $ret = WORKSHOP::where('workshop_id',$workshop_id)->with(['order','workshopDetails'])->first();
            
            \DB::commit();
            return $this->success(null, 200, $ret);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = auth()->user()->user_id;
        $company_id = auth()->user()->company_id;

        $cek = WORKSHOPDETAIL::where('workshop_d_id',$id)->first();
        $product_id = $cek->product_id;
        $workshop_id = $cek->workshop_id;

        $qty_lg = $request->qty_lg;
        $qty_lg = 0;
        $qty_sm = $request->qty_sm; 

        $wh = WAREHOUSE::where('company_id',$company_id)->first();
        $wh_id = $wh->wh_id;


        \DB::beginTransaction();
        try {
            $data = PRODUCT::where('product_id',$product_id)->where('wh_id',$wh_id)->first();
            $unit_lg = $data->unit_lg;
            $unit_sm = $data->unit_sm; 
            $price_lg = $data->price_lg;
            $price_sm = $data->price_sm; 
            $unit_qty = $data->unit_qty; 

            // $qty_smm = $qty_sm % $unit_qty;
            $qty_smm = $qty_sm;
            // $qty_lg = (($qty_sm - $qty_smm )/$unit_qty) + $qty_lg ; 
            $qty_lg = 0;
            $amount = ($qty_lg * $price_lg) + ($qty_smm * $price_sm);
            
            $ar = array(
                'qty_lg'=>$qty_lg,
                'qty_sm'=>$qty_smm,
                'amount'=>$amount,
            );
            WORKSHOPDETAIL::where('workshop_d_id',$id)->update($ar);

            $total = WORKSHOPDETAIL::where('workshop_id',$workshop_id)->sum('amount');

            $update =array(
                'total'=>$total,
            );
            WORKSHOP::where('workshop_id',$workshop_id)->update($update); 

            $ret = WORKSHOP::where('workshop_id',$workshop_id)->with(['order','workshopDetails'])->first();
            
            \DB::commit();
            return $this->success(null, 200, $ret);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 

        $cek = WORKSHOPDETAIL::where('workshop_d_id',$id)->first();
        $workshop_id = $cek->workshop_id;
 
        \DB::beginTransaction();
        try { 
            WORKSHOPDETAIL::where('workshop_d_id',$id)->delete();

       
            $total = WORKSHOPDETAIL::where('workshop_id',$workshop_id)->sum('amount');

            $update =array(
                'total'=>$total,
            );
            WORKSHOP::where('workshop_id',$workshop_id)->update($update); 

            $ret = WORKSHOP::where('workshop_id',$workshop_id)->with(['order','workshopDetails'])->first();
            
            \DB::commit();
            return $this->success(null, 200, $ret);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
