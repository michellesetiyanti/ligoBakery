<?php

namespace App\Http\Controllers\Api\Project;

use App\Models\Master\Aset as ASET;
use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\ManageAset as ManageAsetRequest;
use App\Repositories\Project\ManageAset as ManageAsetRepository;
use App\Models\Project\ManageAset as MASET;

class ManageAsetController extends ApiController
{
    protected $manage_aset;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->manage_aset = new ManageAsetRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $manage_aset = $this->manage_aset->with('aset')->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $manage_aset);
    }

    public function listavailable(\Illuminate\Http\Request $request)
    { 
        $manage_aset = ASET::where('company_id',auth()->user()->company_id)->where('borrowed_by','=',null)->get();
        return $this->success(null, 200, $manage_aset);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(\Illuminate\Http\Request $request)
    {
        \DB::beginTransaction();
        try { 
            $aset_id = $request->aset_id;
            $borrow = $request->name;
            $position = $request->position;
            $date_borrow = $request->date_borrow;
            

            $manage_aset = $this->manage_aset->save($request->all()); 
            

            $data= array(
                'status'=> 'unavailable',
                'borrowed_by'=> $borrow,
            );
            ASET::where('aset_id',$aset_id)->update($data);

            \DB::commit();
            return $this->success(null, 200, $manage_aset);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\manage_aset  $manage_aset
     * @return \Illuminate\Http\Response
     */
    public function show(manage_aset $manage_aset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\manage_aset  $manage_aset
     * @return \Illuminate\Http\Response
     */
    public function edit(manage_aset $manage_aset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\manage_aset  $manage_aset
     * @return \Illuminate\Http\Response
     */
     public function update(ManageAsetRequest $request, $id)
    {
        $manage_aset = $this->manage_aset->show($id);
        \DB::beginTransaction();
        try {
            $manage_aset->update($request->merge([
                'manage_aset_id' => $id,
            ])->all());
            \DB::commit();
            return $this->success(null, 200, $manage_aset);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function itemreturn(\Illuminate\Http\Request $request, $id)
    {
        $manage_aset = MASET::where('manage_aset_id','=',$id)->first();
        \DB::beginTransaction();
        try {
             
            $date_return = $request->date_return;

            $manage = array(
                'date_return'=>$date_return
            );
            $a = MASET::where('manage_aset_id',$id)->update($manage);

            $aset_id = $manage_aset->aset_id;

            $data= array(
                'status'=> null,
                'borrowed_by'=> null,
            );
            ASET::where('aset_id',$aset_id)->update($data);

            \DB::commit();
            return $this->success(null, 200, $manage_aset);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\manage_aset  $manage_aset
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
    {
        $manage_aset = $this->manage_aset->show($id);
        \DB::beginTransaction();
        try { 
            $aset_id = $manage_aset->aset_id;
            $data= array(
                'status'=> null,
                'borrowed_by'=> null,
            );
            ASET::where('aset_id',$aset_id)->update($data);

            $manage_aset->delete(); 
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
