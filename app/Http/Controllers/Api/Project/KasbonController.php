<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\Kasbon as KasbonRequest;
use App\Repositories\Finance\Kasbon as KasbonRepository;
use App\Repositories\Project\Order as OrderRepository;

class KasbonController extends ApiController
{
    protected $kasbon;
    protected $order;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->kasbon = new KasbonRepository;
        $this->order = new OrderRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $kasbon = $this->kasbon->with(['kasbonDetails', 'kasbonRealisasi', 'user'])->all($request->merge(compact('filter'))->all());
        $data = collect();
        foreach ($kasbon as $row) {
            // Notify
            if ($row->approved) {
                $approveDate = \Carbon\Carbon::createFromFormat('d/m/Y', $row->approved->entry_at)->add(2, 'week')->toDateString();
                if ($approveDate <= \Carbon\Carbon::now()->toDateString() && empty($row->kasbon_realisasi)) {
                    $row = collect($row)->merge(['realisasiNotify' => 'actived']);
                } else {
                    $row = collect($row)->merge(['realisasiNotify' => 'unactived']);
                }
            } else {
                $row = collect($row)->merge(['realisasiNotify' => 'unactived']);
            }
            $data->push($row);
        }
        return $this->success(null, 200, $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  KasbonRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KasbonRequest $request)
    {
        if ($request->missing('code') && empty($request->input('code'))) {
            return $this->fail(trans('validation.required', ['attribute' => 'code']), 422);
        }
        $model = $this->kasbon->getModel();
        $kasbon = $model->where('code', $request->input('code'))->first();
        \DB::beginTransaction();
        try {
            $kasbon->update([
                'nominal' => $kasbon->kasbonDetails->sum('nominal'),
                'status' => 'pending',
            ]);
            \DB::commit();
            return $this->success(null, 200, $kasbon);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kasbon = $this->kasbon->with(['kasbonDetails', 'kasbonRealisasi', 'user'])->show($id);
        if ($kasbon) {
            // Notify
            if ($kasbon->approved) {
                $approveDate = \Carbon\Carbon::createFromFormat('d/m/Y', $kasbon->approved->entry_at)->add(2, 'week')->toDateString();
                if ($approveDate <= \Carbon\Carbon::now()->toDateString() && empty($kasbon->kasbon_realisasi)) {
                    $kasbon = collect($kasbon)->merge(['realisasiNotify' => 'actived']);
                } else {
                    $kasbon = collect($kasbon)->merge(['realisasiNotify' => 'unactived']);
                }
            } else {
                $kasbon = collect($kasbon)->merge(['realisasiNotify' => 'unactived']);
            }
            return $this->success(null, 200, $kasbon);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  KasbonRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KasbonRequest $request, $id)
    {
        if ($request->missing('code') && empty($request->input('code'))) {
            return $this->fail(trans('validation.required', ['attribute' => 'code']), 422);
        }
        $kasbon = $this->kasbon->show($id);
        \DB::beginTransaction();
        try{
            $kasbon->update([
                'nominal' => $kasbon->kasbonDetails->sum('nominal'),
                'status' => 'pending',
            ]);
            \DB::commit();
            return $this->success(null, 200, $kasbon);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kasbon = $this->kasbon->show($id);
        \DB::beginTransaction();
        try {
            if ($kasbon->approved) {
                return $this->fail('Transaction status has been approved', 422);
            }
            $kasbon->delete();
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Do Action the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function doAction(\Illuminate\Http\Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'details' => 'sometimes|required|array',
            'details.*.id' => 'integer|exists:kasbon_details,kasbon_d_id',
            'execute' => 'required|string|in:rejected,void,approved',
            'rejected' => 'sometimes|required|boolean',
            'notes' => 'sometimes|required|string',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $kasbon = $this->kasbon->show($id);
        \DB::beginTransaction();
        try {
            $ids = [];
            if ($request->details) {
                foreach ($request->details as $detail) {
                    array_push($ids, $detail['id']);
                }
            }
            switch ($request->execute) {
                case 'rejected':
                    if ($request->missing('rejected')) {
                        return $this->fail(trans('validation.required', ['attribute' => 'rejected']), 422);
                    }
                    $status = $request->boolean('rejected') ? 'reject' : 'pending';
                    $kasbon->update([
                        'status' => $status,
                        'notes' => $request->input('notes'),
                    ]);
                break;
                case 'void':
                    if (in_array($kasbon->status, ['open', 'pending'])) {
                        $kasbon->delete();
                    }
                    $kasbon->update([
                        'status' => 'void',
                        'notes' => $request->input('notes'),
                    ]);
                break;
                case 'approved':
                    if (in_array($kasbon->status, ['pending'])) {
                        $kasbon->kasbonDetails()->whereNotIn('kasbon_d_id', $ids)->delete();
                        $kasbon->update([
                            'nominal' => $kasbon->kasbonDetails->sum('nominal'),
                            'status' => 'close',
                            'approved' => json_encode(authExecute()),
                        ]);
                    }
                break;
            }
            \DB::commit();
            return $this->success(null, 200, $kasbon);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Payment
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function payment(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'kasbon_id' => 'required|integer|exists:kasbon,kasbon_id',
            'date' => 'required|date_format:"d/m/Y"',
            'coa_code' => 'required|string|exists:coa,code',            
            'amount' => 'required|digits_between:1,20',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $kasbon = $this->kasbon->show($request->input('kasbon_id'));
        \DB::beginTransaction();
        try {
            if ($kasbon->paided == 'yes') {
                return $this->fail('Payment completed', 422);
            }
            $paid_off = $kasbon->paid_off + $request->input('amount');
            if ($paid_off > $kasbon->nominal) {
                return $this->fail('over limit', 422, ['limit' => $kasbon->nominal - $kasbon->paid_off]);
            }
            $kasbon->update([
                'paid_off' => $paid_off,                
                'paided' => $paid_off == $kasbon->nominal ? 'yes' : 'no',
            ]);
            // Beban Project
            $order = $this->order->all()->where('code_spk', $kasbon->code_spk)->first();
            $order->update([
                'limit_cost' => $order->limit_cost - $request->input('amount'),
            ]);
            // Post GL
            $message = postpaid($kasbon->company_id, $request->input('coa_code'), $kasbon->code_spk, $kasbon->code.' '.$request->input('date'), $request->input('amount'));
            if ($message['status'] == 'failed') {
                return $this->fail('GL '.$message['message'], 422, $message['errors']);
            }
            \DB::commit();
            return $this->success($message['message'], 200, $kasbon);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Realisasi
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function realisasi(\Illuminate\Http\Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'kasbon_id' => 'required|integer|exists:kasbon,kasbon_id',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $kasbon = $this->kasbon->show($request->input('kasbon_id'));
        \DB::beginTransaction();
        try {
            $kasbon->kasbonRealisasi->map->update(['status' => 'close']);
            \DB::commit();
            return $this->success(null, 200, $kasbon);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    
    public function codeKasbon(\Illuminate\Http\Request $request)
    {
        $now = \Carbon\Carbon::now();
        $auth = auth()->user();
        $model = $this->kasbon->getModel();
        $format = 'KASB-[CV] [NO] %lpar%[WO]%rpar% [CK]-[MM]/[YY]';
        // =====
        $codespk = $request->has('code_spk') ? $request->code_spk : null;
        // =====
        $code_CV = $auth->company->code_alpha;
        $code_WO = $codespk;
        $code_MM = $now->format('m');
        $code_YY = $now->format('y');
        $code_CK = $model->where('code_spk', $codespk)->count() + 1;
        $code_CK = str_pad($code_CK, 2, '0', STR_PAD_LEFT);
        $noUrut = $model->where('company_id', $auth->company_id)->whereMonth('created_at', $now->format('m'))->orderBy('kasbon_id', 'DESC')->get();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('code')->first();
            preg_match('/KASB-'.$code_CV.' (\d{3})/', $noUrut, $match);
            $noUrut = $match[1] + 1;
            $noUrut = str_pad($noUrut, 3, '0', STR_PAD_LEFT);
        } else {
            $noUrut = str_pad(1, 3, '0', STR_PAD_LEFT);
        }
        $code_NO = $noUrut;
        
        return str_replace(
            array('[CV]', '[NO]', '[WO]', '[CK]', '[MM]', '[YY]', '%lpar%', '%rpar%'),
            array($code_CV, $code_NO, $code_WO, $code_CK, $code_MM, $code_YY, '[', ']'),
            $format
        );
    }
}
