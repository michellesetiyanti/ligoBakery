<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use Illuminate\Http\Request; 
use App\Models\Project\Offer as OFFER;
use App\Models\Master\Company as COMPANY;
use App\Models\Project\OfferDetail as OFFERDETAIl;
use Carbon;
use File;
use Illuminate\Support\Facades\Storage;


class PenawaranController extends ApiController
{
    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth'); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company_id = auth()->user()->company_id;
        $data = OFFER::where('company_id',$company_id)->with(['customer','offerDetails'])->get();
        return $this->success(null, 200, $data);
    }

    public function indexpending()
    {
        $company_id = auth()->user()->company_id;
        $data = OFFER::where('company_id',$company_id)->with(['customer','offerDetails'])->where('status','pending')->get();
        return $this->success(null, 200, $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $user_id = auth()->user()->user_id;
        $company_id = auth()->user()->company_id;
        $datacompany = COMPANY::where('company_id',$company_id)->first();
        $code_alpha = $datacompany->code_alpha;

        $code_offer = $this->codeOffer(new Request(['comp_codealpha'=>$code_alpha]));
        
        $nomor = $code_offer;
        $date = $request->date;
        $customer_id = $request->customer;
        $amount = $request->amount;
        $name = $request->name;
        $hasil = $request->hasil;

        $item = $request->item;
        $unit = $request->unit;
        $vol = $request->vol;
        $price = $request->price;
        $note = $request->note; 

        $model = $request->model; 
        
        if($model == 1){
            $total = $vol * $price;
        }

        if (empty($nomor)) {
            $nomor = $this->codeOffer(new Request(['comp_codealpha' => auth()->user()->company->code_alpha]));
        }

        \DB::beginTransaction();
        try { 

            $c = new OFFER;
            $c->user_id = $user_id;
            $c->company_id = $company_id;
            $c->customer_id = $customer_id;
            $c->nomor = $nomor;
            $c->entry_at = $date;
            $c->name = $name;
            $c->hasil = $hasil;
            $c->note = $note;
            $c->amount = $amount;
            $c->status = "open";
            $c->amount = $total;
            $c->model = $model;
            $c->save();

            $offer_id = $c->offer_id;
            if($model == 1){
                $create = new OFFERDETAIL;
                $create->offer_id = $offer_id;
                $create->item = $item;
                $create->unit = $unit;
                $create->vol = $vol;
                $create->price = $price; 
                $create->total = $total; 
                $create->save(); 
            }
            

            $ret = OFFER::where('offer_id',$offer_id)->with(['customer','offerDetails'])->first();
            
            \DB::commit();
            return $this->success(null, 200, $ret);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    public function revisi(Request $request){
        $user_id = auth()->user()->user_id;
        $company_id = auth()->user()->company_id;
        $date = $request->date;
        $id = $request->offer_id;

        $offer_data = OFFER::where('offer_id',$id)->with(['offerDetails'])->first();


        $offer_id = $offer_data->offer_id;
        $customer_id = $offer_data->customer_id;
        $nomor = $offer_data->nomor;
        $name= $offer_data->name;
        $hasil= $offer_data->hasil;
        $note= $offer_data->note;
        $amount= $offer_data->amount;
        $total= $offer_data->total;
        $model= $offer_data->model;
        $offer_details = $offer_data->offerDetails;



        $datacompany = COMPANY::where('company_id',$company_id)->first();
        $code_alpha = $datacompany->code_alpha;

        $code_offer = $this->codeOffer(new Request(['comp_codealpha'=>$code_alpha,'offer_coderev'=>$nomor]));
        $new_nomor = $code_offer;

        \DB::beginTransaction();
        try { 

            $c = new OFFER;
            $c->user_id = $user_id;
            $c->company_id = $company_id;
            $c->customer_id = $customer_id;
            $c->nomor = $new_nomor;
            $c->entry_at = $date;
            $c->name = $name;
            $c->hasil = $hasil;
            $c->note = $note;
            $c->amount = $amount;
            $c->status = "open";
            $c->model = $model;
            $c->save();

            $offer_id = $c->offer_id;
            if($model == 1){
                foreach ($offer_details as $key => $value) {
                    $create = new OFFERDETAIL;
                    $create->offer_id = $offer_id;
                    $create->item = $value->item;
                    $create->unit = $value->unit;
                    $create->vol = $value->vol;
                    $create->price = $value->price; 
                    $create->total = $value->total; 
                    $create->save(); 
                }
                
            }
            

            $ret = OFFER::where('offer_id',$offer_id)->with(['customer','offerDetails'])->first();
           
            \DB::commit();
            return $this->success(null, 200, $ret);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function store2(Request $request)
    { 
        $user_id = auth()->user()->user_id;
        $company_id = auth()->user()->company_id;
        $datacompany = COMPANY::where('company_id',$company_id)->first();
        $code_alpha = $datacompany->code_alpha;

        $code_offer = $this->codeOffer(new Request(['comp_codealpha'=>$code_alpha]));

        $nomor = $code_offer;
        $date = $request->date;
        $customer_id = $request->customer;
        $amount = $request->amount;
        $name = $request->name;
        $hasil = $request->hasil;
        $note = $request->note;
        

        $model = $request->model; 
        
       

        \DB::beginTransaction();
        try { 

            $c = new OFFER;
            $c->user_id = $user_id;
            $c->company_id = $company_id;
            $c->customer_id = $customer_id;
            $c->nomor = $nomor;
            $c->entry_at = $date;
            $c->name = $name;
            $c->hasil = $hasil;
            $c->note = $note;
            $c->amount = $amount;
            $c->status = "open";
            $c->model = $model;
            $c->save();

            $offer_id = $c->offer_id;
            
            

            $ret = OFFER::where('offer_id',$offer_id)->with(['customer','offerDetails'])->first();
            
            \DB::commit();
            return $this->success(null, 200, $ret);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function uploadattachmentmodel2(\Illuminate\Http\Request $request)
    {   
        $offer_id = $request->offer_id; 
        $files = $request->file('filemodel2');
        $extension = $request->file('filemodel2')->extension(); 
        $order= OFFER::where('offer_id',$offer_id)->first();
        $h = \Carbon\Carbon::now()->format('H');
        $i = \Carbon\Carbon::now()->format('i');
        $s = \Carbon\Carbon::now()->format('s');
        $date = \Carbon\Carbon::now()->format('Ymd');
        $val = rand(1,100);
        
        // //custom name masing2 file
        $filename = "OF-$offer_id-"."$date$h$i$s$val.$extension";
        $path = "public/penawaran/$offer_id";
        //upload file 
        if (!file_exists($path)) {
            // path does not exist deletefilerab
            Storage::makeDirectory($path);
            $files->storeAs($path,$filename);
        }else{
            $files->storeAs($path,$filename); 
        }

        $cekfile = Storage::files($path);
        return $cekfile;
    }

    public function getfile($id){
            $path = "public/penawaran/$id";  
          
            $cekfile = Storage::files($path);
            $ct = count($cekfile); 
            $allfile=[];
            for($i=0; $i < $ct ; $i++) {
                $name = $cekfile[$i];
                $total = strlen($name);
                $dt= substr($name , 7, $total);
                $allfile[$i]=$dt;   
            }
 
 
            $data = array(
                'status'=>"success",
                'message'=>"Data loaded successfully", 
                'fileatt'=>$allfile, 
            );
            return $data;
    }

    public function deletefile(\Illuminate\Http\Request $request)
    {   
        $dat = $request->pat;
        $path = "public/$dat";
        Storage::delete($path);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         
        $data = OFFER::where('offer_id',$id)->with(['customer','offerDetails'])->first();

        $array = array(
            'status' => 'success',
            'message' => 'Data loaded successfully',
            'data' =>  $data, 
        );
        return $array;
    }

    public function submit(Request $request, $id)
    {   
        $note = $request->note;
        \DB::beginTransaction();
        try {
            
            $array = array(
                'status'=>'pending',
                'note'=>$note,
            );
            OFFER::where('offer_id',$id)->update($array);
            
            \DB::commit();
            return $this->success("Submit Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

     public function submitrevisi(Request $request, $id)
    {   
        $note = $request->note;
        $date = $request->date;
        $customer = $request->customer;
        $name = $request->name;
        $hasil = $request->hasil;
        $amount = $request->amount;
        \DB::beginTransaction();
        try {
            
            $array = array(
                'entry_at'=>$date,
                'customer_id'=>$customer,
                'name'=>$name,
                'hasil'=>$hasil,
                'status'=>'pending',
                'note'=>$note,
            );
            OFFER::where('offer_id',$id)->update($array);
            
            \DB::commit();
            return $this->success("Submit Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function confirm($id)
    {
        \DB::beginTransaction();
        try {
            
            $array = array(
                'status'=>'complete'
            );
            OFFER::where('offer_id',$id)->update($array);
 
            \DB::commit();
            return $this->success("Confirm Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function reject($id)
    {
        \DB::beginTransaction();
        try {
            
            $array = array(
                'status'=>'rejected'
            );
            OFFER::where('offer_id',$id)->update($array);
            
            \DB::commit();
            return $this->success("Reject Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = auth()->user()->user_id;
        $company_id = auth()->user()->company_id;
        $date = $request->date;
        $customer_id = $request->customer;
        $name = $request->name;
        $hasil = $request->hasil; 
        $note = $request->note;  
        $amount = $request->amount;  
        $model = $request->model;  

        \DB::beginTransaction();
        try { 
 
            $array = array(
                'user_id'=>$user_id,
                'entry_at'=>$date,
                'name'=>$name,
                'hasil'=>$hasil,
                'note'=>$note,
                'amount'=>$amount,
                'model'=>$model,
            );
            OFFER::where('offer_id',$id)->update($array); 

            $ret = OFFER::where('offer_id',$id)->with(['customer','offerDetails'])->first();
            
            \DB::commit();
            return $this->success(null, 200, $ret);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            OFFERDETAIL::where('offer_id',$id)->delete(); 
            OFFER::where('offer_id',$id)->delete(); 
           
            
            \DB::commit();
            return $this->success("Delete Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    public function codeOffer(Request $request)
    {
        $now = \Carbon\Carbon::now();
        $format = 'OFFER-[CV]-[MM][YY]-[NO]-[RV]';
        // =====
        $codealpha = $request->has('comp_codealpha') ? $request->comp_codealpha : null;
        $coderev = $request->has('offer_coderev') ? $request->offer_coderev : null;
        // =====
        $code_CV = $codealpha;
        if ($coderev) {
            preg_match('/OFFER-'.$code_CV.'-(\d{2})(20|21)-(\d{3})-(\d+)/', $coderev, $match);
            $code_MM = $match[1];
            $code_YY = $match[2];
            $code_NO = $match[3];
            $code_RV = $match[4] + 1;
            return str_replace(
                array('[CV]', '[MM]', '[YY]', '[NO]', '[RV]'),
                array($code_CV, $code_MM, $code_YY, $code_NO, $code_RV),
                $format
            );
        }
        $code_MM = $now->format('m');
        $code_YY = $now->format('y');
        $noUrut = OFFER::whereMonth('created_at', $now->format('m'))->orderBy('offer_id', 'DESC')->get();
        $noUrut = $noUrut->where('company.code_alpha', $codealpha)->flatten();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('nomor')->first();
            preg_match('/OFFER-'.$code_CV.'-(\d{2})(20|21)-(\d{3})/', $noUrut, $match);
            
            $noUrut = $match[3] + 1;
            $noUrut = str_pad($noUrut, 3, '0', STR_PAD_LEFT);
        } else {
            $noUrut = str_pad(1, 3, '0', STR_PAD_LEFT);
        }
        $code_NO = $noUrut;
        $code_RV = 0;
        
        return str_replace(
            array('[CV]', '[MM]', '[YY]', '[NO]', '[RV]'),
            array($code_CV, $code_MM, $code_YY, $code_NO, $code_RV),
            $format
        );
    }
}
