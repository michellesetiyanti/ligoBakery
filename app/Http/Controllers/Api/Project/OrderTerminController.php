<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\OrderTermin as OrderTerminRequest;
use App\Repositories\Master\Customer as CustomerRepository;
use App\Repositories\Project\Order as OrderRepository;
use App\Repositories\Project\OrderTermin as OrderTerminRepository;
use App\Models\Project\Order as ORDER;
use App\Models\Project\OrderTermin as TERMIN;

class OrderTerminController extends ApiController
{
    protected $customer;
    protected $order;
    protected $termin;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->customer = new CustomerRepository;
        $this->order = new OrderRepository;
        $this->termin = new OrderTerminRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        $termin = $this->termin->with(['order', 'orderTerminDetails'])->all($request->all());
        return $this->success(null, 200, $termin);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  OrderTerminRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\Illuminate\Http\Request $request)
    {
        \DB::beginTransaction();
        try {
            $company_id = auth()->user()->company_id;
            $order_id = $request->order_id;
            $amountTermin = $request->amountTermin;

            $order= ORDER::where('order_id',$order_id)->where('company_id',$company_id)->first();
            $total = $order->total_project;
            $codewo = $order->work_order;

            $cektotal = TERMIN::where("order_id",$order_id)->sum('amount');
            $cekjml = TERMIN::where("order_id",$order_id)->count();
            $cekjml = $cekjml+1;
            $termin = "Termin-$cekjml";
 
            if($cek > $total){
                return $this->fail("Post Failed", 500, "Total Biaya Yang Di Tagih Melebihi Total Nilai Project"); 
            }

            $term = new TERMIN;
            $term->order_id = $order_id;
            $term->name = $termin;
            $term->amount = $amountTermin;
            $term->termin_total = $amountTermin;
            $term->balance = $amountTermin;
            $term->save();

            $data = TERMIN::where('order_id',$order_id)->get();
            
            $array = array(
                'status'=>'success',
                'message'=>'Data loaded successfully',
                'data'=> $data,
                'total'=>  $cek,
        );

            \DB::commit();
            return $array;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($termin = $this->termin->with(['orderTerminDetails', 'order'])->show($id)) {
            return $this->success(null, 200, $termin);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  OrderTerminRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\Illuminate\Http\Request $request, $id)
    {

        \DB::beginTransaction();
        try {

            $amount = $request->amount;
            $name = $request->name;

            $data = TERMIN::where('order_t_id',$id)->first();
            $order_id= $data->order_id;
            
            $ar = array(
                'name'=>$name,
                'amount'=>$amount,
                'balance'=>$amount,
            );

            TERMIN::where('order_t_id',$id)->update($ar);

            $cektotal = TERMIN::where("order_id",$order_id)->sum('amount');
            $cektotaldetail  = TERMIN::where("order_id",$order_id)->where('paid','=',0)->sum('amount_detail');

            $cektotal = $cektotal + $cektotaldetail;
            $data = TERMIN::where('order_id',$order_id)->get();
            $ar = array(
                'status'=>"success",
                'message'=>"Data loaded successfully",
                'data'=>$data,
                'total_termin'=>$cektotal,
            );
           
            \DB::commit();
            
            return $ar;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function pay(\Illuminate\Http\Request $request, $id){
        \DB::beginTransaction();
        try {
            $company_id = auth()->user()->company_id;
            $coa_code = $request->coa_code;
            $date = $request->date;

            $data = TERMIN::where('order_t_id',$id)->with(['order'])->first();
            $name= $data->name;
            $order_id= $data->order_id;
            $termin_total = $data->termin_total;
            $balance = $termin_total - $termin_total;
            $code_spk = $data->order->code_spk;
            $description= "Recieve Payment $code_spk $name";

            $cekpost = postgl($company_id, $coa_code, $code_spk, $description, $termin_total);
            if($cekpost['status'] =="failed"){
                    return $this->fail("Post Failed", 500, $cekpost['errors']);
                }

            $ar = array(
                'date_paid'=>$date,
                'paid'=>$termin_total,
                'balance'=>$balance,
            );

            TERMIN::where('order_t_id',$id)->update($ar);
            
            $cekadendum =  TERMIN::where("order_id",$order_id)->where('balance','!=',0)->count();
            if($cekadendum < 3){
                $adendum = "true";
            }else{
                $adendum = "false"; 
            }   

            $paid = TERMIN::where("order_id",$order_id)->where('balance','=',0)->sum('paid');
            $update= array(
                'paid'=>$paid,
                'adendum'=>$adendum,
            );
            ORDER::where('order_id',$order_id)->update($update);

            $cektotal = TERMIN::where("order_id",$order_id)->where('paid','=',0)->sum('amount');
            $cektotaldetail  = TERMIN::where("order_id",$order_id)->where('paid','=',0)->sum('amount_detail');

            $cektotal = $cektotal + $cektotaldetail;

            $data = TERMIN::where('order_id',$order_id)->get();
            $ar = array(
                'status'=>"success",
                'message'=>"Data loaded successfully",
                'data'=>$data,
                'total_termin'=>$cektotal,
            );
           
            \DB::commit();
            
            return $ar;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            $data = TERMIN::where('order_t_id',$id)->first();
            $order_id= $data->order_id;

            $termin = TERMIN::where('order_t_id',$id)->delete();

            $cektotal = TERMIN::where("order_id",$order_id)->sum('amount');
            $data = TERMIN::where('order_id',$order_id)->get();
            $ar = array(
                'status'=>"success",
                'message'=>"Data loaded successfully",
                'data'=>$data,
                'total_termin'=>$cektotal,
            );
            \DB::commit();
            return $ar;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    
    public function codeInv(\Illuminate\Http\Request $request)
    {
        $now = \Carbon\Carbon::now();
        $mOrder = $this->order->getModel();
        $mTermin = $this->termin->getModel();
        $format = 'INV [CV]-[NO] %lpar%[WO]%rpar% [TN].[TT]-[MM]/[YY]';
        // =====
        $wo = $request->has('code_wo') ? $request->code_wo : null;
        $term_name = $request->has('term_name') ? $request->term_name : null;
        // =====
        $order = $mOrder->where('work_order', $wo)->first();
        $termin = $mTermin->where('order_id', $order->order_id)->get();

        $code_CV = $order->company->code_alpha;
        $code_MM = $now->format('m');
        $code_YY = $now->format('y');
        $code_WO = $wo;

        $term_numb = substr($term_name, 7,1);

        $code_TN = str_pad($term_numb, 2, '0', STR_PAD_LEFT);
        $code_TT = str_pad($termin->count(), 2, '0', STR_PAD_LEFT);
        $noUrut = $order->orderTermins->where('order.company_id', $order->company_id)->where('inv_code','!=',NULL)->where('created_at.month', $now->format('m'))->flatten();
        $noUrut = $noUrut->count() + 1;
        $code_NO = str_pad($noUrut, strlen($noUrut) > 3 ? strlen($noUrut) : 3, '0', STR_PAD_LEFT);
        
        return str_replace(
            array('[NO]', '[CV]', '[WO]', '[TN]', '[TT]', '[MM]', '[YY]', '%lpar%', '%rpar%'),
            array($code_NO, $code_CV, $code_WO, $code_TN, $code_TT, $code_MM, $code_YY, '[', ']'),
            $format
        );
    }

    public function updatecodeinv (\Illuminate\Http\Request $request ){
        $company_id = auth()->user()->company_id;
        $order_t_id = $request->order_t_id;
        $now = \Carbon\Carbon::now()->format('Y-m-d');

        $data = TERMIN::where('order_t_id',$order_t_id)->first();
        $order_id= $data->order_id;
        $termin_name= $data->name;
        $cekcode= $data->inv_code;

        $order= ORDER::where('order_id',$order_id)->where('company_id',$company_id)->first();
        $codewo = $order->work_order;

        \DB::beginTransaction();
        try {
            if($cekcode == null){
                $code_inv = $this->codeInv(new \Illuminate\Http\Request(['code_wo'=>$codewo ,'term_name'=>$termin_name]));

                    $datainv = array(
                        'inv_code'=>$code_inv,
                        'inv_issued'=>$now
                    );
                TERMIN::where('order_t_id',$order_t_id)->update($datainv);
            }
            
            $ar = array(
                'status'=>"success", 
            );
            
            \DB::commit();
            return $ar;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
