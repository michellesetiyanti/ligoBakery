<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\Ro as RoRequest;
use App\Repositories\Master\Product as ProductRepository;
use App\Repositories\Master\Warehouse as WarehouseRepository;
use App\Repositories\Pembelian\Pembelian as PembelianRepository;
use App\Repositories\Pembelian\PoProject as PoProjectRepository;
use App\Repositories\Penjualan\Penjualan as PenjualanRepository;
use App\Repositories\Project\Estimasi as EstimasiRepository;
use App\Repositories\Project\Order as OrderRepository;

class RoController extends ApiController
{
    protected $estimasi;
    protected $order;
    protected $pembelian;
    protected $penjualan;
    protected $product;
    protected $po;
    protected $warehouse;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->estimasi = new EstimasiRepository;
        $this->order = new OrderRepository;
        $this->pembelian = new PembelianRepository;
        $this->penjualan = new PenjualanRepository;
        $this->product = new ProductRepository;
        $this->po = new PoProjectRepository;
        $this->warehouse = new WarehouseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $po = $this->po->with(['poProjectDetails', 'order', 'poPembelian', 'pembelian', 'penjualan', 'user', 'warehouse'])->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $po);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoRequest $request)
    {
        if (empty(auth()->user()->company->coa_pembelian) || empty(auth()->user()->company->coa_hutang) || empty(auth()->user()->company->coa_penjualan) || empty(auth()->user()->company->coa_piutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $warehouse = $this->warehouse->show($request->wh_id);
        $order = $this->order->all()->where('code_spk', $request->code_spk)->first();
        $est = $order->estimasi->where('wh_id', $request->wh_id)->where('approved', 'complete')->flatten();
        if ($est->isEmpty()) {
            return $this->fail('Please complete estimation', 422);
        }
        $model = $this->po->getModel();
        \DB::beginTransaction();
        try {
            $code = $request->missing('code') || $request->code == 'null' ? null : $request->code;
            if (empty($code)) {
                $code = $this->codeRo($request->code_spk);
            }
            $po = $model->where('code', $code)->first();
            if (empty($po)) {
                $po = $model->create($request->merge([
                    'code' => $code,
                    'total' => $request->ongkir + $request->biaya_lain,
                ])->except(['supplier_id', 'details']));
            }
            $po->update($request->merge([
                'total' => $po->poProjectDetails->sum('amount') + $request->ongkir + $request->biaya_lain,
                'status' => 'pending',
            ])->except(['code', 'supplier_id', 'details']));
            \DB::commit();
            return $this->success(null, 200, $po);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($po = $this->po->with(['poProjectDetails', 'order', 'poPembelian', 'pembelian', 'penjualan', 'user', 'warehouse'])->show($id)) {
            return $this->success(null, 200, $po);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RoRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoRequest $request, $id)
    {
        if (empty(auth()->user()->company->coa_pembelian) || empty(auth()->user()->company->coa_hutang) || empty(auth()->user()->company->coa_penjualan) || empty(auth()->user()->company->coa_piutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $po = $this->po->show($id);
        \DB::beginTransaction();
        try {
            $po->update($request->merge([
                'total' => $po->poProjectDetails->sum('amount') + $request->ongkir + $request->biaya_lain,
                'status' => 'pending',
            ])->except(['code', 'supplier_id', 'details']));
            \DB::commit();
            return $this->success(null, 200, $po);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $po = $this->po->show($id);
        \DB::beginTransaction();
        try {
            if (in_array($po->status, ['close'])) {
                $po->update(['status' => 'void']);
            } else {
                $po->delete();
            }
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Do Action the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function doAction(\Illuminate\Http\Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'details' => 'sometimes|required|array',
            'details.*.id' => 'integer|exists:po_project_details,po_pro_d_id',
            'execute' => 'required|string|in:rejected,void,approved',
            'rejected' => 'sometimes|required|boolean',
            'notes' => 'sometimes|required|string',
            'approved' => 'sometimes|required|array',
            'approved.to_termin' => 'sometimes|nullable|string',
            'approved.payment' => 'sometimes|required|string|in:cash,credit',
            'approved.due_date' => 'sometimes|nullable|required_if:approved.payment,credit',
            'approved.coa_code' => 'sometimes|nullable|string|required_if:approved.payment,cash',
            'approved.ongkir' => 'sometimes|nullable|digits_between:1,20',
            'approved.ongkir_desc' => 'sometimes|nullable|string',
            'approved.biaya_lain' => 'sometimes|nullable|digits_between:1,20',
            'approved.biaya_lain_desc' => 'sometimes|nullable|string',
            'signed' => 'sometimes|nullable|array',
        ]);
        if ($validator->fails()) {
            return $this->fail('fail_validation', 422, $validator->errors());
        }
        if (empty(auth()->user()->company->coa_pembelian) || empty(auth()->user()->company->coa_hutang) || empty(auth()->user()->company->coa_penjualan) || empty(auth()->user()->company->coa_piutang)) {
            return $this->fail('license_setup', 422, auth()->user()->company);
        }
        $po = $this->po->show($id);
        \DB::beginTransaction();
        try {
            $ids = [];
            if ($request->details) {
                foreach ($request->details as $detail) {
                    array_push($ids, $detail['id']);
                }
            }
            switch ($request->execute) {
                case 'rejected':
                    if ($request->has('rejected')) {
                        $status = $request->boolean('rejected') ? 'reject' : 'open';
                        $po->update(['status' => $status, 'notes' => $request->notes]);
                    } else {
                        $errors = $validator->errors()->add('rejected', trans('validation.required', ['attribute' => 'rejected']));
                        return $this->fail('fail_validation', 422, $errors);
                    }
                break;
                case 'void':
                    if (in_array($po->status, ['open', 'pending'])) {
                        $po->delete();
                    } else {
                        $po->update(['status' => 'void', 'notes' => $request->notes]);
                    }
                break;
                case 'approved':
                    $approved = $request->approved;
                    if (!in_array($po->status, ['pending'])) {
                        return $this->fail('fail_status', 422, ['status' => $po->status]);
                    }
                    $po->poProjectDetails()->whereNotIn('po_pro_d_id', $ids)->delete();
                    $signed = $request->input('signed', null);
                    $ongkir = $request->input('approved.ongkir', $po->ongkir);
                    $ongkir_desc = $request->input('approved.ongkir_desc') == 'null' ? null : $request->input('approved.ongkir_desc');
                    $biaya_lain = $request->input('approved.biaya_lain', $po->biaya_lain);
                    $biaya_lain_desc = $request->input('approved.biaya_lain_desc') == 'null' ? null : $request->input('approved.biaya_lain_desc');
                    $po->update([
                        'ongkir' => $ongkir,
                        'ongkir_desc' => $ongkir_desc,
                        'biaya_lain' => $biaya_lain,
                        'biaya_lain_desc' => $biaya_lain_desc,
                        'total' => $po->poProjectDetails()->sum('amount') + ($ongkir + $biaya_lain),
                        'status' => 'close',
                        'approved_at' => \Carbon\Carbon::now()->toDateString(),
                        'signed' => $signed,
                    ]);
                    $payment = $request->input('approved.payment', 'credit');
                    $due_date = $request->input('approved.due_date', 40);
                    $due_date = $due_date && $due_date == 'null' ? null : $due_date;
                    $coa_code = $request->input('approved.coa_code', null);
                    $coa_code = $coa_code && $coa_code == 'null' ? null : $coa_code;
                    app(\App\Http\Controllers\Api\Pembelian\Project\PoPembelianController::class)->store(new \Illuminate\Http\Request([
                        'po_pro_id' => $po->po_pro_id,
                        'company_id' => $po->company_id,
                        'wh_id' => $po->company->warehouse->pluck('wh_id')->first(),
                        'code_spk' => $po->code_spk,
                        'ongkir' => $po->ongkir,
                        'ongkir_desc' => $po->ongkir_desc,
                        'biaya_lain' => $po->biaya_lain,
                        'biaya_lain_desc' => $po->biaya_lain_desc,
                        'total' => $po->total,
                        'payment' => $payment,
                        'due_date' => $due_date,
                        'coa_code' => $coa_code,
                        'details' => $po->poProjectDetails,
                        'to_termin' => $request->input('approved.to_termin', null),
                    ]));
                    app(\App\Http\Controllers\Api\Penjualan\Project\PenjualanController::class)->store(new \Illuminate\Http\Request([
                        'po_pro_id' => $po->po_pro_id,
                        'company_id' => $po->warehouse->company_id,
                        'wh_id' => $po->wh_id,
                        'code_spk' => $po->code_spk,
                        'ongkir' => $po->ongkir,
                        'ongkir_desc' => $po->ongkir_desc,
                        'biaya_lain' => $po->biaya_lain,
                        'biaya_lain_desc' => $po->biaya_lain_desc,
                        'total' => $po->total,
                        'payment' => $payment,
                        'due_date' => $due_date,
                        'coa_code' => $coa_code,
                        'details' => $po->poProjectDetails,
                    ]));
                    // Response Json
                    $message = 'RO has been approved';
                    $response = ['status' => $po->status];
                break;
            }
            \DB::commit();
            return $this->success($message ?? null, 200, $response ?? []);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    
    public function codeRo(array $request, $id = 0)
    {
        $now = \Carbon\Carbon::now();
        $auth = auth()->user();
        $model = $this->po->getModel();
        $format = 'RO-[CV] [NO] %lpar%[WO]%rpar% [RO]-[MM]/[YY]';
        // =====
        $spk = isset($request['code_spk']) ? $request['code_spk'] : null;
        // =====
        $order = $this->order->getModel()->where('code_spk', $spk)->first();

        $code_WO = $order->work_order;
        $code_CV = $auth->company->code_alpha ?? str_pad($auth->company_id, 5, '0', STR_PAD_LEFT);
        $code_RO = $model->where('company_id', $auth->company_id)->where('code_spk', $spk)->count() + 1;
        $code_RO = str_pad($code_RO, 2, '0', STR_PAD_LEFT);
        $code_MM = $now->format('m');
        $code_YY = $now->format('y');
        $noUrut = $model->where('company_id', $auth->company_id)->where('po_pro_id', '<>', $id)->whereMonth('created_at', $now->format('m'))->orderBy('po_pro_id', 'DESC')->get();
        if ($noUrut->count() > 0) {
            $noUrut = $noUrut->pluck('code')->first();
            preg_match('/RO-'.$code_CV.' (\d{3})/', $noUrut, $match);
            $noUrut = $match[1] + 1;
            $noUrut = str_pad($noUrut, 3, '0', STR_PAD_LEFT);
        } else {
            $noUrut = str_pad(1, 3, '0', STR_PAD_LEFT);
        }
        $code_NO = $noUrut;
        
        return str_replace(
            array('[NO]', '[CV]', '[WO]', '[RO]', '[MM]', '[YY]', '%lpar%', '%rpar%'),
            array($code_NO, $code_CV, $code_WO, $code_RO, $code_MM, $code_YY, '[', ']'),
            $format
        );
    }
    
    public function osEst(array $request)
    {
        // =====
        $spk = isset($request['code_spk']) ? $request['code_spk'] : null;
        $prod = isset($request['product_id']) ? $request['product_id'] : null;
        // =====
        $order = $this->order->getModel()->where('code_spk', $spk)->first();
        $po = $this->po->getModel()->where('code_spk', $spk)->where('status', 'close')->get();
        $os = collect();
        foreach ($order->limitOrders as $lo) {
            $poProdId = $po->map->poProjectDetails->map->where('product_id', $lo->product_id)->flatten();
            $args = collect($lo)->replace([
                'qty_lg' => $lo->qty_lg - $poProdId->sum('qty_lg'),
                'qty_sm' => $lo->qty_sm - $poProdId->sum('qty_sm'),
            ]);
            $os->push($args);
        }
        if ($prod) {
            return $os->where('product_id', $prod);
        }
        return $os;
    }
}
