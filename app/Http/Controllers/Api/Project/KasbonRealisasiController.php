<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Repositories\Finance\Kasbon as KasbonRepository;
use App\Repositories\Finance\KasbonRealisasi as KasbonRealisasiRepository;
use App\Repositories\Project\Order as OrderRepository;
use Illuminate\Http\Request;

class KasbonRealisasiController extends ApiController
{
    protected $kasbon;
    protected $kasbonRealisasi;
    protected $order;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->kasbon = new KasbonRepository;
        $this->kasbonRealisasi = new KasbonRealisasiRepository;
        $this->order = new OrderRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $kasbonRealisasi = $this->kasbonRealisasi->with('kasbon')->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $kasbonRealisasi);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'kasbon_id' => 'required|integer|exists:kasbon,kasbon_id',
            'name' => 'required|string',
            'qty' => 'required|integer',
            'amount' => 'required|digits_between:1,20',
            'ref' => 'nullable|string',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $kasbon = $this->kasbon->show($request->kasbon_id);
        \DB::beginTransaction();
        try {
            $totalAmount = $kasbon->kasbonRealisasi->sum('amount') + $request->amount;
            if ($totalAmount > $kasbon->nominal) {
                return $this->fail('over limit', 422, [
                    'limit' => $kasbon->nominal - $totalAmount
                ]);
            }
            $kasbonRealisasi = $kasbon->kasbonRealisasi()->create($request->all());
            \DB::commit();
            return $this->success(null, 200, $kasbonRealisasi);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($kasbonRealisasi = $this->kasbonRealisasi->with('kasbon')->show($id)) {
            return $this->success(null, 200, $kasbonRealisasi);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'kasbon_id' => 'required|integer|exists:kasbon,kasbon_id',
            'name' => 'required|string',
            'qty' => 'required|integer',
            'amount' => 'required|digits_between:1,20',
            'ref' => 'nullable|string',
        ]);
        if ($validator->fails()) {
            return $this->fail(null, 422, $validator->errors());
        }
        $kasbon = $this->kasbon->show($request->kasbon_id);
        $kasbonRealisasi = $kasbon->kasbonRealisasi->where('kasbon_r_id', $id)->first();
        if (in_array($kasbonRealisasi->status, ['close'])) {
            return $this->fail('Realisasi status is '.$kasbonRealisasi->status, 422);
        }
        \DB::beginTransaction();
        try {
            $totalAmount = $kasbon->kasbonRealisasi->sum('amount') - $kasbonRealisasi->amount;
            $totalAmount+= $request->amount;
            if ($totalAmount > $kasbon->nominal) {
                return $this->fail('over limit', 422, [
                    'limit' => $kasbon->nominal - $totalAmount
                ]);
            }
            $kasbonRealisasi->update($request->all());
            \DB::commit();
            return $this->success(null, 200, $kasbonRealisasi);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kasbonRealisasi = $this->kasbonRealisasi->show($id);
        if (!in_array($kasbonRealisasi->status, ['open'])) {
            return $this->fail('Realisasi status is '.$kasbonRealisasi->status, 422);
        }
        \DB::beginTransaction();
        try {
            $kasbonRealisasi->delete();
            \DB::commit();
            return $this->success(null, 200);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
