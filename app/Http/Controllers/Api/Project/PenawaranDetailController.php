<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use Illuminate\Http\Request;
use App\Models\Project\Offer as OFFER;
use App\Models\Project\OfferDetail as OFFERDETAIl;


class PenawaranDetailController extends ApiController
{
    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth'); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
   
    }

    public function indexpending()
    {
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $user_id = auth()->user()->user_id;
        $company_id = auth()->user()->company_id;
        
        $offer_id = $request->offer_id;
        $item = $request->item;
        $unit = $request->unit;
        $vol = $request->vol;
        $price = $request->price;
        $total = $vol * $price;


        \DB::beginTransaction();
        try {  

            $create = new OFFERDETAIL;
            $create->offer_id = $offer_id;
            $create->item = $item;
            $create->unit = $unit;
            $create->vol = $vol;
            $create->price = $price; 
            $create->total = $total; 
            $create->save(); 

            $ret = OFFER::where('offer_id',$offer_id)->with(['customer','offerDetails'])->first();
            
            \DB::commit();
            return $this->success(null, 200, $ret);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
    }

   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_id = auth()->user()->user_id;
        $company_id = auth()->user()->company_id;
        
        $item = $request->item;
        $unit = $request->unit;
        $vol = $request->vol;
        $price = $request->price;
        $total = $vol * $price;


        \DB::beginTransaction();
        try {  
 
            $update = array(
                'item'=>$item,
                'unit'=>$unit,
                'vol'=>$vol,
                'price'=>$price,
                'total'=>$total,
            );
            OFFERDETAIL::where('offer_d_id',$id)->update($update);

            $data = OFFERDETAIL::where('offer_d_id',$id)->first();
            $offer_id = $data->offer_id;

            $total = OFFERDETAIL::where('offer_id',$offer_id)->sum('total');
            $of = array('amount'=>$total);
            OFFER::where('offer_id',$offer_id)->update($of);

            $ret = OFFER::where('offer_id',$offer_id)->with(['customer','offerDetails'])->first();
            
            \DB::commit();
            return $this->success(null, 200, $ret);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::beginTransaction();
        try {
            $data = OFFERDETAIL::where('offer_d_id',$id)->first();
            $offer_id = $data->offer_id;

            OFFERDETAIL::where('offer_d_id',$id)->delete();  

            $total = OFFERDETAIL::where('offer_id',$offer_id)->sum('total');
            $of = array('amount'=>$total);
            OFFER::where('offer_id',$offer_id)->update($of);

            $ret = OFFER::where('offer_id',$offer_id)->with(['customer','offerDetails'])->first();

            
            \DB::commit();
            return $this->success("Delete Success", 200, $ret);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
