<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\Estimasi as EstimasiRequest;
use App\Repositories\Master\Product as ProductRepository;
use App\Repositories\Project\Estimasi as EstimasiRepository;
use App\Repositories\Project\EstimasiDetail as EstimasiDetailRepository;
use App\Repositories\Project\Order as OrderRepository;
use App\Models\Project\Estimasi as ESTIMASI;
use App\Models\Project\EstimasiDetail as ESTIMASIDETAIL;
use App\Models\Master\Product as PRODUCT;
use App\Models\Master\ProductDetail as PRODUCTDETAIL;

class EstimasiDetailController extends ApiController
{
    protected $estimasi;
    protected $estimasiDetail;
    protected $order;
    protected $product;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->estimasi = new EstimasiRepository;
        $this->estimasiDetail = new EstimasiDetailRepository;
        $this->order = new OrderRepository;
        $this->product = new ProductRepository;
    }
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $estimasi = $this->estimasi->all(compact('filter'));
        $filterDetail = 'estimasi_id|'.implode(':', $estimasi->pluck('estimasi_id')->toArray());
        $filterDetail = $request->filter ? $request->filter : $filterDetail;
        $estimasiDetail = $this->estimasiDetail->all($request->merge([
            'filter' => $filterDetail
        ])->all());
        return $this->success(null, 200, $estimasiDetail);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EstimasiRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\Illuminate\Http\Request $request)
    {
        \DB::beginTransaction();
        try {
            $estimasi_id = $request->estimasi_id;
            $product_id = $request->product_id;
            $qty_lg = $request->qty_lg;
            $qty_lg = 0;
            $qty_sm = $request->qty_sm;
            

            $dataproduct = PRODUCT::where('product_id',$product_id)->first();
            $unit_qty = $dataproduct->unit_qty;
            $unit_lg = $dataproduct->unit_lg;
            $unit_sm = $dataproduct->unit_sm;
            $coun = PRODUCTDETAIL::where('product_id',$product_id)->where('status','!=','penjualan')->orderBy('product_d_id','DESC')->count();
            if($coun > 0){
                $dataproductdetail= PRODUCTDETAIL::where('product_id',$product_id)->where('status','!=','penjualan')->where('cur_qty_sm','!=',0)->orderBy('product_d_id','DESC')->first();
                $price_lg = $dataproductdetail->price_lg;
                $price_sm = $dataproductdetail->price_sm;
            }else{
                $price_lg = $dataproduct->price_lg;
                $price_sm = $dataproduct->price_sm;   
            }

            // $qty_sm_m = ($qty_sm % $unit_qty); 
            // $qty_lg = (($qty_sm - $qty_sm_m)/$unit_qty) + $qty_lg;

             
            $amount = ($qty_sm*$price_sm);

            $cek = ESTIMASIDETAIL::where('product_id',$product_id)->where('estimasi_id',$estimasi_id)->count();
            if($cek>0){

                $detaildata = ESTIMASIDETAIL::where('product_id',$product_id)->where('estimasi_id',$estimasi_id)->first();
                $estimasi_d_id = $detaildata->estimasi_d_id;
                $oldqty_lg = $detaildata->qty_lg;
                $oldqty_sm = $detaildata->qty_sm;

                // $totalqty_sm = ($oldqty_sm + $qty_sm) % $unit_qty;
                $totalqty_sm = ($oldqty_sm + $qty_sm) ;
                // $totalqty_lg = ((($oldqty_sm + $qty_sm) - $totalqty_sm)/$unit_qty) + $oldqty_lg + $qty_lg;

               
                $totalamount =  ($totalqty_sm*$price_sm);

                $update = array(
                    'qty_lg'=>$totalqty_lg,
                    'qty_sm'=>$totalqty_sm,
                    'amount'=>$totalamount,
                );
                ESTIMASIDETAIL::where('estimasi_d_id',$estimasi_d_id)->update($update); 

            }else{
                $detail = new ESTIMASIDETAIL;
                $detail->estimasi_id = $estimasi_id;
                $detail->product_id = $product_id;
                $detail->unit_lg = $unit_lg;
                $detail->unit_sm = $unit_sm;
                $detail->qty_lg = $qty_lg;
                $detail->qty_sm = $qty_sm;
                $detail->price_lg = $price_lg;
                $detail->price_sm = $price_sm;
                $detail->amount = $amount; 
                $detail->save();
            } 

            $total = ESTIMASIDETAIL::where('estimasi_id',$estimasi_id)->sum('amount');

            $update = array('total'=>$total);
            ESTIMASI::where('estimasi_id',$estimasi_id)->update($update);

            $estimasi = ESTIMASIDETAIL::where('estimasi_id',$estimasi_id)->with(['product'])->get(); 

            $array = array(
                'status'=>'Success',
                'message'=>'Success Create Estimasi',
                'data'=>$estimasi,
                'total'=>$total,
            ); 

            \DB::commit();
            return $array;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($estimasiDetail = $this->estimasiDetail->with(['product', 'estimasi'])->show($id)) {
            return $this->success(null, 200, $estimasiDetail);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EstimasiRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\Illuminate\Http\Request $request, $id)
    {
         
        \DB::beginTransaction();
        try {
            $data = ESTIMASIDETAIL::where('estimasi_d_id',$id)->first();
            $estimasi_id = $data->estimasi_id;
            $product_id = $data->product_id;
            $qty_lg = $request->qty_lg;
            $qty_sm = $request->qty_sm;

            $dataproduct = PRODUCT::where('product_id',$product_id)->first();
            $unit_qty = $dataproduct->unit_qty; 

            $coun = PRODUCTDETAIL::where('product_id',$product_id)->orderBy('product_d_id','DESC')->count();
            if($coun > 0){
                $dataproductdetail= PRODUCTDETAIL::where('product_id',$product_id)->orderBy('product_d_id','DESC')->first();
                $price_lg = $dataproductdetail->price_lg;
                $price_sm = $dataproductdetail->price_sm;
            }else{
                $price_lg = $dataproduct->price_lg;
                $price_sm = $dataproduct->price_sm;   
            }

            // $qty_sm_m = ($qty_sm % $unit_qty); 
            // $qty_lg = (($qty_sm - $qty_sm_m)/$unit_qty) + $qty_lg;
            // $amount = ($qty_lg*$price_lg) + ($qty_sm_m*$price_sm); 
            $qty_lg = 0;
            $amount =  ($qty_sm*$price_sm); 

            $update = array(
                'qty_lg'=>$qty_lg,
                'qty_sm'=>$qty_sm,
                'price_lg'=>$price_lg,
                'price_sm'=>$price_sm,
                'amount'=>$amount,
            );

            ESTIMASIDETAIL::where('estimasi_d_id',$id)->update($update);

            $total = ESTIMASIDETAIL::where('estimasi_id',$estimasi_id)->sum('amount');

            $update = array('total'=>$total);
            ESTIMASI::where('estimasi_id',$estimasi_id)->update($update);

            $estimasi = ESTIMASIDETAIL::where('estimasi_id',$estimasi_id)->with(['product'])->get(); 

            $array = array(
                'status'=>'Success',
                'message'=>'Success Update Estimasi Detail',
                'data'=>$estimasi,
                'total'=>$total,
            ); 

            \DB::commit();
            return $array;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estimasiDetail = $this->estimasiDetail->with('estimasi')->show($id);
        \DB::beginTransaction();
        try {
            $estimasi_id = $estimasiDetail->estimasi_id;

            $estimasiDetail->delete();

            $total = ESTIMASIDETAIL::where('estimasi_id',$estimasi_id)->sum('amount');

            $update = array('total'=>$total);
            ESTIMASI::where('estimasi_id',$estimasi_id)->update($update);

            $estimasi = ESTIMASIDETAIL::where('estimasi_id',$estimasi_id)->with(['product'])->get(); 

            $array = array(
                'status'=>'Success',
                'message'=>'Success Delete Estimasi Detail',
                'data'=>$estimasi,
                'total'=>$total,
            ); 

            \DB::commit();
            return $array;
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
