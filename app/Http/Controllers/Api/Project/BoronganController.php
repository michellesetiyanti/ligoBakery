<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\Borongan as BoronganRequest;
use App\Repositories\Finance\UpahBorongan as UpahBoronganRepository;
use App\Repositories\Project\Borongan as BoronganRepository;
use App\Repositories\Project\Order as OrderRepository;
use App\Models\Project\Order as ORDER;
use App\Models\Project\Borongan as BORONGAN;
use App\Models\Project\BoronganKerja as BORONGANDETAIL;

class BoronganController extends ApiController
{
    protected $borongan;
    protected $order;
    protected $upahBorongan;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->borongan = new BoronganRepository;
        $this->order = new OrderRepository;
        $this->upahBorongan = new UpahBoronganRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $borongan = $this->borongan->with(['borongan_details', 'order', 'vendor'])->all($request->merge(compact('filter'))->all());
        return $this->success(null, 200, $borongan);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BoronganRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\Illuminate\Http\Request $request)
    {
        \DB::beginTransaction();
        try {
            
            $order_id = $request->order_id;
            $vendor_id = $request->vendor_id;
            $date = $request->date;
            $description = $request->description;
            $totalnilai = $request->totalnilai;
            
            $amount = $request->amount;
            $progress = $request->progress;
            $description2 = $request->description2;

            $cek = BORONGAN::where('order_id',$order_id)->count();

            $brg = new BORONGAN;
            $brg->vendor_id = $vendor_id;
            $brg->order_id = $order_id;
            $brg->description = $description;
            $brg->nominal = $totalnilai;
            $brg->balance = $amount;
            $brg->entry_at = $date;
            $brg->status = 'open';
            $brg->save();

            $borongan_id = $brg->borongan_id;

            $brgdetail = new BORONGANDETAIL;
            $brgdetail->borongan_id = $borongan_id;
            $brgdetail->amount = $amount;
            $brgdetail->progress = $progress;
            $brgdetail->description = $description2;
            $brgdetail->save();

            $cek = BORONGAN::where('borongan_id',$borongan_id)->with(['borongan_details','order','vendor'])->first();

         
            \DB::commit();
            return $this->success(null, 200, $cek);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        
        if ($borongan = $this->borongan->with(['borongan_details', 'vendor', 'order'])->show($id)) {
            return $this->success(null, 200, $borongan);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BoronganRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\Illuminate\Http\Request $request, $id)
    {
        \DB::beginTransaction();
        try {
             
            $order_id = $request->order_id;
            $vendor_id = $request->vendor_id;
            $date = $request->date;
            $description = $request->description;
            $totalnilai = $request->totalnilai;

            $data = array(
                'vendor_id'=>$vendor_id,
                'order_id'=>$order_id,
                'entry_at'=>$date,
                'description'=>$description,
                'nominal'=>$totalnilai,
            );
            BORONGAN::where('borongan_id',$id)->update($data);

            $cek = BORONGAN::where('borongan_id',$id)->with(['borongan_details','order','vendor'])->first();


            \DB::commit();
            return $this->success(null, 200, $cek);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function submit($id){
        $databorongan = BORONGAN::where('borongan_id',$id)->first();
        $totalnilai = $databorongan->nominal;
        $totalamount = BORONGANDETAIL::where('borongan_id',$id)->sum('amount');
        if($totalnilai != $totalamount){
            return $this->fail('Post Failed', 500, 'Total Nilai Termin Tidak Sama dengan Total Nilai Borongan');
        }
        \DB::beginTransaction();
        try {
                
            $data = array(
                'status'=>'pending',
            );
            BORONGAN::where('borongan_id',$id)->update($data); 

            \DB::commit();
            return $this->success("Borongan Submit Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function confirm($id){
    
         \DB::beginTransaction();
        try {
            $cek = BORONGAN::where('borongan_id',$id)->with(['order'])->first();
            $nominal = $cek->nominal;
            $limitcost = $cek->order->limit_cost;
            $order_id = $cek->order_id;

            if($nominal > $limitcost){
                return $this->fail("Confirm Failed", 500, "Total Borongan Lebih Besar Dari Limit Cost");  
            }else{
                
                $total = $limitcost - $nominal;

                $dataorder= array(
                    'limit_cost'=>$total,
                );
                ORDER::where('order_id',$order_id)->update($dataorder);

                $data = array(
                    'status'=>'progress',
                );
                BORONGAN::where('borongan_id',$id)->update($data); 
            }

            \DB::commit();
            return $this->success("Confirm Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function reject($id){
        
        \DB::beginTransaction();
        try {
                
            $data = array(
                'status'=>'rejected',
            );
            BORONGAN::where('borongan_id',$id)->update($data); 
            
            \DB::commit();
            return $this->success("Rejected Success", 200, null);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         
        \DB::beginTransaction();
        try {
            BORONGANDETAIL::where('borongan_id',$id)->delete();
            BORONGAN::where('borongan_id',$id)->delete();
            \DB::commit(); 
           return $this->success("Success Delete Borongan");
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
