<?php

namespace App\Http\Controllers\Api\Project;

use App\Http\Controllers\Api\Controller as ApiController;
use App\Http\Requests\Project\AbsenTukang as AbsenTukangRequest;
use App\Repositories\Finance\UpahTukang as UpahTukangRepository;
use App\Repositories\Master\Tukang as TukangRepository;
use App\Repositories\Project\Order as OrderRepository;
use App\Repositories\Project\UpahHarian as UpahHarianRepository;
use App\Models\Finance\UpahTukang as UPAHTUKANG;
use App\Models\Finance\upahTukangDetail as UPAHTUKANGDETAIL;

class AbsenTukangDetailController extends ApiController
{
    protected $order;
    protected $tukang;
    protected $upahHarian;
    protected $upahTukang;

    /**
    * Instance constructor.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('eog.auth');
        $this->order = new OrderRepository;
        $this->tukang = new TukangRepository;
        $this->upahHarian = new UpahHarianRepository;
        $this->upahTukang = new UpahTukangRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
        switch(auth()->user()->type) {
            case "owner":
                $filter = $request->filter ? $request->filter : '';
            break;
            default:
                $filter = 'company_id|'.auth()->user()->company_id;
                $filter = $request->filter ? $filter.';'.$request->filter : $filter;
            break;
        }
        $upahTukang = $this->upahTukang->with('upahTukangDetails')->all($request->merge(compact('filter'))->all());
        $upahTukangDetails = $upahTukang->flatMap->upahTukangDetails->isNotEmpty() ? $upahTukang->flatMap->upahTukangDetails : [];
        return $this->success(null, 200, $upahTukangDetails);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AbsenTukangRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AbsenTukangRequest $request)
    {
        $datefrom = \Carbon\Carbon::createFromFormat('d/m/Y', $request->date_from);
        $dateto = \Carbon\Carbon::createFromFormat('d/m/Y', $request->date_to);
        $periode = $datefrom->format('d/m/Y').' - '.$dateto->format('d/m/Y');
        if ($datefrom->dayOfWeek != \Carbon\Carbon::FRIDAY || $dateto->dayOfWeek != \Carbon\Carbon::THURSDAY) {
            return $this->fail('Incorrect select day of week', 422, [
                'data_from' => $datefrom->dayName,
                'data_to' => $dateto->dayName
            ]);
        }
        if ($datefrom->diffInDays($dateto->add(1, 'day')) > 7) {
            return $this->fail('Max 7 day', 422);
        }
        $spk = $this->order->getModel()->where('work_order', $request->work_order)->first();
        $tkg = $this->tukang->show($request->tukang_id);
        $model = $this->upahTukang->getModel();
        \DB::beginTransaction();
        try {
            $upahTkg = $model->where('work_order', $spk->work_order)->where('upah_tkg_id', $request->upah_tkg_id)->first();
            if (empty($upahTkg)) {
                $upahTkg = $model->create($request->merge([
                    'company_id' => $spk->company_id,
                    'periode' => $periode,
                ])->except('absent'));
            }
            $upah = 0;
            $uang_makan = [];
            foreach ($request->absent as $day => $calc) {
                $uang_makan[$day]['regular'] = $calc['regular'] == 8 ? $request->input("uangmakan_amount") : 0;
                $uang_makan[$day]['shift_1'] = $calc['shift_1'] == 5 ? $request->input("uangmakan_amount") : 0;
                $uang_makan[$day]['shift_2'] = $calc['shift_2'] == 3 ? $request->input("uangmakan_amount") : 0;
                
                $upah += (($calc['regular'] / 8) * $tkg->upah) + $uang_makan[$day]['regular'];
                $upah += (($calc['shift_1'] / 5) * $tkg->upah) + $uang_makan[$day]['shift_1'];
                $upah += (($calc['shift_2'] / 3) * $tkg->upah) + $uang_makan[$day]['shift_2'];
            }
            if ($uang_makan) {
                $regular = collect($uang_makan)->values()->sum('regular');
                $shift_1 = collect($uang_makan)->values()->sum('shift_1');
                $shift_2 = collect($uang_makan)->values()->sum('shift_2');
                $uang_makan = compact('regular', 'shift_1', 'shift_2');
            }
            $upahTkgDt = $upahTkg->upahTukangDetails()->create([
                'tukang_id' => $tkg->tukang_id,
                'absent' => $request->absent,
                'uangmakan_amount' => $request->input("uangmakan_amount"),
                'uangmakan_total' => json_encode($uang_makan),
                'upah' => $tkg->upah,
                'amount' => $upah,
            ]);
            $upahTkg->update($request->merge([
                'periode' => $periode,
                'nominal' => $upahTkg->upahTukangDetails->sum('amount')
            ])->except('absent'));
            \DB::commit();
            return $this->success(null, 200, $upahTkgDt);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $upahTukang = $this->upahTukang->with('upahTukangDetails')->all();
        if ($upahTukangDetail = $upahTukang->flatMap->upahTukangDetails->where('upah_tkg_d_id', $id)->first()) {
            return $this->success(null, 200, $upahTukangDetail);
        } else {
            return $this->notFound(null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AbsenTukangRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     
    public function update(AbsenTukangRequest $request, $id)
    {
        $datefrom = \Carbon\Carbon::createFromFormat('d/m/Y', $request->date_from);
        $dateto = \Carbon\Carbon::createFromFormat('d/m/Y', $request->date_to);
        $periode = $datefrom->format('d/m/Y').' - '.$dateto->format('d/m/Y');
        if ($datefrom->dayOfWeek != \Carbon\Carbon::FRIDAY || $dateto->dayOfWeek != \Carbon\Carbon::THURSDAY) {
            return $this->fail('Incorrect select day of week', 422, [
                'data_from' => $datefrom->dayName,
                'data_to' => $dateto->dayName
            ]);
        }
        if ($datefrom->diffInDays($dateto->add(1, 'day')) > 7) {
            return $this->fail('Max 7 day', 422);
        }
        $tkg = $this->tukang->show($request->tukang_id);
        $upahTkg = $this->upahTukang->all();
        $upahTkgDt = $upahTkg->flatMap->upahTukangDetails->where('upah_tkg_d_id', $id)->first();
        \DB::beginTransaction();
        try {
            $upah = 0;
            $uang_makan = [];
            foreach ($request->absent as $day => $calc) {
                $uang_makan[$day]['regular'] = $calc['regular'] == 8 ? $request->input("uangmakan_amount") : 0;
                $uang_makan[$day]['shift_1'] = $calc['shift_1'] == 5 ? $request->input("uangmakan_amount") : 0;
                $uang_makan[$day]['shift_2'] = $calc['shift_2'] == 3 ? $request->input("uangmakan_amount") : 0;
                
                $upah += (($calc['regular'] / 8) * $tkg->upah) + $uang_makan[$day]['regular'];
                $upah += (($calc['shift_1'] / 5) * $tkg->upah) + $uang_makan[$day]['shift_1'];
                $upah += (($calc['shift_2'] / 3) * $tkg->upah) + $uang_makan[$day]['shift_2'];
            }
            if ($uang_makan) {
                $regular = collect($uang_makan)->values()->sum('regular');
                $shift_1 = collect($uang_makan)->values()->sum('shift_1');
                $shift_2 = collect($uang_makan)->values()->sum('shift_2');
                $uang_makan = compact('regular', 'shift_1', 'shift_2');
            }
            $upahTkgDt->update([
                'absent' => $request->absent,
                'uangmakan_amount' => $request->input("uangmakan_amount"),
                'uangmakan_total' => json_encode($uang_makan),
                'upah' => $tkg->upah,
                'amount' => $upah,
            ]);
            $upahTkgDt->upahTukang->update($request->merge([
                'periode' => $periode,
                'nominal' => $upahTkgDt->upahTukang->upahTukangDetails->sum('amount')
            ])->except('absent'));
            \DB::commit();
            return $this->success(null, 200, $upahTkgDt);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    public function payfee(\Illuminate\Http\Request $request)
    {
         $upah_tkg_d_id = $request->upah_tkg_d_id;
         $date = $request->date;
         $coa_code = $request->coa_code;
         $company_id = auth()->user()->company_id;

        \DB::beginTransaction();
        try {
            $data = UPAHTUKANGDETAIL::where('upah_tkg_d_id',$upah_tkg_d_id)->with(['tukang'])->first();
            $upah_tkg_id = $data->upah_tkg_id;
            $amount = $data->amount;
            $tukangname = $data->tukang->name;

            $dataupah = UPAHTUKANG::where('upah_tkg_id',$upah_tkg_id)->first();
            $periode = $dataupah->periode;
            $nominal = $dataupah->nominal;

            $array = array(
                'user_pay'=>auth()->user()->user_id,
                'date_pay'=>$date,
            );
            UPAHTUKANGDETAIL::where('upah_tkg_d_id',$upah_tkg_d_id)->update($array);

            $paid = UPAHTUKANGDETAIL::where('upah_tkg_id',$upah_tkg_id)->where('user_pay','!=',null)->sum('amount');
            $balance = $nominal - $paid;
            if($balance == 0){
                $status = "paid";
            }else{
                $status = "unpaid";
            }
            $update = array(
                'paid'=>$paid,
                'balance'=>$balance,
                'status'=>$status
            );
            UPAHTUKANG::where('upah_tkg_id',$upah_tkg_id)->update($update);  

            $ref = "Upah Tukang";
            $description = "Pembayaran Upah Tukang $tukangname $periode";

            $post= postpaid($company_id, $coa_code, $ref, $description, $amount);
            if($post['status'] =="failed"){
                return $this->fail("Post Failed", 500, $post['errors']);
            }

            $upahTukangDetails = UPAHTUKANG::where('upah_tkg_id',$upah_tkg_id)->with(['upahTukangDetails'])->first();

            \DB::commit();
            return $this->success("Payment Success", 200, $upahTukangDetails);
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $upahTukang = $this->upahTukang->all();
        $upahTukangDetail = $upahTukang->flatMap->upahTukangDetails->where('upah_tkg_d_id', $id)->first();
        \DB::beginTransaction();
        try {
            if (empty($upahTukangDetail->upahTukang->approved)) {
                $upahTukangDetail->delete();
                $status = 'success';
                $message = null;
            } else {
                $status = 'fail';
                $message = 'Can\'t be deleted because the transaction status is "approved"';
            }
            \DB::commit();
            switch ($status) {
                case 'success':
                    return $this->success($message);
                break;
                case 'fail':
                    return $this->fail($message, 422);
                break;
            }
        } catch(\Exception $e) {
            \DB::rollback();
            return $this->fail(null, 500, $e->getMessage());
        }
    }
}
