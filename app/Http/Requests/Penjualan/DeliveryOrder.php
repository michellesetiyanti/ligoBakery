<?php

namespace App\Http\Requests\Penjualan;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class DeliveryOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'penjualan_id' => 'required|integer|exists:penjualan,penjualan_id',
            'code' => 'nullable|string|max:100|unique:delivery_orders,code',
            'note' => 'sometimes|required|string',
            'products' => 'required|array',
            'products.*.id' => 'required|integer|exists:products,product_id',
            'products.*.qty_lg' => 'required|integer',
            'products.*.qty_sm' => 'required|integer',
            'confirmed' => 'required|string|in:yes,no',
            'signed' => 'sometimes|nullable|array',
        ];
    }
    
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $penjualan = \App\Models\Penjualan\Penjualan::find($this->get('penjualan_id'));
            if ($this->get('products')) {
                foreach ($this->get('products') as $products) {
                    foreach ($penjualan->os_do as $os_do) {
                        if ($os_do['product_id'] == $products['id']) {
                            $msgProduct = 'Over of outstanding qty Product "'.$os_do['product']['name'].'"';
                            if ($products['qty_lg'] > $os_do['qty_lg']) {
                                $validator->errors()->add('qty_lg', $msgProduct);
                            }
                            if ($products['qty_sm'] > $os_do['qty_sm']) {
                                $validator->errors()->add('qty_sm', $msgProduct);
                            }
                        }
                    }
                }
            }
        });
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
