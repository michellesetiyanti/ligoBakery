<?php

namespace App\Http\Requests\Penjualan\Agro;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Penjualan extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'serial' => 'sometimes|required|string',
            'no_faktur' => 'sometimes|required|string|min:3|max:100',
            'trans_type' => 'sometimes|required|string|in:Free market,Company',
            'ref' => 'sometimes|required|string',
            'ref_date' => 'sometimes|required|date_format:"d/m/Y"',
            'entry_at' => 'sometimes|nullable|required_if:payment,credit|date_format:"d/m/Y"',
            'customer_id' => 'required|integer|exists:customers,customer_id',
            'sales_id' => 'sometimes|required|integer|exists:karyawan,karyawan_id',
            'payment' => 'sometimes|required|string|in:cash,credit',
            'coa_code' => 'sometimes|nullable|string|required_if:payment,cash|exists:coa,code',
            'wh_id' => 'sometimes|required|integer|exists:warehouses,wh_id',
            'ongkir' => 'sometimes|required|digits_between:1,20',
            'ongkir_desc' => 'nullable|string',
            'biaya_lain' => 'sometimes|required|digits_between:1,20',
            'biaya_lain_desc' => 'nullable|string',
            // process detail product
            'details' => 'sometimes|required|array',
            'details.product_id' => 'integer|exists:products,product_id',
            'details.qty' => 'integer',
            'details.price' => 'digits_between:1,20',
            'details.tax' => 'digits_between:1,5',
            'details.discount' => 'digits_between:1,20',            
            // select detail product
            'selected' => 'sometimes|required|array',
            'selected.*.id' => 'integer|exists:penjualan_details,penjualan_d_id',
        ];
    }
    
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $warehouse = \App\Models\Master\Warehouse::find($this->get('wh_id'));
            $warehouseAuth = auth()->user()->company->warehouse;
            if (!in_array($warehouse->wh_id, $warehouseAuth->pluck('wh_id')->toArray())) {
                $validator->errors()->add('wh_id', 'Warehouse is not available in the company.');
            }
            if ($this->get('details')) {
                if ($this->get('trans_type') == 'Company' && $this->get('details')['tax'] == 0) {
                    $validator->errors()->add('details.tax', 'Transaction in "PPN".');
                }
            }
        });
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
