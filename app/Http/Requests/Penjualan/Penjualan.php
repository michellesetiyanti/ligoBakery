<?php

namespace App\Http\Requests\Penjualan;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Penjualan extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_faktur' => 'sometimes|required|string|min:3|max:100',
            'entry_at' => 'sometimes|nullable|required_if:payment,credit',
            'customer_id' => 'required|integer|exists:customers,customer_id',
            'sales_id' => 'sometimes|required|integer|exists:karyawan,karyawan_id',
            'payment' => 'sometimes|required|string|in:cash,credit',
            'coa_code' => 'sometimes|nullable|string|required_if:payment,cash|exists:coa,code',
            'wh_id' => 'sometimes|required|integer|exists:warehouses,wh_id',
            'ongkir' => 'sometimes|required|digits_between:1,20',
            'ongkir_desc' => 'nullable|string',
            'biaya_lain' => 'sometimes|required|digits_between:1,20',
            'biaya_lain_desc' => 'nullable|string',
            // process detail product
            'details' => 'sometimes|required|array',
            'details.product_id' => 'integer|exists:products,product_id',
            'details.qty_lg' => 'integer',
            'details.qty_sm' => 'integer',
            'details.price_lg' => 'digits_between:1,20',
            'details.price_sm' => 'digits_between:1,20',
            'details.tax' => 'digits_between:1,5',
            'details.discount' => 'digits_between:1,20',            
            // select detail product
            'selected' => 'sometimes|required|array',
            'selected.*.id' => 'integer|exists:penjualan_details,penjualan_d_id',
        ];
    }
    
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $warehouse = \App\Models\Master\Warehouse::where('wh_id', $this->get('wh_id'))->first();
            $warehouseAuth = auth()->user()->company->warehouse;
            if (!in_array($warehouse->wh_id, $warehouseAuth->pluck('wh_id')->toArray())) {
                $validator->errors()->add('wh_id', 'Warehouse is not available in the company.');
            }
        });
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
