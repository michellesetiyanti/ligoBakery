<?php

namespace App\Http\Requests\Project;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class AbsenTukang extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tukang_id' => 'sometimes|required|integer|exists:tukang,tukang_id',
            'upah_tkg_id' => 'sometimes|nullable|integer|exists:upah_tukang,upah_tkg_id',
            'work_order' => 'required|string|exists:orders,work_order',
            'date_from' => 'required|date_format:"d/m/Y"',
            'date_to' => 'required|date_format:"d/m/Y"',
            'description' => 'required|string',
            'location' => 'required|string',
            'uangmakan_amount' => 'sometimes|required|string',
            'absent' => 'sometimes|required|array',
            // 'absent.sun' => 'sometimes|array',
            // 'absent.mon' => 'sometimes|array',
            // 'absent.tues' => 'sometimes|array',
            // 'absent.wed' => 'sometimes|array',
            // 'absent.thurs' => 'sometimes|array',
            // 'absent.fri' => 'sometimes|array',
            // 'absent.sat' => 'sometimes|array',
            'absent.*.regular' => 'in:0,1,2,3,4,5,6,7,8',
            'absent.*.shift_1' => 'in:0,1,2,3,4,5',
            'absent.*.shift_2' => 'in:0,1,2,3',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
