<?php

namespace App\Http\Requests\Project;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class OrderTermin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'sometimes|required|integer|exists:companies,company_id',
            'work_order' => 'required|min:3|max:100',
            'cost_project' => 'required|digits_between:1,20',
            'address' => 'required|string', 
            'time_work' => 'required|string',
            'entry_at' => 'required|date_format:"d/m/Y"',
            'customer' => 'required|array',
            'customer.name' => 'required|string',
            'customer.currency_code' => 'required|string',
            'customer.address' => 'nullable|string',
            'customer.phone' => 'nullable|string',
            'name' => 'required|string|min:1|max:100',
            'description' => 'nullable|string',
            'amount' => 'required|digits_between:1,20|not_in:0',
        ];
    }

    public function withValidator($validator)
    {
        $order = \App\Models\Project\Order::where('work_order', $this->get('work_order'))->first();
        $validator->after(function ($validator) use ($order) {
            if (auth()->user()->isOwner() && auth()->user()->isMultiCompany() && empty($this->get('company_id'))) {
                $validator->errors()->add('company_id', 'Select the company to be processed');
            }
            if ($order) {
                $termin = \App\Models\Project\OrderTermin::where('order_id', $order->order_id)->get();
                $amount = 0;
                if ($termin->isNotEmpty()) {
                    if ($terminById = $termin->where('order_t_id', $this->id)->first()) {
                        $amount+= $termin->sum('amount') - $terminById->amount;
                    } else {
                        $amount+= $termin->sum('amount');
                    }
                }
                if (($amount + $this->get('amount')) > $this->get('cost_project')) {
                    $validator->errors()->add('total', sprintf(
                        'The sum of "%s" must be "%s".',
                        'amount',
                        number_format($this->get('cost_project') - $amount, 2)
                    ));
                }
            }
        });
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
