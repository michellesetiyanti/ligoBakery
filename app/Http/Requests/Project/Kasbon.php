<?php

namespace App\Http\Requests\Project;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Kasbon extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'sometimes|required|string',
            'code_spk' => 'required|string|exists:orders,code_spk',
            'entry_at' => 'required|date_format:"d/m/Y"',
            'detail' => 'sometimes|required|array',
            'detail.person' => 'string',
            'detail.description' => 'string',
            'detail.nominal' => 'digits_between:1,20',
        ];
    }

    public function withValidator($validator)
    {
        $limit = 2000000;
        $validator->after(function ($validator) use ($limit) {
            $order = \App\Models\Project\Order::where('code_spk', $this->get('code_spk'))->first();
            if ($nominal = $this->get('detail')['nominal']) {
                if ($nominal > $limit) {
                    $validator->errors()->add('limit', $limit);
                } else {
                    $nominal+= \App\Models\Finance\Kasbon::where('code_spk', $this->get('code_spk'))->sum('nominal');
                    if ($nominal > $order->limit_cost) {
                        $validator->errors()->add('limit', $order->limit_cost - $nominal);
                    }
                }
            }
        });
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
