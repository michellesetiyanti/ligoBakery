<?php

namespace App\Http\Requests\Project;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Ro extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'sometimes|nullable|exists:po_projects,code',
            'code_spk' => 'required|exists:orders,code_spk',
            'wh_id' => 'required|exists:warehouses,wh_id',
            'ongkir' => 'sometimes|required|digits_between:1,20',
            'ongkir_desc' => 'nullable',
            'biaya_lain' => 'sometimes|required|digits_between:1,20',
            'biaya_lain_desc' => 'nullable',
            'details' => 'sometimes|required|array',
            'details.product_id' => 'integer|exists:products,product_id',
            'details.qty_lg' => 'integer',
            'details.qty_sm' => 'integer',
            'details.price_lg' => 'digits_between:1,20',
            'details.price_sm' => 'digits_between:1,20',
            'details.tax' => 'digits_between:1,5',
            'details.discount' => 'digits_between:1,20',
        ];
    }
    
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // $validator->errors()->add('ro_id', trans('validation.required', ['attribute' => 'approved.ro_id']));
        });
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
