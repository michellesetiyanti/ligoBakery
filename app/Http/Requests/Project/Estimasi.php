<?php

namespace App\Http\Requests\Project;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Estimasi extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'work_order' => 'required|string|exists:orders,work_order',
            'description' => 'required|string',
            'wh_id' => 'required|integer|exists:warehouses,wh_id',
            'entry_at' => 'required|date_format:"d/m/Y"',
            'details' => 'sometimes|required|array',
            'details.product_id' => 'integer|exists:products,product_id',
            'details.qty_lg' => 'integer',
            'details.qty_sm' => 'integer'
        ];
    }
    
    public function withValidator($validator)
    {
        $order = \App\Models\Project\Order::where('work_order', '=', $this->get('work_order'))->first();
        $estimasiDetails = \App\Models\Project\Estimasi::with('estimasiDetails')->where('work_order', $this->get('work_order'))->get()->pluck('estimasiDetails')->flatten();
        $validator->after(function ($validator) use ($order, $estimasiDetails) {
            if ($estimasiDetails->sum('amount') > $order->cost_job) {
                $validator->errors()->add('total', sprintf(
                    'The sum of "%s" must be "%s".',
                    'amount',
                    number_format($order->cost_job - $estimasiDetails->sum('amount'), 2)
                ));
            }
        });
    }
    
    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
