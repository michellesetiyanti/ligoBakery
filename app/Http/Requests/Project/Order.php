<?php

namespace App\Http\Requests\Project;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Order extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'sometimes|required|integer|exists:companies,company_id',
            'code' => 'required|string',
            'work_order' => 'sometimes|required|min:3|max:100',
            'address' => 'required|string',
            'time_work' => 'required|string',
            'entry_at' => 'required|date_format:"d/m/Y"',
            'customer' => 'required|array',
            'customer.name' => 'required|string',
            'customer.currency_code' => 'required|string',
            'customer.address' => 'nullable|string',
            'customer.phone' => 'nullable|string',
            'cost_project' => 'required|digits_between:1,20',
            'cost_job' => 'required|digits_between:1,20',
            'profit' => 'required|digits_between:1,5',
        ];
    }

    public function withValidator($validator)
    {
        $order = \App\Models\Project\Order::where('work_order', $this->get('work_order'))->first();
        $validator->after(function ($validator) use ($order) {
            if (empty($order) && auth()->user()->isOwner() && auth()->user()->isMultiCompany() && empty($this->get('company_id'))) {
                $validator->errors()->add('company_id', 'Select the company to be processed');
            }
            $profit = $this->get('cost_project') * ($this->get('profit') / 100);
            $cost_job = $this->get('cost_project') - $profit;
            if ($this->get('cost_job') != $cost_job) {
                $validator->errors()->add('profit', sprintf(
                    'The sum of "%s" must be "%s".',
                    number_format($cost_job, 2),
                    'cost_job'
                ));
            }
        });
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
