<?php

namespace App\Http\Requests\Master;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Karyawan extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? 0;
        return [
            'position' => 'sometimes|required|string|min:2|max:20',
            'name' => 'required|string|min:2|max:191',
            'nik' => 'nullable|string',
            'cradle' => 'nullable|string',
            'born' => 'required|date_format:"d-m-Y"',
            'address' => 'nullable|string',
            'address_now' => 'nullable|string',
            'education' => 'nullable|string',
            'datejoin' => 'required|string',
            'penempatan' => 'required|string',
            'phone' => 'nullable|string',
            'phone2' => 'nullable|string',
            'email' => 'nullable|string',
            'email_perusahaan' => 'nullable|string',
            'gender' => 'nullable|string',
            'marriage' => 'nullable|string',
            'npwp' => 'nullable|string',
            'bpjs' => 'nullable|string',
            'salary' => 'nullable|numeric',
            'bank_name' => 'nullable|string',
            'bank_branch' => 'nullable|string',
            'bank_account' => 'nullable|numeric',
            'status_karyawan' => 'required|string',
            'status' => 'required|string|in:active,inactive',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
