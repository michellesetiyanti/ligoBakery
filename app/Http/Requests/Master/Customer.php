<?php

namespace App\Http\Requests\Master;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Customer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? 0;
        return [
            'code' => 'nullable|min:6|max:20|unique:customers,code,'.$id.',customer_id',
            'name' => 'required|string|max:191|unique:customers,name,'.$id.',customer_id',
            'address' => 'nullable|string',
            'phone' => 'nullable|string',
            'reference' => 'nullable|string',
            'currency_code' => 'sometimes|required|string|min:2|max:5',
            'npwp' => 'nullable|string',
            'pkp_name' => 'nullable|string',
            'pkp_address' => 'nullable|string',
            'credit_limit' => 'nullable|digits_between:1,20',
            'status' => 'required|in:active,inactive',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message = collect();
        collect($errors)->each(function ($item) use ($message) {
            $message->push(implode('<br/>', $item));
        });
        $response = [
            'status' => 'failed',
            // 'message' => 'There was an error with validation.',
            'message' => $message->implode('<br/>'),
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
