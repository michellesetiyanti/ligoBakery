<?php

namespace App\Http\Requests\Master;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Supplier extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'sometimes|required|integer|exists:companies,company_id',
            'name' => 'required|string|min:3|max:191',
            'address' => 'nullable|string',
            'phone' => 'nullable|string',
            'pic' => 'nullable|string',
            'email' => 'nullable|string',
            'reference' => 'nullable|string',
            'currency_code' => 'nullable|min:2|max:5',
            'npwp' => 'nullable|string',
            'bank_name' => 'nullable|string',
            'bank_branch' => 'nullable|string',
            'bank_account' => 'nullable|string',
            'pkp_name' => 'nullable|string',
            'pkp_address' => 'nullable|string',
            'npwp' => 'nullable|string',
            'taxable' => 'required|in:yes,no',
            'taxable_rate' => 'required_if:taxable,yes',
            'region' => 'required|string',
            'code_type' => 'required|string',
            'status' => 'required|string|in:active,inactive',
        ];
    }
    
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (auth()->user()->isOwner() && auth()->user()->isMultiCompany()) {
                if (!$this->exists('company_id')) {
                    $validator->errors()->add('company_id', trans('validation.required', ['attribute' => 'company_id']));
                }
            }
        });
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message = collect();
        collect($errors)->each(function ($item) use ($message) {
            $message->push(implode('<br/>', $item));
        });
        $response = [
            'status' => 'failed',
            // 'message' => 'There was an error with validation.',
            'message' => $message->implode('<br/>'),
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
