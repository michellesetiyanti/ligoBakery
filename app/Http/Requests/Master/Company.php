<?php

namespace App\Http\Requests\Master;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Company extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? 0;
        return [
            'parent_id' => 'nullable|numeric|exists:companies,company_id',
            'name' => 'required|min:6|max:191|unique:companies,name,'.$id.',company_id',
            'address' => 'nullable|string',
            'phone' => 'nullable|string',
            'currency_code' => 'sometimes|required|string|min:2|max:5',
            'npwp' => 'nullable|string',
            'taxable' => 'required|in:yes,no',
            'taxable_rate' => 'required_if:taxable,yes',
            'coa_penjualan' => 'nullable|string',
            'coa_pembelian' => 'nullable|string',
            'coa_hutang' => 'nullable|string',
            'coa_piutang' => 'nullable|string',
            'coa_pajak' => 'nullable|string',
            'coa_discount' => 'nullable|string',
            'user' => 'sometimes|required|array',
            'user.email' => 'nullable|string|email|unique:users,email',
            'user.password' => 'nullable|confirmed|min:6',
            'status' => 'required|in:active,inactive',
            'comp_type' => 'sometimes|nullable|string|in:general,agro',
            'coa_import' => 'sometimes|nullable|boolean',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message = collect();
        collect($errors)->each(function ($item) use ($message) {
            $message->push(implode('<br/>', $item));
        });
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            // 'errors' => $errors,
            'errors' => $message->implode('<br/>'),
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
