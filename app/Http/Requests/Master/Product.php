<?php

namespace App\Http\Requests\Master;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Product extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? 0;
        return [
            'wh_id' => 'sometimes|required|integer|exists:warehouses,wh_id',
            'product_c_id' => 'required|integer|exists:product_categories,product_c_id',
            'code' => 'nullable|min:6|max:20|unique:products,code,'.$id.',product_id',
            'name' => 'required|string|min:3|max:191',
            'description' => 'nullable|string',
            'unit' => 'required|array',
            'unit.lg.code' => 'string|exists:product_units,code',
            // 'unit.sm.code' => 'sometimes|nullable|string|exists:product_units,code',
            // 'unit.sm.value' => 'sometimes|nullable|integer',
            // 'unit.pack.code' => 'sometimes|nullable|string|exists:product_units,code',
            // 'unit.pack.value' => 'sometimes|nullable|integer|required_with:unit.pack.code',
            'price_lg' => 'nullable|digits_between:1,20',
            // 'price_sm' => 'nullable|digits_between:1,20',
            'min_qty' => 'sometimes|nullable',
        ];
    }
    
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!$this->id && auth()->user()->isMultiWarehouse()) {
                if (!$this->exists('wh_id')) {
                    $validator->errors()->add('wh_id', trans('validation.required', ['attribute' => 'wh_id']));
                }
            }
            if (auth()->user()->company->code_numb && auth()->user()->company->code_alpha) {
                if ($this->missing('min_qty')) {
                    $validator->errors()->add('min_qty', trans('validation.required', ['attribute' => 'min_qty']));
                }
            }
            $product = \App\Models\Master\Product::where('code', $this->get('code'))->where('unit_lg',  $this->get('unit')['lg']['code'])->where('unit_sm', $this->get('unit')['sm']['code']);
            if (!$this->id && $product->exists()) {
                $validator->errors()->add('unit_lg', 'The "unit_lg" is available with code "'.$this->get('code').'".');
                $validator->errors()->add('unit_sm', 'The "unit_sm" is available with code "'.$this->get('code').'".');
            }
        });
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message = collect();
        collect($errors)->each(function ($item) use ($message) {
            $message->push(implode('<br/>', $item));
        });
        $response = [
            'status' => 'failed',
            // 'message' => 'There was an error with validation.',
            'message' => $message->implode('<br/>'),
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
