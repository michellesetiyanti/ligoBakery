<?php

namespace App\Http\Requests\Master;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Warehouse extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? 0;
        return [
            'company_id' => 'sometimes|required|integer|exists:companies,company_id',
            'name' => 'required|string|min:6|max:191|unique:warehouses,name,'.$id.',wh_id',
            'address' => 'nullable',
            'phone' => 'nullable',
            'currency_code' => 'nullable|min:2|max:5',
            'npwp' => 'nullable',
            'taxable' => 'required|string|in:yes,no',
            'taxable_rate' => 'required_if:taxable,yes',
            'status' => 'required|string|in:active,inactive',
        ];
    }
    
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (auth()->user()->isOwner() && auth()->user()->isMultiCompany()) {
                if (!$this->id && !$this->exists('company_id')) {
                    $validator->errors()->add('company_id', 'Required field "company_id".');
                }
            }
        });
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message = collect();
        collect($errors)->each(function ($item) use ($message) {
            $message->push(implode('<br/>', $item));
        });
        $response = [
            'status' => 'failed',
            // 'message' => 'There was an error with validation.',
            'message' => $message->implode('<br/>'),
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
