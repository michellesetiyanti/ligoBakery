<?php

namespace App\Http\Requests\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class User extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? 0;
        return [
            'username' => 'nullable|string|min:6|max:20|unique:users,username,'.$id.',user_id',
            'email' => 'required|string|email|max:191|unique:users,email,'.$id.',user_id',
            'password' => ($id != 0) ? 'nullable' : 'required|confirmed|min:6',
            'company_id' => 'sometimes|required|integer|exists:companies,company_id',
            'karyawan_id' => 'required|integer|exists:karyawan,karyawan_id',
            'type' => 'required|string|exists:roles,code',
            'status' => 'required|in:active,inactive',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $message = collect();
        collect($errors)->each(function ($item) use ($message) {
            $message->push(implode('<br/>', $item));
        });
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            // 'errors' => $errors,
            'errors' => $message->implode('<br/>'),
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
