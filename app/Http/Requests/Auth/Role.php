<?php

namespace App\Http\Requests\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Role extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? 0;
        return [
            'code' => 'nullable|min:4|max:30|unique:roles,code,'.$id.',role_id',
            'name' => 'required|min:4|max:30|unique:roles,name,'.$id.',role_id',
            'description' => 'nullable|string',
            'status' => 'required|in:active,inactive',
            'menus' => 'sometimes|required|array',
            'menus.*.menu_id' => 'integer|exists:menus,menu_id',
            'menus.*.access' => 'array',
            'menus.*.access.create' => 'boolean',
            'menus.*.access.read' => 'boolean',
            'menus.*.access.update' => 'boolean',
            'menus.*.access.delete' => 'boolean',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
