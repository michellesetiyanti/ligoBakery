<?php

namespace App\Http\Requests\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Menu extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ?? 0;
        return [
            'parent_id' => 'nullable|integer|exists:menus,menu_id',
            'slug' => 'required|string|min:3|max:30|unique:menus,slug,'.$id.',menu_id',
            'name' => 'required|string|min:3|max:30|unique:menus,name,'.$id.',menu_id',
            'icon' => 'nullable|string',
            'url' => 'nullable|string',
            'status' => 'required|string|in:active,inactive',
        ];
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
