<?php

namespace App\Http\Requests\Pembelian\Agro;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class PoPembelian extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'serial' => 'sometimes|required|string',
            'name' => 'required|string',
            'wh_id' => 'required|integer|exists:warehouses,wh_id',
            'supplier_id' => 'nullable|exists:suppliers,supplier_id',
            'supplier_inv' => 'nullable',
            'payment' => 'sometimes|required|string|in:cash,credit',
            'due_date' => 'nullable|required_if:payment,credit|date_format:"d/m/Y"',
            'coa_code' => 'nullable|required_if:payment,cash|exists:coa,code',
            'ongkir' => 'sometimes|required|digits_between:1,20',
            'ongkir_desc' => 'nullable',
            'biaya_lain' => 'sometimes|required|digits_between:1,20',
            'biaya_lain_desc' => 'nullable',
            'details' => 'sometimes|required|array',
            'details.product_id' => 'integer|exists:products,product_id',
            'details.qty' => 'integer',
            'details.price' => 'digits_between:1,20',
            'details.tax' => 'digits_between:1,5',
            'details.discount' => 'digits_between:1,20',
            // Agro Terms of conditions
            'requests' => 'nullable',
            'toc' => 'sometimes|required|array',
            'toc.follow_up' => 'nullable',
            'toc.payment_terms' => 'nullable',
            'toc.stb' => 'sometimes|required|in:loco,franco,fob,fot,c&f,cif',
            'toc.stb_desc' => 'nullable',
            'toc.retv_schedule' => 'nullable',
        ];
    }
    
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $warehouse = \App\Models\Master\Warehouse::where('wh_id', $this->get('wh_id'))->first();
            $warehouseAuth = auth()->user()->company->warehouse;
            if (!in_array($warehouse->wh_id, $warehouseAuth->pluck('wh_id')->toArray())) {
                $validator->errors()->add('wh_id', 'Warehouse is not available in the company.');
            }
        });
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        $response = [
            'status' => 'failed',
            'message' => 'There was an error with validation.',
            'errors' => $errors,
        ];
        throw new HttpResponseException(response()->json($response, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
