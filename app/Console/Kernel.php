<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use File;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\MakeOptimizeCommand::class,
        Commands\MakeRepositoryCommand::class,
        Commands\MakeScopeCommand::class,
        Commands\MakeCompanyCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        
        $cronLog = storage_path('logs/cron.log');
        if (!File::exists($cronLog)) {
            File::put($cronLog, '');
        }
        /*
         * Cron Format
         * 
         * minute (0-59)
         * hour (0-23)
         * day of month (1-31)
         * month (1-12)
         * day of the week (0 is Sunday, 6 is Saturday)
         */
        // $schedule->command('eog:optimize')->everyMinute()->withoutOverlapping()->appendOutputTo($cronLog);
        $schedule->command('company:request')->everyMinute()->withoutOverlapping()->appendOutputTo($cronLog);
        // $schedule->command('optimize:backup-db')->everyMinute()->withoutOverlapping()->appendOutputTo($cronLog);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
