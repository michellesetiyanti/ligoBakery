<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class MakeBackupDbCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'optimize:backup-db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Schedule Backup Database';
    
    protected $process;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $today = \Carbon\Carbon::now()->toDateString();
        if (!is_dir(storage_path('backups'))) 
            mkdir(storage_path('backups'));
        
        $this->process = new Process(sprintf(
            'C:/xampp/mysql/bin/mysqldump --compact --skip-comments -u%s -p%s %s > %s',
            config('database.connections.mysql.username'),
            config('database.connections.mysql.password'),
            config('database.connections.mysql.database'),
            storage_path("backups/".config('database.connections.mysql.database')."_${today}.sql")
        ));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->process->mustRun();
            $this->info('DB backup - success');
        } catch (\ProcessFailedException $exception) {
            $this->error('DB backup - failed', $exception->getMessage());
        }
    }
}
