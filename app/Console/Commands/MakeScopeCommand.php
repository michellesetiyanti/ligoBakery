<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class MakeScopeCommand extends Command
{
    /**
     * The name and signature of the console command.
     * note: 
     * this is where you make your command structure
     * one, you will use in console, e.g.:
     * php artisan make:scope MyDir\Myscope
     *
     * @var string
     */
    protected $signature = 'make:scope {path}';

    /**
     * The console command description.
     * note:
     * this info will appear next to your command in, 
     * when you list all commands available:
     * >> php artisan list
     * kind of quick help what is it and how to use it
     * content is up to you
     *
     * @var string
     */
    protected $description = 'Create new global scope. Add scope classname with deep path, if desired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*get passed argument*/
        $path = $this->argument('path');
        /*split path*/
        $path_array = preg_split( "/[\\\\\/]/", $path);        
        /*get classname*/
        $className = Str::studly(array_pop($path_array));
        /*get namespace*/
        $path_array = array_map(function ($a) { 
            return Str::studly($a); 
        }, $path_array);
        $slash = empty($path_array) ? '' : '\\';
        $nameSpace = $slash . Str::studly(implode('\\', $path_array));        
        /*get stub file into a string*/
        $stubString = file_get_contents(app_path('Console/Stubs/scope.stub'));        
        /*replace dummies*/
        $readyStub = str_replace(['DummyNamespace', 'DummyClass'], [$nameSpace, $className], $stubString);        
        /*write scope file to location*/
        Storage::disk('app')->put('Scopes/' . $nameSpace . '/' . $className . '.php', $readyStub);
    }
}
