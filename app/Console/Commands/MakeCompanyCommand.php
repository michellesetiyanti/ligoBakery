<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Accounting\PenyusutanAset as Aset;

class MakeCompanyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'company:request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Company Schedule Post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::beginTransaction();
        try {
            $args = collect();
            $aset = Aset::all();
            if ($aset->isNotEmpty()) {
                foreach ($aset as $row) {
                    if ($row->date_use == \Carbon\Carbon::parse($row->date_use)->add(1, 'month')->toDateString()) {
                        $row->update(['bulan_tersisa' => ($row->bulan_tersisa ?? $row->umur_bulan) - 1]);
                        $args->push($row->code_aset);
                    }
                }
            }
            \DB::commit();
            $this->info(\Carbon\Carbon::now()->toDateTimeString().' Kode Aset '.implode(',', $args->toArray()).' successfully Updated!');
        } catch (\Exception $e) {
            \DB::rollback();
            $this->error('ERROR', $e->getMessage());
        }
    }
}
