<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
| = Route::middleware('auth:api')->get('/user', function (Request $request) {
| =     return $request->user();
| = });
*/

Route::group(['middleware' => 'api', 'namespace' => 'Api'], function () {
    // Auth
    Route::group(['prefix' => 'auth', 'as' => 'auth.'], function() {
        Route::post('login', 'Auth\LoginController@login')->name('login');
        Route::get('login/company', 'Auth\LoginController@getCompany')->name('login.company');
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');
        Route::get('listcompany', 'Auth\LoginController@listcompany')->name('listcompany');
        // User
        Route::group(['prefix' => 'users', 'as' => 'users.'], function() {
            Route::get('/', 'Auth\UserController@index')->name('index');
            Route::post('create', 'Auth\UserController@store')->name('create');
            Route::get('{id}/read', 'Auth\UserController@show')->name('read');
            Route::put('{id}/update', 'Auth\UserController@update')->name('update');
            Route::delete('{id}/delete', 'Auth\UserController@destroy')->name('delete');
        });
        // menu
        Route::group(['prefix' => 'menus', 'as' => 'menus.'], function() {
            Route::get('/', 'Auth\MenuController@index')->name('index');
            Route::get('{id}/read', 'Auth\MenuController@show')->name('read');
            Route::put('{id}/update', 'Auth\MenuController@update')->name('update');
        });
        // Role
        Route::group(['prefix' => 'roles', 'as' => 'roles.'], function() {
            Route::get('/', 'Auth\RoleController@index')->name('index');
            Route::get('indexmenu', 'Auth\RoleController@indexmenu')->name('indexmenu');
            Route::post('create', 'Auth\RoleController@store')->name('create');
            Route::get('{id}/read', 'Auth\RoleController@show')->name('read');
            Route::put('{id}/update', 'Auth\RoleController@update')->name('update');
            Route::delete('{id}/delete', 'Auth\RoleController@destroy')->name('delete');
        });
    });
    // Master1
    Route::group(['prefix' => 'master', 'as' => 'master.'], function() {
        // Company
        Route::group(['prefix' => 'companies', 'as' => 'companies.'], function() {
            Route::get('/', 'Master\CompanyController@index')->name('index');
            Route::get('listcompany', 'Master\CompanyController@listcompany')->name('listcompany');
            Route::post('create', 'Master\CompanyController@store')->name('create');
            Route::get('{id}/read', 'Master\CompanyController@show')->name('read');
            Route::put('{id}/update', 'Master\CompanyController@update')->name('update');
            Route::put('{id}/updatelicence', 'Master\CompanyController@updatelicense')->name('updatelicence');
            Route::delete('{id}/delete', 'Master\CompanyController@destroy')->name('delete');
        });
        Route::group(['prefix' => 'itemhpp', 'as' => 'itemhpp.'], function() {
            Route::get('/', 'Master\CompanyController@indexitemhpp')->name('indexitemhpp');
            Route::post('create', 'Master\CompanyController@storeitemhpp')->name('storeitemhpp');
            Route::put('{id}/update', 'Master\CompanyController@updateitemhpp')->name('updateitemhpp');
            Route::delete('{id}/delete', 'Master\CompanyController@deleteitemhpp')->name('deleteitemhpp');
        });
        // Warehouse
        Route::group(['prefix' => 'warehouses', 'as' => 'warehouses.'], function() {
            Route::get('/', 'Master\WarehouseController@index')->name('index');
            Route::post('create', 'Master\WarehouseController@store')->name('create');
            Route::get('{id}/read', 'Master\WarehouseController@show')->name('read');
            Route::put('{id}/update', 'Master\WarehouseController@update')->name('update');
            Route::delete('{id}/delete', 'Master\WarehouseController@destroy')->name('delete');
        });
        //Menu Jual
        Route::group(['prefix' => 'menujuals', 'as' => 'menujuals.'], function() {
            Route::get('/', 'Master\MenuJualController@index')->name('index');
            Route::post('create', 'Master\MenuJualController@store')->name('create');
            Route::post('createpenjualan', 'Master\MenuJualController@store')->name('create');
            Route::get('{id}/read', 'Master\MenuJualController@show')->name('read');
            Route::put('{id}/update', 'Master\MenuJualController@update')->name('update');
            Route::delete('{id}/delete', 'Master\MenuJualController@destroy')->name('delete');

            Route::group(['prefix' => 'recipes', 'as' => 'recipes.'], function() {
                Route::get('/', 'Master\MenuJualController@index');
                Route::post('create', 'Master\MenuJualController@storerecipe');
                Route::put('{id}/updatedetails', 'Master\MenuJualController@updatedetails');
                Route::delete('{id}/deletedetail', 'Master\MenuJualController@deletedetail');
            });
        });
        //Menu Sisa
        Route::group(['prefix' => 'menusisa', 'as' => 'menusisa.'], function() {
            Route::get('/', 'Master\MenuSisaJualController@index');
            Route::post('create', 'Master\MenuSisaJualController@store');
            Route::get('{id}/read', 'Master\MenuSisaJualController@show');
            Route::delete('{id}/delete', 'Master\MenuSisaJualController@destroy');
            Route::put('{id}/submit', 'Master\MenuSisaJualController@submit');

            Route::group(['prefix' => 'details', 'as' => 'details.'], function() {
                Route::post('create', 'Master\MenuSisaJualController@storedetails');
                Route::put('{id}/update', 'Master\MenuSisaJualController@updatedetails');
                Route::delete('{id}/delete', 'Master\MenuSisaJualController@destroydetail');
            });
        });
        // Customer
        Route::group(['prefix' => 'customers', 'as' => 'customers.'], function() {
            Route::get('/', 'Master\CustomerController@index')->name('index');
            Route::post('create', 'Master\CustomerController@store')->name('create');
            Route::get('{id}/read', 'Master\CustomerController@show')->name('read');
            Route::put('{id}/update', 'Master\CustomerController@update')->name('update');
            Route::delete('{id}/delete', 'Master\CustomerController@destroy')->name('delete');
            Route::post('import', 'Master\CustomerController@import')->name('import');
            Route::get('{id}/getfile', 'Master\CustomerController@getfile')->name('getfile');
            Route::post('uploadktp', 'Master\CustomerController@uploadktp')->name('uploadktp');
            Route::post('deletefile', 'Master\CustomerController@deletefile')->name('deletefile');
        });
        // Karyawan
        Route::group(['prefix' => 'karyawan', 'as' => 'karyawan.'], function() {
            Route::get('/', 'Master\KaryawanController@index')->name('index');
            Route::post('create', 'Master\KaryawanController@store')->name('create');
            Route::get('{id}/read', 'Master\KaryawanController@show')->name('read');
            Route::put('{id}/update', 'Master\KaryawanController@update')->name('update');
            Route::delete('{id}/delete', 'Master\KaryawanController@destroy')->name('delete');
            Route::get('{id}/getfile', 'Master\KaryawanController@getfile')->name('getfile');
            Route::post('uploadktp', 'Master\KaryawanController@uploadktp')->name('uploadktp');
            Route::post('uploadnpwp', 'Master\KaryawanController@uploadnpwp')->name('uploadnpwp');
            Route::post('uploadijazah', 'Master\KaryawanController@uploadijazah')->name('uploadijazah');
            Route::post('uploadfoto', 'Master\KaryawanController@uploadfoto')->name('uploadfoto');
            Route::post('uploadmisc', 'Master\KaryawanController@uploadmisc')->name('uploadmisc');
            Route::post('photo', 'Master\KaryawanController@photo')->name('photo');
            Route::post('deletefile', 'Master\KaryawanController@deletefile')->name('deletefile');
        });
        // Supplier
        Route::group(['prefix' => 'suppliers', 'as' => 'suppliers.'], function() {
            Route::get('/', 'Master\SupplierController@index')->name('index');
            Route::post('create', 'Master\SupplierController@store')->name('create');
            Route::get('{id}/read', 'Master\SupplierController@show')->name('read');
            Route::put('{id}/update', 'Master\SupplierController@update')->name('update');
            Route::delete('{id}/delete', 'Master\SupplierController@destroy')->name('delete');
            Route::post('import', 'Master\SupplierController@import')->name('import');
        });
        // Product
        Route::group(['prefix' => 'products', 'as' => 'products.'], function() {
            Route::get('/', 'Master\ProductController@index')->name('index');
            Route::get('indexhpp', 'Master\ProductController@indexhpp')->name('indexhpp');
            Route::get('indexhppitem', 'Master\ProductController@indexhppitem')->name('indexhppitem');
            Route::get('warningqty', 'Master\ProductController@warningqty')->name('warningqty');
            Route::post('create', 'Master\ProductController@store')->name('create');
            Route::get('{id}/read', 'Master\ProductController@show')->name('read');
            Route::put('{id}/update', 'Master\ProductController@update')->name('update');
            Route::delete('{id}/delete', 'Master\ProductController@destroy')->name('delete');
            Route::get('history/purchases', 'Master\ProductController@historyPurchases')->name('historyPurchases');
            Route::get('history/sales', 'Master\ProductController@historySales')->name('historySales');
            Route::get('price/batch', 'Master\ProductController@priceBatch')->name('priceBatch');
            // Product Unit
            Route::group(['prefix' => 'units', 'as' => 'units.'], function() {
                Route::get('/', 'Master\ProductunitController@index')->name('index');
                Route::post('create', 'Master\ProductunitController@store')->name('create');
                Route::get('{id}/read', 'Master\ProductunitController@show')->name('read');
                Route::put('{id}/update', 'Master\ProductunitController@update')->name('update');
                Route::delete('{id}/delete', 'Master\ProductunitController@destroy')->name('delete');
            });
            // Product Category
            Route::group(['prefix' => 'categories', 'as' => 'categories.'], function() {
                Route::get('/', 'Master\ProductCategoryController@index')->name('index');
                Route::post('create', 'Master\ProductCategoryController@store')->name('create');
                Route::get('{id}/read', 'Master\ProductCategoryController@show')->name('read');
                Route::put('{id}/update', 'Master\ProductCategoryController@update')->name('update');
                Route::delete('{id}/delete', 'Master\ProductCategoryController@destroy')->name('delete');
            });
        });
        // Tukang
        Route::group(['prefix' => 'tukang', 'as' => 'tukang.'], function() {
            Route::get('/', 'Master\TukangController@index')->name('index');
            Route::post('create', 'Master\TukangController@store')->name('create');
            Route::get('{id}/read', 'Master\TukangController@show')->name('read');
            Route::put('{id}/update', 'Master\TukangController@update')->name('update');
            Route::delete('{id}/delete', 'Master\TukangController@destroy')->name('delete');
            Route::get('{id}/getfile', 'Master\TukangController@getfile')->name('getfile');
            Route::post('uploadktp', 'Master\TukangController@uploadktp')->name('uploadktp'); 
            Route::post('deletefile', 'Master\TukangController@deletefile')->name('deletefile');
        });
        // Vendor
        Route::group(['prefix' => 'vendors', 'as' => 'vendors.'], function() {
            Route::get('/', 'Master\VendorController@index')->name('index');
            Route::post('create', 'Master\VendorController@store')->name('create');
            Route::get('{id}/read', 'Master\VendorController@show')->name('read');
            Route::put('{id}/update', 'Master\VendorController@update')->name('update');
            Route::delete('{id}/delete', 'Master\VendorController@destroy')->name('delete');
            Route::post('import', 'Master\VendorController@import')->name('import');
        });
        // Category Pembayaran
        Route::group(['prefix' => 'categorypembayaran', 'as' => 'categorypembayaran.'], function() {
            Route::get('/', 'Master\CategoryPembayaranController@index')->name('index');
            Route::post('create', 'Master\CategoryPembayaranController@store')->name('create');
            Route::put('{id}/update', 'Master\CategoryPembayaranController@update')->name('update');
            Route::delete('{id}/delete', 'Master\CategoryPembayaranController@destroy')->name('delete');
        });

        // Jenis Pembayaran
        Route::group(['prefix' => 'jenispembayaran', 'as' => 'jenispembayaran.'], function() {
            Route::get('/', 'Master\JenisPembayaranController@index')->name('index');
            Route::post('create', 'Master\JenisPembayaranController@store')->name('create');
            Route::put('{id}/update', 'Master\JenisPembayaranController@update')->name('update');
            Route::delete('{id}/delete', 'Master\JenisPembayaranController@destroy')->name('delete');
        });
        // Aset
        Route::group(['prefix' => 'asets', 'as' => 'asets.'], function() {
            Route::get('/', 'Master\AsetController@index')->name('index');
            Route::post('create', 'Master\AsetController@store')->name('create');
            Route::get('{id}/read', 'Master\AsetController@show')->name('read');
            Route::put('{id}/update', 'Master\AsetController@update')->name('update');
            Route::delete('{id}/delete', 'Master\AsetController@destroy')->name('delete');
        });
        // Penanggung-jawab
        Route::group(['prefix' => 'pj', 'as' => 'pj.'], function() {
            Route::get('/', 'Master\PenanggungjawabController@index')->name('index');
            Route::post('create', 'Master\PenanggungjawabController@store')->name('create');
            Route::get('{id}/read', 'Master\PenanggungjawabController@show')->name('read');
            Route::put('{id}/update', 'Master\PenanggungjawabController@update')->name('update');
            Route::delete('{id}/delete', 'Master\PenanggungjawabController@destroy')->name('delete');
        });
    });
    // Accounting
    Route::group(['prefix' => 'accounting', 'as' => 'accounting.'], function() {
        // COA
        Route::group(['prefix' => 'coa', 'as' => 'coa.'], function() {
            Route::get('/', 'Accounting\CoaController@index')->name('index');
            Route::post('create', 'Accounting\CoaController@store')->name('create');
            Route::get('{id}/read', 'Accounting\CoaController@show')->name('read');
            Route::put('{id}/update', 'Accounting\CoaController@update')->name('update');
            Route::delete('{id}/delete', 'Accounting\CoaController@destroy')->name('delete');
            Route::post('generategl', 'Accounting\CoaController@generategl')->name('generategl');
        });
        // GL
        Route::group(['prefix' => 'gl', 'as' => 'gl.'], function() {
            Route::get('/', 'Accounting\GeneralLedgerController@index')->name('index');
            // Route::post('create', 'Accounting\GeneralLedgerController@store')->name('create');
            Route::get('{id}/read', 'Accounting\GeneralLedgerController@show')->name('read');
            Route::get('labarugi', 'Accounting\GeneralLedgerController@showlabarugi')->name('labarugi');
            Route::get('indexclosing', 'Accounting\GeneralLedgerController@indexclosing')->name('indexclosing');
            Route::get('closingbulanan', 'Accounting\GeneralLedgerController@closingbulanan')->name('closingbulanan');
            Route::post('openclosing','Accounting\GeneralLedgerController@openclosing')->name('openclosing');
            Route::get('labarugiperiod', 'Accounting\GeneralLedgerController@showlabarugiperiod')->name('labarugiperiod');
            Route::get('neraca', 'Accounting\GeneralLedgerController@showneraca')->name('neraca');
            Route::put('{id}/updatestartbalance', 'Accounting\GeneralLedgerController@updatestartbalance')->name('updatestartbalance');
            // Route::put('{id}/update', 'Accounting\GeneralLedgerController@update')->name('update');
            // Route::delete('{id}/delete', 'Accounting\GeneralLedgerController@destroy')->name('delete');
        });
        Route::group(['prefix' => 'gldetail', 'as' => 'gldetail.'], function() {
            // Route::get('/', 'Accounting\GeneralLedgerDetailController@index')->name('index');
            Route::get('/', 'Accounting\GeneralLedgerDetailController@gldet')->name('index');
            Route::get('searchdata', 'Accounting\GeneralLedgerDetailController@searchdata')->name('searchdata');
        });
        Route::group(['prefix' => 'journal', 'as' => 'journal.'], function() {
            Route::get('/', 'Accounting\JournalController@index')->name('index'); 
            Route::post('create', 'Accounting\JournalController@store')->name('create');
            Route::post('complete', 'Accounting\JournalController@complete')->name('complete');
            Route::get('show', 'Accounting\JournalDetailController@index')->name('show');
            Route::post('add', 'Accounting\JournalDetailController@store')->name('add');
            Route::put('{id}/updatedetail', 'Accounting\JournalDetailController@update')->name('updatedetail');
            Route::delete('{id}/deletedetail', 'Accounting\JournalDetailController@destroy')->name('deletedetail');
            Route::delete('{id}/deletejournal', 'Accounting\JournalController@destroy')->name('deletejournal');
        });
        Route::group(['prefix' => 'penyusutanaset', 'as' => 'penyusutanaset.'], function() {
            Route::get('/', 'Accounting\PenyusutanAsetController@index')->name('index'); 
            Route::post('create', 'Accounting\PenyusutanAsetController@store')->name('create');
            Route::put('{id}/update', 'Accounting\PenyusutanAsetController@update')->name('update');
            Route::delete('{id}/delete', 'Accounting\PenyusutanAsetController@destroy')->name('delete');
           
        });
        // Adjustment
        Route::group(['prefix' => 'adjustment', 'as' => 'adjustment.'], function() {
            Route::get('/', 'Accounting\AdjustmentController@index')->name('index');
            Route::get('cekmutasi', 'Accounting\AdjustmentController@cekMutasi')->name('cekmutasi');
            Route::post('create', 'Accounting\AdjustmentController@store')->name('create');
            Route::get('{id}/read', 'Accounting\AdjustmentController@show')->name('read');
            Route::put('{id}/update', 'Accounting\AdjustmentController@update')->name('update');
            Route::delete('{id}/delete', 'Accounting\AdjustmentController@destroy')->name('delete');
            Route::put('{id}/updatedetail', 'Accounting\AdjustmentDetailController@update')->name('updatedetail');
            Route::delete('{id}/deletedetail', 'Accounting\AdjustmentDetailController@destroy')->name('deletedetail');
            Route::put('{id}/submitadjustment', 'Accounting\AdjustmentController@submitadjustment')->name('submitadjustment');
            Route::put('{id}/confirm', 'Accounting\AdjustmentController@doAction')->name('confirm');
        });
    });
    // Finance
    Route::group(['prefix' => 'finance', 'as' => 'finance.'], function() {
        // COA
        Route::group(['prefix' => 'kas', 'as' => 'kas.'], function() {
            Route::get('/', 'Finance\KasController@index')->name('index');
            Route::post('create', 'Finance\KasController@store')->name('create');
            Route::get('{id}/read', 'Finance\KasController@show')->name('read');
            Route::put('{id}/update', 'Finance\KasController@update')->name('update');
            Route::delete('{id}/delete', 'Finance\KasController@destroy')->name('delete');
        });
    });
    // Project
    Route::group(['prefix' => 'project', 'as' => 'project.'], function() {
        // Order
        Route::group(['prefix' => 'orders', 'as' => 'orders.'], function() {
            Route::get('/', 'Project\OrderController@index')->name('index');
            Route::get('getcodespk', 'Project\OrderController@codeSpk');
            Route::post('create', 'Project\OrderController@store')->name('create');
            Route::get('{id}/read', 'Project\OrderController@show')->name('read');
            Route::put('{id}/update', 'Project\OrderController@update')->name('update');
            Route::post('{id}/submit', 'Project\OrderController@submit')->name('submit');
            Route::post('{id}/confirm', 'Project\OrderController@confirm')->name('confirm');
            Route::post('{id}/reject', 'Project\OrderController@reject')->name('reject');
            Route::delete('{id}/delete', 'Project\OrderController@destroy')->name('delete');
            Route::put('{id}/adendum', 'Project\OrderController@adendum')->name('adendum');
            Route::post('uploadrab', 'Project\OrderController@uploadrab')->name('uploadrab');
            Route::post('uploadrab2', 'Project\OrderController@uploadrab2')->name('uploadrab2');
            Route::post('deletefilerab', 'Project\OrderController@deletefilerab')->name('deletefilerab');
            Route::post('deletefilerab2', 'Project\OrderController@deletefilerab2')->name('deletefilerab2');
            Route::post('import', 'Project\OrderController@import')->name('import');
            Route::get('codespk', 'Project\OrderController@codeSpk')->name('codespk');
            // Termin
            Route::group(['prefix' => 'termins', 'as' => 'termins.'], function() {
                Route::get('/', 'Project\OrderTerminController@index')->name('index');
                Route::get('getinvoicecode', 'Project\OrderTerminController@codeInv');
                Route::get('updatecodeinv', 'Project\OrderTerminController@updatecodeinv');
                Route::post('create', 'Project\OrderTerminController@store')->name('create');
                Route::get('{id}/read', 'Project\OrderTerminController@show')->name('read');
                Route::put('{id}/update', 'Project\OrderTerminController@update')->name('update');
                Route::put('{id}/pay', 'Project\OrderTerminController@pay')->name('pay');
                Route::delete('{id}/delete', 'Project\OrderTerminController@destroy')->name('delete');
                
                Route::group(['prefix' => 'details', 'as' => 'details.'], function() {
                    Route::get('/', 'Project\OrderTerminDetailController@index')->name('index');
                    Route::post('create', 'Project\OrderTerminDetailController@store')->name('create');
                    Route::put('{id}/update', 'Project\OrderTerminDetailController@update')->name('update');
                    Route::delete('{id}/delete', 'Project\OrderTerminDetailController@destroy')->name('delete');
                }); 
            });
        });
        // Workshop
        Route::group(['prefix' => 'workshop', 'as' => 'workshop.'], function() {
            Route::get('/', 'Project\WorkshopController@index')->name('index');
            Route::get('pending', 'Project\WorkshopController@indexpending')->name('pending');
            Route::get('create', 'Project\WorkshopController@create')->name('create');
            Route::post('store', 'Project\WorkshopController@store')->name('store');
            Route::get('{id}/read', 'Project\WorkshopController@show')->name('read'); 
            Route::put('{id}/submit', 'Project\WorkshopController@submit')->name('submit');
            Route::put('{id}/confirm', 'Project\WorkshopController@confirm')->name('confirm');
            Route::put('{id}/reject', 'Project\WorkshopController@reject')->name('reject');
            Route::delete('{id}/delete', 'Project\WorkshopController@destroy')->name('delete');
            // Detail
            Route::group(['prefix' => 'details', 'as' => 'details.'], function() {
                Route::post('store', 'Project\WorkshopDetailController@store')->name('store');
                Route::put('{id}/update', 'Project\WorkshopDetailController@update')->name('update');
                Route::delete('{id}/delete', 'Project\WorkshopDetailController@destroy')->name('delete');
            });
        });
         // Penawaran
         Route::group(['prefix' => 'offer', 'as' => 'offer.'], function() {
            Route::get('/', 'Project\PenawaranController@index')->name('index');
            Route::get('pending', 'Project\PenawaranController@indexpending')->name('pending');
            Route::post('store', 'Project\PenawaranController@store')->name('store');
            Route::post('create2', 'Project\PenawaranController@store2');
            Route::post('uploadatt', 'Project\PenawaranController@uploadattachmentmodel2');
            Route::get('{id}/getfile', 'Project\PenawaranController@getfile');
            Route::get('{id}/read', 'Project\PenawaranController@show')->name('read'); 
            Route::put('{id}/update', 'Project\PenawaranController@update')->name('update');
            Route::post('revisi', 'Project\PenawaranController@revisi');
            Route::put('{id}/submit', 'Project\PenawaranController@submit')->name('submit');
            Route::put('{id}/submitrevisi', 'Project\PenawaranController@submitrevisi')->name('submitrevisi');
            Route::put('{id}/confirm', 'Project\PenawaranController@confirm')->name('confirm');
            Route::put('{id}/reject', 'Project\PenawaranController@reject')->name('reject');
            Route::delete('{id}/delete', 'Project\PenawaranController@destroy')->name('delete');
            Route::post('deletefile', 'Project\PenawaranController@deletefile');
            // Detail
            Route::group(['prefix' => 'details', 'as' => 'details.'], function() {
                Route::post('store', 'Project\PenawaranDetailController@store')->name('store');
                Route::put('{id}/update', 'Project\PenawaranDetailController@update')->name('update');
                Route::delete('{id}/delete', 'Project\PenawaranDetailController@destroy')->name('delete');
            });
        });
        // Estimasi
        Route::group(['prefix' => 'estimasi', 'as' => 'estimasi.'], function() {
            Route::get('/', 'Project\EstimasiController@index')->name('index');
            Route::post('create', 'Project\EstimasiController@store')->name('create');
            Route::get('{id}/read', 'Project\EstimasiController@show')->name('read');
            Route::get('{id}/readtotal', 'Project\EstimasiController@showtotal')->name('readtotal');
            Route::get('{id}/readpending', 'Project\EstimasiController@showpending')->name('readpending');
            Route::put('{id}/update', 'Project\EstimasiController@update')->name('update');
            Route::put('{id}/submit', 'Project\EstimasiController@submit')->name('submit');
            Route::put('{id}/confirm', 'Project\EstimasiController@confirm')->name('confirm');
            Route::put('{id}/reject', 'Project\EstimasiController@reject')->name('reject');
            // Detail
            Route::group(['prefix' => 'details', 'as' => 'details.'], function() {
                Route::post('create', 'Project\EstimasiDetailController@store')->name('create');
                Route::put('{id}/update', 'Project\EstimasiDetailController@update')->name('update');
                Route::delete('{id}/delete', 'Project\EstimasiDetailController@destroy')->name('delete');
            });
        });
        
        // Absen Tukang
        Route::group(['prefix' => 'absentukang', 'as' => 'absentukang.'], function() {
            Route::get('/', 'Project\AbsenTukangController@index')->name('index');
            Route::get('indexapprove', 'Project\AbsenTukangController@indexapprove')->name('indexapprove');
            Route::post('create', 'Project\AbsenTukangController@store')->name('create');
            Route::get('{id}/read', 'Project\AbsenTukangController@show')->name('read');
            Route::put('{id}/update', 'Project\AbsenTukangController@update')->name('update');
            Route::delete('{id}/delete', 'Project\AbsenTukangController@destroy')->name('delete');
            Route::post('{id}/do_action', 'Project\AbsenTukangController@doAction')->name('doAction');
            // Detail
            Route::group(['prefix' => 'details', 'as' => 'details.'], function() {
                Route::get('/', 'Project\AbsenTukangDetailController@index')->name('index'); 
                Route::post('create', 'Project\AbsenTukangDetailController@store')->name('create');
                Route::post('payfee', 'Project\AbsenTukangDetailController@payfee')->name('payfee');
                Route::get('{id}/read', 'Project\AbsenTukangDetailController@show')->name('read');
                Route::put('{id}/update', 'Project\AbsenTukangDetailController@update')->name('update');
                Route::delete('{id}/delete', 'Project\AbsenTukangDetailController@destroy')->name('delete');
            });
        });
        // Borongan
        Route::group(['prefix' => 'borongan', 'as' => 'borongan.'], function() {
            Route::get('/', 'Project\BoronganController@index')->name('index');
            Route::post('create', 'Project\BoronganController@store')->name('create');
            Route::get('{id}/read', 'Project\BoronganController@show')->name('read');
            Route::put('{id}/update', 'Project\BoronganController@update')->name('update');
            Route::put('{id}/confirm', 'Project\BoronganController@confirm')->name('confirm');
            Route::put('{id}/reject', 'Project\BoronganController@reject')->name('reject');
            Route::put('{id}/submit', 'Project\BoronganController@submit')->name('submit');
            Route::delete('{id}/delete', 'Project\BoronganController@destroy')->name('delete');
            // Detail
            Route::group(['prefix' => 'details', 'as' => 'details.'], function() {
                Route::post('create', 'Project\BoronganDetailController@store')->name('create');
                Route::put('{id}/update', 'Project\BoronganDetailController@update')->name('update');
                Route::put('{id}/pay', 'Project\BoronganDetailController@payborongan')->name('pay');
                Route::delete('{id}/delete', 'Project\BoronganDetailController@destroy')->name('delete');
            });
        });
        // Manage Aset
        Route::group(['prefix' => 'manageasets', 'as' => 'manageasets.'], function() {
            Route::get('/', 'Project\ManageAsetController@index')->name('index');
            Route::get('listavailable', 'Project\ManageAsetController@listavailable')->name('listavailable');
            Route::post('create', 'Project\ManageAsetController@store')->name('create');
            Route::get('{id}/read', 'Project\ManageAsetController@show')->name('read');
            Route::put('{id}/update', 'Project\ManageAsetController@update')->name('update');
            Route::put('{id}/returnitem', 'Project\ManageAsetController@itemreturn')->name('returnitem');
            Route::delete('{id}/delete', 'Project\ManageAsetController@destroy')->name('delete');
        });
        // Kasbon
        Route::group(['prefix' => 'kasbon', 'as' => 'kasbon.'], function() {
            Route::get('/', 'Project\KasbonController@index')->name('index');
            Route::post('create', 'Project\KasbonController@store')->name('create');
            Route::get('{id}/read', 'Project\KasbonController@show')->name('read');
            Route::put('{id}/update', 'Project\KasbonController@update')->name('update');
            Route::delete('{id}/delete', 'Project\KasbonController@destroy')->name('delete');
            Route::post('{id}/do_action', 'Project\KasbonController@doAction')->name('doAction');
            Route::post('payment', 'Project\KasbonController@payment')->name('payment');
            Route::post('realisasi/submit', 'Project\KasbonController@realisasi')->name('realisasi.submit');
            // Detail
            Route::group(['prefix' => 'details', 'as' => 'details.'], function() {
                Route::get('/', 'Project\KasbonDetailController@index')->name('index');
                Route::post('create', 'Project\KasbonDetailController@store')->name('create');
                Route::get('{id}/read', 'Project\KasbonDetailController@show')->name('read');
                Route::put('{id}/update', 'Project\KasbonDetailController@update')->name('update');
                Route::delete('{id}/delete', 'Project\KasbonDetailController@destroy')->name('delete');
            });
            // Realisasi
            Route::group(['prefix' => 'realisasi', 'as' => 'realisasi.'], function() {
                Route::get('/', 'Project\KasbonRealisasiController@index')->name('index');
                Route::post('create', 'Project\KasbonRealisasiController@store')->name('create');
                Route::get('{id}/read', 'Project\KasbonRealisasiController@show')->name('read');
                Route::put('{id}/update', 'Project\KasbonRealisasiController@update')->name('update');
                Route::delete('{id}/delete', 'Project\KasbonRealisasiController@destroy')->name('delete');
            });
        });
        // Request Order
        Route::group(['prefix' => 'ro', 'as' => 'ro.'], function() {
            Route::get('/', 'Project\RoController@index')->name('index');
            Route::post('create', 'Project\RoController@store')->name('create');
            Route::get('{id}/read', 'Project\RoController@show')->name('read');
            Route::put('{id}/update', 'Project\RoController@update')->name('update');
            Route::delete('{id}/delete', 'Project\RoController@destroy')->name('delete');
            Route::post('{id}/do_action', 'Project\RoController@doAction')->name('doAction');
            // Detail
            Route::group(['prefix' => 'details', 'as' => 'details.'], function() {
                Route::get('/', 'Project\RoDetailController@index')->name('index');
                Route::post('create', 'Project\RoDetailController@store')->name('create');
                Route::get('{id}/read', 'Project\RoDetailController@show')->name('read');
                Route::put('{id}/update', 'Project\RoDetailController@update')->name('update');
                Route::delete('{id}/delete', 'Project\RoDetailController@destroy')->name('delete');
            });
        });
    });
    // Pembelian
    Route::group(['prefix' => 'pembelian', 'as' => 'pembelian.'], function() {
        // Project
        Route::group(['prefix' => 'pro', 'as' => 'pro.'], function() {
            Route::get('po', 'Pembelian\Project\PoPembelianController@index')->name('po.index');
            Route::get('po/{id}/read', 'Pembelian\Project\PoPembelianController@show')->name('po.read');
            Route::post('po/{id}/reject', 'Pembelian\Project\PoPembelianController@reject')->name('po.reject');
            Route::post('po/{id}/void', 'Pembelian\Project\PoPembelianController@void')->name('po.void');
            Route::post('po/{id}/approve', 'Pembelian\Project\PoPembelianController@approve')->name('po.approve');
            Route::get('beli', 'Pembelian\Project\PembelianController@index')->name('beli.index');
            Route::get('beli/{id}/read', 'Pembelian\Project\PembelianController@show')->name('beli.read');
        });
        // PO Pembelian
        Route::group(['prefix' => 'po', 'as' => 'po.'], function() {
            Route::get('/', 'Pembelian\PoPembelianController@index')->name('index');
            Route::post('create', 'Pembelian\PoPembelianController@store')->name('create');
            Route::get('{id}/read', 'Pembelian\PoPembelianController@show')->name('read');
            Route::put('{id}/update', 'Pembelian\PoPembelianController@update')->name('update');
            Route::delete('{id}/delete', 'Pembelian\PoPembelianController@destroy')->name('delete');
            Route::post('{id}/do_action', 'Pembelian\PoPembelianController@doAction')->name('doAction');
            Route::get('{toJson}/codepo', 'Pembelian\PoPembelianController@codePo')->name('codePo');
            // Detail Product
            Route::group(['prefix' => 'details', 'as' => 'details.'], function() {
                Route::get('/', 'Pembelian\PoPembelianDetailController@index')->name('index');
                Route::post('create', 'Pembelian\PoPembelianDetailController@store')->name('create');
                Route::get('{id}/read', 'Pembelian\PoPembelianDetailController@show')->name('read');
                Route::put('{id}/update', 'Pembelian\PoPembelianDetailController@update')->name('update');
                Route::delete('{id}/delete', 'Pembelian\PoPembelianDetailController@destroy')->name('delete');
            });
        });
        // Pembelian
        Route::group(['prefix' => 'pembelian', 'as' => 'pembelian.'], function() {
            Route::get('/', 'Pembelian\PembelianController@index')->name('index');
            Route::get('{id}/read', 'Pembelian\PembelianController@show')->name('read');
            Route::delete('{id}/delete', 'Pembelian\PembelianController@destroy')->name('delete');
            Route::group(['prefix' => 'revisi', 'as' => 'revisi.'], function() {
                Route::post('status', 'Pembelian\RevisiPembelianController@status')->name('status');
                Route::post('{id}/submit', 'Pembelian\RevisiPembelianController@submit')->name('submit');
                Route::post('{id}/approve', 'Pembelian\RevisiPembelianController@approve')->name('approve');
                Route::post('{id}/reject', 'Pembelian\RevisiPembelianController@reject')->name('reject');
                Route::post('detail/{id}/update', 'Pembelian\RevisiPembelianController@detailUpdate')->name('detailUpdate');
                Route::post('detail/{id}/delete', 'Pembelian\RevisiPembelianController@detailDelete')->name('detailDelete');
            });
        });
        // Hutang
        Route::group(['prefix' => 'hutang', 'as' => 'hutang.'], function() {
            Route::get('/', 'Pembelian\HutangController@index')->name('index');
            Route::get('{id}/read', 'Pembelian\HutangController@show')->name('read');
            Route::post('{id}/do_action', 'Pembelian\HutangController@doAction')->name('doAction');
        });
    });
    // Penjualan
    Route::group(['prefix' => 'penjualan', 'as' => 'penjualan.'], function() {
        //Menu
        Route::group(['prefix' => 'menu', 'as' => 'menu.'], function() {
            Route::post('create', 'Master\PenjualanMenuController@store');
            Route::post('update', 'Master\PenjualanMenuController@update');
            Route::post('delete', 'Master\PenjualanMenuController@destroy');
            Route::post('sync', 'Master\PenjualanMenuController@initialize');
        });
        

        // Project
        Route::group(['prefix' => 'pro', 'as' => 'pro.'], function() {
            Route::get('jual', 'Penjualan\Project\PenjualanController@index')->name('jual.index');
            Route::get('jual/{id}/read', 'Penjualan\Project\PenjualanController@show')->name('jual.read');
            Route::post('jual/{id}/reject', 'Penjualan\Project\PenjualanController@reject')->name('jual.reject');
            Route::post('jual/{id}/void', 'Penjualan\Project\PenjualanController@void')->name('jual.void');
            Route::post('jual/{id}/approve', 'Penjualan\Project\PenjualanController@approve')->name('jual.approve');
        });
        // Penjualan
        Route::group(['prefix' => 'penjualan', 'as' => 'penjualan.'], function() {
            Route::get('/', 'Penjualan\PenjualanController@index')->name('index');
            Route::post('create', 'Penjualan\PenjualanController@store')->name('create');
            Route::get('{id}/read', 'Penjualan\PenjualanController@show')->name('read');
            Route::put('{id}/update', 'Penjualan\PenjualanController@update')->name('update');
            Route::delete('{id}/delete', 'Penjualan\PenjualanController@destroy')->name('delete');
            Route::get('codefaktur', 'Penjualan\PenjualanController@codeFaktur')->name('codeFaktur');
            // Detail Product
            Route::group(['prefix' => 'details', 'as' => 'details.'], function() {
                Route::get('/', 'Penjualan\PenjualanDetailController@index')->name('index');
                Route::post('create', 'Penjualan\PenjualanDetailController@store')->name('create');
                Route::get('{id}/read', 'Penjualan\PenjualanDetailController@show')->name('read');
                Route::put('{id}/update', 'Penjualan\PenjualanDetailController@update')->name('update');
                Route::delete('{id}/delete', 'Penjualan\PenjualanDetailController@destroy')->name('delete');
            });
        });
        // Piutang
        Route::group(['prefix' => 'piutang', 'as' => 'piutang.'], function() {
            Route::get('/', 'Penjualan\PiutangController@index')->name('index');
            Route::get('{id}/read', 'Penjualan\PiutangController@show')->name('read');
            Route::post('{id}/do_action', 'Penjualan\PiutangController@doAction')->name('doAction');
        });
        // Delivery Order
        Route::group(['prefix' => 'do', 'as' => 'do.'], function() {
            Route::get('/', 'Penjualan\DeliveryOrderController@index')->name('index');
            Route::post('create', 'Penjualan\DeliveryOrderController@store')->name('create');
            Route::get('{id}/read', 'Penjualan\DeliveryOrderController@show')->name('read');
            Route::post('do_action', 'Penjualan\DeliveryOrderController@doAction')->name('doAction');
            Route::get('codedo', 'Penjualan\DeliveryOrderController@codeDo')->name('codeDo');
        });
        // Revisi Faktur
        Route::group(['prefix' => 'revisifaktur', 'as' => 'revisifaktur.'], function() {
            Route::get('/', 'Penjualan\RevisiFakturController@index')->name('index');
            Route::get('{id}/read', 'Penjualan\RevisiFakturController@show')->name('read');
            Route::put('{id}/update', 'Penjualan\RevisiFakturController@update')->name('update');
            Route::delete('{id}/delete', 'Penjualan\RevisiFakturController@destroy')->name('delete');
            Route::post('{id}/do_action', 'Penjualan\RevisiFakturController@doAction')->name('doAction');
            Route::post('revisi', 'Penjualan\RevisiFakturController@revisi')->name('revisi');
        });
    });

    Route::group(['prefix' => 'produksi', 'as' => 'produksi.'], function() {
            Route::get('/', 'Master\ProduksiController@index');
            Route::post('create', 'Master\ProduksiController@store');
            Route::post('adddetail', 'Master\ProduksiController@storedetail');
            Route::put('{id}/update', 'Master\ProduksiController@update');
            Route::put('{id}/updatedetail', 'Master\ProduksiController@updatedetail');
            Route::put('{id}/submitproduksi', 'Master\ProduksiController@submitproduksi');
            Route::delete('{id}/delete', 'Master\ProduksiController@destroy');
            Route::delete('{id}/deletedetail', 'Master\ProduksiController@destroydetail');
        });
        Route::group(['prefix' => 'produksigagal', 'as' => 'produksigagal.'], function() {
            Route::get('/', 'Master\ProduksiController@indexgagal');
            Route::post('create', 'Master\ProduksiController@storegagal');
            Route::put('{id}/update', 'Master\ProduksiController@updategagal');
            Route::delete('{id}/delete', 'Master\ProduksiController@destroygagal');
           
        });
    // Report
    Route::group(['prefix' => 'reports', 'as' => 'reports.'], function() {
        Route::get('hutang', 'Report\HutangController@hutang')->name('hutang');
        Route::get('hutangterbayar', 'Report\HutangController@hutangTerbayar')->name('hutangTerbayar');
        Route::get('pembelian', 'Report\PembelianController@pembelian')->name('pembelian');
        Route::get('pembeliandetail', 'Report\PembelianController@pembelianDetail')->name('pembelianDetail');
        Route::get('downloadExcelEFakturPembelian', 'Report\PembelianController@createExcelEFakturPembelian');
        Route::get('downloadExcelReportPembelianDetail', 'Report\PembelianController@createExcelPembelianDetail');
        Route::get('penjualan', 'Report\PenjualanController@penjualan')->name('penjualan');
        Route::get('downloadExcelEFaktur', 'Report\PenjualanController@createExcelEFaktur');
        Route::get('penjualandetail', 'Report\PenjualanController@penjualanDetail')->name('penjualanDetail');
        Route::get('downloadExcelReportPenjualanDetail', 'Report\PenjualanController@createExcelPenjualanDetail');
        Route::get('piutang', 'Report\PiutangController@piutang')->name('piutang');
        Route::get('piutangterbayar', 'Report\PiutangController@piutangTerbayar')->name('piutangTerbayar');
        Route::get('jumlahstock', 'Report\StockController@jumlahStock')->name('jumlahStock');
        Route::get('mutasistock', 'Report\StockController@mutasiStock')->name('mutasiStock');
    });
});
