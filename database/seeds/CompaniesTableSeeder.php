<?php

use App\Models\Master\Company;
use App\Models\Master\Warehouse;
use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new \Faker\Generator;
        $faker->addProvider(new \Faker\Provider\Base($faker));
        $faker->addProvider(new \Faker\Provider\Biased($faker));
        $faker->addProvider(new \Faker\Provider\de_CH\Person($faker));
        $faker->addProvider(new \Faker\Provider\id_ID\Person($faker));
        $faker->addProvider(new \Faker\Provider\id_ID\Address($faker));
        $faker->addProvider(new \Faker\Provider\id_ID\PhoneNumber($faker));
        $faker->addProvider(new \Faker\Provider\id_ID\Company($faker));
        /*
         * Add New
         *
         */
        $Items = [
            [
                'name' => 'CV. Cipta Indah Konstruksi',
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
                'npwp' => $faker->avs13,
                'taxable' => 'yes', // in:yes,no
                'taxable_rate' => 10,
                'currency_code' => 'IDR',
                'code_numb' => '01',
                'code_alpha' => 'CIKON',
                'type' => 'project',
            ], [
                'name' => 'CV. Cipta Indah Kreasi',
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
                'npwp' => $faker->avs13,
                'taxable' => 'yes', // in:yes,no
                'taxable_rate' => 10,
                'currency_code' => 'IDR',
                'code_numb' => '02',
                'code_alpha' => 'CIKRE',
                'type' => 'gudang',
            ]
        ];
        echo "\e[32mSeeding:\e[0m CompaniesTableSeeder\r\n";
        for ($i=0; $i < count($Items); $i++) { 
            $child = createCompany([
                'name' => $Items[$i]['name'],
                'address' => $Items[$i]['address'],
                'phone' => $Items[$i]['phone'],
                'npwp' => $Items[$i]['npwp'],
                'taxable' => $Items[$i]['taxable'],
                'taxable_rate' => $Items[$i]['taxable_rate'],
                'currency_code' => $Items[$i]['currency_code'],
                'code_numb' => $Items[$i]['code_numb'],
                'code_alpha' => $Items[$i]['code_alpha'],
                'type' => $Items[$i]['type'],
                'coa_penjualan' => '5.01.01',
                'coa_pembelian' => '5.03.01',
                'coa_hutang' => '3.01.01',
                'coa_piutang' => '1.03.01',
                'coa_pajak' => '1.04.01',
                'coa_pajak_out' => '3.03.03',
                'coa_discount' => '5.01.03',
                'coa_discount_buy' => '5.03.03',
                'coa_ikhtisar_labarugi' => '4.02.01',
                'coa_labarugi_ditahan' => '4.02.02'
            ], true, true);
            echo "\e[32mSeeding:\e[0m CompaniesTableSeeder - Company:".$child->name."\r\n";
        }
    }
}
