<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesTableSeeder::class,
            MenusTableSeeder::class,
            RoleMenusTableSeeder::class,
            CompaniesTableSeeder::class,
            KaryawanTableSeeder::class,
            UsersTableSeeder::class,
            SuppliersTableSeeder::class,
            CustomersTableSeeder::class,
            TukangTableSeeder::class,
            VendorsTableSeeder::class,
            ProductTableSeeder::class,
        ]);
    }
}
