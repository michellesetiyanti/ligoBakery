<?php

use App\Models\Auth\User;
use App\Models\Master\Company;
use App\Models\Master\Karyawan;
use Illuminate\Database\Seeder;

class KaryawanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $companies = Company::whereNotNull('parent_id')->get();
        $companies = Company::whereNull('parent_id')->get();
        echo "\e[32mSeeding:\e[0m KaryawanTableSeeder\r\n";
        foreach ($companies as $company) {
            factory(Karyawan::class, 3)->create([
                'company_id' => $company->company_id,
            ])->each(function ($karyawan) {
                echo "\e[32mSeeding:\e[0m KaryawanTableSeeder - Karyawan:".$karyawan->name." (".$karyawan->position.") \r\n";
            });
            factory(Karyawan::class, 2)->create([
                'company_id' => $company->company_id,
                'position' => 'sales',
            ])->each(function ($karyawan) {
                echo "\e[32mSeeding:\e[0m KaryawanTableSeeder - Karyawan:".$karyawan->name." (".$karyawan->position.") \r\n";
            });
        }
    }
}
