<?php

use App\Models\Auth\User;
use App\Models\Master\Vendor;
use Illuminate\Database\Seeder;

class VendorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::where('type', 'superadmin')->get();
        echo "\e[32mSeeding:\e[0m VendorTableSeeder\r\n";
        foreach ($users as $user) {
            factory(Vendor::class)->create([
                'company_id' => $user->company_id,
                'code' => 'FACTORY_VEND'.$user->company_id,
                'user_id' => $user->user_id,
            ])->each(function ($vendors) {
                echo "\e[32mSeeding:\e[0m VendorTableSeeder - Vendor:".$vendors->name."\r\n";
            });
        }
    }
}
