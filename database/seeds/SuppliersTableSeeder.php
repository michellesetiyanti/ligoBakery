<?php

use App\Models\Auth\User;
use App\Models\Master\Supplier;
use Illuminate\Database\Seeder;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::where('type', 'superadmin')->get();
        echo "\e[32mSeeding:\e[0m SupplierTableSeeder\r\n";
        foreach ($users as $user) {
            $args = array('SPL', 'INT', 'ME', 'PL');
            $region = array(61, 62, 63, 64);
            for ($i=0; $i < count($args); $i++) { 
                $suppliers = factory(Supplier::class)->create([
                    'company_id' => $user->company_id,
                    'user_id' => $user->user_id,
                    'code' => $args[$i].'-'.$user->company->code_alpha.' '.str_pad(rand(1, 9), 3, '0', STR_PAD_LEFT).'-'.$region[$i],
                    'region' => $region[$i],
                    'name' => $user->company->name,
                    'address' => $user->company->address,
                    'phone' => $user->company->phone,
                    'currency_code' => $user->company->currency_code,
                    'npwp' => null,
                    'taxable' => $user->company->taxable,
                    'taxable_rate' => $user->company->taxable_rate,
                    'status' => 'active',
                ]);
                echo "\e[32mSeeding:\e[0m SupplierTableSeeder - Supplier:".$suppliers->name."\r\n";
            }
        }
    }
}
