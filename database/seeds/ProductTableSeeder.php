<?php

use App\Models\Master\Product;
use App\Models\Master\ProductCategory;
use App\Models\Master\ProductUnit;
use App\Models\Master\Supplier;
use App\Models\Master\Company;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add New
         *
         */
        $Units = [
            ['code' => 'ton', 'name' => 'Tonne'],
            ['code' => 'kg', 'name' => 'Kilogram'],
            ['code' => 'g', 'name' => 'Gram'],
            ['code' => 'mg', 'name' => 'Miligram'],
            ['code' => 'lb', 'name' => 'Pound'],
            ['code' => 'ons', 'name' => 'Ounce'],
            ['code' => 'helai', 'name' => 'Helai'],
            ['code' => 'lusin', 'name' => 'Lusin'],
            ['code' => 'kodi', 'name' => 'Kodi'],
            ['code' => 'gross', 'name' => 'Gross'],
            ['code' => 'rim', 'name' => 'Rim'],
            ['code' => 'lt', 'name' => 'Liter'],
            ['code' => 'ml', 'name' => 'Mililiter'],
            ['code' => 'box', 'name' => 'Box'],
            ['code' => 'botol', 'name' => 'Botol'],
            ['code' => 'pcs', 'name' => 'Piece'],
            ['code' => 'bag', 'name' => 'Bags'],
        ];
        echo "\e[32mSeeding:\e[0m ProductTableSeeder\r\n";
        for ($i=0; $i < count($Units); $i++) { 
            factory(ProductUnit::class)->create($Units[$i]);
            echo "\e[32mSeeding:\e[0m ProductTableSeeder - Product Units:".$Units[$i]['name']."\r\n";
        }
        
        $Categories = [
            ['name' => 'Bahan Bangunan', 'description' => null],
            ['name' => 'Bahan Makanan', 'description' => null],
        ];
        for ($i=0; $i < count($Categories); $i++) { 
            factory(ProductCategory::class)->create($Categories[$i])->each(function ($productCategory) {
                echo "\e[32mSeeding:\e[0m ProductTableSeeder - Product Categories:".$productCategory->name."\r\n";
            });
        }
               
        $Products = [
            [
                'category' => 'Bahan Bangunan',
                'name' => 'Paku (kg)',
                'unit_lg' => 'kg',
                'unit_sm' => 'kg',
                'unit_qty' => 1,
            ], [
                'category' => 'Bahan Bangunan',
                'name' => 'Paku (ons)',
                'unit_lg' => 'ons',
                'unit_sm' => 'ons',
                'unit_qty' => 1,
            ], [
                'category' => 'Bahan Bangunan',
                'name' => 'Triplek (lusin)',
                'unit_lg' => 'lusin',
                'unit_sm' => 'lusin',
                'unit_qty' => 1,
            ], [
                'category' => 'Bahan Bangunan',
                'name' => 'Triplek (helai)',
                'unit_lg' => 'helai',
                'unit_sm' => 'helai',
                'unit_qty' => 1,
            ]
        ];
        $companies = Company::has('warehouse')->get();
        foreach ($companies as $company) {
            foreach ($Products as $product) {
                $productCategory = ProductCategory::where('name', $product['category'])->first();
                $wh_id = $company->warehouse->pluck('wh_id')->first();
                $code = app(\App\Http\Controllers\Api\Master\ProductController::class)->codeProduct([
                    'wh_id' => $wh_id,
                    'comp_code' => $company->code_alpha,
                ]);
                factory(Product::class)->create([
                    'code' => $code,
                    'wh_id' => $wh_id,
                    'user_id' => 1,
                    'product_c_id' => $productCategory->product_c_id,
                    'name' => $product['name'],
                    'unit_lg' => $product['unit_lg'],
                    'unit_sm' => $product['unit_sm'],
                    'unit_qty' => $product['unit_qty'],
                ])->each(function ($newProduct) {
                    echo "\e[32mSeeding:\e[0m ProductTableSeeder - Product:".$newProduct->name."\r\n";
                });
            }
        }
    }
}
