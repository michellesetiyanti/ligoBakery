<?php

use App\Models\Auth\User;
use App\Models\Master\Customer;
use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::where('type', '<>', 'owner')->get();
        echo "\e[32mSeeding:\e[0m CustomerTableSeeder\r\n";
        foreach ($users as $user) {
            factory(Customer::class, 5)->create([
                'company_id' => $user->company_id,
                'user_id' => $user->user_id,
            ])->each(function ($customer) {
                echo "\e[32mSeeding:\e[0m CustomerTableSeeder - Customer:".$customer->name."\r\n";
            });
        }
    }
}
