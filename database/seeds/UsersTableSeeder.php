<?php

use App\Models\Auth\Role;
use App\Models\Auth\User;
use App\Models\Master\Company;
use App\Models\Master\ResponsiblePerson;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add New
         *
         */
        $roles = Role::all();
        echo "\e[32mSeeding:\e[0m UsersTableSeeder\r\n";
        // $companies = Company::whereNotNull('parent_id')->get();
        $companies = Company::whereNull('parent_id')->get();
        for ($i=0; $i < $companies->count(); $i++) {
            // Admin
            if ($type = $roles->where('code', '=', 'superadmin')->pluck('code')->first()) {
                $uniqueAdmin = 'admin'.uniqueCompany($companies[$i]->name);
                $uniqueAdmin = factory(User::class)->create([
                    'company_id' => $companies[$i]->company_id,
                    'type' => $type,
                    'username' => $uniqueAdmin,
                    'email' => $uniqueAdmin.'@email.com',
                    'password' => 'admin123',
                ]);
                echo "\e[32mSeeding:\e[0m UsersTableSeeder - User Admin:".$uniqueAdmin->email."\r\n";
            }
            // Sales
            if ($type = $roles->where('code', '=', 'sales')->pluck('code')->first()) {
                $uniqueSales = 'sales'.uniqueCompany($companies[$i]->name);
                $uniqueSales = factory(User::class)->create([
                    'company_id' => $companies[$i]->company_id,
                    'karyawan_id' => $companies[$i]->karyawan->where('position', 'sales')->pluck('karyawan_id')->first(),
                    'type' => $type,
                    'username' => $uniqueSales,
                    'email' => $uniqueSales.'@email.com',
                    'password' => 'sales123',
                ]);
                echo "\e[32mSeeding:\e[0m UsersTableSeeder - User Sales:".$uniqueSales->email."\r\n";
            }
        }

        $owners = User::where('type', 'owner')->get();
        foreach ($owners as $owner) {
            $user_id = $owner->user_id;
            $username = $owner->username;
            $email = $owner->email;
            $type = $owner->type;
            $company_id = $owner->company_id;
            $company_name = $owner->company->name;
            $karyawan_id = $owner->karyawan_id;
            $karyawan_name = $owner->karyawan->name ?? null;

            $rp = ResponsiblePerson::where('name', $owner->role->name)->first();
            if (empty($rp)) {
                $user = collect();
                $user->push(compact('user_id', 'username', 'email', 'type', 'company_id', 'company_name', 'karyawan_id', 'karyawan_name'));
                $rp = ResponsiblePerson::create([
                    'name' => $owner->role->name,
                    'description' => $owner->role->description,
                    'user' => json_encode($user),
                ]);
            } else {
                $user = collect($rp->user);
                $user->push(compact('user_id', 'username', 'email', 'type', 'company_id', 'company_name', 'karyawan_id', 'karyawan_name'));
                $rp->update([
                    'user' => json_encode($user)
                ]);
            }
        }
    }
}
