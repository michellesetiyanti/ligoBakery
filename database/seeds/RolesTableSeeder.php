<?php

use Illuminate\Database\Seeder;
use App\Models\Auth\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Add New
         *
         */
        $Items = [
            [
                'code' => 'owner',
                'name' => 'Owner',
                'description' => 'The "Owner" has all privileges and could not be removed.'
            ], [
                'code' => 'superadmin',
                'name' => 'Super Admin',
                'description' => 'The "Super Admin" is required to assist the Owner, has different privileges and may be created by Owner.'
            ], [
                'code' => 'accounting',
                'name' => 'Accounting',
                'description' => 'The "Accounting" is required to assist the Super Admin, has different privileges and may be created by Owner / Super Admin.'
            ], [
                'code' => 'sales',
                'name' => 'Sales',
                'description' => 'The "Sales" is required to assist the Super Admin, has different privileges and may be created by Owner / Super Admin.'
            ], [
                'code' => 'adminfaktur',
                'name' => 'Admin Faktur',
                'description' => 'The "Admin Faktur" is required to assist the Super Admin, has different privileges and may be created by Owner / Super Admin.'
            ], [
                'code' => 'admingudang',
                'name' => 'Admin Gudang',
                'description' => 'The "Admin Gudang" is required to assist the Super Admin, has different privileges and may be created by Owner / Super Admin.'
            ]
        ];
        echo "\e[32mSeeding:\e[0m RolesTableSeeder\r\n";
        foreach ($Items as $Item) {
            if (is_null(Role::where('code', '=', $Item['code'])->first())) {
                $newItem = Role::create([
                    'code' => $Item['code'],
                    'name' => $Item['name'],
                    'description' => $Item['description']
                ]);
                echo "\e[32mSeeding:\e[0m RolesTableSeeder - Role:".$Item['name']."\r\n";
            }
        }
    }
}
