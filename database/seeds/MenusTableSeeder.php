<?php

use App\Models\Auth\Menu;
use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Menus = file_get_contents(base_path('import').'/menus.csv');
        $Menus = str_replace(['(',')','"',';',"'"], '', $Menus);
        $Menus = explode("\n", $Menus);
        echo "\e[32mSeeding:\e[0m MenusTableSeeder\r\n";
        foreach ($Menus as $Menu) {
            list($menu_id, $name, $icon, $showw, $created_at, $updated_at) = array_pad(explode(',', $Menu), count($Menus), null);
            if ($name) {
                if (is_null(Menu::where('name', $name)->first())) {
                    Menu::create([
                        'name' => str_replace(array('%lpar%', '%rpar%'), array('(', ')'), trim($name)),
                        'icon' => trim($icon),
                        'showw' => trim($showw),
                    ]);
                }
            }
        }
        
        $MenuDetails = file_get_contents(base_path('import').'/menu_details.csv');
        $MenuDetails = str_replace(['(',')','"',';',"'"], '', $MenuDetails);
        $MenuDetails = explode("\n", $MenuDetails);
        foreach ($MenuDetails as $MenuDetail) {
            list($menu_d_id, $menu_id, $slug, $name, $url, $readd, $createe, $confirmm, $updatee, $deletee, $created_at, $updated_at) = array_pad(explode(',', $MenuDetail), count($MenuDetails), null);
            if ($MenuById = Menu::find($menu_id)) {
                if ($MenuById->submenu->where('slug', $slug)->isEmpty()) {
                    $MenuById->submenu()->create([
                        'slug' => trim($slug),
                        'name' => trim($name),
                        'url' => trim($url),
                        'readd' => trim($readd),
                        'createe' => trim($createe),
                        'confirmm' => trim($confirmm),
                        'updatee' => trim($updatee),
                        'deletee' => trim($deletee),
                    ]);
                    echo "\e[32mSeeding:\e[0m MenusTableSeeder - Menu:".$name."\r\n";
                }
            }
        }
    }
}
