<?php

use App\Models\Auth\User;
use App\Models\Master\Tukang;
use Illuminate\Database\Seeder;

class TukangTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::where('type', '<>', 'owner')->get();
        echo "\e[32mSeeding:\e[0m TukangTableSeeder\r\n";
        foreach ($users as $user) {
            $tukang = factory(Tukang::class, 5)->create([
                'company_id' => $user->company_id,
                'user_id' => $user->user_id,
            ]);
            foreach ($tukang as $tkg) {
                echo "\e[32mSeeding:\e[0m TukangTableSeeder - Tukang:".$tkg->name."\r\n";
            }
        }
    }
}
