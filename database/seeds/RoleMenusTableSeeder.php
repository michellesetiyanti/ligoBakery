<?php

use App\Models\Auth\Role;
use Illuminate\Database\Seeder;

class RoleMenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $RoleMenus = file_get_contents(base_path('import').'/role_menus.csv');
        $RoleMenus = str_replace(['(',')','"',';',"'"], '', $RoleMenus);
        $RoleMenus = explode("\n", $RoleMenus);
        echo "\e[32mSeeding:\e[0m RoleMenusTableSeeder\r\n";
        foreach ($RoleMenus as $RoleMenu) {
            list($role_m_id, $role_id, $access, $created_at, $updated_at) = array_pad(explode(',', $RoleMenu), count($RoleMenus), null);
            if ($RoleById = Role::find($role_id)) {
                if (empty($RoleById->roleMenus)) {
                    preg_match('/0x(.*)/i', $access, $matches);
                    $access = hex2bin($matches[1]);
                    $RoleById->roleMenus()->create(compact('access'));
                    echo "\e[32mSeeding:\e[0m RoleMenusTableSeeder - Role Menus:".$RoleById->name."\r\n";
                }
            }
        }
    }
}
