<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Master\ProductCategory::class, function (Faker $faker) {
    return [
        'name' => 'Bahan Bangunan',
        'description' => null,
    ];
});
