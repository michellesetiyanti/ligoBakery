<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Master\Tukang::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Base($faker));
    $faker->addProvider(new \Faker\Provider\Biased($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Person($faker));
    $faker->addProvider(new \Faker\Provider\it_CH\Person($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Address($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\PhoneNumber($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Company($faker));
    return [
        'name' => $faker->name('male'),
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'upah' => $faker->biasedNumberBetween(50000, 200000),
        'description' => null,
        'entry_at' => \Carbon\Carbon::now()->format('d/m/Y'),
        'status' => 'active',
    ];
});
