<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Master\Supplier::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Base($faker));
    $faker->addProvider(new \Faker\Provider\Biased($faker));
    $faker->addProvider(new \Faker\Provider\de_CH\Person($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Person($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Address($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\PhoneNumber($faker));
    return [
        'code' => Str::random(10),
        'name' => $faker->name,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'reference' => $faker->realText($faker->numberBetween(10, 20)),
        'currency_code' => 'IDR',
        'npwp' => $faker->avs13,
        'taxable' => 'yes',
        'taxable_rate' => '10',
        'status' => 'active',
    ];
});
