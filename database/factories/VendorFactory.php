<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Master\Vendor::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Base($faker));
    $faker->addProvider(new \Faker\Provider\Biased($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Person($faker));
    $faker->addProvider(new \Faker\Provider\it_CH\Person($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Address($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\PhoneNumber($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Company($faker));
    return [
        'code' => Str::random(10),
        'name' => $faker->name,
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'reference' => $faker->realText($faker->numberBetween(10,20)),
        'currency_code' => 'IDR',
        'taxable' => 'no',
        'taxable_rate' => '0',
        'status' => 'active',
    ];
});
