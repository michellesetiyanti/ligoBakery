<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Master\Product::class, function (Faker $faker) {
    return [
        'wh_id' => 1,
        'user_id' => 1,
        'code' => null,
        'name' => 'Paku',
        'description' => null,
        'unit_lg' => 'kg',
        'unit_sm' => 'ons',
        'unit_qty' => 10,
        'total_lg_qty' => 0,
        'total_sm_qty' => 0,
    ];
});
