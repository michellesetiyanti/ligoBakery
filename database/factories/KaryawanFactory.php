<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Master\Karyawan::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Base($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Address($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Company($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Person($faker));
    $faker->addProvider(new \Faker\Provider\it_CH\Person($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\PhoneNumber($faker));
    $faker->addProvider(new \Faker\Provider\ms_MY\Payment($faker));
    $faker->addProvider(new \Ottaviano\Faker\Gravatar($faker));
    return [
        'company_id' => 1,
        'position' => 'staff',
        'name' => $faker->name('male'),
        'nik' => $faker->avs13,
        'ttl' => 'Jakarta, 30-12-2000',
        'address' => $faker->address,
        'phone' => $faker->e164PhoneNumber,
        'email' => $faker->unique()->safeEmail,
        'gender' => 'Laki-laki',
        'marriage' => 'Kawin',
        'photo' => null,
        'npwp' => $faker->avs13,
        'salary' => $faker->biasedNumberBetween(1000000, 5000000),
        'bank_name' => $faker->bank,
        'bank_account' => $faker->bankAccountNumber,
        'status' => 'active',
    ];
});
