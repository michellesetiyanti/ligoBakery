<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Models\Auth\User::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Base($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Person($faker));
    $faker->addProvider(new \Faker\Provider\it_CH\Person($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Address($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\PhoneNumber($faker));
    $faker->addProvider(new \Faker\Provider\id_ID\Company($faker));
    return [
        'company_id' => 1,
        'karyawan_id' => null,
        'username' => $faker->username,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '12345678',
        'remember_token' => Str::random(10),
        'type' => 'unverified',
        'locale' => app()->getLocale(),
        'status' => 'active',
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
