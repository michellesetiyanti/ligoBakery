<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(App\Models\Master\ProductUnit::class, function (Faker $faker) {
    $faker->addProvider(new \Faker\Provider\Base($faker));
    return [
        'code' => 'kg',
        'name' => 'Kilogram',
        'description' => null,
    ];
});
