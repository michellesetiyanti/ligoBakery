<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLimitOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('limit_orders', function (Blueprint $table) {
            $table->bigIncrements('limit_id');
            $table->integer('company_id');
            $table->integer('order_id');
            $table->integer('user_id');
            $table->integer('product_id');
            $table->integer('qty_lg');
            $table->integer('qty_sm');
            $table->double('price_lg')->default('0.00');
            $table->double('price_sm')->default('0.00');
            $table->string('entry_at' , 255); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('limit_orders');
    }
}
