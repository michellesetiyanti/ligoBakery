<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkshopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workshops', function (Blueprint $table) {
            $table->bigIncrements('workshop_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('order_id');
            $table->string('workshop_code');
            $table->double('total', 20, 2)->default('0.00');
            $table->string('status')->default('open');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('order_id')->references('order_id')->on('orders');
        });
        
        Schema::create('workshop_details', function (Blueprint $table) {
            $table->bigIncrements('workshop_d_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('wh_id');
            $table->unsignedBigInteger('workshop_id');
            $table->unsignedBigInteger('product_id');
            $table->string('unit_lg');
            $table->string('unit_sm');
            $table->integer('qty_lg')->default('0');
            $table->integer('qty_sm')->default('0');
            $table->double('price_lg', 20, 2)->default('0.00');
            $table->double('price_sm', 20, 2)->default('0.00'); 
            $table->double('amount', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('wh_id')->references('wh_id')->on('warehouses');
            $table->foreign('workshop_id')->references('workshop_id')->on('workshops')->onDelete('cascade');
            $table->foreign('product_id')->references('product_id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workshops');
        Schema::dropIfExists('workshop_details');
    }
}
