<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJenisPembayaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_pembayarans', function (Blueprint $table) {
            $table->bigIncrements('jenis_pembayaran_id');
            $table->unsignedBigInteger('category_pembayaran_id');
            $table->string('name');
            $table->string('coa_code');
            $table->timestamps();
            $table->foreign('category_pembayaran_id')->references('category_pembayaran_id')->on('category_pembayarans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenis_pembayarans');
    }
}
