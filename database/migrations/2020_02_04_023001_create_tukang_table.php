<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTukangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tukang', function (Blueprint $table) {
            $table->bigIncrements('tukang_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->string('code')->nullable();
            $table->string('nik')->nullable();
            $table->string('name');
            $table->string('ttl')->nullable();
            $table->text('address')->nullable();
            $table->text('address_now')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone2')->nullable();
            $table->string('sibling_phone')->nullable();
            $table->double('upah', 20, 2)->default('0.00')->comment('per hari');
            $table->string('bank_name')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('bank_account')->nullable();
            $table->text('description')->nullable()->comment('per WO');
            $table->date('entry_at');
            $table->string('penempatan')->nullable();
            $table->string('induk_tukang')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tukang');
    }
}
