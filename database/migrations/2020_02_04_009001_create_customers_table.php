<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('customer_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->string('code', 20)->nullable()->unique();
            $table->string('name');
            $table->string('nik')->nullable();
            $table->string('nib')->nullable();
            $table->string('spkp')->nullable();
            $table->string('birthday_place')->nullable();
            $table->string('note')->nullable();
            $table->text('address')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('vendor_loc')->nullable();
            $table->string('vendor_pic')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone2')->nullable();
            $table->string('reference')->nullable();
            $table->string('currency_code', 10)->index()->comment('IDR, USD, etc');
            $table->string('npwp')->nullable();
            $table->string('pkp_name')->nullable();
            $table->text('pkp_address')->nullable();
            $table->double('credit_limit', 20, 2)->nullable()->default('0.00');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->enum('type', ['pribadi', 'corporate'])->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->foreign('company_id')->references('company_id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
