<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produksis', function (Blueprint $table) {
            $table->bigIncrements('id_produksi');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('input_by');
            $table->string('date');
            $table->string('description')->nullable();
            $table->string('status')->default('open');
            $table->timestamps();
            $table->foreign('company_id')->references('company_id')->on('companies')->onDelete('cascade');
        });

         Schema::create('produksi_details', function (Blueprint $table) {
            $table->bigIncrements('id_produksi_details');
            $table->unsignedBigInteger('id_produksi');
            $table->integer('id_menu_jual');
            $table->integer('qty_produksi');
            $table->integer('qty_gagal');
            $table->integer('qty_jual');
            $table->timestamps();
            $table->foreign('id_produksi')->references('id_produksi')->on('produksis')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produksis');
        Schema::dropIfExists('produksi_details');
    }
}
