<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('offer_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('customer_id');
            $table->string('entry_at');
            $table->string('nomor');
            $table->string('name');
            $table->string('hasil');
            $table->text('note')->nullable();
            $table->double('amount', 20, 2)->default('0.00');
            $table->string('status');
            $table->integer('model');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('customer_id')->references('customer_id')->on('customers');
        });

        Schema::create('offer_details', function (Blueprint $table) {
            $table->bigIncrements('offer_d_id');
            $table->unsignedBigInteger('offer_id');
            $table->string('item');
            $table->string('unit');
            $table->integer('vol');
            $table->double('price', 20, 2)->default('0.00');
            $table->double('total', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('offer_id')->references('offer_id')->on('offers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
        Schema::dropIfExists('offer_details');
    }
}
