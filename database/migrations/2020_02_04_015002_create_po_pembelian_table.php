<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePoPembelianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_pembelian', function (Blueprint $table) {
            $table->bigIncrements('po_pb_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('supplier_id')->nullable();
            $table->unsignedBigInteger('wh_id');
            $table->unsignedBigInteger('po_pro_id')->nullable();
            $table->string('code', 100)->nullable();
            $table->string('code_spk')->nullable();
            $table->string('name')->unique();
            $table->double('ongkir', 20, 2)->default('0.00');
            $table->text('ongkir_desc')->nullable();
            $table->double('biaya_lain', 20, 2)->default('0.00');
            $table->text('biaya_lain_desc')->nullable();
            $table->double('total', 20, 2)->default('0.00');
            $table->date('due_date')->nullable();
            $table->enum('payment', ['paid', 'unpaid'])->nullable();
            $table->string('coa_code')->nullable();
            $table->enum('status', ['open', 'pending', 'reject', 'void', 'close'])->default('open');
            $table->text('notes')->nullable();
            $table->text('requests')->nullable();
            $table->string('serial', 100)->nullable();
            $table->binary('signed')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('supplier_id')->references('supplier_id')->on('suppliers');
            $table->foreign('wh_id')->references('wh_id')->on('warehouses');
            $table->foreign('po_pro_id')->references('po_pro_id')->on('po_projects');
        });
        
        Schema::create('po_pembelian_details', function (Blueprint $table) {
            $table->bigIncrements('po_pb_d_id');
            $table->unsignedBigInteger('po_pb_id');
            $table->unsignedBigInteger('product_id');
            $table->string('unit_lg');
            $table->string('unit_sm');
            $table->float('qty_lg', 15, 2)->default('0');
            $table->float('qty_sm', 15, 2)->default('0');
            $table->double('price_lg', 20, 2)->default('0.00');
            $table->double('price_sm', 20, 2)->default('0.00');
            $table->float('tax', 5, 2)->default('0.00')->comment('%');
            $table->double('discount', 20, 2)->default('0.00');
            $table->double('amount', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('po_pb_id')->references('po_pb_id')->on('po_pembelian')->onDelete('cascade');
            $table->foreign('product_id')->references('product_id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('po_pembelian');
        Schema::dropIfExists('po_pembelian_details');
    }
}
