<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduksiGagalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produksi_gagals', function (Blueprint $table) {
            $table->bigIncrements('id_produksi_gagal');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('id_produksi_details');
            $table->integer('qty');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->foreign('id_produksi_details')->references('id_produksi_details')->on('produksi_details')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produksi_gagals');
    }
}
