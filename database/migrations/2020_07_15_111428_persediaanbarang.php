<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Persediaanbarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persediaan_barang', function (Blueprint $table) {
            $table->bigIncrements('persediaan_barang_id');
            $table->unsignedBigInteger('company_id');
            $table->string('bulan');
            $table->string('tahun');
            $table->double('jumlah', 20, 2)->default('0.00');
            $table->foreign('company_id')->references('company_id')->on('companies')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persediaan_barang');

    }
}
