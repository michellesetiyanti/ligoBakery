<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePoProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_projects', function (Blueprint $table) {
            $table->bigIncrements('po_pro_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('wh_id');
            $table->string('code');
            $table->string('code_spk');
            $table->double('ongkir', 20, 2)->default('0.00');
            $table->text('ongkir_desc')->nullable();
            $table->double('biaya_lain', 20, 2)->default('0.00');
            $table->text('biaya_lain_desc')->nullable();
            $table->double('total', 20, 2)->default('0.00');
            $table->enum('status', ['open', 'pending', 'reject', 'void', 'close'])->default('open');
            $table->text('notes')->nullable();
            $table->date('approved_at')->nullable();
            $table->binary('signed')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('wh_id')->references('wh_id')->on('warehouses');
        });
        
        Schema::create('po_project_details', function (Blueprint $table) {
            $table->bigIncrements('po_pro_d_id');
            $table->unsignedBigInteger('po_pro_id');
            $table->unsignedBigInteger('product_id');
            $table->string('unit_lg');
            $table->string('unit_sm');
            $table->float('qty_lg', 15, 2)->default('0');
            $table->float('qty_sm', 15, 2)->default('0');
            $table->double('price_lg', 20, 2)->default('0.00');
            $table->double('price_sm', 20, 2)->default('0.00');
            $table->float('tax', 5, 2)->default('0.00')->comment('%');
            $table->double('discount', 20, 2)->default('0.00');
            $table->double('amount', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('po_pro_id')->references('po_pro_id')->on('po_projects')->onDelete('cascade');
            $table->foreign('product_id')->references('product_id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('po_projects');
        Schema::dropIfExists('po_project_details');
    }
}
