<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjustments', function (Blueprint $table) {
            $table->bigIncrements('adjust_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('wh_id');
            $table->string('date_entry');
            $table->string('reference');
            $table->text('description')->nullable();
            $table->enum('status', ['open','pending', 'complete'])->default('pending');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies')->onDelete('cascade');
            $table->foreign('wh_id')->references('wh_id')->on('warehouses');
        });
        
        Schema::create('adjustment_details', function (Blueprint $table) {
            $table->bigIncrements('adjust_d_id');
            $table->unsignedBigInteger('adjust_id');
            $table->unsignedBigInteger('product_id'); 
            $table->integer('qty_lg')->default('0');
            $table->integer('qty_sm')->default('0');
            $table->integer('price_sm')->default('0');
            $table->timestamps();
            $table->foreign('adjust_id')->references('adjust_id')->on('adjustments')->onDelete('cascade');
            $table->foreign('product_id')->references('product_id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adjustments');
        Schema::dropIfExists('adjustment_details');
    }
}
