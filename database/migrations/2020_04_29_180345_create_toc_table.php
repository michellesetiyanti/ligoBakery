<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toc', function (Blueprint $table) {
            $table->bigIncrements('toc_id');
            $table->unsignedBigInteger('po_pb_id');
            $table->unsignedBigInteger('pembelian_id')->nullable();
            $table->string('follow_up')->nullable();
            $table->string('payment_terms')->nullable();
            $table->enum('stb', ['loco', 'franco', 'fob', 'fot', 'c&f', 'cif'])->nullable();
            $table->string('stb_desc')->nullable();
            $table->string('retv_schedule')->nullable();
            $table->timestamps();
            $table->foreign('po_pb_id')->references('po_pb_id')->on('po_pembelian')->onDelete('cascade');
            $table->foreign('pembelian_id')->references('pembelian_id')->on('pembelian')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toc');
    }
}
