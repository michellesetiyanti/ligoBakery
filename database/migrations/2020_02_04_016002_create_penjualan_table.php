<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenjualanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penjualan', function (Blueprint $table) {
            $table->bigIncrements('penjualan_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('sales_id')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('wh_id');
            $table->unsignedBigInteger('po_pro_id')->nullable();
            $table->string('code', 100)->unique();
            $table->string('code_spk')->nullable();
            $table->double('ongkir', 20, 2)->default('0.00');
            $table->text('ongkir_desc')->nullable();
            $table->double('biaya_lain', 20, 2)->default('0.00');
            $table->text('biaya_lain_desc')->nullable();
            $table->date('due_date')->nullable();
            $table->enum('status', ['paid', 'unpaid'])->default('unpaid');
            $table->string('coa_code')->nullable();
            $table->enum('faktur', ['unverified', 'pending', 'actived', 'unactived', 'approved', 'reject', 'void'])->default('unverified');
            $table->text('notes')->nullable();
            $table->enum('piutang_status', ['paid', 'unpaid'])->nullable();
            $table->date('approve_date')->nullable();
            // Agro
            $table->string('ref')->nullable();
            $table->date('ref_date')->nullable();
            $table->enum('trans_type', ['Free market', 'Company'])->nullable();
            $table->string('serial', 100)->nullable();
            $table->binary('signed')->nullable();
            $table->timestamps();
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('sales_id')->references('karyawan_id')->on('karyawan')->onDelete('set null');
            $table->foreign('customer_id')->references('customer_id')->on('customers')->onDelete('set null');
            $table->foreign('wh_id')->references('wh_id')->on('warehouses');
            $table->foreign('po_pro_id')->references('po_pro_id')->on('po_projects')->onDelete('set null');
        });
        
        Schema::create('penjualan_details', function (Blueprint $table) {
            $table->bigIncrements('penjualan_d_id');
            $table->unsignedBigInteger('penjualan_id');
            $table->unsignedBigInteger('product_id');
            $table->string('unit_lg');
            $table->string('unit_sm');
            $table->float('qty_lg', 15, 2)->default('0');
            $table->float('qty_sm', 15, 2)->default('0');
            $table->double('price_lg', 20, 2)->default('0.00');
            $table->double('price_sm', 20, 2)->default('0.00');
            $table->float('tax', 5, 2)->default('0.00')->comment('%');
            $table->double('discount', 20, 2)->default('0.00');
            $table->double('amount', 20, 2)->default('0.00');
            $table->enum('revisi', ['edit', 'delete'])->nullable();
            $table->float('rev_qty_lg', 15, 2)->default('0');
            $table->float('rev_qty_sm', 15, 2)->default('0');
            $table->timestamps();
            $table->foreign('penjualan_id')->references('penjualan_id')->on('penjualan')->onDelete('cascade');
            $table->foreign('product_id')->references('product_id')->on('products');
        });

        Schema::create('penagihan', function (Blueprint $table) {
            $table->bigIncrements('penagihan_id');
            $table->unsignedBigInteger('penjualan_id');
            $table->text('address');
            $table->string('receiver');
            $table->string('expedition');
            $table->date('due_date');
            $table->double('nominal', 20, 2)->default('0.00');
            $table->enum('status', ['open', 'close'])->default('open');
            $table->string('serial', 20);
            $table->timestamps();
            $table->foreign('penjualan_id')->references('penjualan_id')->on('penjualan')->onDelete('cascade');
        });

        Schema::create('denda', function (Blueprint $table) {
            $table->bigIncrements('denda_id');
            $table->string('code_penjualan', 100)->unique();
            $table->tinyInteger('bulan')->nullable();
            $table->float('persentase', 5, 2)->default('0.00')->comment('%');
            $table->double('nominal', 20, 2)->default('0.00');
            $table->enum('status', ['open', 'close'])->default('open');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penjualan');
        Schema::dropIfExists('penjualan_details');
        Schema::dropIfExists('penagihan');
    }
}
