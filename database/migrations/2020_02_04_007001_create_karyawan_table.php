<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKaryawanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->bigIncrements('karyawan_id');
            $table->unsignedBigInteger('company_id');
            $table->string('position', 20)->default('staff');
            $table->string('name');
            $table->string('nik')->nullable();
            $table->string('ttl')->nullable();
            $table->text('address')->nullable();
            $table->text('address_now')->nullable();
            $table->string('religion')->nullable();
            $table->string('education')->nullable();
            $table->string('datejoin')->nullable();
            $table->string('penempatan')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone2')->nullable();
            $table->string('email')->nullable()->unique();
            $table->string('emailperusahaan')->nullable()->unique();
            $table->string('gender')->nullable();
            $table->string('marriage')->nullable();
            $table->string('photo')->nullable();
            $table->string('npwp')->nullable();
            $table->string('bpjs')->nullable();
            $table->double('salary', 15, 2)->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('status_karyawan')->nullable();
            $table->string('induk_karyawan')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->timestamps();
            $table->foreign('company_id')->references('company_id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('karyawan', function (Blueprint $table) {
            $table->dropForeign([
                'company_id'
            ]);
        });
        Schema::dropIfExists('karyawan');
    }
}
