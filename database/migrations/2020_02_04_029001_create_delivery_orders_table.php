<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_orders', function (Blueprint $table) {
            $table->bigIncrements('do_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('penjualan_id');
            $table->string('code', 100)->unique();
            $table->text('note')->nullable();
            $table->text('trans_type')->nullable();
            $table->string('penempatan')->nullable();
            $table->binary('signed')->nullable();
            $table->timestamps();
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('penjualan_id')->references('penjualan_id')->on('penjualan')->onDelete('cascade');
        });

        Schema::create('delivery_order_details', function (Blueprint $table) {
            $table->bigIncrements('do_d_id');
            $table->unsignedBigInteger('do_id');
            $table->unsignedBigInteger('product_id');
            $table->string('unit_lg');
            $table->string('unit_sm');
            $table->float('delv_qty_lg', 15, 2)->default('0');
            $table->float('delv_qty_sm', 15, 2)->default('0');
            $table->float('recv_qty_lg', 15, 2)->default('0');
            $table->float('recv_qty_sm', 15, 2)->default('0');
            $table->text('note')->nullable();
            $table->timestamps();
            $table->foreign('do_id')->references('do_id')->on('delivery_orders')->onDelete('cascade');
            $table->foreign('product_id')->references('product_id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_orders');
        Schema::dropIfExists('delivery_order_details');
    }
}
