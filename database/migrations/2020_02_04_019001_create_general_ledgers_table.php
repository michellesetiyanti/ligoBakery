<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_ledgers', function (Blueprint $table) {
            $table->bigIncrements('gl_id');
            $table->unsignedBigInteger('company_id');
            $table->string('coa_code');
            $table->string('coa_name');
            $table->double('debit', 20, 2)->default('0.00');
            $table->double('credit', 20, 2)->default('0.00');
            $table->double('start_balance', 20, 2)->default('0.00');
            $table->double('balance', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('company_id')->references('company_id')->on('companies')->onDelete('cascade');
            $table->index('coa_code')->references('code')->on('coa')->onUpdate('cascade');
        });
        
        Schema::create('general_ledger_details', function (Blueprint $table) {
            $table->bigIncrements('gl_d_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('gl_id');
            $table->string('reference');
            $table->text('description')->nullable();
            $table->double('debit', 20, 2)->default('0.00');
            $table->double('credit', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('company_id')->references('company_id')->on('companies')->onDelete('cascade');
            $table->foreign('gl_id')->references('gl_id')->on('general_ledgers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_ledgers');
        Schema::dropIfExists('general_ledger_details');
    }
}
