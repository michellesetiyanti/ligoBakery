<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('order_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('customer_id');
            $table->string('offer_id')->unique();
            $table->string('code_spk')->unique();
            $table->string('work_order');
            $table->string('address')->nullable();
            $table->text('add_agree')->nullable();
            $table->integer('time_work')->nullable();
            $table->double('total_project', 20, 2)->default('0.00');
            $table->decimal('profit', 5, 2)->default('0.00')->comment('%');
            $table->double('amount_profit', 20, 2)->default('0.00');
            $table->double('cost_project', 20, 2)->default('0.00');
            $table->double('limit_cost', 20, 2)->default('0.00');
            $table->double('cost_job', 20, 2)->default('0.00');
            $table->decimal('cost', 5, 2)->default('0.00')->comment('%');
            $table->double('discount', 20, 2)->default('0.00');
            $table->double('tax', 20, 2)->default('0.00');
            $table->double('realisasi_total', 20, 2)->default('0.00');
            $table->double('realisasi_profit', 20, 2)->default('0.00');
            $table->double('realisasi_cost', 20, 2)->default('0.00');
            $table->double('amount_estimasi', 20, 2)->default('0.00');
            $table->double('paid', 20, 2)->default('0.00');
            $table->enum('to_workshop', ['true', 'false'])->default('false');
            $table->string('jenis_pekerjaan')->nullable();
            $table->double('nominal', 20, 2)->default('0.00');
            $table->string('adendum')->default('false');
            $table->string('entry_at');
            $table->string('status')->default('open');
            $table->string('status_estimasi')->default('open');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('customer_id')->references('customer_id')->on('customers')->onDelete('cascade');
        });

        Schema::create('order_termins', function (Blueprint $table) {
            $table->bigIncrements('order_t_id');
            $table->unsignedBigInteger('order_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->double('amount', 20, 2)->default('0.00');
            $table->double('amount_detail', 20, 2)->default('0.00');
            $table->double('termin_total', 20, 2)->default('0.00');
            $table->double('paid', 20, 2)->default('0.00');
            $table->double('balance', 20, 2)->default('0.00');
            $table->string('date_paid')->nullable();
            $table->string('inv_issued')->nullable();
            $table->timestamps();
            $table->foreign('order_id')->references('order_id')->on('orders')->onDelete('cascade');
        });

        Schema::create('order_termin_details', function (Blueprint $table) {
            $table->bigIncrements('order_t_d_id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('order_t_id');
            $table->string('description');
            $table->integer('qty');
            $table->double('price', 20, 2)->default('0.00'); 
            $table->double('total', 20, 2)->default('0.00'); 
            $table->timestamps();
            $table->foreign('order_id')->references('order_id')->on('orders')->onDelete('cascade');
            $table->foreign('order_t_id')->references('order_t_id')->on('order_termins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_termins');
        Schema::dropIfExists('order_termin_details');
    }
}
