<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKasbonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kasbon', function (Blueprint $table) {
            $table->bigIncrements('kasbon_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->string('code', 100)->unique();
            $table->string('code_spk');
            $table->double('nominal', 20, 2)->default('0.00');
            $table->date('entry_at');
            $table->enum('status', ['open', 'pending', 'reject', 'void', 'close'])->default('open');
            $table->text('notes')->nullable();
            $table->binary('approved')->nullable();
            $table->enum('paided', ['yes', 'no'])->default('no');
            $table->double('paid_off', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
        });
        
        Schema::create('kasbon_details', function (Blueprint $table) {
            $table->bigIncrements('kasbon_d_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('kasbon_id');
            $table->string('name');
            $table->text('description');
            $table->double('nominal', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('kasbon_id')->references('kasbon_id')->on('kasbon')->onDelete('cascade');
        });
        
        Schema::create('kasbon_realisasi', function (Blueprint $table) {
            $table->bigIncrements('kasbon_r_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('kasbon_id');
            $table->string('name');
            $table->string('ref');
            $table->float('qty', 15, 2)->default('0');
            $table->double('amount', 20, 2)->default('0.00');
            $table->boolean('etc')->default('0');
            $table->enum('status', ['open', 'close'])->default('open');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('kasbon_id')->references('kasbon_id')->on('kasbon')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kasbon');
        Schema::dropIfExists('kasbon_details');
    }
}
