<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHutangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hutang', function (Blueprint $table) {
            $table->bigIncrements('hutang_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('pembelian_id');
            $table->double('total', 20, 2)->default('0.00');
            $table->double('paid_off', 20, 2)->default('0.00');
            $table->double('balance', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('pembelian_id')->references('pembelian_id')->on('pembelian')->onDelete('cascade');
        });
        
        Schema::create('hutang_details', function (Blueprint $table) {
            $table->bigIncrements('hutang_d_id');
            $table->unsignedBigInteger('hutang_id');
            $table->string('coa_code');
            $table->double('total', 15, 2)->default('0.00');
            $table->double('paid_off', 20, 2)->default('0.00');
            $table->double('balance', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('hutang_id')->references('hutang_id')->on('hutang')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hutangs');
        Schema::dropIfExists('hutang_details');
    }
}
