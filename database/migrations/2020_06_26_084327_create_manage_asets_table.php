<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManageAsetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manage_asets', function (Blueprint $table) {
            $table->bigIncrements('manage_aset_id');
            $table->unsignedBigInteger('company_id');
            $table->string('aset_id', 20)->unique();
            $table->string('date_borrow')->nullable();
            $table->string('date_return')->nullable();
            $table->string('name');
            $table->string('position');
            $table->text('description')->nullable();
            $table->timestamps();
            $table->foreign('company_id')->references('company_id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manage_asets');
    }
}
