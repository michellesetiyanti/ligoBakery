<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenjualanMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penjualan_menus', function (Blueprint $table) {
            $table->bigIncrements('penjualan_menu_id');
            $table->unsignedBigInteger('user_id');
            $table->string('nomor_penjualan_menu');
            $table->string('date');
            $table->string('note');
            $table->integer('subtotal');
            $table->integer('tax');
            $table->integer('discount');
            $table->integer('grandtotal');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
        });

         Schema::create('penjualan_menu_details', function (Blueprint $table) {
            $table->bigIncrements('penjualan_menu_detail_id');
            $table->unsignedBigInteger('penjualan_menu_id');
            $table->unsignedBigInteger('id_menu_jual');
            $table->string('note');
            $table->integer('qty');
            $table->integer('price');
            $table->integer('discount');
            $table->integer('subtotal');
            $table->timestamps();
            $table->foreign('id_menu_jual')->references('id_menu_jual')->on('menu_juals')->onDelete('cascade');
            $table->foreign('penjualan_menu_id')->references('penjualan_menu_id')->on('penjualan_menus')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penjualan_menus');
    }
}
