<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpahTukangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upah_tukang', function (Blueprint $table) {
            $table->bigIncrements('upah_tkg_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->string('work_order');
            $table->string('periode');
            $table->string('location');
            $table->text('description')->nullable();
            $table->double('nominal', 20, 2)->default('0.00');
            $table->double('paid', 20, 2)->default('0.00');
            $table->double('balance', 20, 2)->default('0.00');
            $table->binary('approved')->nullable();
            $table->enum('status', ['paid', 'unpaid'])->default('unpaid');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->index('work_order')->references('work_order')->on('orders')->onDelete('cascade');
        });
               
        Schema::create('upah_tukang_details', function (Blueprint $table) {
            $table->bigIncrements('upah_tkg_d_id');
            $table->unsignedBigInteger('upah_tkg_id');
            $table->unsignedBigInteger('tukang_id');
            $table->binary('absent');
            $table->string('uangmakan_amount');
            $table->binary('uangmakan_total')->nullable();
            $table->double('upah', 15, 2)->default('0.00')->comment('per hari');
            $table->double('amount', 15, 2)->default('0.00');
            $table->integer('user_pay')->nullable();
            $table->string('date_pay',255)->nullable();
            $table->timestamps();
            $table->foreign('upah_tkg_id')->references('upah_tkg_id')->on('upah_tukang')->onDelete('cascade');
            $table->foreign('tukang_id')->references('tukang_id')->on('tukang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upah_tukang');
        Schema::dropIfExists('upah_tukang_details');
    }
}
