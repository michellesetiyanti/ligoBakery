<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('company_id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('name');
            $table->text('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('currency_code', 5)->index()->comment('IDR, USD, etc');
            $table->string('npwp')->nullable();
            $table->enum('taxable', ['yes', 'no'])->default('no');
            $table->float('taxable_rate', 5, 2)->default('0.00')->comment('%');
            $table->string('code_numb')->nullable();
            $table->string('code_alpha')->nullable();
            $table->string('coa_penjualan')->nullable();
            $table->string('coa_pembelian')->nullable();
            $table->string('coa_hutang')->nullable();
            $table->string('coa_piutang')->nullable();
            $table->string('coa_pajak')->nullable();
            $table->string('coa_pajak_out')->nullable();
            $table->string('coa_discount')->nullable();
            $table->string('coa_discount_buy')->nullable();
            $table->string('coa_ikhtisar_labarugi')->nullable();
            $table->string('coa_labarugi_ditahan')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('pkp_name')->nullable();
            $table->string('pkp_address')->nullable();
            $table->string('type')->nullable();
            $table->string('owner')->nullable();
            $table->string('owner_related')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
