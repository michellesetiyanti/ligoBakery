<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstimasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimasi', function (Blueprint $table) {
            $table->bigIncrements('estimasi_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->integer('wh_id');
            $table->integer('order_id');
            $table->string('entry_at',255);
            $table->double('total', 20, 2)->default('0.00');
            $table->string('approved');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
        });
        
        Schema::create('estimasi_details', function (Blueprint $table) {
            $table->bigIncrements('estimasi_d_id');
            $table->unsignedBigInteger('estimasi_id');
            $table->unsignedBigInteger('product_id');
            $table->string('unit_lg');
            $table->string('unit_sm');
            $table->integer('qty_lg')->default('0');
            $table->integer('qty_sm')->default('0');
            $table->double('price_lg', 20, 2)->default('0.00');
            $table->double('price_sm', 20, 2)->default('0.00');
            $table->float('tax', 5, 2)->default('0.00')->comment('%');
            $table->double('discount', 20, 2)->default('0.00');
            $table->double('amount', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('estimasi_id')->references('estimasi_id')->on('estimasi')->onDelete('cascade');
            $table->foreign('product_id')->references('product_id')->on('products');
        });
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimasi');
        Schema::dropIfExists('estimasi_details');
    }
}
