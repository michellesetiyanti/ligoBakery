<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdditionalColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('company_id')->after('id');
            $table->unsignedBigInteger('karyawan_id')->nullable()->after('company_id');
            $table->string('type', 20)->default('unverified')->after('name');
            $table->string('locale', 10)->after('password');
            $table->enum('status', ['active', 'inactive'])->default('active')->after('remember_token');
            $table->foreign('company_id')->references('company_id')->on('companies')->onDelete('cascade');
            $table->foreign('karyawan_id')->references('karyawan_id')->on('karyawan')->onDelete('set null');
            $table->index('type')->references('code')->on('roles')->onUpdate('cascade');
            $table->renameColumn('id', 'user_id');
            $table->renameColumn('name', 'username');
            $table->dropUnique('users_email_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign([
                'company_id',
                'karyawan_id'
            ]);
            $table->dropUnique('username');
            $table->renameColumn('username', 'name');
            $table->renameColumn('user_id', 'id');
            $table->dropColumn([
                'company_id',
                'karyawan_id',
                'type',
                'api_token',
                'status'
            ]);
        });
    }
}
