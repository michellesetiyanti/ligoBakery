<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coa', function (Blueprint $table) {
            $table->bigIncrements('coa_id');
            $table->unsignedBigInteger('company_id');
            $table->string('parent');
            $table->string('code', 20);
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('level');
            $table->string('fromcode');
            $table->enum('normal_balance', ['D', 'K']);
            $table->timestamps();
            $table->foreign('company_id')->references('company_id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coa');
    }
}
