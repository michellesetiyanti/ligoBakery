<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenyusutanAsetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penyusutan_asets', function (Blueprint $table) {
            $table->bigIncrements('aset_id');
            $table->unsignedBigInteger('company_id');
            $table->string('code_aset');
            $table->string('name');
            $table->string('description');
            $table->string('date_buy');
            $table->string('date_use');
            $table->string('coa_code');
            $table->string('coa_penyusutan');
            $table->string('coa_beban');
            $table->integer('umur_bulan');
            $table->integer('bulan_tersisa');
            $table->integer('qty');
            $table->string('coa_pengeluaran');
            $table->string('keterangan_pengeluaran');
            $table->decimal('amount', 20, 2)->default('0.00');
            $table->unsignedBigInteger('input_by');
            $table->timestamps();
            $table->foreign('input_by')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penyusutan_asets');
    }
}
