<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePiutangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('piutang', function (Blueprint $table) {
            $table->bigIncrements('piutang_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('penjualan_id');
            $table->double('total', 20, 2)->default('0.00');
            $table->double('paid_off', 20, 2)->default('0.00');
            $table->double('balance', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('penjualan_id')->references('penjualan_id')->on('penjualan')->onDelete('cascade');
        });
        
        Schema::create('piutang_details', function (Blueprint $table) {
            $table->bigIncrements('piutang_d_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('piutang_id');
            $table->string('coa_code');
            $table->double('total', 20, 2)->default('0.00');
            $table->double('paid_off', 20, 2)->default('0.00');
            $table->double('balance', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('piutang_id')->references('piutang_id')->on('piutang')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('piutang');        
        Schema::dropIfExists('piutang_details');
    }
}
