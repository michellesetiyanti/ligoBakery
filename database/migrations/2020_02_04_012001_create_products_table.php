<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_units', function (Blueprint $table) {
            $table->bigIncrements('product_u_id');
            $table->string('code', 20)->unique();
            $table->string('name');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('product_categories', function (Blueprint $table) {
            $table->bigIncrements('product_c_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('product_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('wh_id');
            $table->unsignedBigInteger('product_c_id')->nullable();
            $table->string('code', 30)->unique();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('unit_lg');
            $table->string('unit_sm');
            $table->integer('unit_qty');
            $table->enum('pack', ['kg', 'g', 'lt', 'ml'])->nullable();
            $table->integer('pack_qty')->nullable();
            $table->float('total_lg_qty', 15, 2)->default('0');
            $table->float('total_sm_qty', 15, 2)->default('0');
            $table->double('price_lg', 20, 2)->default('0.00');
            $table->double('price_sm', 20, 2)->default('0.00');
            $table->integer('min_qty')->default('0');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('wh_id')->references('wh_id')->on('warehouses');
            $table->foreign('product_c_id')->references('product_c_id')->on('product_categories')->onDelete('set null');
            $table->index('unit_lg')->references('code')->on('product_units');
            $table->index('unit_sm')->references('code')->on('product_units');
        });

        Schema::create('product_details', function (Blueprint $table) {
            $table->bigIncrements('product_d_id');
            $table->unsignedBigInteger('product_id');
            $table->enum('status', ['mutasi tambah', 'mutasi kurang', 'pembelian', 'penjualan']);
            $table->string('ref');
            $table->string('batch');
            $table->string('unit_lg');
            $table->string('unit_sm');
            $table->float('qty_lg', 15, 2)->default('0');
            $table->float('qty_sm', 15, 2)->default('0');
            $table->float('cur_qty_lg', 15, 2)->default('0');
            $table->float('cur_qty_sm', 15, 2)->default('0');
            $table->double('price_lg', 20, 2)->default('0.00');
            $table->double('price_sm', 20, 2)->default('0.00');
            $table->timestamps();
            $table->foreign('product_id')->references('product_id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_units');
        Schema::dropIfExists('product_categories');
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_details');
    }
}
