<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->bigIncrements('journal_id');
            $table->unsignedBigInteger('user_id')->default('1');
            $table->unsignedBigInteger('company_id');
            $table->string('code', 20)->unique();
            $table->string('name');
            $table->text('description')->nullable();
            $table->decimal('debit', 20, 2)->default('0.00');
            $table->decimal('credit', 20, 2)->default('0.00');
            $table->string('status');
            $table->date('entry_at'); 
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies')->onDelete('cascade');
        });
        
        Schema::create('journal_details', function (Blueprint $table) {
            $table->bigIncrements('journal_d_id');
            $table->unsignedBigInteger('user_id')->default('1');
            $table->unsignedBigInteger('journal_id');
            $table->string('coa_code');
            $table->text('description')->nullable();
            $table->decimal('debit', 15, 2)->default('0.00');
            $table->decimal('credit', 15, 2)->default('0.00');
            $table->date('entry_at');
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('journal_id')->references('journal_id')->on('journals')->onDelete('cascade');
            $table->index('coa_code')->references('code')->on('chart_of_accounts')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journals');
        Schema::dropIfExists('journal_details');
    }
}
