<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuSisaJualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_sisa_juals', function (Blueprint $table) {
            $table->bigIncrements('id_menu_sisa_jual');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('user_id');
            $table->string('date');
            $table->string('status');
            $table->timestamps();
            $table->foreign('company_id')->references('company_id')->on('companies')->onDelete('cascade');
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
        });
        Schema::create('menu_sisa_jual_details', function (Blueprint $table) {
            $table->bigIncrements('id_menu_sisa_jual_detail');
            $table->unsignedBigInteger('id_menu_sisa_jual');
            $table->unsignedBigInteger('id_menu_jual');
            $table->integer('qty');
            $table->timestamps();
            $table->foreign('id_menu_sisa_jual')->references('id_menu_sisa_jual')->on('menu_sisa_juals')->onDelete('cascade');
            $table->foreign('id_menu_jual')->references('id_menu_jual')->on('menu_juals')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_sisa_juals');
    }
}
