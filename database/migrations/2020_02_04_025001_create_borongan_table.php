<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoronganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borongan', function (Blueprint $table) {
            $table->bigIncrements('borongan_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('vendor_id');
            $table->unsignedBigInteger('order_id');
            $table->string('description');
            $table->double('nominal', 20, 2)->default('0.00');
            $table->double('paid', 20, 2)->default('0.00');
            $table->double('balance', 20, 2)->default('0.00');
            $table->string('status');
            $table->string('entry_at',255);
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('company_id')->references('company_id')->on('companies');
            $table->foreign('vendor_id')->references('vendor_id')->on('vendors');
        });
        
        Schema::create('borongan_kerja', function (Blueprint $table) {
            $table->bigIncrements('borongan_d_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('borongan_id');
            $table->string('description',255);
            $table->integer('progress')->comment("%");
            $table->double('amount', 20, 2)->default('0.00');
            $table->string('user_pay',255)->nullable();
            $table->string('date_pay',255)->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('user_id')->on('users');
            $table->foreign('borongan_id')->references('borongan_id')->on('borongan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borongan');
        Schema::dropIfExists('borongan_kerja');
    }
}
