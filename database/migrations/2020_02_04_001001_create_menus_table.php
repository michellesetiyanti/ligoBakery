<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('menu_id');
            $table->string('name');
            $table->string('icon')->nullable();
            $table->tinyInteger('showw')->default('0');
            $table->timestamps();
        });

        Schema::create('menu_details', function (Blueprint $table) {
            $table->bigIncrements('menu_d_id');
            $table->unsignedBigInteger('menu_id');
            $table->string('slug')->unique();
            $table->string('name');
            $table->string('url')->nullable();
            $table->tinyInteger('readd')->default('0');
            $table->tinyInteger('createe')->default('0');
            $table->tinyInteger('confirmm')->default('0');
            $table->tinyInteger('updatee')->default('0');
            $table->tinyInteger('deletee')->default('0');
            $table->timestamps();
            $table->foreign('menu_id')->references('menu_id')->on('menus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
        Schema::dropIfExists('menu_details');
    }
}
